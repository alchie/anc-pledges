/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

"use strict";

  $('.count_chars').keyup(function(){
    $('.count_chars_output').text( $('.count_chars').val().length );  
  });
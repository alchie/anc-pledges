<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('pledgers_years'))
{
    function pledgers_years($Customer_ListID=FALSE, $orientation='DESC', $limit=0)
    {
        $CI = get_instance();
        $CI->load->model('Qb_salesreceipt_salesreceiptline_model');
        $pledges = new $CI->Qb_salesreceipt_salesreceiptline_model('j');
        $pledges->setItemListid($CI->config->item('pledgers_item_id'),true);

        $pledges->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
        $pledges->set_order('s.TxnDate', $orientation);

        if( $Customer_ListID ) {
            $pledges->set_where('s.Customer_ListID="'.$Customer_ListID.'"');
        }
        
        $pledges->set_select('YEAR(s.TxnDate) as year');
        $pledges->set_group_by('YEAR(s.TxnDate)');
        $pledges->set_limit($limit);
        //$pledges->set_order('YEAR(s.TxnDate) ', $orientation);
        return $pledges->populate();
    }
}


// ------------------------------------------------------------------------

if ( ! function_exists('update_pledgers_option'))
{
    function update_pledgers_option($c,$m,$k,$v)
    {
        $CI = get_instance();
        $CI->load->model('Pledgers_options_model');
        $option = new $CI->Pledgers_options_model('j');
        $option->setController($c,true);
        $option->setMethod($m,true);
        $option->setOptionKey($k,true);
        if( $option->nonEmpty() ) {
            $option->setOptionValue($v,false,true);
            $option->update();
        } else {
            $option->setController($c);
            $option->setMethod($m);
            $option->setOptionKey($k);
            $option->setOptionValue($v);
            $option->insert();
        }
    }
}

if ( ! function_exists('get_pledgers_option'))
{
    function get_pledgers_option($c,$m,$k)
    {
        $CI = get_instance();
        $CI->load->model('Pledgers_options_model');
        $option = new $CI->Pledgers_options_model('j');
        $option->setController($c,true);
        $option->setMethod($m,true);
        $option->setOptionKey($k,true);
        $data = $option->get();
        if( $data ) {
            return $data->option_value;
        } else {
            return '';
        }
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('stisla_pagination'))
{
    function stisla_pagination($config=array(), $url_suffix=NULL)
    {
        $defaults['base_url'] = base_url();
        $defaults['total_rows'] = 10;
        $defaults['per_page'] = 0;

        $defaults['full_tag_open'] = '<ul class="pagination pagination-sm justify-content-center">';
        $defaults['full_tag_close'] = '</ul>';

        $defaults['first_tag_open'] = '<li class="page-item">';
        $defaults['first_tag_close'] = '</li>';
        $defaults['first_link'] = '&laquo;';

        $defaults['prev_tag_open'] = '<li class="page-item">';
        $defaults['prev_tag_close'] = '</li>';
        $defaults['prev_link'] = '&lsaquo;';

        $defaults['cur_tag_open'] = '<li class="page-item active"><a class="page-link"><strong>';
        $defaults['cur_tag_close'] = '</strong></a></li>';

        $defaults['num_tag_open'] = '<li class="page-item">';
        $defaults['num_tag_close'] = '</li>';

        $defaults['next_tag_open'] = '<li class="page-item">';
        $defaults['next_tag_close'] = '</li>';
        $defaults['next_link'] = '&rsaquo;';

        $defaults['last_tag_open'] = '<li class="page-item">';
        $defaults['last_tag_close'] = '</li>';
        $defaults['last_link'] = '&raquo;';

        $defaults['attributes'] = array('class' => 'page-link');
        
        $new_config = array_merge($defaults, $config);

        if( isset($config['ajax']) && ($config['ajax']===true) ) {
            $new_config['attributes']['class'] .= ' ajaxPage';
        }

        $CI = get_instance();
        $pagination = new $CI->pagination;
        $pagination->initialize($new_config);

        $pagination_html = $pagination->create_links();
        if( $url_suffix ) {
            $pagination_html = str_replace('" class="', $url_suffix . '" class="', $pagination_html);
        }
        return $pagination_html;
    }
}

if ( ! function_exists('stisla_alert'))
{
    function stisla_alert($message, $type='success')
    {
        $html = '<div class="alert alert-'.$type.' alert-dismissible" role="alert">';
        $html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $html .= $message.'</div>';
        return $html;
    }
}


// ------------------------------------------------------------------------
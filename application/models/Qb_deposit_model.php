<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_deposit_model Class
 *
 * Manipulates `qb_deposit` table on database

CREATE TABLE `qb_deposit` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `TxnDate` date DEFAULT NULL,
  `DepositToAccount_ListID` varchar(40) DEFAULT NULL,
  `DepositToAccount_FullName` varchar(255) DEFAULT NULL,
  `Memo` text,
  `DepositTotal` decimal(10,2) DEFAULT NULL,
  `CashBackInfo_TxnLineID` varchar(40) DEFAULT NULL,
  `CashBackInfo_Account_ListID` varchar(40) DEFAULT NULL,
  `CashBackInfo_Account_FullName` varchar(255) DEFAULT NULL,
  `CashBackInfo_Memo` text,
  `CashBackInfo_Amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `TxnDate` (`TxnDate`),
  KEY `DepositToAccount_ListID` (`DepositToAccount_ListID`),
  KEY `CashBackInfo_TxnLineID` (`CashBackInfo_TxnLineID`),
  KEY `CashBackInfo_Account_ListID` (`CashBackInfo_Account_ListID`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_deposit` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_deposit` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_deposit` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_deposit` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_deposit` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_deposit` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_deposit` ADD  `DepositToAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit` ADD  `DepositToAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_deposit` ADD  `Memo` text NULL   ;
ALTER TABLE  `qb_deposit` ADD  `DepositTotal` decimal(10,2) NULL   ;
ALTER TABLE  `qb_deposit` ADD  `CashBackInfo_TxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit` ADD  `CashBackInfo_Account_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit` ADD  `CashBackInfo_Account_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_deposit` ADD  `CashBackInfo_Memo` text NULL   ;
ALTER TABLE  `qb_deposit` ADD  `CashBackInfo_Amount` decimal(10,2) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_deposit_model extends MY_Model {

	protected $qbxml_id;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $TxnDate;
	protected $DepositToAccount_ListID;
	protected $DepositToAccount_FullName;
	protected $Memo;
	protected $DepositTotal;
	protected $CashBackInfo_TxnLineID;
	protected $CashBackInfo_Account_ListID;
	protected $CashBackInfo_Account_FullName;
	protected $CashBackInfo_Memo;
	protected $CashBackInfo_Amount;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_deposit';
		$this->_short_name = 'qb_deposit';
		$this->_fields = array("qbxml_id","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","TxnDate","DepositToAccount_ListID","DepositToAccount_FullName","Memo","DepositTotal","CashBackInfo_TxnLineID","CashBackInfo_Account_ListID","CashBackInfo_Account_FullName","CashBackInfo_Memo","CashBackInfo_Amount");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: DepositToAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `DepositToAccount_ListID` variable
	* @access public
	*/

	public function setDeposittoaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DepositToAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DepositToAccount_ListID` variable
	* @access public
	*/

	public function getDeposittoaccountListid() {
		return $this->DepositToAccount_ListID;
	}

	public function get_DepositToAccount_ListID() {
		return $this->DepositToAccount_ListID;
	}

	
// ------------------------------ End Field: DepositToAccount_ListID --------------------------------------


// ---------------------------- Start Field: DepositToAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `DepositToAccount_FullName` variable
	* @access public
	*/

	public function setDeposittoaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DepositToAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DepositToAccount_FullName` variable
	* @access public
	*/

	public function getDeposittoaccountFullname() {
		return $this->DepositToAccount_FullName;
	}

	public function get_DepositToAccount_FullName() {
		return $this->DepositToAccount_FullName;
	}

	
// ------------------------------ End Field: DepositToAccount_FullName --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------


// ---------------------------- Start Field: DepositTotal -------------------------------------- 

	/** 
	* Sets a value to `DepositTotal` variable
	* @access public
	*/

	public function setDeposittotal($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositTotal', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DepositTotal($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositTotal', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DepositTotal` variable
	* @access public
	*/

	public function getDeposittotal() {
		return $this->DepositTotal;
	}

	public function get_DepositTotal() {
		return $this->DepositTotal;
	}

	
// ------------------------------ End Field: DepositTotal --------------------------------------


// ---------------------------- Start Field: CashBackInfo_TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `CashBackInfo_TxnLineID` variable
	* @access public
	*/

	public function setCashbackinfoTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashBackInfo_TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CashBackInfo_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashBackInfo_TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CashBackInfo_TxnLineID` variable
	* @access public
	*/

	public function getCashbackinfoTxnlineid() {
		return $this->CashBackInfo_TxnLineID;
	}

	public function get_CashBackInfo_TxnLineID() {
		return $this->CashBackInfo_TxnLineID;
	}

	
// ------------------------------ End Field: CashBackInfo_TxnLineID --------------------------------------


// ---------------------------- Start Field: CashBackInfo_Account_ListID -------------------------------------- 

	/** 
	* Sets a value to `CashBackInfo_Account_ListID` variable
	* @access public
	*/

	public function setCashbackinfoAccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashBackInfo_Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CashBackInfo_Account_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashBackInfo_Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CashBackInfo_Account_ListID` variable
	* @access public
	*/

	public function getCashbackinfoAccountListid() {
		return $this->CashBackInfo_Account_ListID;
	}

	public function get_CashBackInfo_Account_ListID() {
		return $this->CashBackInfo_Account_ListID;
	}

	
// ------------------------------ End Field: CashBackInfo_Account_ListID --------------------------------------


// ---------------------------- Start Field: CashBackInfo_Account_FullName -------------------------------------- 

	/** 
	* Sets a value to `CashBackInfo_Account_FullName` variable
	* @access public
	*/

	public function setCashbackinfoAccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashBackInfo_Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CashBackInfo_Account_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashBackInfo_Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CashBackInfo_Account_FullName` variable
	* @access public
	*/

	public function getCashbackinfoAccountFullname() {
		return $this->CashBackInfo_Account_FullName;
	}

	public function get_CashBackInfo_Account_FullName() {
		return $this->CashBackInfo_Account_FullName;
	}

	
// ------------------------------ End Field: CashBackInfo_Account_FullName --------------------------------------


// ---------------------------- Start Field: CashBackInfo_Memo -------------------------------------- 

	/** 
	* Sets a value to `CashBackInfo_Memo` variable
	* @access public
	*/

	public function setCashbackinfoMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashBackInfo_Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CashBackInfo_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashBackInfo_Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CashBackInfo_Memo` variable
	* @access public
	*/

	public function getCashbackinfoMemo() {
		return $this->CashBackInfo_Memo;
	}

	public function get_CashBackInfo_Memo() {
		return $this->CashBackInfo_Memo;
	}

	
// ------------------------------ End Field: CashBackInfo_Memo --------------------------------------


// ---------------------------- Start Field: CashBackInfo_Amount -------------------------------------- 

	/** 
	* Sets a value to `CashBackInfo_Amount` variable
	* @access public
	*/

	public function setCashbackinfoAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashBackInfo_Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CashBackInfo_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashBackInfo_Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CashBackInfo_Amount` variable
	* @access public
	*/

	public function getCashbackinfoAmount() {
		return $this->CashBackInfo_Amount;
	}

	public function get_CashBackInfo_Amount() {
		return $this->CashBackInfo_Amount;
	}

	
// ------------------------------ End Field: CashBackInfo_Amount --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'DepositToAccount_ListID' => (object) array(
										'Field'=>'DepositToAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'DepositToAccount_FullName' => (object) array(
										'Field'=>'DepositToAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DepositTotal' => (object) array(
										'Field'=>'DepositTotal',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CashBackInfo_TxnLineID' => (object) array(
										'Field'=>'CashBackInfo_TxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CashBackInfo_Account_ListID' => (object) array(
										'Field'=>'CashBackInfo_Account_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CashBackInfo_Account_FullName' => (object) array(
										'Field'=>'CashBackInfo_Account_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CashBackInfo_Memo' => (object) array(
										'Field'=>'CashBackInfo_Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CashBackInfo_Amount' => (object) array(
										'Field'=>'CashBackInfo_Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_deposit` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'TxnID' => "ALTER TABLE  `qb_deposit` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_deposit` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_deposit` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_deposit` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_deposit` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'TxnDate' => "ALTER TABLE  `qb_deposit` ADD  `TxnDate` date NULL   ;",
			'DepositToAccount_ListID' => "ALTER TABLE  `qb_deposit` ADD  `DepositToAccount_ListID` varchar(40) NULL   ;",
			'DepositToAccount_FullName' => "ALTER TABLE  `qb_deposit` ADD  `DepositToAccount_FullName` varchar(255) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_deposit` ADD  `Memo` text NULL   ;",
			'DepositTotal' => "ALTER TABLE  `qb_deposit` ADD  `DepositTotal` decimal(10,2) NULL   ;",
			'CashBackInfo_TxnLineID' => "ALTER TABLE  `qb_deposit` ADD  `CashBackInfo_TxnLineID` varchar(40) NULL   ;",
			'CashBackInfo_Account_ListID' => "ALTER TABLE  `qb_deposit` ADD  `CashBackInfo_Account_ListID` varchar(40) NULL   ;",
			'CashBackInfo_Account_FullName' => "ALTER TABLE  `qb_deposit` ADD  `CashBackInfo_Account_FullName` varchar(255) NULL   ;",
			'CashBackInfo_Memo' => "ALTER TABLE  `qb_deposit` ADD  `CashBackInfo_Memo` text NULL   ;",
			'CashBackInfo_Amount' => "ALTER TABLE  `qb_deposit` ADD  `CashBackInfo_Amount` decimal(10,2) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setTxndate() - TxnDate
//setDeposittoaccountListid() - DepositToAccount_ListID
//setDeposittoaccountFullname() - DepositToAccount_FullName
//setMemo() - Memo
//setDeposittotal() - DepositTotal
//setCashbackinfoTxnlineid() - CashBackInfo_TxnLineID
//setCashbackinfoAccountListid() - CashBackInfo_Account_ListID
//setCashbackinfoAccountFullname() - CashBackInfo_Account_FullName
//setCashbackinfoMemo() - CashBackInfo_Memo
//setCashbackinfoAmount() - CashBackInfo_Amount

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_TxnDate() - TxnDate
//set_DepositToAccount_ListID() - DepositToAccount_ListID
//set_DepositToAccount_FullName() - DepositToAccount_FullName
//set_Memo() - Memo
//set_DepositTotal() - DepositTotal
//set_CashBackInfo_TxnLineID() - CashBackInfo_TxnLineID
//set_CashBackInfo_Account_ListID() - CashBackInfo_Account_ListID
//set_CashBackInfo_Account_FullName() - CashBackInfo_Account_FullName
//set_CashBackInfo_Memo() - CashBackInfo_Memo
//set_CashBackInfo_Amount() - CashBackInfo_Amount

*/
/* End of file Qb_deposit_model.php */
/* Location: ./application/models/Qb_deposit_model.php */

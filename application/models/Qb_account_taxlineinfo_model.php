<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_account_taxlineinfo_model Class
 *
 * Manipulates `qb_account_taxlineinfo` table on database

CREATE TABLE `qb_account_taxlineinfo` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Account_ListID` varchar(40) DEFAULT NULL,
  `Account_FullName` varchar(255) DEFAULT NULL,
  `TaxLineInfo_TaxLineID` int(10) unsigned DEFAULT '0',
  `TaxLineInfo_TaxLineName` text,
  PRIMARY KEY (`qbxml_id`),
  KEY `Account_ListID` (`Account_ListID`),
  KEY `TaxLineInfo_TaxLineID` (`TaxLineInfo_TaxLineID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_account_taxlineinfo` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_account_taxlineinfo` ADD  `Account_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_account_taxlineinfo` ADD  `Account_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_account_taxlineinfo` ADD  `TaxLineInfo_TaxLineID` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_account_taxlineinfo` ADD  `TaxLineInfo_TaxLineName` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_account_taxlineinfo_model extends MY_Model {

	protected $qbxml_id;
	protected $Account_ListID;
	protected $Account_FullName;
	protected $TaxLineInfo_TaxLineID;
	protected $TaxLineInfo_TaxLineName;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_account_taxlineinfo';
		$this->_short_name = 'qb_account_taxlineinfo';
		$this->_fields = array("qbxml_id","Account_ListID","Account_FullName","TaxLineInfo_TaxLineID","TaxLineInfo_TaxLineName");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: Account_ListID -------------------------------------- 

	/** 
	* Sets a value to `Account_ListID` variable
	* @access public
	*/

	public function setAccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Account_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Account_ListID` variable
	* @access public
	*/

	public function getAccountListid() {
		return $this->Account_ListID;
	}

	public function get_Account_ListID() {
		return $this->Account_ListID;
	}

	
// ------------------------------ End Field: Account_ListID --------------------------------------


// ---------------------------- Start Field: Account_FullName -------------------------------------- 

	/** 
	* Sets a value to `Account_FullName` variable
	* @access public
	*/

	public function setAccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Account_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Account_FullName` variable
	* @access public
	*/

	public function getAccountFullname() {
		return $this->Account_FullName;
	}

	public function get_Account_FullName() {
		return $this->Account_FullName;
	}

	
// ------------------------------ End Field: Account_FullName --------------------------------------


// ---------------------------- Start Field: TaxLineInfo_TaxLineID -------------------------------------- 

	/** 
	* Sets a value to `TaxLineInfo_TaxLineID` variable
	* @access public
	*/

	public function setTaxlineinfoTaxlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TaxLineInfo_TaxLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TaxLineInfo_TaxLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TaxLineInfo_TaxLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TaxLineInfo_TaxLineID` variable
	* @access public
	*/

	public function getTaxlineinfoTaxlineid() {
		return $this->TaxLineInfo_TaxLineID;
	}

	public function get_TaxLineInfo_TaxLineID() {
		return $this->TaxLineInfo_TaxLineID;
	}

	
// ------------------------------ End Field: TaxLineInfo_TaxLineID --------------------------------------


// ---------------------------- Start Field: TaxLineInfo_TaxLineName -------------------------------------- 

	/** 
	* Sets a value to `TaxLineInfo_TaxLineName` variable
	* @access public
	*/

	public function setTaxlineinfoTaxlinename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TaxLineInfo_TaxLineName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TaxLineInfo_TaxLineName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TaxLineInfo_TaxLineName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TaxLineInfo_TaxLineName` variable
	* @access public
	*/

	public function getTaxlineinfoTaxlinename() {
		return $this->TaxLineInfo_TaxLineName;
	}

	public function get_TaxLineInfo_TaxLineName() {
		return $this->TaxLineInfo_TaxLineName;
	}

	
// ------------------------------ End Field: TaxLineInfo_TaxLineName --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'Account_ListID' => (object) array(
										'Field'=>'Account_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Account_FullName' => (object) array(
										'Field'=>'Account_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TaxLineInfo_TaxLineID' => (object) array(
										'Field'=>'TaxLineInfo_TaxLineID',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'TaxLineInfo_TaxLineName' => (object) array(
										'Field'=>'TaxLineInfo_TaxLineName',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_account_taxlineinfo` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'Account_ListID' => "ALTER TABLE  `qb_account_taxlineinfo` ADD  `Account_ListID` varchar(40) NULL   ;",
			'Account_FullName' => "ALTER TABLE  `qb_account_taxlineinfo` ADD  `Account_FullName` varchar(255) NULL   ;",
			'TaxLineInfo_TaxLineID' => "ALTER TABLE  `qb_account_taxlineinfo` ADD  `TaxLineInfo_TaxLineID` int(10) unsigned NULL   DEFAULT '0';",
			'TaxLineInfo_TaxLineName' => "ALTER TABLE  `qb_account_taxlineinfo` ADD  `TaxLineInfo_TaxLineName` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setAccountListid() - Account_ListID
//setAccountFullname() - Account_FullName
//setTaxlineinfoTaxlineid() - TaxLineInfo_TaxLineID
//setTaxlineinfoTaxlinename() - TaxLineInfo_TaxLineName

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_Account_ListID() - Account_ListID
//set_Account_FullName() - Account_FullName
//set_TaxLineInfo_TaxLineID() - TaxLineInfo_TaxLineID
//set_TaxLineInfo_TaxLineName() - TaxLineInfo_TaxLineName

*/
/* End of file Qb_account_taxlineinfo_model.php */
/* Location: ./application/models/Qb_account_taxlineinfo_model.php */

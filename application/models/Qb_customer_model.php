<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_customer_model Class
 *
 * Manipulates `qb_customer` table on database

CREATE TABLE `qb_customer` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ListID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Name` varchar(41) DEFAULT NULL,
  `FullName` varchar(255) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT '0',
  `Parent_ListID` varchar(40) DEFAULT NULL,
  `Parent_FullName` varchar(255) DEFAULT NULL,
  `Sublevel` int(10) unsigned DEFAULT '0',
  `CompanyName` varchar(41) DEFAULT NULL,
  `Salutation` varchar(15) DEFAULT NULL,
  `FirstName` varchar(25) DEFAULT NULL,
  `MiddleName` varchar(5) DEFAULT NULL,
  `LastName` varchar(25) DEFAULT NULL,
  `BillAddress_Addr1` varchar(41) DEFAULT NULL,
  `BillAddress_Addr2` varchar(41) DEFAULT NULL,
  `BillAddress_Addr3` varchar(41) DEFAULT NULL,
  `BillAddress_Addr4` varchar(41) DEFAULT NULL,
  `BillAddress_Addr5` varchar(41) DEFAULT NULL,
  `BillAddress_City` varchar(31) DEFAULT NULL,
  `BillAddress_State` varchar(21) DEFAULT NULL,
  `BillAddress_PostalCode` varchar(13) DEFAULT NULL,
  `BillAddress_Country` varchar(31) DEFAULT NULL,
  `BillAddress_Note` varchar(41) DEFAULT NULL,
  `BillAddressBlock_Addr1` text,
  `BillAddressBlock_Addr2` text,
  `BillAddressBlock_Addr3` text,
  `BillAddressBlock_Addr4` text,
  `BillAddressBlock_Addr5` text,
  `ShipAddress_Addr1` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr2` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr3` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr4` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr5` varchar(41) DEFAULT NULL,
  `ShipAddress_City` varchar(31) DEFAULT NULL,
  `ShipAddress_State` varchar(21) DEFAULT NULL,
  `ShipAddress_PostalCode` varchar(13) DEFAULT NULL,
  `ShipAddress_Country` varchar(31) DEFAULT NULL,
  `ShipAddress_Note` varchar(41) DEFAULT NULL,
  `ShipAddressBlock_Addr1` text,
  `ShipAddressBlock_Addr2` text,
  `ShipAddressBlock_Addr3` text,
  `ShipAddressBlock_Addr4` text,
  `ShipAddressBlock_Addr5` text,
  `Phone` varchar(21) DEFAULT NULL,
  `AltPhone` varchar(21) DEFAULT NULL,
  `Fax` varchar(21) DEFAULT NULL,
  `Email` text,
  `AltEmail` text,
  `Contact` varchar(41) DEFAULT NULL,
  `AltContact` varchar(41) DEFAULT NULL,
  `CustomerType_ListID` varchar(40) DEFAULT NULL,
  `CustomerType_FullName` varchar(255) DEFAULT NULL,
  `Terms_ListID` varchar(40) DEFAULT NULL,
  `Terms_FullName` varchar(255) DEFAULT NULL,
  `SalesRep_ListID` varchar(40) DEFAULT NULL,
  `SalesRep_FullName` varchar(255) DEFAULT NULL,
  `Balance` decimal(10,2) DEFAULT NULL,
  `TotalBalance` decimal(10,2) DEFAULT NULL,
  `SalesTaxCode_ListID` varchar(40) DEFAULT NULL,
  `SalesTaxCode_FullName` varchar(255) DEFAULT NULL,
  `ItemSalesTax_ListID` varchar(40) DEFAULT NULL,
  `ItemSalesTax_FullName` varchar(255) DEFAULT NULL,
  `ResaleNumber` varchar(15) DEFAULT NULL,
  `AccountNumber` varchar(99) DEFAULT NULL,
  `CreditLimit` decimal(10,2) DEFAULT NULL,
  `PreferredPaymentMethod_ListID` varchar(40) DEFAULT NULL,
  `PreferredPaymentMethod_FullName` varchar(255) DEFAULT NULL,
  `CreditCardInfo_CreditCardNumber` varchar(25) DEFAULT NULL,
  `CreditCardInfo_ExpirationMonth` int(10) unsigned DEFAULT NULL,
  `CreditCardInfo_ExpirationYear` int(10) unsigned DEFAULT NULL,
  `CreditCardInfo_NameOnCard` varchar(41) DEFAULT NULL,
  `CreditCardInfo_CreditCardAddress` varchar(41) DEFAULT NULL,
  `CreditCardInfo_CreditCardPostalCode` varchar(41) DEFAULT NULL,
  `JobStatus` varchar(40) DEFAULT NULL,
  `JobStartDate` date DEFAULT NULL,
  `JobProjectedEndDate` date DEFAULT NULL,
  `JobEndDate` date DEFAULT NULL,
  `JobDesc` varchar(99) DEFAULT NULL,
  `JobType_ListID` varchar(40) DEFAULT NULL,
  `JobType_FullName` varchar(255) DEFAULT NULL,
  `Notes` text,
  `PriceLevel_ListID` varchar(40) DEFAULT NULL,
  `PriceLevel_FullName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `FullName` (`FullName`),
  KEY `IsActive` (`IsActive`),
  KEY `Parent_ListID` (`Parent_ListID`),
  KEY `CompanyName` (`CompanyName`),
  KEY `LastName` (`LastName`),
  KEY `BillAddress_Country` (`BillAddress_Country`),
  KEY `ShipAddress_Country` (`ShipAddress_Country`),
  KEY `CustomerType_ListID` (`CustomerType_ListID`),
  KEY `Terms_ListID` (`Terms_ListID`),
  KEY `SalesRep_ListID` (`SalesRep_ListID`),
  KEY `SalesTaxCode_ListID` (`SalesTaxCode_ListID`),
  KEY `ItemSalesTax_ListID` (`ItemSalesTax_ListID`),
  KEY `PreferredPaymentMethod_ListID` (`PreferredPaymentMethod_ListID`),
  KEY `JobType_ListID` (`JobType_ListID`),
  KEY `PriceLevel_ListID` (`PriceLevel_ListID`),
  KEY `ListID` (`ListID`),
  KEY `Name` (`Name`)
) ENGINE=MyISAM AUTO_INCREMENT=1423 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_customer` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_customer` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_customer` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_customer` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_customer` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `Name` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_customer` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_customer` ADD  `Parent_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_customer` ADD  `Parent_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_customer` ADD  `Sublevel` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_customer` ADD  `CompanyName` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `Salutation` varchar(15) NULL   ;
ALTER TABLE  `qb_customer` ADD  `FirstName` varchar(25) NULL   ;
ALTER TABLE  `qb_customer` ADD  `MiddleName` varchar(5) NULL   ;
ALTER TABLE  `qb_customer` ADD  `LastName` varchar(25) NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddress_Addr1` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddress_Addr2` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddress_Addr3` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddress_Addr4` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddress_Addr5` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddress_City` varchar(31) NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddress_State` varchar(21) NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddress_PostalCode` varchar(13) NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddress_Country` varchar(31) NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddress_Note` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddressBlock_Addr1` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddressBlock_Addr2` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddressBlock_Addr3` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddressBlock_Addr4` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `BillAddressBlock_Addr5` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddress_Addr1` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddress_Addr2` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddress_Addr3` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddress_Addr4` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddress_Addr5` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddress_City` varchar(31) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddress_State` varchar(21) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddress_PostalCode` varchar(13) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddress_Country` varchar(31) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddress_Note` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddressBlock_Addr1` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddressBlock_Addr2` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddressBlock_Addr3` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddressBlock_Addr4` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `ShipAddressBlock_Addr5` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `Phone` varchar(21) NULL   ;
ALTER TABLE  `qb_customer` ADD  `AltPhone` varchar(21) NULL   ;
ALTER TABLE  `qb_customer` ADD  `Fax` varchar(21) NULL   ;
ALTER TABLE  `qb_customer` ADD  `Email` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `AltEmail` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `Contact` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `AltContact` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `CustomerType_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_customer` ADD  `CustomerType_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_customer` ADD  `Terms_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_customer` ADD  `Terms_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_customer` ADD  `SalesRep_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_customer` ADD  `SalesRep_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_customer` ADD  `Balance` decimal(10,2) NULL   ;
ALTER TABLE  `qb_customer` ADD  `TotalBalance` decimal(10,2) NULL   ;
ALTER TABLE  `qb_customer` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_customer` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ItemSalesTax_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ItemSalesTax_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_customer` ADD  `ResaleNumber` varchar(15) NULL   ;
ALTER TABLE  `qb_customer` ADD  `AccountNumber` varchar(99) NULL   ;
ALTER TABLE  `qb_customer` ADD  `CreditLimit` decimal(10,2) NULL   ;
ALTER TABLE  `qb_customer` ADD  `PreferredPaymentMethod_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_customer` ADD  `PreferredPaymentMethod_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_CreditCardNumber` varchar(25) NULL   ;
ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_ExpirationMonth` int(10) unsigned NULL   ;
ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_ExpirationYear` int(10) unsigned NULL   ;
ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_NameOnCard` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_CreditCardAddress` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_CreditCardPostalCode` varchar(41) NULL   ;
ALTER TABLE  `qb_customer` ADD  `JobStatus` varchar(40) NULL   ;
ALTER TABLE  `qb_customer` ADD  `JobStartDate` date NULL   ;
ALTER TABLE  `qb_customer` ADD  `JobProjectedEndDate` date NULL   ;
ALTER TABLE  `qb_customer` ADD  `JobEndDate` date NULL   ;
ALTER TABLE  `qb_customer` ADD  `JobDesc` varchar(99) NULL   ;
ALTER TABLE  `qb_customer` ADD  `JobType_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_customer` ADD  `JobType_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_customer` ADD  `Notes` text NULL   ;
ALTER TABLE  `qb_customer` ADD  `PriceLevel_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_customer` ADD  `PriceLevel_FullName` varchar(255) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_customer_model extends MY_Model {

	protected $qbxml_id;
	protected $ListID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Name;
	protected $FullName;
	protected $IsActive;
	protected $Parent_ListID;
	protected $Parent_FullName;
	protected $Sublevel;
	protected $CompanyName;
	protected $Salutation;
	protected $FirstName;
	protected $MiddleName;
	protected $LastName;
	protected $BillAddress_Addr1;
	protected $BillAddress_Addr2;
	protected $BillAddress_Addr3;
	protected $BillAddress_Addr4;
	protected $BillAddress_Addr5;
	protected $BillAddress_City;
	protected $BillAddress_State;
	protected $BillAddress_PostalCode;
	protected $BillAddress_Country;
	protected $BillAddress_Note;
	protected $BillAddressBlock_Addr1;
	protected $BillAddressBlock_Addr2;
	protected $BillAddressBlock_Addr3;
	protected $BillAddressBlock_Addr4;
	protected $BillAddressBlock_Addr5;
	protected $ShipAddress_Addr1;
	protected $ShipAddress_Addr2;
	protected $ShipAddress_Addr3;
	protected $ShipAddress_Addr4;
	protected $ShipAddress_Addr5;
	protected $ShipAddress_City;
	protected $ShipAddress_State;
	protected $ShipAddress_PostalCode;
	protected $ShipAddress_Country;
	protected $ShipAddress_Note;
	protected $ShipAddressBlock_Addr1;
	protected $ShipAddressBlock_Addr2;
	protected $ShipAddressBlock_Addr3;
	protected $ShipAddressBlock_Addr4;
	protected $ShipAddressBlock_Addr5;
	protected $Phone;
	protected $AltPhone;
	protected $Fax;
	protected $Email;
	protected $AltEmail;
	protected $Contact;
	protected $AltContact;
	protected $CustomerType_ListID;
	protected $CustomerType_FullName;
	protected $Terms_ListID;
	protected $Terms_FullName;
	protected $SalesRep_ListID;
	protected $SalesRep_FullName;
	protected $Balance;
	protected $TotalBalance;
	protected $SalesTaxCode_ListID;
	protected $SalesTaxCode_FullName;
	protected $ItemSalesTax_ListID;
	protected $ItemSalesTax_FullName;
	protected $ResaleNumber;
	protected $AccountNumber;
	protected $CreditLimit;
	protected $PreferredPaymentMethod_ListID;
	protected $PreferredPaymentMethod_FullName;
	protected $CreditCardInfo_CreditCardNumber;
	protected $CreditCardInfo_ExpirationMonth;
	protected $CreditCardInfo_ExpirationYear;
	protected $CreditCardInfo_NameOnCard;
	protected $CreditCardInfo_CreditCardAddress;
	protected $CreditCardInfo_CreditCardPostalCode;
	protected $JobStatus;
	protected $JobStartDate;
	protected $JobProjectedEndDate;
	protected $JobEndDate;
	protected $JobDesc;
	protected $JobType_ListID;
	protected $JobType_FullName;
	protected $Notes;
	protected $PriceLevel_ListID;
	protected $PriceLevel_FullName;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_customer';
		$this->_short_name = 'qb_customer';
		$this->_fields = array("qbxml_id","ListID","TimeCreated","TimeModified","EditSequence","Name","FullName","IsActive","Parent_ListID","Parent_FullName","Sublevel","CompanyName","Salutation","FirstName","MiddleName","LastName","BillAddress_Addr1","BillAddress_Addr2","BillAddress_Addr3","BillAddress_Addr4","BillAddress_Addr5","BillAddress_City","BillAddress_State","BillAddress_PostalCode","BillAddress_Country","BillAddress_Note","BillAddressBlock_Addr1","BillAddressBlock_Addr2","BillAddressBlock_Addr3","BillAddressBlock_Addr4","BillAddressBlock_Addr5","ShipAddress_Addr1","ShipAddress_Addr2","ShipAddress_Addr3","ShipAddress_Addr4","ShipAddress_Addr5","ShipAddress_City","ShipAddress_State","ShipAddress_PostalCode","ShipAddress_Country","ShipAddress_Note","ShipAddressBlock_Addr1","ShipAddressBlock_Addr2","ShipAddressBlock_Addr3","ShipAddressBlock_Addr4","ShipAddressBlock_Addr5","Phone","AltPhone","Fax","Email","AltEmail","Contact","AltContact","CustomerType_ListID","CustomerType_FullName","Terms_ListID","Terms_FullName","SalesRep_ListID","SalesRep_FullName","Balance","TotalBalance","SalesTaxCode_ListID","SalesTaxCode_FullName","ItemSalesTax_ListID","ItemSalesTax_FullName","ResaleNumber","AccountNumber","CreditLimit","PreferredPaymentMethod_ListID","PreferredPaymentMethod_FullName","CreditCardInfo_CreditCardNumber","CreditCardInfo_ExpirationMonth","CreditCardInfo_ExpirationYear","CreditCardInfo_NameOnCard","CreditCardInfo_CreditCardAddress","CreditCardInfo_CreditCardPostalCode","JobStatus","JobStartDate","JobProjectedEndDate","JobEndDate","JobDesc","JobType_ListID","JobType_FullName","Notes","PriceLevel_ListID","PriceLevel_FullName");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: FullName -------------------------------------- 

	/** 
	* Sets a value to `FullName` variable
	* @access public
	*/

	public function setFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FullName` variable
	* @access public
	*/

	public function getFullname() {
		return $this->FullName;
	}

	public function get_FullName() {
		return $this->FullName;
	}

	
// ------------------------------ End Field: FullName --------------------------------------


// ---------------------------- Start Field: IsActive -------------------------------------- 

	/** 
	* Sets a value to `IsActive` variable
	* @access public
	*/

	public function setIsactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsActive` variable
	* @access public
	*/

	public function getIsactive() {
		return $this->IsActive;
	}

	public function get_IsActive() {
		return $this->IsActive;
	}

	
// ------------------------------ End Field: IsActive --------------------------------------


// ---------------------------- Start Field: Parent_ListID -------------------------------------- 

	/** 
	* Sets a value to `Parent_ListID` variable
	* @access public
	*/

	public function setParentListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Parent_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Parent_ListID` variable
	* @access public
	*/

	public function getParentListid() {
		return $this->Parent_ListID;
	}

	public function get_Parent_ListID() {
		return $this->Parent_ListID;
	}

	
// ------------------------------ End Field: Parent_ListID --------------------------------------


// ---------------------------- Start Field: Parent_FullName -------------------------------------- 

	/** 
	* Sets a value to `Parent_FullName` variable
	* @access public
	*/

	public function setParentFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Parent_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Parent_FullName` variable
	* @access public
	*/

	public function getParentFullname() {
		return $this->Parent_FullName;
	}

	public function get_Parent_FullName() {
		return $this->Parent_FullName;
	}

	
// ------------------------------ End Field: Parent_FullName --------------------------------------


// ---------------------------- Start Field: Sublevel -------------------------------------- 

	/** 
	* Sets a value to `Sublevel` variable
	* @access public
	*/

	public function setSublevel($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Sublevel', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Sublevel($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Sublevel', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Sublevel` variable
	* @access public
	*/

	public function getSublevel() {
		return $this->Sublevel;
	}

	public function get_Sublevel() {
		return $this->Sublevel;
	}

	
// ------------------------------ End Field: Sublevel --------------------------------------


// ---------------------------- Start Field: CompanyName -------------------------------------- 

	/** 
	* Sets a value to `CompanyName` variable
	* @access public
	*/

	public function setCompanyname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CompanyName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CompanyName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CompanyName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CompanyName` variable
	* @access public
	*/

	public function getCompanyname() {
		return $this->CompanyName;
	}

	public function get_CompanyName() {
		return $this->CompanyName;
	}

	
// ------------------------------ End Field: CompanyName --------------------------------------


// ---------------------------- Start Field: Salutation -------------------------------------- 

	/** 
	* Sets a value to `Salutation` variable
	* @access public
	*/

	public function setSalutation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Salutation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Salutation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Salutation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Salutation` variable
	* @access public
	*/

	public function getSalutation() {
		return $this->Salutation;
	}

	public function get_Salutation() {
		return $this->Salutation;
	}

	
// ------------------------------ End Field: Salutation --------------------------------------


// ---------------------------- Start Field: FirstName -------------------------------------- 

	/** 
	* Sets a value to `FirstName` variable
	* @access public
	*/

	public function setFirstname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FirstName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FirstName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FirstName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FirstName` variable
	* @access public
	*/

	public function getFirstname() {
		return $this->FirstName;
	}

	public function get_FirstName() {
		return $this->FirstName;
	}

	
// ------------------------------ End Field: FirstName --------------------------------------


// ---------------------------- Start Field: MiddleName -------------------------------------- 

	/** 
	* Sets a value to `MiddleName` variable
	* @access public
	*/

	public function setMiddlename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MiddleName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_MiddleName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MiddleName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `MiddleName` variable
	* @access public
	*/

	public function getMiddlename() {
		return $this->MiddleName;
	}

	public function get_MiddleName() {
		return $this->MiddleName;
	}

	
// ------------------------------ End Field: MiddleName --------------------------------------


// ---------------------------- Start Field: LastName -------------------------------------- 

	/** 
	* Sets a value to `LastName` variable
	* @access public
	*/

	public function setLastname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LastName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LastName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LastName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LastName` variable
	* @access public
	*/

	public function getLastname() {
		return $this->LastName;
	}

	public function get_LastName() {
		return $this->LastName;
	}

	
// ------------------------------ End Field: LastName --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr1` variable
	* @access public
	*/

	public function setBilladdressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr1` variable
	* @access public
	*/

	public function getBilladdressAddr1() {
		return $this->BillAddress_Addr1;
	}

	public function get_BillAddress_Addr1() {
		return $this->BillAddress_Addr1;
	}

	
// ------------------------------ End Field: BillAddress_Addr1 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr2` variable
	* @access public
	*/

	public function setBilladdressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr2` variable
	* @access public
	*/

	public function getBilladdressAddr2() {
		return $this->BillAddress_Addr2;
	}

	public function get_BillAddress_Addr2() {
		return $this->BillAddress_Addr2;
	}

	
// ------------------------------ End Field: BillAddress_Addr2 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr3` variable
	* @access public
	*/

	public function setBilladdressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr3` variable
	* @access public
	*/

	public function getBilladdressAddr3() {
		return $this->BillAddress_Addr3;
	}

	public function get_BillAddress_Addr3() {
		return $this->BillAddress_Addr3;
	}

	
// ------------------------------ End Field: BillAddress_Addr3 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr4` variable
	* @access public
	*/

	public function setBilladdressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr4` variable
	* @access public
	*/

	public function getBilladdressAddr4() {
		return $this->BillAddress_Addr4;
	}

	public function get_BillAddress_Addr4() {
		return $this->BillAddress_Addr4;
	}

	
// ------------------------------ End Field: BillAddress_Addr4 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr5` variable
	* @access public
	*/

	public function setBilladdressAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr5` variable
	* @access public
	*/

	public function getBilladdressAddr5() {
		return $this->BillAddress_Addr5;
	}

	public function get_BillAddress_Addr5() {
		return $this->BillAddress_Addr5;
	}

	
// ------------------------------ End Field: BillAddress_Addr5 --------------------------------------


// ---------------------------- Start Field: BillAddress_City -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_City` variable
	* @access public
	*/

	public function setBilladdressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_City` variable
	* @access public
	*/

	public function getBilladdressCity() {
		return $this->BillAddress_City;
	}

	public function get_BillAddress_City() {
		return $this->BillAddress_City;
	}

	
// ------------------------------ End Field: BillAddress_City --------------------------------------


// ---------------------------- Start Field: BillAddress_State -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_State` variable
	* @access public
	*/

	public function setBilladdressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_State` variable
	* @access public
	*/

	public function getBilladdressState() {
		return $this->BillAddress_State;
	}

	public function get_BillAddress_State() {
		return $this->BillAddress_State;
	}

	
// ------------------------------ End Field: BillAddress_State --------------------------------------


// ---------------------------- Start Field: BillAddress_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_PostalCode` variable
	* @access public
	*/

	public function setBilladdressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_PostalCode` variable
	* @access public
	*/

	public function getBilladdressPostalcode() {
		return $this->BillAddress_PostalCode;
	}

	public function get_BillAddress_PostalCode() {
		return $this->BillAddress_PostalCode;
	}

	
// ------------------------------ End Field: BillAddress_PostalCode --------------------------------------


// ---------------------------- Start Field: BillAddress_Country -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Country` variable
	* @access public
	*/

	public function setBilladdressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Country` variable
	* @access public
	*/

	public function getBilladdressCountry() {
		return $this->BillAddress_Country;
	}

	public function get_BillAddress_Country() {
		return $this->BillAddress_Country;
	}

	
// ------------------------------ End Field: BillAddress_Country --------------------------------------


// ---------------------------- Start Field: BillAddress_Note -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Note` variable
	* @access public
	*/

	public function setBilladdressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Note` variable
	* @access public
	*/

	public function getBilladdressNote() {
		return $this->BillAddress_Note;
	}

	public function get_BillAddress_Note() {
		return $this->BillAddress_Note;
	}

	
// ------------------------------ End Field: BillAddress_Note --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr1` variable
	* @access public
	*/

	public function setBilladdressblockAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr1` variable
	* @access public
	*/

	public function getBilladdressblockAddr1() {
		return $this->BillAddressBlock_Addr1;
	}

	public function get_BillAddressBlock_Addr1() {
		return $this->BillAddressBlock_Addr1;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr1 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr2` variable
	* @access public
	*/

	public function setBilladdressblockAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr2` variable
	* @access public
	*/

	public function getBilladdressblockAddr2() {
		return $this->BillAddressBlock_Addr2;
	}

	public function get_BillAddressBlock_Addr2() {
		return $this->BillAddressBlock_Addr2;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr2 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr3` variable
	* @access public
	*/

	public function setBilladdressblockAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr3` variable
	* @access public
	*/

	public function getBilladdressblockAddr3() {
		return $this->BillAddressBlock_Addr3;
	}

	public function get_BillAddressBlock_Addr3() {
		return $this->BillAddressBlock_Addr3;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr3 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr4` variable
	* @access public
	*/

	public function setBilladdressblockAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr4` variable
	* @access public
	*/

	public function getBilladdressblockAddr4() {
		return $this->BillAddressBlock_Addr4;
	}

	public function get_BillAddressBlock_Addr4() {
		return $this->BillAddressBlock_Addr4;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr4 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr5` variable
	* @access public
	*/

	public function setBilladdressblockAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr5` variable
	* @access public
	*/

	public function getBilladdressblockAddr5() {
		return $this->BillAddressBlock_Addr5;
	}

	public function get_BillAddressBlock_Addr5() {
		return $this->BillAddressBlock_Addr5;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr5 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr1` variable
	* @access public
	*/

	public function setShipaddressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr1` variable
	* @access public
	*/

	public function getShipaddressAddr1() {
		return $this->ShipAddress_Addr1;
	}

	public function get_ShipAddress_Addr1() {
		return $this->ShipAddress_Addr1;
	}

	
// ------------------------------ End Field: ShipAddress_Addr1 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr2` variable
	* @access public
	*/

	public function setShipaddressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr2` variable
	* @access public
	*/

	public function getShipaddressAddr2() {
		return $this->ShipAddress_Addr2;
	}

	public function get_ShipAddress_Addr2() {
		return $this->ShipAddress_Addr2;
	}

	
// ------------------------------ End Field: ShipAddress_Addr2 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr3` variable
	* @access public
	*/

	public function setShipaddressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr3` variable
	* @access public
	*/

	public function getShipaddressAddr3() {
		return $this->ShipAddress_Addr3;
	}

	public function get_ShipAddress_Addr3() {
		return $this->ShipAddress_Addr3;
	}

	
// ------------------------------ End Field: ShipAddress_Addr3 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr4` variable
	* @access public
	*/

	public function setShipaddressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr4` variable
	* @access public
	*/

	public function getShipaddressAddr4() {
		return $this->ShipAddress_Addr4;
	}

	public function get_ShipAddress_Addr4() {
		return $this->ShipAddress_Addr4;
	}

	
// ------------------------------ End Field: ShipAddress_Addr4 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr5` variable
	* @access public
	*/

	public function setShipaddressAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr5` variable
	* @access public
	*/

	public function getShipaddressAddr5() {
		return $this->ShipAddress_Addr5;
	}

	public function get_ShipAddress_Addr5() {
		return $this->ShipAddress_Addr5;
	}

	
// ------------------------------ End Field: ShipAddress_Addr5 --------------------------------------


// ---------------------------- Start Field: ShipAddress_City -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_City` variable
	* @access public
	*/

	public function setShipaddressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_City` variable
	* @access public
	*/

	public function getShipaddressCity() {
		return $this->ShipAddress_City;
	}

	public function get_ShipAddress_City() {
		return $this->ShipAddress_City;
	}

	
// ------------------------------ End Field: ShipAddress_City --------------------------------------


// ---------------------------- Start Field: ShipAddress_State -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_State` variable
	* @access public
	*/

	public function setShipaddressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_State` variable
	* @access public
	*/

	public function getShipaddressState() {
		return $this->ShipAddress_State;
	}

	public function get_ShipAddress_State() {
		return $this->ShipAddress_State;
	}

	
// ------------------------------ End Field: ShipAddress_State --------------------------------------


// ---------------------------- Start Field: ShipAddress_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_PostalCode` variable
	* @access public
	*/

	public function setShipaddressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_PostalCode` variable
	* @access public
	*/

	public function getShipaddressPostalcode() {
		return $this->ShipAddress_PostalCode;
	}

	public function get_ShipAddress_PostalCode() {
		return $this->ShipAddress_PostalCode;
	}

	
// ------------------------------ End Field: ShipAddress_PostalCode --------------------------------------


// ---------------------------- Start Field: ShipAddress_Country -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Country` variable
	* @access public
	*/

	public function setShipaddressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Country` variable
	* @access public
	*/

	public function getShipaddressCountry() {
		return $this->ShipAddress_Country;
	}

	public function get_ShipAddress_Country() {
		return $this->ShipAddress_Country;
	}

	
// ------------------------------ End Field: ShipAddress_Country --------------------------------------


// ---------------------------- Start Field: ShipAddress_Note -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Note` variable
	* @access public
	*/

	public function setShipaddressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Note` variable
	* @access public
	*/

	public function getShipaddressNote() {
		return $this->ShipAddress_Note;
	}

	public function get_ShipAddress_Note() {
		return $this->ShipAddress_Note;
	}

	
// ------------------------------ End Field: ShipAddress_Note --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr1` variable
	* @access public
	*/

	public function setShipaddressblockAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr1` variable
	* @access public
	*/

	public function getShipaddressblockAddr1() {
		return $this->ShipAddressBlock_Addr1;
	}

	public function get_ShipAddressBlock_Addr1() {
		return $this->ShipAddressBlock_Addr1;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr1 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr2` variable
	* @access public
	*/

	public function setShipaddressblockAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr2` variable
	* @access public
	*/

	public function getShipaddressblockAddr2() {
		return $this->ShipAddressBlock_Addr2;
	}

	public function get_ShipAddressBlock_Addr2() {
		return $this->ShipAddressBlock_Addr2;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr2 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr3` variable
	* @access public
	*/

	public function setShipaddressblockAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr3` variable
	* @access public
	*/

	public function getShipaddressblockAddr3() {
		return $this->ShipAddressBlock_Addr3;
	}

	public function get_ShipAddressBlock_Addr3() {
		return $this->ShipAddressBlock_Addr3;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr3 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr4` variable
	* @access public
	*/

	public function setShipaddressblockAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr4` variable
	* @access public
	*/

	public function getShipaddressblockAddr4() {
		return $this->ShipAddressBlock_Addr4;
	}

	public function get_ShipAddressBlock_Addr4() {
		return $this->ShipAddressBlock_Addr4;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr4 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr5` variable
	* @access public
	*/

	public function setShipaddressblockAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr5` variable
	* @access public
	*/

	public function getShipaddressblockAddr5() {
		return $this->ShipAddressBlock_Addr5;
	}

	public function get_ShipAddressBlock_Addr5() {
		return $this->ShipAddressBlock_Addr5;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr5 --------------------------------------


// ---------------------------- Start Field: Phone -------------------------------------- 

	/** 
	* Sets a value to `Phone` variable
	* @access public
	*/

	public function setPhone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Phone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Phone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Phone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Phone` variable
	* @access public
	*/

	public function getPhone() {
		return $this->Phone;
	}

	public function get_Phone() {
		return $this->Phone;
	}

	
// ------------------------------ End Field: Phone --------------------------------------


// ---------------------------- Start Field: AltPhone -------------------------------------- 

	/** 
	* Sets a value to `AltPhone` variable
	* @access public
	*/

	public function setAltphone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltPhone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AltPhone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltPhone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AltPhone` variable
	* @access public
	*/

	public function getAltphone() {
		return $this->AltPhone;
	}

	public function get_AltPhone() {
		return $this->AltPhone;
	}

	
// ------------------------------ End Field: AltPhone --------------------------------------


// ---------------------------- Start Field: Fax -------------------------------------- 

	/** 
	* Sets a value to `Fax` variable
	* @access public
	*/

	public function setFax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Fax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Fax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Fax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Fax` variable
	* @access public
	*/

	public function getFax() {
		return $this->Fax;
	}

	public function get_Fax() {
		return $this->Fax;
	}

	
// ------------------------------ End Field: Fax --------------------------------------


// ---------------------------- Start Field: Email -------------------------------------- 

	/** 
	* Sets a value to `Email` variable
	* @access public
	*/

	public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Email($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Email` variable
	* @access public
	*/

	public function getEmail() {
		return $this->Email;
	}

	public function get_Email() {
		return $this->Email;
	}

	
// ------------------------------ End Field: Email --------------------------------------


// ---------------------------- Start Field: AltEmail -------------------------------------- 

	/** 
	* Sets a value to `AltEmail` variable
	* @access public
	*/

	public function setAltemail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltEmail', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AltEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltEmail', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AltEmail` variable
	* @access public
	*/

	public function getAltemail() {
		return $this->AltEmail;
	}

	public function get_AltEmail() {
		return $this->AltEmail;
	}

	
// ------------------------------ End Field: AltEmail --------------------------------------


// ---------------------------- Start Field: Contact -------------------------------------- 

	/** 
	* Sets a value to `Contact` variable
	* @access public
	*/

	public function setContact($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Contact', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Contact($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Contact', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Contact` variable
	* @access public
	*/

	public function getContact() {
		return $this->Contact;
	}

	public function get_Contact() {
		return $this->Contact;
	}

	
// ------------------------------ End Field: Contact --------------------------------------


// ---------------------------- Start Field: AltContact -------------------------------------- 

	/** 
	* Sets a value to `AltContact` variable
	* @access public
	*/

	public function setAltcontact($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltContact', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AltContact($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltContact', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AltContact` variable
	* @access public
	*/

	public function getAltcontact() {
		return $this->AltContact;
	}

	public function get_AltContact() {
		return $this->AltContact;
	}

	
// ------------------------------ End Field: AltContact --------------------------------------


// ---------------------------- Start Field: CustomerType_ListID -------------------------------------- 

	/** 
	* Sets a value to `CustomerType_ListID` variable
	* @access public
	*/

	public function setCustomertypeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerType_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomerType_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerType_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomerType_ListID` variable
	* @access public
	*/

	public function getCustomertypeListid() {
		return $this->CustomerType_ListID;
	}

	public function get_CustomerType_ListID() {
		return $this->CustomerType_ListID;
	}

	
// ------------------------------ End Field: CustomerType_ListID --------------------------------------


// ---------------------------- Start Field: CustomerType_FullName -------------------------------------- 

	/** 
	* Sets a value to `CustomerType_FullName` variable
	* @access public
	*/

	public function setCustomertypeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerType_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomerType_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerType_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomerType_FullName` variable
	* @access public
	*/

	public function getCustomertypeFullname() {
		return $this->CustomerType_FullName;
	}

	public function get_CustomerType_FullName() {
		return $this->CustomerType_FullName;
	}

	
// ------------------------------ End Field: CustomerType_FullName --------------------------------------


// ---------------------------- Start Field: Terms_ListID -------------------------------------- 

	/** 
	* Sets a value to `Terms_ListID` variable
	* @access public
	*/

	public function setTermsListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Terms_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Terms_ListID` variable
	* @access public
	*/

	public function getTermsListid() {
		return $this->Terms_ListID;
	}

	public function get_Terms_ListID() {
		return $this->Terms_ListID;
	}

	
// ------------------------------ End Field: Terms_ListID --------------------------------------


// ---------------------------- Start Field: Terms_FullName -------------------------------------- 

	/** 
	* Sets a value to `Terms_FullName` variable
	* @access public
	*/

	public function setTermsFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Terms_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Terms_FullName` variable
	* @access public
	*/

	public function getTermsFullname() {
		return $this->Terms_FullName;
	}

	public function get_Terms_FullName() {
		return $this->Terms_FullName;
	}

	
// ------------------------------ End Field: Terms_FullName --------------------------------------


// ---------------------------- Start Field: SalesRep_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesRep_ListID` variable
	* @access public
	*/

	public function setSalesrepListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesRep_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesRep_ListID` variable
	* @access public
	*/

	public function getSalesrepListid() {
		return $this->SalesRep_ListID;
	}

	public function get_SalesRep_ListID() {
		return $this->SalesRep_ListID;
	}

	
// ------------------------------ End Field: SalesRep_ListID --------------------------------------


// ---------------------------- Start Field: SalesRep_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesRep_FullName` variable
	* @access public
	*/

	public function setSalesrepFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesRep_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesRep_FullName` variable
	* @access public
	*/

	public function getSalesrepFullname() {
		return $this->SalesRep_FullName;
	}

	public function get_SalesRep_FullName() {
		return $this->SalesRep_FullName;
	}

	
// ------------------------------ End Field: SalesRep_FullName --------------------------------------


// ---------------------------- Start Field: Balance -------------------------------------- 

	/** 
	* Sets a value to `Balance` variable
	* @access public
	*/

	public function setBalance($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Balance', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Balance($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Balance', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Balance` variable
	* @access public
	*/

	public function getBalance() {
		return $this->Balance;
	}

	public function get_Balance() {
		return $this->Balance;
	}

	
// ------------------------------ End Field: Balance --------------------------------------


// ---------------------------- Start Field: TotalBalance -------------------------------------- 

	/** 
	* Sets a value to `TotalBalance` variable
	* @access public
	*/

	public function setTotalbalance($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalBalance', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TotalBalance($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalBalance', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TotalBalance` variable
	* @access public
	*/

	public function getTotalbalance() {
		return $this->TotalBalance;
	}

	public function get_TotalBalance() {
		return $this->TotalBalance;
	}

	
// ------------------------------ End Field: TotalBalance --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function setSalestaxcodeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function getSalestaxcodeListid() {
		return $this->SalesTaxCode_ListID;
	}

	public function get_SalesTaxCode_ListID() {
		return $this->SalesTaxCode_ListID;
	}

	
// ------------------------------ End Field: SalesTaxCode_ListID --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function setSalestaxcodeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function getSalestaxcodeFullname() {
		return $this->SalesTaxCode_FullName;
	}

	public function get_SalesTaxCode_FullName() {
		return $this->SalesTaxCode_FullName;
	}

	
// ------------------------------ End Field: SalesTaxCode_FullName --------------------------------------


// ---------------------------- Start Field: ItemSalesTax_ListID -------------------------------------- 

	/** 
	* Sets a value to `ItemSalesTax_ListID` variable
	* @access public
	*/

	public function setItemsalestaxListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemSalesTax_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemSalesTax_ListID` variable
	* @access public
	*/

	public function getItemsalestaxListid() {
		return $this->ItemSalesTax_ListID;
	}

	public function get_ItemSalesTax_ListID() {
		return $this->ItemSalesTax_ListID;
	}

	
// ------------------------------ End Field: ItemSalesTax_ListID --------------------------------------


// ---------------------------- Start Field: ItemSalesTax_FullName -------------------------------------- 

	/** 
	* Sets a value to `ItemSalesTax_FullName` variable
	* @access public
	*/

	public function setItemsalestaxFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemSalesTax_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemSalesTax_FullName` variable
	* @access public
	*/

	public function getItemsalestaxFullname() {
		return $this->ItemSalesTax_FullName;
	}

	public function get_ItemSalesTax_FullName() {
		return $this->ItemSalesTax_FullName;
	}

	
// ------------------------------ End Field: ItemSalesTax_FullName --------------------------------------


// ---------------------------- Start Field: ResaleNumber -------------------------------------- 

	/** 
	* Sets a value to `ResaleNumber` variable
	* @access public
	*/

	public function setResalenumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ResaleNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ResaleNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ResaleNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ResaleNumber` variable
	* @access public
	*/

	public function getResalenumber() {
		return $this->ResaleNumber;
	}

	public function get_ResaleNumber() {
		return $this->ResaleNumber;
	}

	
// ------------------------------ End Field: ResaleNumber --------------------------------------


// ---------------------------- Start Field: AccountNumber -------------------------------------- 

	/** 
	* Sets a value to `AccountNumber` variable
	* @access public
	*/

	public function setAccountnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AccountNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AccountNumber` variable
	* @access public
	*/

	public function getAccountnumber() {
		return $this->AccountNumber;
	}

	public function get_AccountNumber() {
		return $this->AccountNumber;
	}

	
// ------------------------------ End Field: AccountNumber --------------------------------------


// ---------------------------- Start Field: CreditLimit -------------------------------------- 

	/** 
	* Sets a value to `CreditLimit` variable
	* @access public
	*/

	public function setCreditlimit($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditLimit', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditLimit($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditLimit', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditLimit` variable
	* @access public
	*/

	public function getCreditlimit() {
		return $this->CreditLimit;
	}

	public function get_CreditLimit() {
		return $this->CreditLimit;
	}

	
// ------------------------------ End Field: CreditLimit --------------------------------------


// ---------------------------- Start Field: PreferredPaymentMethod_ListID -------------------------------------- 

	/** 
	* Sets a value to `PreferredPaymentMethod_ListID` variable
	* @access public
	*/

	public function setPreferredpaymentmethodListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PreferredPaymentMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PreferredPaymentMethod_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PreferredPaymentMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PreferredPaymentMethod_ListID` variable
	* @access public
	*/

	public function getPreferredpaymentmethodListid() {
		return $this->PreferredPaymentMethod_ListID;
	}

	public function get_PreferredPaymentMethod_ListID() {
		return $this->PreferredPaymentMethod_ListID;
	}

	
// ------------------------------ End Field: PreferredPaymentMethod_ListID --------------------------------------


// ---------------------------- Start Field: PreferredPaymentMethod_FullName -------------------------------------- 

	/** 
	* Sets a value to `PreferredPaymentMethod_FullName` variable
	* @access public
	*/

	public function setPreferredpaymentmethodFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PreferredPaymentMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PreferredPaymentMethod_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PreferredPaymentMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PreferredPaymentMethod_FullName` variable
	* @access public
	*/

	public function getPreferredpaymentmethodFullname() {
		return $this->PreferredPaymentMethod_FullName;
	}

	public function get_PreferredPaymentMethod_FullName() {
		return $this->PreferredPaymentMethod_FullName;
	}

	
// ------------------------------ End Field: PreferredPaymentMethod_FullName --------------------------------------


// ---------------------------- Start Field: CreditCardInfo_CreditCardNumber -------------------------------------- 

	/** 
	* Sets a value to `CreditCardInfo_CreditCardNumber` variable
	* @access public
	*/

	public function setCreditcardinfoCreditcardnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_CreditCardNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardInfo_CreditCardNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_CreditCardNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardInfo_CreditCardNumber` variable
	* @access public
	*/

	public function getCreditcardinfoCreditcardnumber() {
		return $this->CreditCardInfo_CreditCardNumber;
	}

	public function get_CreditCardInfo_CreditCardNumber() {
		return $this->CreditCardInfo_CreditCardNumber;
	}

	
// ------------------------------ End Field: CreditCardInfo_CreditCardNumber --------------------------------------


// ---------------------------- Start Field: CreditCardInfo_ExpirationMonth -------------------------------------- 

	/** 
	* Sets a value to `CreditCardInfo_ExpirationMonth` variable
	* @access public
	*/

	public function setCreditcardinfoExpirationmonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_ExpirationMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardInfo_ExpirationMonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_ExpirationMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardInfo_ExpirationMonth` variable
	* @access public
	*/

	public function getCreditcardinfoExpirationmonth() {
		return $this->CreditCardInfo_ExpirationMonth;
	}

	public function get_CreditCardInfo_ExpirationMonth() {
		return $this->CreditCardInfo_ExpirationMonth;
	}

	
// ------------------------------ End Field: CreditCardInfo_ExpirationMonth --------------------------------------


// ---------------------------- Start Field: CreditCardInfo_ExpirationYear -------------------------------------- 

	/** 
	* Sets a value to `CreditCardInfo_ExpirationYear` variable
	* @access public
	*/

	public function setCreditcardinfoExpirationyear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_ExpirationYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardInfo_ExpirationYear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_ExpirationYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardInfo_ExpirationYear` variable
	* @access public
	*/

	public function getCreditcardinfoExpirationyear() {
		return $this->CreditCardInfo_ExpirationYear;
	}

	public function get_CreditCardInfo_ExpirationYear() {
		return $this->CreditCardInfo_ExpirationYear;
	}

	
// ------------------------------ End Field: CreditCardInfo_ExpirationYear --------------------------------------


// ---------------------------- Start Field: CreditCardInfo_NameOnCard -------------------------------------- 

	/** 
	* Sets a value to `CreditCardInfo_NameOnCard` variable
	* @access public
	*/

	public function setCreditcardinfoNameoncard($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_NameOnCard', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardInfo_NameOnCard($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_NameOnCard', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardInfo_NameOnCard` variable
	* @access public
	*/

	public function getCreditcardinfoNameoncard() {
		return $this->CreditCardInfo_NameOnCard;
	}

	public function get_CreditCardInfo_NameOnCard() {
		return $this->CreditCardInfo_NameOnCard;
	}

	
// ------------------------------ End Field: CreditCardInfo_NameOnCard --------------------------------------


// ---------------------------- Start Field: CreditCardInfo_CreditCardAddress -------------------------------------- 

	/** 
	* Sets a value to `CreditCardInfo_CreditCardAddress` variable
	* @access public
	*/

	public function setCreditcardinfoCreditcardaddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_CreditCardAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardInfo_CreditCardAddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_CreditCardAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardInfo_CreditCardAddress` variable
	* @access public
	*/

	public function getCreditcardinfoCreditcardaddress() {
		return $this->CreditCardInfo_CreditCardAddress;
	}

	public function get_CreditCardInfo_CreditCardAddress() {
		return $this->CreditCardInfo_CreditCardAddress;
	}

	
// ------------------------------ End Field: CreditCardInfo_CreditCardAddress --------------------------------------


// ---------------------------- Start Field: CreditCardInfo_CreditCardPostalCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardInfo_CreditCardPostalCode` variable
	* @access public
	*/

	public function setCreditcardinfoCreditcardpostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_CreditCardPostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardInfo_CreditCardPostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardInfo_CreditCardPostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardInfo_CreditCardPostalCode` variable
	* @access public
	*/

	public function getCreditcardinfoCreditcardpostalcode() {
		return $this->CreditCardInfo_CreditCardPostalCode;
	}

	public function get_CreditCardInfo_CreditCardPostalCode() {
		return $this->CreditCardInfo_CreditCardPostalCode;
	}

	
// ------------------------------ End Field: CreditCardInfo_CreditCardPostalCode --------------------------------------


// ---------------------------- Start Field: JobStatus -------------------------------------- 

	/** 
	* Sets a value to `JobStatus` variable
	* @access public
	*/

	public function setJobstatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_JobStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `JobStatus` variable
	* @access public
	*/

	public function getJobstatus() {
		return $this->JobStatus;
	}

	public function get_JobStatus() {
		return $this->JobStatus;
	}

	
// ------------------------------ End Field: JobStatus --------------------------------------


// ---------------------------- Start Field: JobStartDate -------------------------------------- 

	/** 
	* Sets a value to `JobStartDate` variable
	* @access public
	*/

	public function setJobstartdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobStartDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_JobStartDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobStartDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `JobStartDate` variable
	* @access public
	*/

	public function getJobstartdate() {
		return $this->JobStartDate;
	}

	public function get_JobStartDate() {
		return $this->JobStartDate;
	}

	
// ------------------------------ End Field: JobStartDate --------------------------------------


// ---------------------------- Start Field: JobProjectedEndDate -------------------------------------- 

	/** 
	* Sets a value to `JobProjectedEndDate` variable
	* @access public
	*/

	public function setJobprojectedenddate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobProjectedEndDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_JobProjectedEndDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobProjectedEndDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `JobProjectedEndDate` variable
	* @access public
	*/

	public function getJobprojectedenddate() {
		return $this->JobProjectedEndDate;
	}

	public function get_JobProjectedEndDate() {
		return $this->JobProjectedEndDate;
	}

	
// ------------------------------ End Field: JobProjectedEndDate --------------------------------------


// ---------------------------- Start Field: JobEndDate -------------------------------------- 

	/** 
	* Sets a value to `JobEndDate` variable
	* @access public
	*/

	public function setJobenddate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobEndDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_JobEndDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobEndDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `JobEndDate` variable
	* @access public
	*/

	public function getJobenddate() {
		return $this->JobEndDate;
	}

	public function get_JobEndDate() {
		return $this->JobEndDate;
	}

	
// ------------------------------ End Field: JobEndDate --------------------------------------


// ---------------------------- Start Field: JobDesc -------------------------------------- 

	/** 
	* Sets a value to `JobDesc` variable
	* @access public
	*/

	public function setJobdesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_JobDesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `JobDesc` variable
	* @access public
	*/

	public function getJobdesc() {
		return $this->JobDesc;
	}

	public function get_JobDesc() {
		return $this->JobDesc;
	}

	
// ------------------------------ End Field: JobDesc --------------------------------------


// ---------------------------- Start Field: JobType_ListID -------------------------------------- 

	/** 
	* Sets a value to `JobType_ListID` variable
	* @access public
	*/

	public function setJobtypeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobType_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_JobType_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobType_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `JobType_ListID` variable
	* @access public
	*/

	public function getJobtypeListid() {
		return $this->JobType_ListID;
	}

	public function get_JobType_ListID() {
		return $this->JobType_ListID;
	}

	
// ------------------------------ End Field: JobType_ListID --------------------------------------


// ---------------------------- Start Field: JobType_FullName -------------------------------------- 

	/** 
	* Sets a value to `JobType_FullName` variable
	* @access public
	*/

	public function setJobtypeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobType_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_JobType_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobType_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `JobType_FullName` variable
	* @access public
	*/

	public function getJobtypeFullname() {
		return $this->JobType_FullName;
	}

	public function get_JobType_FullName() {
		return $this->JobType_FullName;
	}

	
// ------------------------------ End Field: JobType_FullName --------------------------------------


// ---------------------------- Start Field: Notes -------------------------------------- 

	/** 
	* Sets a value to `Notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Notes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->Notes;
	}

	public function get_Notes() {
		return $this->Notes;
	}

	
// ------------------------------ End Field: Notes --------------------------------------


// ---------------------------- Start Field: PriceLevel_ListID -------------------------------------- 

	/** 
	* Sets a value to `PriceLevel_ListID` variable
	* @access public
	*/

	public function setPricelevelListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PriceLevel_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PriceLevel_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PriceLevel_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PriceLevel_ListID` variable
	* @access public
	*/

	public function getPricelevelListid() {
		return $this->PriceLevel_ListID;
	}

	public function get_PriceLevel_ListID() {
		return $this->PriceLevel_ListID;
	}

	
// ------------------------------ End Field: PriceLevel_ListID --------------------------------------


// ---------------------------- Start Field: PriceLevel_FullName -------------------------------------- 

	/** 
	* Sets a value to `PriceLevel_FullName` variable
	* @access public
	*/

	public function setPricelevelFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PriceLevel_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PriceLevel_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PriceLevel_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PriceLevel_FullName` variable
	* @access public
	*/

	public function getPricelevelFullname() {
		return $this->PriceLevel_FullName;
	}

	public function get_PriceLevel_FullName() {
		return $this->PriceLevel_FullName;
	}

	
// ------------------------------ End Field: PriceLevel_FullName --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'FullName' => (object) array(
										'Field'=>'FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsActive' => (object) array(
										'Field'=>'IsActive',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'Parent_ListID' => (object) array(
										'Field'=>'Parent_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Parent_FullName' => (object) array(
										'Field'=>'Parent_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Sublevel' => (object) array(
										'Field'=>'Sublevel',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'CompanyName' => (object) array(
										'Field'=>'CompanyName',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Salutation' => (object) array(
										'Field'=>'Salutation',
										'Type'=>'varchar(15)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FirstName' => (object) array(
										'Field'=>'FirstName',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'MiddleName' => (object) array(
										'Field'=>'MiddleName',
										'Type'=>'varchar(5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LastName' => (object) array(
										'Field'=>'LastName',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr1' => (object) array(
										'Field'=>'BillAddress_Addr1',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr2' => (object) array(
										'Field'=>'BillAddress_Addr2',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr3' => (object) array(
										'Field'=>'BillAddress_Addr3',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr4' => (object) array(
										'Field'=>'BillAddress_Addr4',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr5' => (object) array(
										'Field'=>'BillAddress_Addr5',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_City' => (object) array(
										'Field'=>'BillAddress_City',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_State' => (object) array(
										'Field'=>'BillAddress_State',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_PostalCode' => (object) array(
										'Field'=>'BillAddress_PostalCode',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Country' => (object) array(
										'Field'=>'BillAddress_Country',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Note' => (object) array(
										'Field'=>'BillAddress_Note',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr1' => (object) array(
										'Field'=>'BillAddressBlock_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr2' => (object) array(
										'Field'=>'BillAddressBlock_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr3' => (object) array(
										'Field'=>'BillAddressBlock_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr4' => (object) array(
										'Field'=>'BillAddressBlock_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr5' => (object) array(
										'Field'=>'BillAddressBlock_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr1' => (object) array(
										'Field'=>'ShipAddress_Addr1',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr2' => (object) array(
										'Field'=>'ShipAddress_Addr2',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr3' => (object) array(
										'Field'=>'ShipAddress_Addr3',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr4' => (object) array(
										'Field'=>'ShipAddress_Addr4',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr5' => (object) array(
										'Field'=>'ShipAddress_Addr5',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_City' => (object) array(
										'Field'=>'ShipAddress_City',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_State' => (object) array(
										'Field'=>'ShipAddress_State',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_PostalCode' => (object) array(
										'Field'=>'ShipAddress_PostalCode',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Country' => (object) array(
										'Field'=>'ShipAddress_Country',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Note' => (object) array(
										'Field'=>'ShipAddress_Note',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr1' => (object) array(
										'Field'=>'ShipAddressBlock_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr2' => (object) array(
										'Field'=>'ShipAddressBlock_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr3' => (object) array(
										'Field'=>'ShipAddressBlock_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr4' => (object) array(
										'Field'=>'ShipAddressBlock_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr5' => (object) array(
										'Field'=>'ShipAddressBlock_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Phone' => (object) array(
										'Field'=>'Phone',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AltPhone' => (object) array(
										'Field'=>'AltPhone',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Fax' => (object) array(
										'Field'=>'Fax',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Email' => (object) array(
										'Field'=>'Email',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AltEmail' => (object) array(
										'Field'=>'AltEmail',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Contact' => (object) array(
										'Field'=>'Contact',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AltContact' => (object) array(
										'Field'=>'AltContact',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomerType_ListID' => (object) array(
										'Field'=>'CustomerType_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomerType_FullName' => (object) array(
										'Field'=>'CustomerType_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Terms_ListID' => (object) array(
										'Field'=>'Terms_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Terms_FullName' => (object) array(
										'Field'=>'Terms_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesRep_ListID' => (object) array(
										'Field'=>'SalesRep_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesRep_FullName' => (object) array(
										'Field'=>'SalesRep_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Balance' => (object) array(
										'Field'=>'Balance',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TotalBalance' => (object) array(
										'Field'=>'TotalBalance',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_ListID' => (object) array(
										'Field'=>'SalesTaxCode_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_FullName' => (object) array(
										'Field'=>'SalesTaxCode_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemSalesTax_ListID' => (object) array(
										'Field'=>'ItemSalesTax_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemSalesTax_FullName' => (object) array(
										'Field'=>'ItemSalesTax_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ResaleNumber' => (object) array(
										'Field'=>'ResaleNumber',
										'Type'=>'varchar(15)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AccountNumber' => (object) array(
										'Field'=>'AccountNumber',
										'Type'=>'varchar(99)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditLimit' => (object) array(
										'Field'=>'CreditLimit',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PreferredPaymentMethod_ListID' => (object) array(
										'Field'=>'PreferredPaymentMethod_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PreferredPaymentMethod_FullName' => (object) array(
										'Field'=>'PreferredPaymentMethod_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardInfo_CreditCardNumber' => (object) array(
										'Field'=>'CreditCardInfo_CreditCardNumber',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardInfo_ExpirationMonth' => (object) array(
										'Field'=>'CreditCardInfo_ExpirationMonth',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardInfo_ExpirationYear' => (object) array(
										'Field'=>'CreditCardInfo_ExpirationYear',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardInfo_NameOnCard' => (object) array(
										'Field'=>'CreditCardInfo_NameOnCard',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardInfo_CreditCardAddress' => (object) array(
										'Field'=>'CreditCardInfo_CreditCardAddress',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardInfo_CreditCardPostalCode' => (object) array(
										'Field'=>'CreditCardInfo_CreditCardPostalCode',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'JobStatus' => (object) array(
										'Field'=>'JobStatus',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'JobStartDate' => (object) array(
										'Field'=>'JobStartDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'JobProjectedEndDate' => (object) array(
										'Field'=>'JobProjectedEndDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'JobEndDate' => (object) array(
										'Field'=>'JobEndDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'JobDesc' => (object) array(
										'Field'=>'JobDesc',
										'Type'=>'varchar(99)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'JobType_ListID' => (object) array(
										'Field'=>'JobType_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'JobType_FullName' => (object) array(
										'Field'=>'JobType_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Notes' => (object) array(
										'Field'=>'Notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PriceLevel_ListID' => (object) array(
										'Field'=>'PriceLevel_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PriceLevel_FullName' => (object) array(
										'Field'=>'PriceLevel_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_customer` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ListID' => "ALTER TABLE  `qb_customer` ADD  `ListID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_customer` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_customer` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_customer` ADD  `EditSequence` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_customer` ADD  `Name` varchar(41) NULL   ;",
			'FullName' => "ALTER TABLE  `qb_customer` ADD  `FullName` varchar(255) NULL   ;",
			'IsActive' => "ALTER TABLE  `qb_customer` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';",
			'Parent_ListID' => "ALTER TABLE  `qb_customer` ADD  `Parent_ListID` varchar(40) NULL   ;",
			'Parent_FullName' => "ALTER TABLE  `qb_customer` ADD  `Parent_FullName` varchar(255) NULL   ;",
			'Sublevel' => "ALTER TABLE  `qb_customer` ADD  `Sublevel` int(10) unsigned NULL   DEFAULT '0';",
			'CompanyName' => "ALTER TABLE  `qb_customer` ADD  `CompanyName` varchar(41) NULL   ;",
			'Salutation' => "ALTER TABLE  `qb_customer` ADD  `Salutation` varchar(15) NULL   ;",
			'FirstName' => "ALTER TABLE  `qb_customer` ADD  `FirstName` varchar(25) NULL   ;",
			'MiddleName' => "ALTER TABLE  `qb_customer` ADD  `MiddleName` varchar(5) NULL   ;",
			'LastName' => "ALTER TABLE  `qb_customer` ADD  `LastName` varchar(25) NULL   ;",
			'BillAddress_Addr1' => "ALTER TABLE  `qb_customer` ADD  `BillAddress_Addr1` varchar(41) NULL   ;",
			'BillAddress_Addr2' => "ALTER TABLE  `qb_customer` ADD  `BillAddress_Addr2` varchar(41) NULL   ;",
			'BillAddress_Addr3' => "ALTER TABLE  `qb_customer` ADD  `BillAddress_Addr3` varchar(41) NULL   ;",
			'BillAddress_Addr4' => "ALTER TABLE  `qb_customer` ADD  `BillAddress_Addr4` varchar(41) NULL   ;",
			'BillAddress_Addr5' => "ALTER TABLE  `qb_customer` ADD  `BillAddress_Addr5` varchar(41) NULL   ;",
			'BillAddress_City' => "ALTER TABLE  `qb_customer` ADD  `BillAddress_City` varchar(31) NULL   ;",
			'BillAddress_State' => "ALTER TABLE  `qb_customer` ADD  `BillAddress_State` varchar(21) NULL   ;",
			'BillAddress_PostalCode' => "ALTER TABLE  `qb_customer` ADD  `BillAddress_PostalCode` varchar(13) NULL   ;",
			'BillAddress_Country' => "ALTER TABLE  `qb_customer` ADD  `BillAddress_Country` varchar(31) NULL   ;",
			'BillAddress_Note' => "ALTER TABLE  `qb_customer` ADD  `BillAddress_Note` varchar(41) NULL   ;",
			'BillAddressBlock_Addr1' => "ALTER TABLE  `qb_customer` ADD  `BillAddressBlock_Addr1` text NULL   ;",
			'BillAddressBlock_Addr2' => "ALTER TABLE  `qb_customer` ADD  `BillAddressBlock_Addr2` text NULL   ;",
			'BillAddressBlock_Addr3' => "ALTER TABLE  `qb_customer` ADD  `BillAddressBlock_Addr3` text NULL   ;",
			'BillAddressBlock_Addr4' => "ALTER TABLE  `qb_customer` ADD  `BillAddressBlock_Addr4` text NULL   ;",
			'BillAddressBlock_Addr5' => "ALTER TABLE  `qb_customer` ADD  `BillAddressBlock_Addr5` text NULL   ;",
			'ShipAddress_Addr1' => "ALTER TABLE  `qb_customer` ADD  `ShipAddress_Addr1` varchar(41) NULL   ;",
			'ShipAddress_Addr2' => "ALTER TABLE  `qb_customer` ADD  `ShipAddress_Addr2` varchar(41) NULL   ;",
			'ShipAddress_Addr3' => "ALTER TABLE  `qb_customer` ADD  `ShipAddress_Addr3` varchar(41) NULL   ;",
			'ShipAddress_Addr4' => "ALTER TABLE  `qb_customer` ADD  `ShipAddress_Addr4` varchar(41) NULL   ;",
			'ShipAddress_Addr5' => "ALTER TABLE  `qb_customer` ADD  `ShipAddress_Addr5` varchar(41) NULL   ;",
			'ShipAddress_City' => "ALTER TABLE  `qb_customer` ADD  `ShipAddress_City` varchar(31) NULL   ;",
			'ShipAddress_State' => "ALTER TABLE  `qb_customer` ADD  `ShipAddress_State` varchar(21) NULL   ;",
			'ShipAddress_PostalCode' => "ALTER TABLE  `qb_customer` ADD  `ShipAddress_PostalCode` varchar(13) NULL   ;",
			'ShipAddress_Country' => "ALTER TABLE  `qb_customer` ADD  `ShipAddress_Country` varchar(31) NULL   ;",
			'ShipAddress_Note' => "ALTER TABLE  `qb_customer` ADD  `ShipAddress_Note` varchar(41) NULL   ;",
			'ShipAddressBlock_Addr1' => "ALTER TABLE  `qb_customer` ADD  `ShipAddressBlock_Addr1` text NULL   ;",
			'ShipAddressBlock_Addr2' => "ALTER TABLE  `qb_customer` ADD  `ShipAddressBlock_Addr2` text NULL   ;",
			'ShipAddressBlock_Addr3' => "ALTER TABLE  `qb_customer` ADD  `ShipAddressBlock_Addr3` text NULL   ;",
			'ShipAddressBlock_Addr4' => "ALTER TABLE  `qb_customer` ADD  `ShipAddressBlock_Addr4` text NULL   ;",
			'ShipAddressBlock_Addr5' => "ALTER TABLE  `qb_customer` ADD  `ShipAddressBlock_Addr5` text NULL   ;",
			'Phone' => "ALTER TABLE  `qb_customer` ADD  `Phone` varchar(21) NULL   ;",
			'AltPhone' => "ALTER TABLE  `qb_customer` ADD  `AltPhone` varchar(21) NULL   ;",
			'Fax' => "ALTER TABLE  `qb_customer` ADD  `Fax` varchar(21) NULL   ;",
			'Email' => "ALTER TABLE  `qb_customer` ADD  `Email` text NULL   ;",
			'AltEmail' => "ALTER TABLE  `qb_customer` ADD  `AltEmail` text NULL   ;",
			'Contact' => "ALTER TABLE  `qb_customer` ADD  `Contact` varchar(41) NULL   ;",
			'AltContact' => "ALTER TABLE  `qb_customer` ADD  `AltContact` varchar(41) NULL   ;",
			'CustomerType_ListID' => "ALTER TABLE  `qb_customer` ADD  `CustomerType_ListID` varchar(40) NULL   ;",
			'CustomerType_FullName' => "ALTER TABLE  `qb_customer` ADD  `CustomerType_FullName` varchar(255) NULL   ;",
			'Terms_ListID' => "ALTER TABLE  `qb_customer` ADD  `Terms_ListID` varchar(40) NULL   ;",
			'Terms_FullName' => "ALTER TABLE  `qb_customer` ADD  `Terms_FullName` varchar(255) NULL   ;",
			'SalesRep_ListID' => "ALTER TABLE  `qb_customer` ADD  `SalesRep_ListID` varchar(40) NULL   ;",
			'SalesRep_FullName' => "ALTER TABLE  `qb_customer` ADD  `SalesRep_FullName` varchar(255) NULL   ;",
			'Balance' => "ALTER TABLE  `qb_customer` ADD  `Balance` decimal(10,2) NULL   ;",
			'TotalBalance' => "ALTER TABLE  `qb_customer` ADD  `TotalBalance` decimal(10,2) NULL   ;",
			'SalesTaxCode_ListID' => "ALTER TABLE  `qb_customer` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;",
			'SalesTaxCode_FullName' => "ALTER TABLE  `qb_customer` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;",
			'ItemSalesTax_ListID' => "ALTER TABLE  `qb_customer` ADD  `ItemSalesTax_ListID` varchar(40) NULL   ;",
			'ItemSalesTax_FullName' => "ALTER TABLE  `qb_customer` ADD  `ItemSalesTax_FullName` varchar(255) NULL   ;",
			'ResaleNumber' => "ALTER TABLE  `qb_customer` ADD  `ResaleNumber` varchar(15) NULL   ;",
			'AccountNumber' => "ALTER TABLE  `qb_customer` ADD  `AccountNumber` varchar(99) NULL   ;",
			'CreditLimit' => "ALTER TABLE  `qb_customer` ADD  `CreditLimit` decimal(10,2) NULL   ;",
			'PreferredPaymentMethod_ListID' => "ALTER TABLE  `qb_customer` ADD  `PreferredPaymentMethod_ListID` varchar(40) NULL   ;",
			'PreferredPaymentMethod_FullName' => "ALTER TABLE  `qb_customer` ADD  `PreferredPaymentMethod_FullName` varchar(255) NULL   ;",
			'CreditCardInfo_CreditCardNumber' => "ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_CreditCardNumber` varchar(25) NULL   ;",
			'CreditCardInfo_ExpirationMonth' => "ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_ExpirationMonth` int(10) unsigned NULL   ;",
			'CreditCardInfo_ExpirationYear' => "ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_ExpirationYear` int(10) unsigned NULL   ;",
			'CreditCardInfo_NameOnCard' => "ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_NameOnCard` varchar(41) NULL   ;",
			'CreditCardInfo_CreditCardAddress' => "ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_CreditCardAddress` varchar(41) NULL   ;",
			'CreditCardInfo_CreditCardPostalCode' => "ALTER TABLE  `qb_customer` ADD  `CreditCardInfo_CreditCardPostalCode` varchar(41) NULL   ;",
			'JobStatus' => "ALTER TABLE  `qb_customer` ADD  `JobStatus` varchar(40) NULL   ;",
			'JobStartDate' => "ALTER TABLE  `qb_customer` ADD  `JobStartDate` date NULL   ;",
			'JobProjectedEndDate' => "ALTER TABLE  `qb_customer` ADD  `JobProjectedEndDate` date NULL   ;",
			'JobEndDate' => "ALTER TABLE  `qb_customer` ADD  `JobEndDate` date NULL   ;",
			'JobDesc' => "ALTER TABLE  `qb_customer` ADD  `JobDesc` varchar(99) NULL   ;",
			'JobType_ListID' => "ALTER TABLE  `qb_customer` ADD  `JobType_ListID` varchar(40) NULL   ;",
			'JobType_FullName' => "ALTER TABLE  `qb_customer` ADD  `JobType_FullName` varchar(255) NULL   ;",
			'Notes' => "ALTER TABLE  `qb_customer` ADD  `Notes` text NULL   ;",
			'PriceLevel_ListID' => "ALTER TABLE  `qb_customer` ADD  `PriceLevel_ListID` varchar(40) NULL   ;",
			'PriceLevel_FullName' => "ALTER TABLE  `qb_customer` ADD  `PriceLevel_FullName` varchar(255) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setListid() - ListID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setName() - Name
//setFullname() - FullName
//setIsactive() - IsActive
//setParentListid() - Parent_ListID
//setParentFullname() - Parent_FullName
//setSublevel() - Sublevel
//setCompanyname() - CompanyName
//setSalutation() - Salutation
//setFirstname() - FirstName
//setMiddlename() - MiddleName
//setLastname() - LastName
//setBilladdressAddr1() - BillAddress_Addr1
//setBilladdressAddr2() - BillAddress_Addr2
//setBilladdressAddr3() - BillAddress_Addr3
//setBilladdressAddr4() - BillAddress_Addr4
//setBilladdressAddr5() - BillAddress_Addr5
//setBilladdressCity() - BillAddress_City
//setBilladdressState() - BillAddress_State
//setBilladdressPostalcode() - BillAddress_PostalCode
//setBilladdressCountry() - BillAddress_Country
//setBilladdressNote() - BillAddress_Note
//setBilladdressblockAddr1() - BillAddressBlock_Addr1
//setBilladdressblockAddr2() - BillAddressBlock_Addr2
//setBilladdressblockAddr3() - BillAddressBlock_Addr3
//setBilladdressblockAddr4() - BillAddressBlock_Addr4
//setBilladdressblockAddr5() - BillAddressBlock_Addr5
//setShipaddressAddr1() - ShipAddress_Addr1
//setShipaddressAddr2() - ShipAddress_Addr2
//setShipaddressAddr3() - ShipAddress_Addr3
//setShipaddressAddr4() - ShipAddress_Addr4
//setShipaddressAddr5() - ShipAddress_Addr5
//setShipaddressCity() - ShipAddress_City
//setShipaddressState() - ShipAddress_State
//setShipaddressPostalcode() - ShipAddress_PostalCode
//setShipaddressCountry() - ShipAddress_Country
//setShipaddressNote() - ShipAddress_Note
//setShipaddressblockAddr1() - ShipAddressBlock_Addr1
//setShipaddressblockAddr2() - ShipAddressBlock_Addr2
//setShipaddressblockAddr3() - ShipAddressBlock_Addr3
//setShipaddressblockAddr4() - ShipAddressBlock_Addr4
//setShipaddressblockAddr5() - ShipAddressBlock_Addr5
//setPhone() - Phone
//setAltphone() - AltPhone
//setFax() - Fax
//setEmail() - Email
//setAltemail() - AltEmail
//setContact() - Contact
//setAltcontact() - AltContact
//setCustomertypeListid() - CustomerType_ListID
//setCustomertypeFullname() - CustomerType_FullName
//setTermsListid() - Terms_ListID
//setTermsFullname() - Terms_FullName
//setSalesrepListid() - SalesRep_ListID
//setSalesrepFullname() - SalesRep_FullName
//setBalance() - Balance
//setTotalbalance() - TotalBalance
//setSalestaxcodeListid() - SalesTaxCode_ListID
//setSalestaxcodeFullname() - SalesTaxCode_FullName
//setItemsalestaxListid() - ItemSalesTax_ListID
//setItemsalestaxFullname() - ItemSalesTax_FullName
//setResalenumber() - ResaleNumber
//setAccountnumber() - AccountNumber
//setCreditlimit() - CreditLimit
//setPreferredpaymentmethodListid() - PreferredPaymentMethod_ListID
//setPreferredpaymentmethodFullname() - PreferredPaymentMethod_FullName
//setCreditcardinfoCreditcardnumber() - CreditCardInfo_CreditCardNumber
//setCreditcardinfoExpirationmonth() - CreditCardInfo_ExpirationMonth
//setCreditcardinfoExpirationyear() - CreditCardInfo_ExpirationYear
//setCreditcardinfoNameoncard() - CreditCardInfo_NameOnCard
//setCreditcardinfoCreditcardaddress() - CreditCardInfo_CreditCardAddress
//setCreditcardinfoCreditcardpostalcode() - CreditCardInfo_CreditCardPostalCode
//setJobstatus() - JobStatus
//setJobstartdate() - JobStartDate
//setJobprojectedenddate() - JobProjectedEndDate
//setJobenddate() - JobEndDate
//setJobdesc() - JobDesc
//setJobtypeListid() - JobType_ListID
//setJobtypeFullname() - JobType_FullName
//setNotes() - Notes
//setPricelevelListid() - PriceLevel_ListID
//setPricelevelFullname() - PriceLevel_FullName

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ListID() - ListID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Name() - Name
//set_FullName() - FullName
//set_IsActive() - IsActive
//set_Parent_ListID() - Parent_ListID
//set_Parent_FullName() - Parent_FullName
//set_Sublevel() - Sublevel
//set_CompanyName() - CompanyName
//set_Salutation() - Salutation
//set_FirstName() - FirstName
//set_MiddleName() - MiddleName
//set_LastName() - LastName
//set_BillAddress_Addr1() - BillAddress_Addr1
//set_BillAddress_Addr2() - BillAddress_Addr2
//set_BillAddress_Addr3() - BillAddress_Addr3
//set_BillAddress_Addr4() - BillAddress_Addr4
//set_BillAddress_Addr5() - BillAddress_Addr5
//set_BillAddress_City() - BillAddress_City
//set_BillAddress_State() - BillAddress_State
//set_BillAddress_PostalCode() - BillAddress_PostalCode
//set_BillAddress_Country() - BillAddress_Country
//set_BillAddress_Note() - BillAddress_Note
//set_BillAddressBlock_Addr1() - BillAddressBlock_Addr1
//set_BillAddressBlock_Addr2() - BillAddressBlock_Addr2
//set_BillAddressBlock_Addr3() - BillAddressBlock_Addr3
//set_BillAddressBlock_Addr4() - BillAddressBlock_Addr4
//set_BillAddressBlock_Addr5() - BillAddressBlock_Addr5
//set_ShipAddress_Addr1() - ShipAddress_Addr1
//set_ShipAddress_Addr2() - ShipAddress_Addr2
//set_ShipAddress_Addr3() - ShipAddress_Addr3
//set_ShipAddress_Addr4() - ShipAddress_Addr4
//set_ShipAddress_Addr5() - ShipAddress_Addr5
//set_ShipAddress_City() - ShipAddress_City
//set_ShipAddress_State() - ShipAddress_State
//set_ShipAddress_PostalCode() - ShipAddress_PostalCode
//set_ShipAddress_Country() - ShipAddress_Country
//set_ShipAddress_Note() - ShipAddress_Note
//set_ShipAddressBlock_Addr1() - ShipAddressBlock_Addr1
//set_ShipAddressBlock_Addr2() - ShipAddressBlock_Addr2
//set_ShipAddressBlock_Addr3() - ShipAddressBlock_Addr3
//set_ShipAddressBlock_Addr4() - ShipAddressBlock_Addr4
//set_ShipAddressBlock_Addr5() - ShipAddressBlock_Addr5
//set_Phone() - Phone
//set_AltPhone() - AltPhone
//set_Fax() - Fax
//set_Email() - Email
//set_AltEmail() - AltEmail
//set_Contact() - Contact
//set_AltContact() - AltContact
//set_CustomerType_ListID() - CustomerType_ListID
//set_CustomerType_FullName() - CustomerType_FullName
//set_Terms_ListID() - Terms_ListID
//set_Terms_FullName() - Terms_FullName
//set_SalesRep_ListID() - SalesRep_ListID
//set_SalesRep_FullName() - SalesRep_FullName
//set_Balance() - Balance
//set_TotalBalance() - TotalBalance
//set_SalesTaxCode_ListID() - SalesTaxCode_ListID
//set_SalesTaxCode_FullName() - SalesTaxCode_FullName
//set_ItemSalesTax_ListID() - ItemSalesTax_ListID
//set_ItemSalesTax_FullName() - ItemSalesTax_FullName
//set_ResaleNumber() - ResaleNumber
//set_AccountNumber() - AccountNumber
//set_CreditLimit() - CreditLimit
//set_PreferredPaymentMethod_ListID() - PreferredPaymentMethod_ListID
//set_PreferredPaymentMethod_FullName() - PreferredPaymentMethod_FullName
//set_CreditCardInfo_CreditCardNumber() - CreditCardInfo_CreditCardNumber
//set_CreditCardInfo_ExpirationMonth() - CreditCardInfo_ExpirationMonth
//set_CreditCardInfo_ExpirationYear() - CreditCardInfo_ExpirationYear
//set_CreditCardInfo_NameOnCard() - CreditCardInfo_NameOnCard
//set_CreditCardInfo_CreditCardAddress() - CreditCardInfo_CreditCardAddress
//set_CreditCardInfo_CreditCardPostalCode() - CreditCardInfo_CreditCardPostalCode
//set_JobStatus() - JobStatus
//set_JobStartDate() - JobStartDate
//set_JobProjectedEndDate() - JobProjectedEndDate
//set_JobEndDate() - JobEndDate
//set_JobDesc() - JobDesc
//set_JobType_ListID() - JobType_ListID
//set_JobType_FullName() - JobType_FullName
//set_Notes() - Notes
//set_PriceLevel_ListID() - PriceLevel_ListID
//set_PriceLevel_FullName() - PriceLevel_FullName

*/
/* End of file Qb_customer_model.php */
/* Location: ./application/models/Qb_customer_model.php */

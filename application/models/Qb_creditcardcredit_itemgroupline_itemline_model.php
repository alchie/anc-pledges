<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_creditcardcredit_itemgroupline_itemline_model Class
 *
 * Manipulates `qb_creditcardcredit_itemgroupline_itemline` table on database

CREATE TABLE `qb_creditcardcredit_itemgroupline_itemline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CreditCardCredit_TxnID` varchar(40) DEFAULT NULL,
  `CreditCardCredit_ItemGroupLine_TxnLineID` text,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `TxnLineID` varchar(40) DEFAULT NULL,
  `ItemRef` text,
  `Descrip` text,
  `Quantity` decimal(12,5) DEFAULT '0.00000',
  `UnitOfMeasure` text,
  `OverrideUOMSetRef` text,
  `Cost` decimal(13,5) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `CustomerRef` text,
  `ClassRef` text,
  `BillableStatus` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `CreditCardCredit_TxnID` (`CreditCardCredit_TxnID`),
  KEY `TxnLineID` (`TxnLineID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `CreditCardCredit_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `CreditCardCredit_ItemGroupLine_TxnLineID` text NULL   ;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `TxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `ItemRef` text NULL   ;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `Descrip` text NULL   ;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `UnitOfMeasure` text NULL   ;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `OverrideUOMSetRef` text NULL   ;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `Cost` decimal(13,5) NULL   ;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `Amount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `CustomerRef` text NULL   ;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `ClassRef` text NULL   ;
ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `BillableStatus` varchar(40) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_creditcardcredit_itemgroupline_itemline_model extends MY_Model {

	protected $qbxml_id;
	protected $CreditCardCredit_TxnID;
	protected $CreditCardCredit_ItemGroupLine_TxnLineID;
	protected $SortOrder;
	protected $TxnLineID;
	protected $ItemRef;
	protected $Descrip;
	protected $Quantity;
	protected $UnitOfMeasure;
	protected $OverrideUOMSetRef;
	protected $Cost;
	protected $Amount;
	protected $CustomerRef;
	protected $ClassRef;
	protected $BillableStatus;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_creditcardcredit_itemgroupline_itemline';
		$this->_short_name = 'qb_creditcardcredit_itemgroupline_itemline';
		$this->_fields = array("qbxml_id","CreditCardCredit_TxnID","CreditCardCredit_ItemGroupLine_TxnLineID","SortOrder","TxnLineID","ItemRef","Descrip","Quantity","UnitOfMeasure","OverrideUOMSetRef","Cost","Amount","CustomerRef","ClassRef","BillableStatus");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: CreditCardCredit_TxnID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardCredit_TxnID` variable
	* @access public
	*/

	public function setCreditcardcreditTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardCredit_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardCredit_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardCredit_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardCredit_TxnID` variable
	* @access public
	*/

	public function getCreditcardcreditTxnid() {
		return $this->CreditCardCredit_TxnID;
	}

	public function get_CreditCardCredit_TxnID() {
		return $this->CreditCardCredit_TxnID;
	}

	
// ------------------------------ End Field: CreditCardCredit_TxnID --------------------------------------


// ---------------------------- Start Field: CreditCardCredit_ItemGroupLine_TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardCredit_ItemGroupLine_TxnLineID` variable
	* @access public
	*/

	public function setCreditcardcreditItemgrouplineTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardCredit_ItemGroupLine_TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardCredit_ItemGroupLine_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardCredit_ItemGroupLine_TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardCredit_ItemGroupLine_TxnLineID` variable
	* @access public
	*/

	public function getCreditcardcreditItemgrouplineTxnlineid() {
		return $this->CreditCardCredit_ItemGroupLine_TxnLineID;
	}

	public function get_CreditCardCredit_ItemGroupLine_TxnLineID() {
		return $this->CreditCardCredit_ItemGroupLine_TxnLineID;
	}

	
// ------------------------------ End Field: CreditCardCredit_ItemGroupLine_TxnLineID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `TxnLineID` variable
	* @access public
	*/

	public function setTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnLineID` variable
	* @access public
	*/

	public function getTxnlineid() {
		return $this->TxnLineID;
	}

	public function get_TxnLineID() {
		return $this->TxnLineID;
	}

	
// ------------------------------ End Field: TxnLineID --------------------------------------


// ---------------------------- Start Field: ItemRef -------------------------------------- 

	/** 
	* Sets a value to `ItemRef` variable
	* @access public
	*/

	public function setItemref($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemRef', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemRef($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemRef', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemRef` variable
	* @access public
	*/

	public function getItemref() {
		return $this->ItemRef;
	}

	public function get_ItemRef() {
		return $this->ItemRef;
	}

	
// ------------------------------ End Field: ItemRef --------------------------------------


// ---------------------------- Start Field: Descrip -------------------------------------- 

	/** 
	* Sets a value to `Descrip` variable
	* @access public
	*/

	public function setDescrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Descrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Descrip` variable
	* @access public
	*/

	public function getDescrip() {
		return $this->Descrip;
	}

	public function get_Descrip() {
		return $this->Descrip;
	}

	
// ------------------------------ End Field: Descrip --------------------------------------


// ---------------------------- Start Field: Quantity -------------------------------------- 

	/** 
	* Sets a value to `Quantity` variable
	* @access public
	*/

	public function setQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Quantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Quantity` variable
	* @access public
	*/

	public function getQuantity() {
		return $this->Quantity;
	}

	public function get_Quantity() {
		return $this->Quantity;
	}

	
// ------------------------------ End Field: Quantity --------------------------------------


// ---------------------------- Start Field: UnitOfMeasure -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasure` variable
	* @access public
	*/

	public function setUnitofmeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasure` variable
	* @access public
	*/

	public function getUnitofmeasure() {
		return $this->UnitOfMeasure;
	}

	public function get_UnitOfMeasure() {
		return $this->UnitOfMeasure;
	}

	
// ------------------------------ End Field: UnitOfMeasure --------------------------------------


// ---------------------------- Start Field: OverrideUOMSetRef -------------------------------------- 

	/** 
	* Sets a value to `OverrideUOMSetRef` variable
	* @access public
	*/

	public function setOverrideuomsetref($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSetRef', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OverrideUOMSetRef($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSetRef', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OverrideUOMSetRef` variable
	* @access public
	*/

	public function getOverrideuomsetref() {
		return $this->OverrideUOMSetRef;
	}

	public function get_OverrideUOMSetRef() {
		return $this->OverrideUOMSetRef;
	}

	
// ------------------------------ End Field: OverrideUOMSetRef --------------------------------------


// ---------------------------- Start Field: Cost -------------------------------------- 

	/** 
	* Sets a value to `Cost` variable
	* @access public
	*/

	public function setCost($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Cost', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Cost($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Cost', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Cost` variable
	* @access public
	*/

	public function getCost() {
		return $this->Cost;
	}

	public function get_Cost() {
		return $this->Cost;
	}

	
// ------------------------------ End Field: Cost --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------


// ---------------------------- Start Field: CustomerRef -------------------------------------- 

	/** 
	* Sets a value to `CustomerRef` variable
	* @access public
	*/

	public function setCustomerref($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerRef', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomerRef($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerRef', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomerRef` variable
	* @access public
	*/

	public function getCustomerref() {
		return $this->CustomerRef;
	}

	public function get_CustomerRef() {
		return $this->CustomerRef;
	}

	
// ------------------------------ End Field: CustomerRef --------------------------------------


// ---------------------------- Start Field: ClassRef -------------------------------------- 

	/** 
	* Sets a value to `ClassRef` variable
	* @access public
	*/

	public function setClassref($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ClassRef', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ClassRef($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ClassRef', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ClassRef` variable
	* @access public
	*/

	public function getClassref() {
		return $this->ClassRef;
	}

	public function get_ClassRef() {
		return $this->ClassRef;
	}

	
// ------------------------------ End Field: ClassRef --------------------------------------


// ---------------------------- Start Field: BillableStatus -------------------------------------- 

	/** 
	* Sets a value to `BillableStatus` variable
	* @access public
	*/

	public function setBillablestatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillableStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillableStatus` variable
	* @access public
	*/

	public function getBillablestatus() {
		return $this->BillableStatus;
	}

	public function get_BillableStatus() {
		return $this->BillableStatus;
	}

	
// ------------------------------ End Field: BillableStatus --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'CreditCardCredit_TxnID' => (object) array(
										'Field'=>'CreditCardCredit_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardCredit_ItemGroupLine_TxnLineID' => (object) array(
										'Field'=>'CreditCardCredit_ItemGroupLine_TxnLineID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnLineID' => (object) array(
										'Field'=>'TxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemRef' => (object) array(
										'Field'=>'ItemRef',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Descrip' => (object) array(
										'Field'=>'Descrip',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Quantity' => (object) array(
										'Field'=>'Quantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'UnitOfMeasure' => (object) array(
										'Field'=>'UnitOfMeasure',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'OverrideUOMSetRef' => (object) array(
										'Field'=>'OverrideUOMSetRef',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Cost' => (object) array(
										'Field'=>'Cost',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomerRef' => (object) array(
										'Field'=>'CustomerRef',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ClassRef' => (object) array(
										'Field'=>'ClassRef',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillableStatus' => (object) array(
										'Field'=>'BillableStatus',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'CreditCardCredit_TxnID' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `CreditCardCredit_TxnID` varchar(40) NULL   ;",
			'CreditCardCredit_ItemGroupLine_TxnLineID' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `CreditCardCredit_ItemGroupLine_TxnLineID` text NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'TxnLineID' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `TxnLineID` varchar(40) NULL   ;",
			'ItemRef' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `ItemRef` text NULL   ;",
			'Descrip' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `Descrip` text NULL   ;",
			'Quantity' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';",
			'UnitOfMeasure' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `UnitOfMeasure` text NULL   ;",
			'OverrideUOMSetRef' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `OverrideUOMSetRef` text NULL   ;",
			'Cost' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `Cost` decimal(13,5) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `Amount` decimal(10,2) NULL   ;",
			'CustomerRef' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `CustomerRef` text NULL   ;",
			'ClassRef' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `ClassRef` text NULL   ;",
			'BillableStatus' => "ALTER TABLE  `qb_creditcardcredit_itemgroupline_itemline` ADD  `BillableStatus` varchar(40) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setCreditcardcreditTxnid() - CreditCardCredit_TxnID
//setCreditcardcreditItemgrouplineTxnlineid() - CreditCardCredit_ItemGroupLine_TxnLineID
//setSortorder() - SortOrder
//setTxnlineid() - TxnLineID
//setItemref() - ItemRef
//setDescrip() - Descrip
//setQuantity() - Quantity
//setUnitofmeasure() - UnitOfMeasure
//setOverrideuomsetref() - OverrideUOMSetRef
//setCost() - Cost
//setAmount() - Amount
//setCustomerref() - CustomerRef
//setClassref() - ClassRef
//setBillablestatus() - BillableStatus

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_CreditCardCredit_TxnID() - CreditCardCredit_TxnID
//set_CreditCardCredit_ItemGroupLine_TxnLineID() - CreditCardCredit_ItemGroupLine_TxnLineID
//set_SortOrder() - SortOrder
//set_TxnLineID() - TxnLineID
//set_ItemRef() - ItemRef
//set_Descrip() - Descrip
//set_Quantity() - Quantity
//set_UnitOfMeasure() - UnitOfMeasure
//set_OverrideUOMSetRef() - OverrideUOMSetRef
//set_Cost() - Cost
//set_Amount() - Amount
//set_CustomerRef() - CustomerRef
//set_ClassRef() - ClassRef
//set_BillableStatus() - BillableStatus

*/
/* End of file Qb_creditcardcredit_itemgroupline_itemline_model.php */
/* Location: ./application/models/Qb_creditcardcredit_itemgroupline_itemline_model.php */

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Pledgers_options_model Class
 *
 * Manipulates `pledgers_options` table on database

CREATE TABLE `pledgers_options` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `controller` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  `option_key` varchar(200) NOT NULL,
  `option_value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `pledgers_options` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `pledgers_options` ADD  `controller` varchar(50) NOT NULL   ;
ALTER TABLE  `pledgers_options` ADD  `method` varchar(50) NOT NULL   ;
ALTER TABLE  `pledgers_options` ADD  `option_key` varchar(200) NOT NULL   ;
ALTER TABLE  `pledgers_options` ADD  `option_value` text NOT NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Pledgers_options_model extends MY_Model {

	protected $id;
	protected $controller;
	protected $method;
	protected $option_key;
	protected $option_value;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'pledgers_options';
		$this->_short_name = 'pledgers_options';
		$this->_fields = array("id","controller","method","option_key","option_value");
		$this->_required = array("controller","method","option_key","option_value");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: controller -------------------------------------- 

	/** 
	* Sets a value to `controller` variable
	* @access public
	*/

	public function setController($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('controller', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_controller($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('controller', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `controller` variable
	* @access public
	*/

	public function getController() {
		return $this->controller;
	}

	public function get_controller() {
		return $this->controller;
	}

	
// ------------------------------ End Field: controller --------------------------------------


// ---------------------------- Start Field: method -------------------------------------- 

	/** 
	* Sets a value to `method` variable
	* @access public
	*/

	public function setMethod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('method', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_method($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('method', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `method` variable
	* @access public
	*/

	public function getMethod() {
		return $this->method;
	}

	public function get_method() {
		return $this->method;
	}

	
// ------------------------------ End Field: method --------------------------------------


// ---------------------------- Start Field: option_key -------------------------------------- 

	/** 
	* Sets a value to `option_key` variable
	* @access public
	*/

	public function setOptionKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('option_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_option_key($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('option_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `option_key` variable
	* @access public
	*/

	public function getOptionKey() {
		return $this->option_key;
	}

	public function get_option_key() {
		return $this->option_key;
	}

	
// ------------------------------ End Field: option_key --------------------------------------


// ---------------------------- Start Field: option_value -------------------------------------- 

	/** 
	* Sets a value to `option_value` variable
	* @access public
	*/

	public function setOptionValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('option_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_option_value($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('option_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `option_value` variable
	* @access public
	*/

	public function getOptionValue() {
		return $this->option_value;
	}

	public function get_option_value() {
		return $this->option_value;
	}

	
// ------------------------------ End Field: option_value --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'controller' => (object) array(
										'Field'=>'controller',
										'Type'=>'varchar(50)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'method' => (object) array(
										'Field'=>'method',
										'Type'=>'varchar(50)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'option_key' => (object) array(
										'Field'=>'option_key',
										'Type'=>'varchar(200)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'option_value' => (object) array(
										'Field'=>'option_value',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `pledgers_options` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'controller' => "ALTER TABLE  `pledgers_options` ADD  `controller` varchar(50) NOT NULL   ;",
			'method' => "ALTER TABLE  `pledgers_options` ADD  `method` varchar(50) NOT NULL   ;",
			'option_key' => "ALTER TABLE  `pledgers_options` ADD  `option_key` varchar(200) NOT NULL   ;",
			'option_value' => "ALTER TABLE  `pledgers_options` ADD  `option_value` text NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setController() - controller
//setMethod() - method
//setOptionKey() - option_key
//setOptionValue() - option_value

--------------------------------------

//set_id() - id
//set_controller() - controller
//set_method() - method
//set_option_key() - option_key
//set_option_value() - option_value

*/
/* End of file Pledgers_options_model.php */
/* Location: ./application/models/Pledgers_options_model.php */

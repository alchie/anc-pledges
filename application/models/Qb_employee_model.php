<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_employee_model Class
 *
 * Manipulates `qb_employee` table on database

CREATE TABLE `qb_employee` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ListID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Name` text,
  `IsActive` tinyint(1) DEFAULT '0',
  `Salutation` varchar(15) DEFAULT NULL,
  `FirstName` varchar(25) DEFAULT NULL,
  `MiddleName` varchar(5) DEFAULT NULL,
  `LastName` varchar(25) DEFAULT NULL,
  `EmployeeAddress_Addr1` varchar(41) DEFAULT NULL,
  `EmployeeAddress_Addr2` varchar(41) DEFAULT NULL,
  `EmployeeAddress_City` varchar(31) DEFAULT NULL,
  `EmployeeAddress_State` varchar(21) DEFAULT NULL,
  `EmployeeAddress_PostalCode` varchar(13) DEFAULT NULL,
  `PrintAs` varchar(41) DEFAULT NULL,
  `Phone` varchar(21) DEFAULT NULL,
  `Mobile` varchar(21) DEFAULT NULL,
  `Pager` varchar(21) DEFAULT NULL,
  `PagerPIN` varchar(10) DEFAULT NULL,
  `AltPhone` varchar(21) DEFAULT NULL,
  `Fax` varchar(21) DEFAULT NULL,
  `SSN` varchar(15) DEFAULT NULL,
  `Email` text,
  `EmployeeType` varchar(40) DEFAULT NULL,
  `Gender` varchar(40) DEFAULT NULL,
  `HiredDate` date DEFAULT NULL,
  `ReleasedDate` date DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `AccountNumber` varchar(99) DEFAULT NULL,
  `Notes` text,
  `BillingRate_ListID` varchar(40) DEFAULT NULL,
  `BillingRate_FullName` varchar(255) DEFAULT NULL,
  `EmployeePayrollInfo_PayPeriod` varchar(40) DEFAULT NULL,
  `EmployeePayrollInfo_Class_ListID` varchar(40) DEFAULT NULL,
  `EmployeePayrollInfo_Class_FullName` varchar(255) DEFAULT NULL,
  `EmployeePayrollInfo_ClearEarnings` tinyint(1) DEFAULT NULL,
  `EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks` tinyint(1) DEFAULT NULL,
  `EmployeePayrollInfo_UseTimeDataToCreatePaychecks` varchar(40) DEFAULT NULL,
  `EmployeePayrollInfo_SickHours_HoursAvailable` text,
  `EmployeePayrollInfo_SickHours_AccrualPeriod` varchar(40) DEFAULT NULL,
  `EmployeePayrollInfo_SickHours_HoursAccrued` text,
  `EmployeePayrollInfo_SickHours_MaximumHours` text,
  `EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear` tinyint(1) DEFAULT NULL,
  `EmployeePayrollInfo_SickHours_HoursUsed` text,
  `EmployeePayrollInfo_SickHours_AccrualStartDate` date DEFAULT NULL,
  `EmployeePayrollInfo_VacationHours_HoursAvailable` text,
  `EmployeePayrollInfo_VacationHours_AccrualPeriod` varchar(40) DEFAULT NULL,
  `EmployeePayrollInfo_VacationHours_HoursAccrued` text,
  `EmployeePayrollInfo_VacationHours_MaximumHours` text,
  `EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear` tinyint(1) DEFAULT NULL,
  `EmployeePayrollInfo_VacationHours_HoursUsed` text,
  `EmployeePayrollInfo_VacationHours_AccrualStartDate` date DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `IsActive` (`IsActive`),
  KEY `LastName` (`LastName`),
  KEY `BillingRate_ListID` (`BillingRate_ListID`),
  KEY `EmployeePayrollInfo_Class_ListID` (`EmployeePayrollInfo_Class_ListID`),
  KEY `ListID` (`ListID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_employee` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_employee` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_employee` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_employee` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_employee` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `Name` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_employee` ADD  `Salutation` varchar(15) NULL   ;
ALTER TABLE  `qb_employee` ADD  `FirstName` varchar(25) NULL   ;
ALTER TABLE  `qb_employee` ADD  `MiddleName` varchar(5) NULL   ;
ALTER TABLE  `qb_employee` ADD  `LastName` varchar(25) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeeAddress_Addr1` varchar(41) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeeAddress_Addr2` varchar(41) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeeAddress_City` varchar(31) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeeAddress_State` varchar(21) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeeAddress_PostalCode` varchar(13) NULL   ;
ALTER TABLE  `qb_employee` ADD  `PrintAs` varchar(41) NULL   ;
ALTER TABLE  `qb_employee` ADD  `Phone` varchar(21) NULL   ;
ALTER TABLE  `qb_employee` ADD  `Mobile` varchar(21) NULL   ;
ALTER TABLE  `qb_employee` ADD  `Pager` varchar(21) NULL   ;
ALTER TABLE  `qb_employee` ADD  `PagerPIN` varchar(10) NULL   ;
ALTER TABLE  `qb_employee` ADD  `AltPhone` varchar(21) NULL   ;
ALTER TABLE  `qb_employee` ADD  `Fax` varchar(21) NULL   ;
ALTER TABLE  `qb_employee` ADD  `SSN` varchar(15) NULL   ;
ALTER TABLE  `qb_employee` ADD  `Email` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeeType` varchar(40) NULL   ;
ALTER TABLE  `qb_employee` ADD  `Gender` varchar(40) NULL   ;
ALTER TABLE  `qb_employee` ADD  `HiredDate` date NULL   ;
ALTER TABLE  `qb_employee` ADD  `ReleasedDate` date NULL   ;
ALTER TABLE  `qb_employee` ADD  `BirthDate` date NULL   ;
ALTER TABLE  `qb_employee` ADD  `AccountNumber` varchar(99) NULL   ;
ALTER TABLE  `qb_employee` ADD  `Notes` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `BillingRate_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_employee` ADD  `BillingRate_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_PayPeriod` varchar(40) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_ClearEarnings` tinyint(1) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks` tinyint(1) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_UseTimeDataToCreatePaychecks` varchar(40) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_HoursAvailable` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_AccrualPeriod` varchar(40) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_HoursAccrued` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_MaximumHours` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear` tinyint(1) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_HoursUsed` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_AccrualStartDate` date NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_HoursAvailable` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_AccrualPeriod` varchar(40) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_HoursAccrued` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_MaximumHours` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear` tinyint(1) NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_HoursUsed` text NULL   ;
ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_AccrualStartDate` date NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_employee_model extends MY_Model {

	protected $qbxml_id;
	protected $ListID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Name;
	protected $IsActive;
	protected $Salutation;
	protected $FirstName;
	protected $MiddleName;
	protected $LastName;
	protected $EmployeeAddress_Addr1;
	protected $EmployeeAddress_Addr2;
	protected $EmployeeAddress_City;
	protected $EmployeeAddress_State;
	protected $EmployeeAddress_PostalCode;
	protected $PrintAs;
	protected $Phone;
	protected $Mobile;
	protected $Pager;
	protected $PagerPIN;
	protected $AltPhone;
	protected $Fax;
	protected $SSN;
	protected $Email;
	protected $EmployeeType;
	protected $Gender;
	protected $HiredDate;
	protected $ReleasedDate;
	protected $BirthDate;
	protected $AccountNumber;
	protected $Notes;
	protected $BillingRate_ListID;
	protected $BillingRate_FullName;
	protected $EmployeePayrollInfo_PayPeriod;
	protected $EmployeePayrollInfo_Class_ListID;
	protected $EmployeePayrollInfo_Class_FullName;
	protected $EmployeePayrollInfo_ClearEarnings;
	protected $EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks;
	protected $EmployeePayrollInfo_UseTimeDataToCreatePaychecks;
	protected $EmployeePayrollInfo_SickHours_HoursAvailable;
	protected $EmployeePayrollInfo_SickHours_AccrualPeriod;
	protected $EmployeePayrollInfo_SickHours_HoursAccrued;
	protected $EmployeePayrollInfo_SickHours_MaximumHours;
	protected $EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear;
	protected $EmployeePayrollInfo_SickHours_HoursUsed;
	protected $EmployeePayrollInfo_SickHours_AccrualStartDate;
	protected $EmployeePayrollInfo_VacationHours_HoursAvailable;
	protected $EmployeePayrollInfo_VacationHours_AccrualPeriod;
	protected $EmployeePayrollInfo_VacationHours_HoursAccrued;
	protected $EmployeePayrollInfo_VacationHours_MaximumHours;
	protected $EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear;
	protected $EmployeePayrollInfo_VacationHours_HoursUsed;
	protected $EmployeePayrollInfo_VacationHours_AccrualStartDate;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_employee';
		$this->_short_name = 'qb_employee';
		$this->_fields = array("qbxml_id","ListID","TimeCreated","TimeModified","EditSequence","Name","IsActive","Salutation","FirstName","MiddleName","LastName","EmployeeAddress_Addr1","EmployeeAddress_Addr2","EmployeeAddress_City","EmployeeAddress_State","EmployeeAddress_PostalCode","PrintAs","Phone","Mobile","Pager","PagerPIN","AltPhone","Fax","SSN","Email","EmployeeType","Gender","HiredDate","ReleasedDate","BirthDate","AccountNumber","Notes","BillingRate_ListID","BillingRate_FullName","EmployeePayrollInfo_PayPeriod","EmployeePayrollInfo_Class_ListID","EmployeePayrollInfo_Class_FullName","EmployeePayrollInfo_ClearEarnings","EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks","EmployeePayrollInfo_UseTimeDataToCreatePaychecks","EmployeePayrollInfo_SickHours_HoursAvailable","EmployeePayrollInfo_SickHours_AccrualPeriod","EmployeePayrollInfo_SickHours_HoursAccrued","EmployeePayrollInfo_SickHours_MaximumHours","EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear","EmployeePayrollInfo_SickHours_HoursUsed","EmployeePayrollInfo_SickHours_AccrualStartDate","EmployeePayrollInfo_VacationHours_HoursAvailable","EmployeePayrollInfo_VacationHours_AccrualPeriod","EmployeePayrollInfo_VacationHours_HoursAccrued","EmployeePayrollInfo_VacationHours_MaximumHours","EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear","EmployeePayrollInfo_VacationHours_HoursUsed","EmployeePayrollInfo_VacationHours_AccrualStartDate");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: IsActive -------------------------------------- 

	/** 
	* Sets a value to `IsActive` variable
	* @access public
	*/

	public function setIsactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsActive` variable
	* @access public
	*/

	public function getIsactive() {
		return $this->IsActive;
	}

	public function get_IsActive() {
		return $this->IsActive;
	}

	
// ------------------------------ End Field: IsActive --------------------------------------


// ---------------------------- Start Field: Salutation -------------------------------------- 

	/** 
	* Sets a value to `Salutation` variable
	* @access public
	*/

	public function setSalutation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Salutation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Salutation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Salutation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Salutation` variable
	* @access public
	*/

	public function getSalutation() {
		return $this->Salutation;
	}

	public function get_Salutation() {
		return $this->Salutation;
	}

	
// ------------------------------ End Field: Salutation --------------------------------------


// ---------------------------- Start Field: FirstName -------------------------------------- 

	/** 
	* Sets a value to `FirstName` variable
	* @access public
	*/

	public function setFirstname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FirstName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FirstName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FirstName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FirstName` variable
	* @access public
	*/

	public function getFirstname() {
		return $this->FirstName;
	}

	public function get_FirstName() {
		return $this->FirstName;
	}

	
// ------------------------------ End Field: FirstName --------------------------------------


// ---------------------------- Start Field: MiddleName -------------------------------------- 

	/** 
	* Sets a value to `MiddleName` variable
	* @access public
	*/

	public function setMiddlename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MiddleName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_MiddleName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MiddleName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `MiddleName` variable
	* @access public
	*/

	public function getMiddlename() {
		return $this->MiddleName;
	}

	public function get_MiddleName() {
		return $this->MiddleName;
	}

	
// ------------------------------ End Field: MiddleName --------------------------------------


// ---------------------------- Start Field: LastName -------------------------------------- 

	/** 
	* Sets a value to `LastName` variable
	* @access public
	*/

	public function setLastname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LastName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LastName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LastName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LastName` variable
	* @access public
	*/

	public function getLastname() {
		return $this->LastName;
	}

	public function get_LastName() {
		return $this->LastName;
	}

	
// ------------------------------ End Field: LastName --------------------------------------


// ---------------------------- Start Field: EmployeeAddress_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `EmployeeAddress_Addr1` variable
	* @access public
	*/

	public function setEmployeeaddressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeeAddress_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeeAddress_Addr1` variable
	* @access public
	*/

	public function getEmployeeaddressAddr1() {
		return $this->EmployeeAddress_Addr1;
	}

	public function get_EmployeeAddress_Addr1() {
		return $this->EmployeeAddress_Addr1;
	}

	
// ------------------------------ End Field: EmployeeAddress_Addr1 --------------------------------------


// ---------------------------- Start Field: EmployeeAddress_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `EmployeeAddress_Addr2` variable
	* @access public
	*/

	public function setEmployeeaddressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeeAddress_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeeAddress_Addr2` variable
	* @access public
	*/

	public function getEmployeeaddressAddr2() {
		return $this->EmployeeAddress_Addr2;
	}

	public function get_EmployeeAddress_Addr2() {
		return $this->EmployeeAddress_Addr2;
	}

	
// ------------------------------ End Field: EmployeeAddress_Addr2 --------------------------------------


// ---------------------------- Start Field: EmployeeAddress_City -------------------------------------- 

	/** 
	* Sets a value to `EmployeeAddress_City` variable
	* @access public
	*/

	public function setEmployeeaddressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeeAddress_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeeAddress_City` variable
	* @access public
	*/

	public function getEmployeeaddressCity() {
		return $this->EmployeeAddress_City;
	}

	public function get_EmployeeAddress_City() {
		return $this->EmployeeAddress_City;
	}

	
// ------------------------------ End Field: EmployeeAddress_City --------------------------------------


// ---------------------------- Start Field: EmployeeAddress_State -------------------------------------- 

	/** 
	* Sets a value to `EmployeeAddress_State` variable
	* @access public
	*/

	public function setEmployeeaddressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeeAddress_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeeAddress_State` variable
	* @access public
	*/

	public function getEmployeeaddressState() {
		return $this->EmployeeAddress_State;
	}

	public function get_EmployeeAddress_State() {
		return $this->EmployeeAddress_State;
	}

	
// ------------------------------ End Field: EmployeeAddress_State --------------------------------------


// ---------------------------- Start Field: EmployeeAddress_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `EmployeeAddress_PostalCode` variable
	* @access public
	*/

	public function setEmployeeaddressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeeAddress_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeeAddress_PostalCode` variable
	* @access public
	*/

	public function getEmployeeaddressPostalcode() {
		return $this->EmployeeAddress_PostalCode;
	}

	public function get_EmployeeAddress_PostalCode() {
		return $this->EmployeeAddress_PostalCode;
	}

	
// ------------------------------ End Field: EmployeeAddress_PostalCode --------------------------------------


// ---------------------------- Start Field: PrintAs -------------------------------------- 

	/** 
	* Sets a value to `PrintAs` variable
	* @access public
	*/

	public function setPrintas($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PrintAs', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PrintAs($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PrintAs', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PrintAs` variable
	* @access public
	*/

	public function getPrintas() {
		return $this->PrintAs;
	}

	public function get_PrintAs() {
		return $this->PrintAs;
	}

	
// ------------------------------ End Field: PrintAs --------------------------------------


// ---------------------------- Start Field: Phone -------------------------------------- 

	/** 
	* Sets a value to `Phone` variable
	* @access public
	*/

	public function setPhone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Phone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Phone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Phone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Phone` variable
	* @access public
	*/

	public function getPhone() {
		return $this->Phone;
	}

	public function get_Phone() {
		return $this->Phone;
	}

	
// ------------------------------ End Field: Phone --------------------------------------


// ---------------------------- Start Field: Mobile -------------------------------------- 

	/** 
	* Sets a value to `Mobile` variable
	* @access public
	*/

	public function setMobile($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Mobile', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Mobile($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Mobile', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Mobile` variable
	* @access public
	*/

	public function getMobile() {
		return $this->Mobile;
	}

	public function get_Mobile() {
		return $this->Mobile;
	}

	
// ------------------------------ End Field: Mobile --------------------------------------


// ---------------------------- Start Field: Pager -------------------------------------- 

	/** 
	* Sets a value to `Pager` variable
	* @access public
	*/

	public function setPager($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Pager', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Pager($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Pager', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Pager` variable
	* @access public
	*/

	public function getPager() {
		return $this->Pager;
	}

	public function get_Pager() {
		return $this->Pager;
	}

	
// ------------------------------ End Field: Pager --------------------------------------


// ---------------------------- Start Field: PagerPIN -------------------------------------- 

	/** 
	* Sets a value to `PagerPIN` variable
	* @access public
	*/

	public function setPagerpin($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PagerPIN', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PagerPIN($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PagerPIN', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PagerPIN` variable
	* @access public
	*/

	public function getPagerpin() {
		return $this->PagerPIN;
	}

	public function get_PagerPIN() {
		return $this->PagerPIN;
	}

	
// ------------------------------ End Field: PagerPIN --------------------------------------


// ---------------------------- Start Field: AltPhone -------------------------------------- 

	/** 
	* Sets a value to `AltPhone` variable
	* @access public
	*/

	public function setAltphone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltPhone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AltPhone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltPhone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AltPhone` variable
	* @access public
	*/

	public function getAltphone() {
		return $this->AltPhone;
	}

	public function get_AltPhone() {
		return $this->AltPhone;
	}

	
// ------------------------------ End Field: AltPhone --------------------------------------


// ---------------------------- Start Field: Fax -------------------------------------- 

	/** 
	* Sets a value to `Fax` variable
	* @access public
	*/

	public function setFax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Fax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Fax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Fax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Fax` variable
	* @access public
	*/

	public function getFax() {
		return $this->Fax;
	}

	public function get_Fax() {
		return $this->Fax;
	}

	
// ------------------------------ End Field: Fax --------------------------------------


// ---------------------------- Start Field: SSN -------------------------------------- 

	/** 
	* Sets a value to `SSN` variable
	* @access public
	*/

	public function setSsn($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SSN', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SSN($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SSN', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SSN` variable
	* @access public
	*/

	public function getSsn() {
		return $this->SSN;
	}

	public function get_SSN() {
		return $this->SSN;
	}

	
// ------------------------------ End Field: SSN --------------------------------------


// ---------------------------- Start Field: Email -------------------------------------- 

	/** 
	* Sets a value to `Email` variable
	* @access public
	*/

	public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Email($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Email` variable
	* @access public
	*/

	public function getEmail() {
		return $this->Email;
	}

	public function get_Email() {
		return $this->Email;
	}

	
// ------------------------------ End Field: Email --------------------------------------


// ---------------------------- Start Field: EmployeeType -------------------------------------- 

	/** 
	* Sets a value to `EmployeeType` variable
	* @access public
	*/

	public function setEmployeetype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeeType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeeType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeeType` variable
	* @access public
	*/

	public function getEmployeetype() {
		return $this->EmployeeType;
	}

	public function get_EmployeeType() {
		return $this->EmployeeType;
	}

	
// ------------------------------ End Field: EmployeeType --------------------------------------


// ---------------------------- Start Field: Gender -------------------------------------- 

	/** 
	* Sets a value to `Gender` variable
	* @access public
	*/

	public function setGender($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Gender', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Gender($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Gender', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Gender` variable
	* @access public
	*/

	public function getGender() {
		return $this->Gender;
	}

	public function get_Gender() {
		return $this->Gender;
	}

	
// ------------------------------ End Field: Gender --------------------------------------


// ---------------------------- Start Field: HiredDate -------------------------------------- 

	/** 
	* Sets a value to `HiredDate` variable
	* @access public
	*/

	public function setHireddate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('HiredDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_HiredDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('HiredDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `HiredDate` variable
	* @access public
	*/

	public function getHireddate() {
		return $this->HiredDate;
	}

	public function get_HiredDate() {
		return $this->HiredDate;
	}

	
// ------------------------------ End Field: HiredDate --------------------------------------


// ---------------------------- Start Field: ReleasedDate -------------------------------------- 

	/** 
	* Sets a value to `ReleasedDate` variable
	* @access public
	*/

	public function setReleaseddate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ReleasedDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ReleasedDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ReleasedDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ReleasedDate` variable
	* @access public
	*/

	public function getReleaseddate() {
		return $this->ReleasedDate;
	}

	public function get_ReleasedDate() {
		return $this->ReleasedDate;
	}

	
// ------------------------------ End Field: ReleasedDate --------------------------------------


// ---------------------------- Start Field: BirthDate -------------------------------------- 

	/** 
	* Sets a value to `BirthDate` variable
	* @access public
	*/

	public function setBirthdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BirthDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BirthDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BirthDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BirthDate` variable
	* @access public
	*/

	public function getBirthdate() {
		return $this->BirthDate;
	}

	public function get_BirthDate() {
		return $this->BirthDate;
	}

	
// ------------------------------ End Field: BirthDate --------------------------------------


// ---------------------------- Start Field: AccountNumber -------------------------------------- 

	/** 
	* Sets a value to `AccountNumber` variable
	* @access public
	*/

	public function setAccountnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AccountNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AccountNumber` variable
	* @access public
	*/

	public function getAccountnumber() {
		return $this->AccountNumber;
	}

	public function get_AccountNumber() {
		return $this->AccountNumber;
	}

	
// ------------------------------ End Field: AccountNumber --------------------------------------


// ---------------------------- Start Field: Notes -------------------------------------- 

	/** 
	* Sets a value to `Notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Notes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->Notes;
	}

	public function get_Notes() {
		return $this->Notes;
	}

	
// ------------------------------ End Field: Notes --------------------------------------


// ---------------------------- Start Field: BillingRate_ListID -------------------------------------- 

	/** 
	* Sets a value to `BillingRate_ListID` variable
	* @access public
	*/

	public function setBillingrateListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillingRate_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillingRate_ListID` variable
	* @access public
	*/

	public function getBillingrateListid() {
		return $this->BillingRate_ListID;
	}

	public function get_BillingRate_ListID() {
		return $this->BillingRate_ListID;
	}

	
// ------------------------------ End Field: BillingRate_ListID --------------------------------------


// ---------------------------- Start Field: BillingRate_FullName -------------------------------------- 

	/** 
	* Sets a value to `BillingRate_FullName` variable
	* @access public
	*/

	public function setBillingrateFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillingRate_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillingRate_FullName` variable
	* @access public
	*/

	public function getBillingrateFullname() {
		return $this->BillingRate_FullName;
	}

	public function get_BillingRate_FullName() {
		return $this->BillingRate_FullName;
	}

	
// ------------------------------ End Field: BillingRate_FullName --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_PayPeriod -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_PayPeriod` variable
	* @access public
	*/

	public function setEmployeepayrollinfoPayperiod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_PayPeriod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_PayPeriod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_PayPeriod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_PayPeriod` variable
	* @access public
	*/

	public function getEmployeepayrollinfoPayperiod() {
		return $this->EmployeePayrollInfo_PayPeriod;
	}

	public function get_EmployeePayrollInfo_PayPeriod() {
		return $this->EmployeePayrollInfo_PayPeriod;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_PayPeriod --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_Class_ListID` variable
	* @access public
	*/

	public function setEmployeepayrollinfoClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_Class_ListID` variable
	* @access public
	*/

	public function getEmployeepayrollinfoClassListid() {
		return $this->EmployeePayrollInfo_Class_ListID;
	}

	public function get_EmployeePayrollInfo_Class_ListID() {
		return $this->EmployeePayrollInfo_Class_ListID;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_Class_ListID --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_Class_FullName` variable
	* @access public
	*/

	public function setEmployeepayrollinfoClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_Class_FullName` variable
	* @access public
	*/

	public function getEmployeepayrollinfoClassFullname() {
		return $this->EmployeePayrollInfo_Class_FullName;
	}

	public function get_EmployeePayrollInfo_Class_FullName() {
		return $this->EmployeePayrollInfo_Class_FullName;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_Class_FullName --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_ClearEarnings -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_ClearEarnings` variable
	* @access public
	*/

	public function setEmployeepayrollinfoClearearnings($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_ClearEarnings', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_ClearEarnings($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_ClearEarnings', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_ClearEarnings` variable
	* @access public
	*/

	public function getEmployeepayrollinfoClearearnings() {
		return $this->EmployeePayrollInfo_ClearEarnings;
	}

	public function get_EmployeePayrollInfo_ClearEarnings() {
		return $this->EmployeePayrollInfo_ClearEarnings;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_ClearEarnings --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks` variable
	* @access public
	*/

	public function setEmployeepayrollinfoIsusingtimedatatocreatepaychecks($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks` variable
	* @access public
	*/

	public function getEmployeepayrollinfoIsusingtimedatatocreatepaychecks() {
		return $this->EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks;
	}

	public function get_EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks() {
		return $this->EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_UseTimeDataToCreatePaychecks -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_UseTimeDataToCreatePaychecks` variable
	* @access public
	*/

	public function setEmployeepayrollinfoUsetimedatatocreatepaychecks($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_UseTimeDataToCreatePaychecks', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_UseTimeDataToCreatePaychecks($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_UseTimeDataToCreatePaychecks', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_UseTimeDataToCreatePaychecks` variable
	* @access public
	*/

	public function getEmployeepayrollinfoUsetimedatatocreatepaychecks() {
		return $this->EmployeePayrollInfo_UseTimeDataToCreatePaychecks;
	}

	public function get_EmployeePayrollInfo_UseTimeDataToCreatePaychecks() {
		return $this->EmployeePayrollInfo_UseTimeDataToCreatePaychecks;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_UseTimeDataToCreatePaychecks --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_SickHours_HoursAvailable -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_SickHours_HoursAvailable` variable
	* @access public
	*/

	public function setEmployeepayrollinfoSickhoursHoursavailable($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_HoursAvailable', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_SickHours_HoursAvailable($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_HoursAvailable', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_SickHours_HoursAvailable` variable
	* @access public
	*/

	public function getEmployeepayrollinfoSickhoursHoursavailable() {
		return $this->EmployeePayrollInfo_SickHours_HoursAvailable;
	}

	public function get_EmployeePayrollInfo_SickHours_HoursAvailable() {
		return $this->EmployeePayrollInfo_SickHours_HoursAvailable;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_SickHours_HoursAvailable --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_SickHours_AccrualPeriod -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_SickHours_AccrualPeriod` variable
	* @access public
	*/

	public function setEmployeepayrollinfoSickhoursAccrualperiod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_AccrualPeriod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_SickHours_AccrualPeriod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_AccrualPeriod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_SickHours_AccrualPeriod` variable
	* @access public
	*/

	public function getEmployeepayrollinfoSickhoursAccrualperiod() {
		return $this->EmployeePayrollInfo_SickHours_AccrualPeriod;
	}

	public function get_EmployeePayrollInfo_SickHours_AccrualPeriod() {
		return $this->EmployeePayrollInfo_SickHours_AccrualPeriod;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_SickHours_AccrualPeriod --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_SickHours_HoursAccrued -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_SickHours_HoursAccrued` variable
	* @access public
	*/

	public function setEmployeepayrollinfoSickhoursHoursaccrued($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_HoursAccrued', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_SickHours_HoursAccrued($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_HoursAccrued', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_SickHours_HoursAccrued` variable
	* @access public
	*/

	public function getEmployeepayrollinfoSickhoursHoursaccrued() {
		return $this->EmployeePayrollInfo_SickHours_HoursAccrued;
	}

	public function get_EmployeePayrollInfo_SickHours_HoursAccrued() {
		return $this->EmployeePayrollInfo_SickHours_HoursAccrued;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_SickHours_HoursAccrued --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_SickHours_MaximumHours -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_SickHours_MaximumHours` variable
	* @access public
	*/

	public function setEmployeepayrollinfoSickhoursMaximumhours($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_MaximumHours', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_SickHours_MaximumHours($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_MaximumHours', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_SickHours_MaximumHours` variable
	* @access public
	*/

	public function getEmployeepayrollinfoSickhoursMaximumhours() {
		return $this->EmployeePayrollInfo_SickHours_MaximumHours;
	}

	public function get_EmployeePayrollInfo_SickHours_MaximumHours() {
		return $this->EmployeePayrollInfo_SickHours_MaximumHours;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_SickHours_MaximumHours --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear` variable
	* @access public
	*/

	public function setEmployeepayrollinfoSickhoursIsresettinghourseachnewyear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear` variable
	* @access public
	*/

	public function getEmployeepayrollinfoSickhoursIsresettinghourseachnewyear() {
		return $this->EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear;
	}

	public function get_EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear() {
		return $this->EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_SickHours_HoursUsed -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_SickHours_HoursUsed` variable
	* @access public
	*/

	public function setEmployeepayrollinfoSickhoursHoursused($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_HoursUsed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_SickHours_HoursUsed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_HoursUsed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_SickHours_HoursUsed` variable
	* @access public
	*/

	public function getEmployeepayrollinfoSickhoursHoursused() {
		return $this->EmployeePayrollInfo_SickHours_HoursUsed;
	}

	public function get_EmployeePayrollInfo_SickHours_HoursUsed() {
		return $this->EmployeePayrollInfo_SickHours_HoursUsed;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_SickHours_HoursUsed --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_SickHours_AccrualStartDate -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_SickHours_AccrualStartDate` variable
	* @access public
	*/

	public function setEmployeepayrollinfoSickhoursAccrualstartdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_AccrualStartDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_SickHours_AccrualStartDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_SickHours_AccrualStartDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_SickHours_AccrualStartDate` variable
	* @access public
	*/

	public function getEmployeepayrollinfoSickhoursAccrualstartdate() {
		return $this->EmployeePayrollInfo_SickHours_AccrualStartDate;
	}

	public function get_EmployeePayrollInfo_SickHours_AccrualStartDate() {
		return $this->EmployeePayrollInfo_SickHours_AccrualStartDate;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_SickHours_AccrualStartDate --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_VacationHours_HoursAvailable -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_VacationHours_HoursAvailable` variable
	* @access public
	*/

	public function setEmployeepayrollinfoVacationhoursHoursavailable($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_HoursAvailable', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_VacationHours_HoursAvailable($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_HoursAvailable', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_VacationHours_HoursAvailable` variable
	* @access public
	*/

	public function getEmployeepayrollinfoVacationhoursHoursavailable() {
		return $this->EmployeePayrollInfo_VacationHours_HoursAvailable;
	}

	public function get_EmployeePayrollInfo_VacationHours_HoursAvailable() {
		return $this->EmployeePayrollInfo_VacationHours_HoursAvailable;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_VacationHours_HoursAvailable --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_VacationHours_AccrualPeriod -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_VacationHours_AccrualPeriod` variable
	* @access public
	*/

	public function setEmployeepayrollinfoVacationhoursAccrualperiod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_AccrualPeriod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_VacationHours_AccrualPeriod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_AccrualPeriod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_VacationHours_AccrualPeriod` variable
	* @access public
	*/

	public function getEmployeepayrollinfoVacationhoursAccrualperiod() {
		return $this->EmployeePayrollInfo_VacationHours_AccrualPeriod;
	}

	public function get_EmployeePayrollInfo_VacationHours_AccrualPeriod() {
		return $this->EmployeePayrollInfo_VacationHours_AccrualPeriod;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_VacationHours_AccrualPeriod --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_VacationHours_HoursAccrued -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_VacationHours_HoursAccrued` variable
	* @access public
	*/

	public function setEmployeepayrollinfoVacationhoursHoursaccrued($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_HoursAccrued', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_VacationHours_HoursAccrued($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_HoursAccrued', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_VacationHours_HoursAccrued` variable
	* @access public
	*/

	public function getEmployeepayrollinfoVacationhoursHoursaccrued() {
		return $this->EmployeePayrollInfo_VacationHours_HoursAccrued;
	}

	public function get_EmployeePayrollInfo_VacationHours_HoursAccrued() {
		return $this->EmployeePayrollInfo_VacationHours_HoursAccrued;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_VacationHours_HoursAccrued --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_VacationHours_MaximumHours -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_VacationHours_MaximumHours` variable
	* @access public
	*/

	public function setEmployeepayrollinfoVacationhoursMaximumhours($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_MaximumHours', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_VacationHours_MaximumHours($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_MaximumHours', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_VacationHours_MaximumHours` variable
	* @access public
	*/

	public function getEmployeepayrollinfoVacationhoursMaximumhours() {
		return $this->EmployeePayrollInfo_VacationHours_MaximumHours;
	}

	public function get_EmployeePayrollInfo_VacationHours_MaximumHours() {
		return $this->EmployeePayrollInfo_VacationHours_MaximumHours;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_VacationHours_MaximumHours --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear` variable
	* @access public
	*/

	public function setEmployeepayrollinfoVacationhoursIsresettinghourseachnewyear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear` variable
	* @access public
	*/

	public function getEmployeepayrollinfoVacationhoursIsresettinghourseachnewyear() {
		return $this->EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear;
	}

	public function get_EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear() {
		return $this->EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_VacationHours_HoursUsed -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_VacationHours_HoursUsed` variable
	* @access public
	*/

	public function setEmployeepayrollinfoVacationhoursHoursused($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_HoursUsed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_VacationHours_HoursUsed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_HoursUsed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_VacationHours_HoursUsed` variable
	* @access public
	*/

	public function getEmployeepayrollinfoVacationhoursHoursused() {
		return $this->EmployeePayrollInfo_VacationHours_HoursUsed;
	}

	public function get_EmployeePayrollInfo_VacationHours_HoursUsed() {
		return $this->EmployeePayrollInfo_VacationHours_HoursUsed;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_VacationHours_HoursUsed --------------------------------------


// ---------------------------- Start Field: EmployeePayrollInfo_VacationHours_AccrualStartDate -------------------------------------- 

	/** 
	* Sets a value to `EmployeePayrollInfo_VacationHours_AccrualStartDate` variable
	* @access public
	*/

	public function setEmployeepayrollinfoVacationhoursAccrualstartdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_AccrualStartDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EmployeePayrollInfo_VacationHours_AccrualStartDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EmployeePayrollInfo_VacationHours_AccrualStartDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EmployeePayrollInfo_VacationHours_AccrualStartDate` variable
	* @access public
	*/

	public function getEmployeepayrollinfoVacationhoursAccrualstartdate() {
		return $this->EmployeePayrollInfo_VacationHours_AccrualStartDate;
	}

	public function get_EmployeePayrollInfo_VacationHours_AccrualStartDate() {
		return $this->EmployeePayrollInfo_VacationHours_AccrualStartDate;
	}

	
// ------------------------------ End Field: EmployeePayrollInfo_VacationHours_AccrualStartDate --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsActive' => (object) array(
										'Field'=>'IsActive',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'Salutation' => (object) array(
										'Field'=>'Salutation',
										'Type'=>'varchar(15)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FirstName' => (object) array(
										'Field'=>'FirstName',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'MiddleName' => (object) array(
										'Field'=>'MiddleName',
										'Type'=>'varchar(5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LastName' => (object) array(
										'Field'=>'LastName',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeeAddress_Addr1' => (object) array(
										'Field'=>'EmployeeAddress_Addr1',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeeAddress_Addr2' => (object) array(
										'Field'=>'EmployeeAddress_Addr2',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeeAddress_City' => (object) array(
										'Field'=>'EmployeeAddress_City',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeeAddress_State' => (object) array(
										'Field'=>'EmployeeAddress_State',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeeAddress_PostalCode' => (object) array(
										'Field'=>'EmployeeAddress_PostalCode',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PrintAs' => (object) array(
										'Field'=>'PrintAs',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Phone' => (object) array(
										'Field'=>'Phone',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Mobile' => (object) array(
										'Field'=>'Mobile',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Pager' => (object) array(
										'Field'=>'Pager',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PagerPIN' => (object) array(
										'Field'=>'PagerPIN',
										'Type'=>'varchar(10)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AltPhone' => (object) array(
										'Field'=>'AltPhone',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Fax' => (object) array(
										'Field'=>'Fax',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SSN' => (object) array(
										'Field'=>'SSN',
										'Type'=>'varchar(15)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Email' => (object) array(
										'Field'=>'Email',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeeType' => (object) array(
										'Field'=>'EmployeeType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Gender' => (object) array(
										'Field'=>'Gender',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'HiredDate' => (object) array(
										'Field'=>'HiredDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ReleasedDate' => (object) array(
										'Field'=>'ReleasedDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BirthDate' => (object) array(
										'Field'=>'BirthDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AccountNumber' => (object) array(
										'Field'=>'AccountNumber',
										'Type'=>'varchar(99)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Notes' => (object) array(
										'Field'=>'Notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillingRate_ListID' => (object) array(
										'Field'=>'BillingRate_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BillingRate_FullName' => (object) array(
										'Field'=>'BillingRate_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_PayPeriod' => (object) array(
										'Field'=>'EmployeePayrollInfo_PayPeriod',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_Class_ListID' => (object) array(
										'Field'=>'EmployeePayrollInfo_Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_Class_FullName' => (object) array(
										'Field'=>'EmployeePayrollInfo_Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_ClearEarnings' => (object) array(
										'Field'=>'EmployeePayrollInfo_ClearEarnings',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks' => (object) array(
										'Field'=>'EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_UseTimeDataToCreatePaychecks' => (object) array(
										'Field'=>'EmployeePayrollInfo_UseTimeDataToCreatePaychecks',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_SickHours_HoursAvailable' => (object) array(
										'Field'=>'EmployeePayrollInfo_SickHours_HoursAvailable',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_SickHours_AccrualPeriod' => (object) array(
										'Field'=>'EmployeePayrollInfo_SickHours_AccrualPeriod',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_SickHours_HoursAccrued' => (object) array(
										'Field'=>'EmployeePayrollInfo_SickHours_HoursAccrued',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_SickHours_MaximumHours' => (object) array(
										'Field'=>'EmployeePayrollInfo_SickHours_MaximumHours',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear' => (object) array(
										'Field'=>'EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_SickHours_HoursUsed' => (object) array(
										'Field'=>'EmployeePayrollInfo_SickHours_HoursUsed',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_SickHours_AccrualStartDate' => (object) array(
										'Field'=>'EmployeePayrollInfo_SickHours_AccrualStartDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_VacationHours_HoursAvailable' => (object) array(
										'Field'=>'EmployeePayrollInfo_VacationHours_HoursAvailable',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_VacationHours_AccrualPeriod' => (object) array(
										'Field'=>'EmployeePayrollInfo_VacationHours_AccrualPeriod',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_VacationHours_HoursAccrued' => (object) array(
										'Field'=>'EmployeePayrollInfo_VacationHours_HoursAccrued',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_VacationHours_MaximumHours' => (object) array(
										'Field'=>'EmployeePayrollInfo_VacationHours_MaximumHours',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear' => (object) array(
										'Field'=>'EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_VacationHours_HoursUsed' => (object) array(
										'Field'=>'EmployeePayrollInfo_VacationHours_HoursUsed',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EmployeePayrollInfo_VacationHours_AccrualStartDate' => (object) array(
										'Field'=>'EmployeePayrollInfo_VacationHours_AccrualStartDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_employee` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ListID' => "ALTER TABLE  `qb_employee` ADD  `ListID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_employee` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_employee` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_employee` ADD  `EditSequence` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_employee` ADD  `Name` text NULL   ;",
			'IsActive' => "ALTER TABLE  `qb_employee` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';",
			'Salutation' => "ALTER TABLE  `qb_employee` ADD  `Salutation` varchar(15) NULL   ;",
			'FirstName' => "ALTER TABLE  `qb_employee` ADD  `FirstName` varchar(25) NULL   ;",
			'MiddleName' => "ALTER TABLE  `qb_employee` ADD  `MiddleName` varchar(5) NULL   ;",
			'LastName' => "ALTER TABLE  `qb_employee` ADD  `LastName` varchar(25) NULL   ;",
			'EmployeeAddress_Addr1' => "ALTER TABLE  `qb_employee` ADD  `EmployeeAddress_Addr1` varchar(41) NULL   ;",
			'EmployeeAddress_Addr2' => "ALTER TABLE  `qb_employee` ADD  `EmployeeAddress_Addr2` varchar(41) NULL   ;",
			'EmployeeAddress_City' => "ALTER TABLE  `qb_employee` ADD  `EmployeeAddress_City` varchar(31) NULL   ;",
			'EmployeeAddress_State' => "ALTER TABLE  `qb_employee` ADD  `EmployeeAddress_State` varchar(21) NULL   ;",
			'EmployeeAddress_PostalCode' => "ALTER TABLE  `qb_employee` ADD  `EmployeeAddress_PostalCode` varchar(13) NULL   ;",
			'PrintAs' => "ALTER TABLE  `qb_employee` ADD  `PrintAs` varchar(41) NULL   ;",
			'Phone' => "ALTER TABLE  `qb_employee` ADD  `Phone` varchar(21) NULL   ;",
			'Mobile' => "ALTER TABLE  `qb_employee` ADD  `Mobile` varchar(21) NULL   ;",
			'Pager' => "ALTER TABLE  `qb_employee` ADD  `Pager` varchar(21) NULL   ;",
			'PagerPIN' => "ALTER TABLE  `qb_employee` ADD  `PagerPIN` varchar(10) NULL   ;",
			'AltPhone' => "ALTER TABLE  `qb_employee` ADD  `AltPhone` varchar(21) NULL   ;",
			'Fax' => "ALTER TABLE  `qb_employee` ADD  `Fax` varchar(21) NULL   ;",
			'SSN' => "ALTER TABLE  `qb_employee` ADD  `SSN` varchar(15) NULL   ;",
			'Email' => "ALTER TABLE  `qb_employee` ADD  `Email` text NULL   ;",
			'EmployeeType' => "ALTER TABLE  `qb_employee` ADD  `EmployeeType` varchar(40) NULL   ;",
			'Gender' => "ALTER TABLE  `qb_employee` ADD  `Gender` varchar(40) NULL   ;",
			'HiredDate' => "ALTER TABLE  `qb_employee` ADD  `HiredDate` date NULL   ;",
			'ReleasedDate' => "ALTER TABLE  `qb_employee` ADD  `ReleasedDate` date NULL   ;",
			'BirthDate' => "ALTER TABLE  `qb_employee` ADD  `BirthDate` date NULL   ;",
			'AccountNumber' => "ALTER TABLE  `qb_employee` ADD  `AccountNumber` varchar(99) NULL   ;",
			'Notes' => "ALTER TABLE  `qb_employee` ADD  `Notes` text NULL   ;",
			'BillingRate_ListID' => "ALTER TABLE  `qb_employee` ADD  `BillingRate_ListID` varchar(40) NULL   ;",
			'BillingRate_FullName' => "ALTER TABLE  `qb_employee` ADD  `BillingRate_FullName` varchar(255) NULL   ;",
			'EmployeePayrollInfo_PayPeriod' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_PayPeriod` varchar(40) NULL   ;",
			'EmployeePayrollInfo_Class_ListID' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_Class_ListID` varchar(40) NULL   ;",
			'EmployeePayrollInfo_Class_FullName' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_Class_FullName` varchar(255) NULL   ;",
			'EmployeePayrollInfo_ClearEarnings' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_ClearEarnings` tinyint(1) NULL   ;",
			'EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks` tinyint(1) NULL   ;",
			'EmployeePayrollInfo_UseTimeDataToCreatePaychecks' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_UseTimeDataToCreatePaychecks` varchar(40) NULL   ;",
			'EmployeePayrollInfo_SickHours_HoursAvailable' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_HoursAvailable` text NULL   ;",
			'EmployeePayrollInfo_SickHours_AccrualPeriod' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_AccrualPeriod` varchar(40) NULL   ;",
			'EmployeePayrollInfo_SickHours_HoursAccrued' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_HoursAccrued` text NULL   ;",
			'EmployeePayrollInfo_SickHours_MaximumHours' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_MaximumHours` text NULL   ;",
			'EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear` tinyint(1) NULL   ;",
			'EmployeePayrollInfo_SickHours_HoursUsed' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_HoursUsed` text NULL   ;",
			'EmployeePayrollInfo_SickHours_AccrualStartDate' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_SickHours_AccrualStartDate` date NULL   ;",
			'EmployeePayrollInfo_VacationHours_HoursAvailable' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_HoursAvailable` text NULL   ;",
			'EmployeePayrollInfo_VacationHours_AccrualPeriod' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_AccrualPeriod` varchar(40) NULL   ;",
			'EmployeePayrollInfo_VacationHours_HoursAccrued' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_HoursAccrued` text NULL   ;",
			'EmployeePayrollInfo_VacationHours_MaximumHours' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_MaximumHours` text NULL   ;",
			'EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear` tinyint(1) NULL   ;",
			'EmployeePayrollInfo_VacationHours_HoursUsed' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_HoursUsed` text NULL   ;",
			'EmployeePayrollInfo_VacationHours_AccrualStartDate' => "ALTER TABLE  `qb_employee` ADD  `EmployeePayrollInfo_VacationHours_AccrualStartDate` date NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setListid() - ListID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setName() - Name
//setIsactive() - IsActive
//setSalutation() - Salutation
//setFirstname() - FirstName
//setMiddlename() - MiddleName
//setLastname() - LastName
//setEmployeeaddressAddr1() - EmployeeAddress_Addr1
//setEmployeeaddressAddr2() - EmployeeAddress_Addr2
//setEmployeeaddressCity() - EmployeeAddress_City
//setEmployeeaddressState() - EmployeeAddress_State
//setEmployeeaddressPostalcode() - EmployeeAddress_PostalCode
//setPrintas() - PrintAs
//setPhone() - Phone
//setMobile() - Mobile
//setPager() - Pager
//setPagerpin() - PagerPIN
//setAltphone() - AltPhone
//setFax() - Fax
//setSsn() - SSN
//setEmail() - Email
//setEmployeetype() - EmployeeType
//setGender() - Gender
//setHireddate() - HiredDate
//setReleaseddate() - ReleasedDate
//setBirthdate() - BirthDate
//setAccountnumber() - AccountNumber
//setNotes() - Notes
//setBillingrateListid() - BillingRate_ListID
//setBillingrateFullname() - BillingRate_FullName
//setEmployeepayrollinfoPayperiod() - EmployeePayrollInfo_PayPeriod
//setEmployeepayrollinfoClassListid() - EmployeePayrollInfo_Class_ListID
//setEmployeepayrollinfoClassFullname() - EmployeePayrollInfo_Class_FullName
//setEmployeepayrollinfoClearearnings() - EmployeePayrollInfo_ClearEarnings
//setEmployeepayrollinfoIsusingtimedatatocreatepaychecks() - EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks
//setEmployeepayrollinfoUsetimedatatocreatepaychecks() - EmployeePayrollInfo_UseTimeDataToCreatePaychecks
//setEmployeepayrollinfoSickhoursHoursavailable() - EmployeePayrollInfo_SickHours_HoursAvailable
//setEmployeepayrollinfoSickhoursAccrualperiod() - EmployeePayrollInfo_SickHours_AccrualPeriod
//setEmployeepayrollinfoSickhoursHoursaccrued() - EmployeePayrollInfo_SickHours_HoursAccrued
//setEmployeepayrollinfoSickhoursMaximumhours() - EmployeePayrollInfo_SickHours_MaximumHours
//setEmployeepayrollinfoSickhoursIsresettinghourseachnewyear() - EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear
//setEmployeepayrollinfoSickhoursHoursused() - EmployeePayrollInfo_SickHours_HoursUsed
//setEmployeepayrollinfoSickhoursAccrualstartdate() - EmployeePayrollInfo_SickHours_AccrualStartDate
//setEmployeepayrollinfoVacationhoursHoursavailable() - EmployeePayrollInfo_VacationHours_HoursAvailable
//setEmployeepayrollinfoVacationhoursAccrualperiod() - EmployeePayrollInfo_VacationHours_AccrualPeriod
//setEmployeepayrollinfoVacationhoursHoursaccrued() - EmployeePayrollInfo_VacationHours_HoursAccrued
//setEmployeepayrollinfoVacationhoursMaximumhours() - EmployeePayrollInfo_VacationHours_MaximumHours
//setEmployeepayrollinfoVacationhoursIsresettinghourseachnewyear() - EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear
//setEmployeepayrollinfoVacationhoursHoursused() - EmployeePayrollInfo_VacationHours_HoursUsed
//setEmployeepayrollinfoVacationhoursAccrualstartdate() - EmployeePayrollInfo_VacationHours_AccrualStartDate

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ListID() - ListID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Name() - Name
//set_IsActive() - IsActive
//set_Salutation() - Salutation
//set_FirstName() - FirstName
//set_MiddleName() - MiddleName
//set_LastName() - LastName
//set_EmployeeAddress_Addr1() - EmployeeAddress_Addr1
//set_EmployeeAddress_Addr2() - EmployeeAddress_Addr2
//set_EmployeeAddress_City() - EmployeeAddress_City
//set_EmployeeAddress_State() - EmployeeAddress_State
//set_EmployeeAddress_PostalCode() - EmployeeAddress_PostalCode
//set_PrintAs() - PrintAs
//set_Phone() - Phone
//set_Mobile() - Mobile
//set_Pager() - Pager
//set_PagerPIN() - PagerPIN
//set_AltPhone() - AltPhone
//set_Fax() - Fax
//set_SSN() - SSN
//set_Email() - Email
//set_EmployeeType() - EmployeeType
//set_Gender() - Gender
//set_HiredDate() - HiredDate
//set_ReleasedDate() - ReleasedDate
//set_BirthDate() - BirthDate
//set_AccountNumber() - AccountNumber
//set_Notes() - Notes
//set_BillingRate_ListID() - BillingRate_ListID
//set_BillingRate_FullName() - BillingRate_FullName
//set_EmployeePayrollInfo_PayPeriod() - EmployeePayrollInfo_PayPeriod
//set_EmployeePayrollInfo_Class_ListID() - EmployeePayrollInfo_Class_ListID
//set_EmployeePayrollInfo_Class_FullName() - EmployeePayrollInfo_Class_FullName
//set_EmployeePayrollInfo_ClearEarnings() - EmployeePayrollInfo_ClearEarnings
//set_EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks() - EmployeePayrollInfo_IsUsingTimeDataToCreatePaychecks
//set_EmployeePayrollInfo_UseTimeDataToCreatePaychecks() - EmployeePayrollInfo_UseTimeDataToCreatePaychecks
//set_EmployeePayrollInfo_SickHours_HoursAvailable() - EmployeePayrollInfo_SickHours_HoursAvailable
//set_EmployeePayrollInfo_SickHours_AccrualPeriod() - EmployeePayrollInfo_SickHours_AccrualPeriod
//set_EmployeePayrollInfo_SickHours_HoursAccrued() - EmployeePayrollInfo_SickHours_HoursAccrued
//set_EmployeePayrollInfo_SickHours_MaximumHours() - EmployeePayrollInfo_SickHours_MaximumHours
//set_EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear() - EmployeePayrollInfo_SickHours_IsResettingHoursEachNewYear
//set_EmployeePayrollInfo_SickHours_HoursUsed() - EmployeePayrollInfo_SickHours_HoursUsed
//set_EmployeePayrollInfo_SickHours_AccrualStartDate() - EmployeePayrollInfo_SickHours_AccrualStartDate
//set_EmployeePayrollInfo_VacationHours_HoursAvailable() - EmployeePayrollInfo_VacationHours_HoursAvailable
//set_EmployeePayrollInfo_VacationHours_AccrualPeriod() - EmployeePayrollInfo_VacationHours_AccrualPeriod
//set_EmployeePayrollInfo_VacationHours_HoursAccrued() - EmployeePayrollInfo_VacationHours_HoursAccrued
//set_EmployeePayrollInfo_VacationHours_MaximumHours() - EmployeePayrollInfo_VacationHours_MaximumHours
//set_EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear() - EmployeePayrollInfo_VacationHours_IsResettingHoursEachNewYear
//set_EmployeePayrollInfo_VacationHours_HoursUsed() - EmployeePayrollInfo_VacationHours_HoursUsed
//set_EmployeePayrollInfo_VacationHours_AccrualStartDate() - EmployeePayrollInfo_VacationHours_AccrualStartDate

*/
/* End of file Qb_employee_model.php */
/* Location: ./application/models/Qb_employee_model.php */

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_customer_dataext_model Class
 *
 * Manipulates `qb_customer_dataext` table on database

CREATE TABLE `qb_customer_dataext` (
  `qbxml_id` int(10) NOT NULL AUTO_INCREMENT,
  `OwnerID` int(10) DEFAULT '0',
  `DataExtName` varchar(200) DEFAULT NULL,
  `DataExtType` varchar(200) DEFAULT NULL,
  `DataExtValue` text,
  PRIMARY KEY (`qbxml_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_customer_dataext` ADD  `qbxml_id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_customer_dataext` ADD  `OwnerID` int(10) NULL   DEFAULT '0';
ALTER TABLE  `qb_customer_dataext` ADD  `DataExtName` varchar(200) NULL   ;
ALTER TABLE  `qb_customer_dataext` ADD  `DataExtType` varchar(200) NULL   ;
ALTER TABLE  `qb_customer_dataext` ADD  `DataExtValue` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_customer_dataext_model extends MY_Model {

	protected $qbxml_id;
	protected $OwnerID;
	protected $DataExtName;
	protected $DataExtType;
	protected $DataExtValue;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_customer_dataext';
		$this->_short_name = 'qb_customer_dataext';
		$this->_fields = array("qbxml_id","OwnerID","DataExtName","DataExtType","DataExtValue");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: OwnerID -------------------------------------- 

	/** 
	* Sets a value to `OwnerID` variable
	* @access public
	*/

	public function setOwnerid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OwnerID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OwnerID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OwnerID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OwnerID` variable
	* @access public
	*/

	public function getOwnerid() {
		return $this->OwnerID;
	}

	public function get_OwnerID() {
		return $this->OwnerID;
	}

	
// ------------------------------ End Field: OwnerID --------------------------------------


// ---------------------------- Start Field: DataExtName -------------------------------------- 

	/** 
	* Sets a value to `DataExtName` variable
	* @access public
	*/

	public function setDataextname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtName` variable
	* @access public
	*/

	public function getDataextname() {
		return $this->DataExtName;
	}

	public function get_DataExtName() {
		return $this->DataExtName;
	}

	
// ------------------------------ End Field: DataExtName --------------------------------------


// ---------------------------- Start Field: DataExtType -------------------------------------- 

	/** 
	* Sets a value to `DataExtType` variable
	* @access public
	*/

	public function setDataexttype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtType` variable
	* @access public
	*/

	public function getDataexttype() {
		return $this->DataExtType;
	}

	public function get_DataExtType() {
		return $this->DataExtType;
	}

	
// ------------------------------ End Field: DataExtType --------------------------------------


// ---------------------------- Start Field: DataExtValue -------------------------------------- 

	/** 
	* Sets a value to `DataExtValue` variable
	* @access public
	*/

	public function setDataextvalue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtValue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtValue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtValue` variable
	* @access public
	*/

	public function getDataextvalue() {
		return $this->DataExtValue;
	}

	public function get_DataExtValue() {
		return $this->DataExtValue;
	}

	
// ------------------------------ End Field: DataExtValue --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'OwnerID' => (object) array(
										'Field'=>'OwnerID',
										'Type'=>'int(10)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'DataExtName' => (object) array(
										'Field'=>'DataExtName',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtType' => (object) array(
										'Field'=>'DataExtType',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtValue' => (object) array(
										'Field'=>'DataExtValue',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_customer_dataext` ADD  `qbxml_id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'OwnerID' => "ALTER TABLE  `qb_customer_dataext` ADD  `OwnerID` int(10) NULL   DEFAULT '0';",
			'DataExtName' => "ALTER TABLE  `qb_customer_dataext` ADD  `DataExtName` varchar(200) NULL   ;",
			'DataExtType' => "ALTER TABLE  `qb_customer_dataext` ADD  `DataExtType` varchar(200) NULL   ;",
			'DataExtValue' => "ALTER TABLE  `qb_customer_dataext` ADD  `DataExtValue` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setOwnerid() - OwnerID
//setDataextname() - DataExtName
//setDataexttype() - DataExtType
//setDataextvalue() - DataExtValue

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_OwnerID() - OwnerID
//set_DataExtName() - DataExtName
//set_DataExtType() - DataExtType
//set_DataExtValue() - DataExtValue

*/
/* End of file Qb_customer_dataext_model.php */
/* Location: ./application/models/Qb_customer_dataext_model.php */

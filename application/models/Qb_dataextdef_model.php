<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_dataextdef_model Class
 *
 * Manipulates `qb_dataextdef` table on database

CREATE TABLE `qb_dataextdef` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OwnerID` text,
  `DataExtID` text,
  `DataExtName` text,
  `DataExtType` varchar(40) DEFAULT NULL,
  `DataExtListRequire` text,
  `DataExtTxnRequire` text,
  `DataExtFormatString` text,
  PRIMARY KEY (`qbxml_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_dataextdef` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_dataextdef` ADD  `OwnerID` text NULL   ;
ALTER TABLE  `qb_dataextdef` ADD  `DataExtID` text NULL   ;
ALTER TABLE  `qb_dataextdef` ADD  `DataExtName` text NULL   ;
ALTER TABLE  `qb_dataextdef` ADD  `DataExtType` varchar(40) NULL   ;
ALTER TABLE  `qb_dataextdef` ADD  `DataExtListRequire` text NULL   ;
ALTER TABLE  `qb_dataextdef` ADD  `DataExtTxnRequire` text NULL   ;
ALTER TABLE  `qb_dataextdef` ADD  `DataExtFormatString` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_dataextdef_model extends MY_Model {

	protected $qbxml_id;
	protected $OwnerID;
	protected $DataExtID;
	protected $DataExtName;
	protected $DataExtType;
	protected $DataExtListRequire;
	protected $DataExtTxnRequire;
	protected $DataExtFormatString;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_dataextdef';
		$this->_short_name = 'qb_dataextdef';
		$this->_fields = array("qbxml_id","OwnerID","DataExtID","DataExtName","DataExtType","DataExtListRequire","DataExtTxnRequire","DataExtFormatString");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: OwnerID -------------------------------------- 

	/** 
	* Sets a value to `OwnerID` variable
	* @access public
	*/

	public function setOwnerid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OwnerID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OwnerID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OwnerID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OwnerID` variable
	* @access public
	*/

	public function getOwnerid() {
		return $this->OwnerID;
	}

	public function get_OwnerID() {
		return $this->OwnerID;
	}

	
// ------------------------------ End Field: OwnerID --------------------------------------


// ---------------------------- Start Field: DataExtID -------------------------------------- 

	/** 
	* Sets a value to `DataExtID` variable
	* @access public
	*/

	public function setDataextid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtID` variable
	* @access public
	*/

	public function getDataextid() {
		return $this->DataExtID;
	}

	public function get_DataExtID() {
		return $this->DataExtID;
	}

	
// ------------------------------ End Field: DataExtID --------------------------------------


// ---------------------------- Start Field: DataExtName -------------------------------------- 

	/** 
	* Sets a value to `DataExtName` variable
	* @access public
	*/

	public function setDataextname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtName` variable
	* @access public
	*/

	public function getDataextname() {
		return $this->DataExtName;
	}

	public function get_DataExtName() {
		return $this->DataExtName;
	}

	
// ------------------------------ End Field: DataExtName --------------------------------------


// ---------------------------- Start Field: DataExtType -------------------------------------- 

	/** 
	* Sets a value to `DataExtType` variable
	* @access public
	*/

	public function setDataexttype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtType` variable
	* @access public
	*/

	public function getDataexttype() {
		return $this->DataExtType;
	}

	public function get_DataExtType() {
		return $this->DataExtType;
	}

	
// ------------------------------ End Field: DataExtType --------------------------------------


// ---------------------------- Start Field: DataExtListRequire -------------------------------------- 

	/** 
	* Sets a value to `DataExtListRequire` variable
	* @access public
	*/

	public function setDataextlistrequire($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtListRequire', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtListRequire($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtListRequire', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtListRequire` variable
	* @access public
	*/

	public function getDataextlistrequire() {
		return $this->DataExtListRequire;
	}

	public function get_DataExtListRequire() {
		return $this->DataExtListRequire;
	}

	
// ------------------------------ End Field: DataExtListRequire --------------------------------------


// ---------------------------- Start Field: DataExtTxnRequire -------------------------------------- 

	/** 
	* Sets a value to `DataExtTxnRequire` variable
	* @access public
	*/

	public function setDataexttxnrequire($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtTxnRequire', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtTxnRequire($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtTxnRequire', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtTxnRequire` variable
	* @access public
	*/

	public function getDataexttxnrequire() {
		return $this->DataExtTxnRequire;
	}

	public function get_DataExtTxnRequire() {
		return $this->DataExtTxnRequire;
	}

	
// ------------------------------ End Field: DataExtTxnRequire --------------------------------------


// ---------------------------- Start Field: DataExtFormatString -------------------------------------- 

	/** 
	* Sets a value to `DataExtFormatString` variable
	* @access public
	*/

	public function setDataextformatstring($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtFormatString', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtFormatString($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtFormatString', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtFormatString` variable
	* @access public
	*/

	public function getDataextformatstring() {
		return $this->DataExtFormatString;
	}

	public function get_DataExtFormatString() {
		return $this->DataExtFormatString;
	}

	
// ------------------------------ End Field: DataExtFormatString --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'OwnerID' => (object) array(
										'Field'=>'OwnerID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtID' => (object) array(
										'Field'=>'DataExtID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtName' => (object) array(
										'Field'=>'DataExtName',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtType' => (object) array(
										'Field'=>'DataExtType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtListRequire' => (object) array(
										'Field'=>'DataExtListRequire',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtTxnRequire' => (object) array(
										'Field'=>'DataExtTxnRequire',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtFormatString' => (object) array(
										'Field'=>'DataExtFormatString',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_dataextdef` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'OwnerID' => "ALTER TABLE  `qb_dataextdef` ADD  `OwnerID` text NULL   ;",
			'DataExtID' => "ALTER TABLE  `qb_dataextdef` ADD  `DataExtID` text NULL   ;",
			'DataExtName' => "ALTER TABLE  `qb_dataextdef` ADD  `DataExtName` text NULL   ;",
			'DataExtType' => "ALTER TABLE  `qb_dataextdef` ADD  `DataExtType` varchar(40) NULL   ;",
			'DataExtListRequire' => "ALTER TABLE  `qb_dataextdef` ADD  `DataExtListRequire` text NULL   ;",
			'DataExtTxnRequire' => "ALTER TABLE  `qb_dataextdef` ADD  `DataExtTxnRequire` text NULL   ;",
			'DataExtFormatString' => "ALTER TABLE  `qb_dataextdef` ADD  `DataExtFormatString` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setOwnerid() - OwnerID
//setDataextid() - DataExtID
//setDataextname() - DataExtName
//setDataexttype() - DataExtType
//setDataextlistrequire() - DataExtListRequire
//setDataexttxnrequire() - DataExtTxnRequire
//setDataextformatstring() - DataExtFormatString

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_OwnerID() - OwnerID
//set_DataExtID() - DataExtID
//set_DataExtName() - DataExtName
//set_DataExtType() - DataExtType
//set_DataExtListRequire() - DataExtListRequire
//set_DataExtTxnRequire() - DataExtTxnRequire
//set_DataExtFormatString() - DataExtFormatString

*/
/* End of file Qb_dataextdef_model.php */
/* Location: ./application/models/Qb_dataextdef_model.php */

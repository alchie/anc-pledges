<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_dataextdef_assigntoobject_model Class
 *
 * Manipulates `qb_dataextdef_assigntoobject` table on database

CREATE TABLE `qb_dataextdef_assigntoobject` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AssignToObject` varchar(40) DEFAULT NULL,
  `DataExtDef_OwnerID` varchar(40) DEFAULT NULL,
  `DataExtDef_DataExtName` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_dataextdef_assigntoobject` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_dataextdef_assigntoobject` ADD  `AssignToObject` varchar(40) NULL   ;
ALTER TABLE  `qb_dataextdef_assigntoobject` ADD  `DataExtDef_OwnerID` varchar(40) NULL   ;
ALTER TABLE  `qb_dataextdef_assigntoobject` ADD  `DataExtDef_DataExtName` varchar(40) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_dataextdef_assigntoobject_model extends MY_Model {

	protected $qbxml_id;
	protected $AssignToObject;
	protected $DataExtDef_OwnerID;
	protected $DataExtDef_DataExtName;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_dataextdef_assigntoobject';
		$this->_short_name = 'qb_dataextdef_assigntoobject';
		$this->_fields = array("qbxml_id","AssignToObject","DataExtDef_OwnerID","DataExtDef_DataExtName");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: AssignToObject -------------------------------------- 

	/** 
	* Sets a value to `AssignToObject` variable
	* @access public
	*/

	public function setAssigntoobject($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssignToObject', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AssignToObject($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssignToObject', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AssignToObject` variable
	* @access public
	*/

	public function getAssigntoobject() {
		return $this->AssignToObject;
	}

	public function get_AssignToObject() {
		return $this->AssignToObject;
	}

	
// ------------------------------ End Field: AssignToObject --------------------------------------


// ---------------------------- Start Field: DataExtDef_OwnerID -------------------------------------- 

	/** 
	* Sets a value to `DataExtDef_OwnerID` variable
	* @access public
	*/

	public function setDataextdefOwnerid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtDef_OwnerID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtDef_OwnerID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtDef_OwnerID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtDef_OwnerID` variable
	* @access public
	*/

	public function getDataextdefOwnerid() {
		return $this->DataExtDef_OwnerID;
	}

	public function get_DataExtDef_OwnerID() {
		return $this->DataExtDef_OwnerID;
	}

	
// ------------------------------ End Field: DataExtDef_OwnerID --------------------------------------


// ---------------------------- Start Field: DataExtDef_DataExtName -------------------------------------- 

	/** 
	* Sets a value to `DataExtDef_DataExtName` variable
	* @access public
	*/

	public function setDataextdefDataextname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtDef_DataExtName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtDef_DataExtName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtDef_DataExtName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtDef_DataExtName` variable
	* @access public
	*/

	public function getDataextdefDataextname() {
		return $this->DataExtDef_DataExtName;
	}

	public function get_DataExtDef_DataExtName() {
		return $this->DataExtDef_DataExtName;
	}

	
// ------------------------------ End Field: DataExtDef_DataExtName --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'AssignToObject' => (object) array(
										'Field'=>'AssignToObject',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtDef_OwnerID' => (object) array(
										'Field'=>'DataExtDef_OwnerID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtDef_DataExtName' => (object) array(
										'Field'=>'DataExtDef_DataExtName',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_dataextdef_assigntoobject` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'AssignToObject' => "ALTER TABLE  `qb_dataextdef_assigntoobject` ADD  `AssignToObject` varchar(40) NULL   ;",
			'DataExtDef_OwnerID' => "ALTER TABLE  `qb_dataextdef_assigntoobject` ADD  `DataExtDef_OwnerID` varchar(40) NULL   ;",
			'DataExtDef_DataExtName' => "ALTER TABLE  `qb_dataextdef_assigntoobject` ADD  `DataExtDef_DataExtName` varchar(40) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setAssigntoobject() - AssignToObject
//setDataextdefOwnerid() - DataExtDef_OwnerID
//setDataextdefDataextname() - DataExtDef_DataExtName

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_AssignToObject() - AssignToObject
//set_DataExtDef_OwnerID() - DataExtDef_OwnerID
//set_DataExtDef_DataExtName() - DataExtDef_DataExtName

*/
/* End of file Qb_dataextdef_assigntoobject_model.php */
/* Location: ./application/models/Qb_dataextdef_assigntoobject_model.php */

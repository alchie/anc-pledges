<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_buildassembly_model Class
 *
 * Manipulates `qb_buildassembly` table on database

CREATE TABLE `qb_buildassembly` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `ItemInventoryAssembly_ListID` varchar(40) DEFAULT NULL,
  `ItemInventoryAssembly_FullName` varchar(255) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `RefNumber` varchar(20) DEFAULT NULL,
  `Memo` text,
  `IsPending` tinyint(1) DEFAULT '0',
  `QuantityToBuild` decimal(10,5) DEFAULT NULL,
  `QuantityCanBuild` decimal(10,5) DEFAULT NULL,
  `QuantityOnHand` decimal(10,5) DEFAULT NULL,
  `QuantityOnSalesOrder` decimal(10,5) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `ItemInventoryAssembly_ListID` (`ItemInventoryAssembly_ListID`),
  KEY `TxnDate` (`TxnDate`),
  KEY `RefNumber` (`RefNumber`),
  KEY `IsPending` (`IsPending`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_buildassembly` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_buildassembly` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_buildassembly` ADD  `ItemInventoryAssembly_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `ItemInventoryAssembly_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `RefNumber` varchar(20) NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `Memo` text NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `IsPending` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_buildassembly` ADD  `QuantityToBuild` decimal(10,5) NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `QuantityCanBuild` decimal(10,5) NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `QuantityOnHand` decimal(10,5) NULL   ;
ALTER TABLE  `qb_buildassembly` ADD  `QuantityOnSalesOrder` decimal(10,5) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_buildassembly_model extends MY_Model {

	protected $qbxml_id;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $ItemInventoryAssembly_ListID;
	protected $ItemInventoryAssembly_FullName;
	protected $TxnDate;
	protected $RefNumber;
	protected $Memo;
	protected $IsPending;
	protected $QuantityToBuild;
	protected $QuantityCanBuild;
	protected $QuantityOnHand;
	protected $QuantityOnSalesOrder;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_buildassembly';
		$this->_short_name = 'qb_buildassembly';
		$this->_fields = array("qbxml_id","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","ItemInventoryAssembly_ListID","ItemInventoryAssembly_FullName","TxnDate","RefNumber","Memo","IsPending","QuantityToBuild","QuantityCanBuild","QuantityOnHand","QuantityOnSalesOrder");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: ItemInventoryAssembly_ListID -------------------------------------- 

	/** 
	* Sets a value to `ItemInventoryAssembly_ListID` variable
	* @access public
	*/

	public function setIteminventoryassemblyListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemInventoryAssembly_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemInventoryAssembly_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemInventoryAssembly_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemInventoryAssembly_ListID` variable
	* @access public
	*/

	public function getIteminventoryassemblyListid() {
		return $this->ItemInventoryAssembly_ListID;
	}

	public function get_ItemInventoryAssembly_ListID() {
		return $this->ItemInventoryAssembly_ListID;
	}

	
// ------------------------------ End Field: ItemInventoryAssembly_ListID --------------------------------------


// ---------------------------- Start Field: ItemInventoryAssembly_FullName -------------------------------------- 

	/** 
	* Sets a value to `ItemInventoryAssembly_FullName` variable
	* @access public
	*/

	public function setIteminventoryassemblyFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemInventoryAssembly_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemInventoryAssembly_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemInventoryAssembly_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemInventoryAssembly_FullName` variable
	* @access public
	*/

	public function getIteminventoryassemblyFullname() {
		return $this->ItemInventoryAssembly_FullName;
	}

	public function get_ItemInventoryAssembly_FullName() {
		return $this->ItemInventoryAssembly_FullName;
	}

	
// ------------------------------ End Field: ItemInventoryAssembly_FullName --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------


// ---------------------------- Start Field: IsPending -------------------------------------- 

	/** 
	* Sets a value to `IsPending` variable
	* @access public
	*/

	public function setIspending($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPending', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsPending($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPending', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsPending` variable
	* @access public
	*/

	public function getIspending() {
		return $this->IsPending;
	}

	public function get_IsPending() {
		return $this->IsPending;
	}

	
// ------------------------------ End Field: IsPending --------------------------------------


// ---------------------------- Start Field: QuantityToBuild -------------------------------------- 

	/** 
	* Sets a value to `QuantityToBuild` variable
	* @access public
	*/

	public function setQuantitytobuild($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityToBuild', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_QuantityToBuild($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityToBuild', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `QuantityToBuild` variable
	* @access public
	*/

	public function getQuantitytobuild() {
		return $this->QuantityToBuild;
	}

	public function get_QuantityToBuild() {
		return $this->QuantityToBuild;
	}

	
// ------------------------------ End Field: QuantityToBuild --------------------------------------


// ---------------------------- Start Field: QuantityCanBuild -------------------------------------- 

	/** 
	* Sets a value to `QuantityCanBuild` variable
	* @access public
	*/

	public function setQuantitycanbuild($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityCanBuild', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_QuantityCanBuild($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityCanBuild', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `QuantityCanBuild` variable
	* @access public
	*/

	public function getQuantitycanbuild() {
		return $this->QuantityCanBuild;
	}

	public function get_QuantityCanBuild() {
		return $this->QuantityCanBuild;
	}

	
// ------------------------------ End Field: QuantityCanBuild --------------------------------------


// ---------------------------- Start Field: QuantityOnHand -------------------------------------- 

	/** 
	* Sets a value to `QuantityOnHand` variable
	* @access public
	*/

	public function setQuantityonhand($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityOnHand', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_QuantityOnHand($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityOnHand', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `QuantityOnHand` variable
	* @access public
	*/

	public function getQuantityonhand() {
		return $this->QuantityOnHand;
	}

	public function get_QuantityOnHand() {
		return $this->QuantityOnHand;
	}

	
// ------------------------------ End Field: QuantityOnHand --------------------------------------


// ---------------------------- Start Field: QuantityOnSalesOrder -------------------------------------- 

	/** 
	* Sets a value to `QuantityOnSalesOrder` variable
	* @access public
	*/

	public function setQuantityonsalesorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityOnSalesOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_QuantityOnSalesOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityOnSalesOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `QuantityOnSalesOrder` variable
	* @access public
	*/

	public function getQuantityonsalesorder() {
		return $this->QuantityOnSalesOrder;
	}

	public function get_QuantityOnSalesOrder() {
		return $this->QuantityOnSalesOrder;
	}

	
// ------------------------------ End Field: QuantityOnSalesOrder --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'ItemInventoryAssembly_ListID' => (object) array(
										'Field'=>'ItemInventoryAssembly_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemInventoryAssembly_FullName' => (object) array(
										'Field'=>'ItemInventoryAssembly_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'varchar(20)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsPending' => (object) array(
										'Field'=>'IsPending',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'QuantityToBuild' => (object) array(
										'Field'=>'QuantityToBuild',
										'Type'=>'decimal(10,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'QuantityCanBuild' => (object) array(
										'Field'=>'QuantityCanBuild',
										'Type'=>'decimal(10,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'QuantityOnHand' => (object) array(
										'Field'=>'QuantityOnHand',
										'Type'=>'decimal(10,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'QuantityOnSalesOrder' => (object) array(
										'Field'=>'QuantityOnSalesOrder',
										'Type'=>'decimal(10,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_buildassembly` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'TxnID' => "ALTER TABLE  `qb_buildassembly` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_buildassembly` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_buildassembly` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_buildassembly` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_buildassembly` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'ItemInventoryAssembly_ListID' => "ALTER TABLE  `qb_buildassembly` ADD  `ItemInventoryAssembly_ListID` varchar(40) NULL   ;",
			'ItemInventoryAssembly_FullName' => "ALTER TABLE  `qb_buildassembly` ADD  `ItemInventoryAssembly_FullName` varchar(255) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_buildassembly` ADD  `TxnDate` date NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_buildassembly` ADD  `RefNumber` varchar(20) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_buildassembly` ADD  `Memo` text NULL   ;",
			'IsPending' => "ALTER TABLE  `qb_buildassembly` ADD  `IsPending` tinyint(1) NULL   DEFAULT '0';",
			'QuantityToBuild' => "ALTER TABLE  `qb_buildassembly` ADD  `QuantityToBuild` decimal(10,5) NULL   ;",
			'QuantityCanBuild' => "ALTER TABLE  `qb_buildassembly` ADD  `QuantityCanBuild` decimal(10,5) NULL   ;",
			'QuantityOnHand' => "ALTER TABLE  `qb_buildassembly` ADD  `QuantityOnHand` decimal(10,5) NULL   ;",
			'QuantityOnSalesOrder' => "ALTER TABLE  `qb_buildassembly` ADD  `QuantityOnSalesOrder` decimal(10,5) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setIteminventoryassemblyListid() - ItemInventoryAssembly_ListID
//setIteminventoryassemblyFullname() - ItemInventoryAssembly_FullName
//setTxndate() - TxnDate
//setRefnumber() - RefNumber
//setMemo() - Memo
//setIspending() - IsPending
//setQuantitytobuild() - QuantityToBuild
//setQuantitycanbuild() - QuantityCanBuild
//setQuantityonhand() - QuantityOnHand
//setQuantityonsalesorder() - QuantityOnSalesOrder

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_ItemInventoryAssembly_ListID() - ItemInventoryAssembly_ListID
//set_ItemInventoryAssembly_FullName() - ItemInventoryAssembly_FullName
//set_TxnDate() - TxnDate
//set_RefNumber() - RefNumber
//set_Memo() - Memo
//set_IsPending() - IsPending
//set_QuantityToBuild() - QuantityToBuild
//set_QuantityCanBuild() - QuantityCanBuild
//set_QuantityOnHand() - QuantityOnHand
//set_QuantityOnSalesOrder() - QuantityOnSalesOrder

*/
/* End of file Qb_buildassembly_model.php */
/* Location: ./application/models/Qb_buildassembly_model.php */

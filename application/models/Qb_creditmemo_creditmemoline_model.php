<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_creditmemo_creditmemoline_model Class
 *
 * Manipulates `qb_creditmemo_creditmemoline` table on database

CREATE TABLE `qb_creditmemo_creditmemoline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CreditMemo_TxnID` varchar(40) DEFAULT NULL,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `TxnLineID` varchar(40) DEFAULT NULL,
  `Item_ListID` varchar(40) DEFAULT NULL,
  `Item_FullName` varchar(255) DEFAULT NULL,
  `Descrip` text,
  `Quantity` decimal(12,5) DEFAULT '0.00000',
  `UnitOfMeasure` text,
  `OverrideUOMSet_ListID` varchar(40) DEFAULT NULL,
  `OverrideUOMSet_FullName` varchar(255) DEFAULT NULL,
  `Rate` decimal(13,5) DEFAULT NULL,
  `RatePercent` decimal(12,5) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `ServiceDate` date DEFAULT NULL,
  `SalesTaxCode_ListID` varchar(40) DEFAULT NULL,
  `SalesTaxCode_FullName` varchar(255) DEFAULT NULL,
  `Other1` text,
  `Other2` text,
  `CreditCardTxnInputInfo_CreditCardNumber` text,
  `CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInputInfo_NameOnCard` text,
  `CreditCardTxnInputInfo_CreditCardAddress` text,
  `CreditCardTxnInputInfo_CreditCardPostalCode` text,
  `CreditCardTxnInputInfo_CommercialCardCode` text,
  `CreditCardTxnInputInfo_TransactionMode` varchar(40) DEFAULT NULL,
  `CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) DEFAULT NULL,
  `CreditCardTxnResultInfo_ResultCode` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnResultInfo_ResultMessage` text,
  `CreditCardTxnResultInfo_CreditCardTransID` text,
  `CreditCardTxnResultInfo_MerchantAccountNumber` text,
  `CreditCardTxnResultInfo_AuthorizationCode` text,
  `CreditCardTxnResultInfo_AVSStreet` varchar(40) DEFAULT NULL,
  `CreditCardTxnResultInfo_AVSZip` varchar(40) DEFAULT NULL,
  `CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) DEFAULT NULL,
  `CreditCardTxnResultInfo_ReconBatchID` text,
  `CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnResultInfo_PaymentStatus` varchar(40) DEFAULT NULL,
  `CreditCardTxnResultInfo_TxnAuthorizationTime` datetime DEFAULT NULL,
  `CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnResultInfo_ClientTransID` text,
  PRIMARY KEY (`qbxml_id`),
  KEY `CreditMemo_TxnID` (`CreditMemo_TxnID`),
  KEY `TxnLineID` (`TxnLineID`),
  KEY `Item_ListID` (`Item_ListID`),
  KEY `OverrideUOMSet_ListID` (`OverrideUOMSet_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `SalesTaxCode_ListID` (`SalesTaxCode_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditMemo_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `TxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Item_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Item_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Descrip` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `UnitOfMeasure` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `OverrideUOMSet_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `OverrideUOMSet_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Rate` decimal(13,5) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `RatePercent` decimal(12,5) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Amount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `ServiceDate` date NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Other1` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Other2` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_CreditCardNumber` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_NameOnCard` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_CreditCardAddress` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_CreditCardPostalCode` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_CommercialCardCode` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_TransactionMode` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_ResultCode` int(10) unsigned NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_ResultMessage` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_CreditCardTransID` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_MerchantAccountNumber` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_AuthorizationCode` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_AVSStreet` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_AVSZip` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_ReconBatchID` text NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_PaymentStatus` varchar(40) NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_TxnAuthorizationTime` datetime NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned NULL   ;
ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_ClientTransID` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_creditmemo_creditmemoline_model extends MY_Model {

	protected $qbxml_id;
	protected $CreditMemo_TxnID;
	protected $SortOrder;
	protected $TxnLineID;
	protected $Item_ListID;
	protected $Item_FullName;
	protected $Descrip;
	protected $Quantity;
	protected $UnitOfMeasure;
	protected $OverrideUOMSet_ListID;
	protected $OverrideUOMSet_FullName;
	protected $Rate;
	protected $RatePercent;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $Amount;
	protected $ServiceDate;
	protected $SalesTaxCode_ListID;
	protected $SalesTaxCode_FullName;
	protected $Other1;
	protected $Other2;
	protected $CreditCardTxnInputInfo_CreditCardNumber;
	protected $CreditCardTxnInputInfo_ExpirationMonth;
	protected $CreditCardTxnInputInfo_ExpirationYear;
	protected $CreditCardTxnInputInfo_NameOnCard;
	protected $CreditCardTxnInputInfo_CreditCardAddress;
	protected $CreditCardTxnInputInfo_CreditCardPostalCode;
	protected $CreditCardTxnInputInfo_CommercialCardCode;
	protected $CreditCardTxnInputInfo_TransactionMode;
	protected $CreditCardTxnInputInfo_CreditCardTxnType;
	protected $CreditCardTxnResultInfo_ResultCode;
	protected $CreditCardTxnResultInfo_ResultMessage;
	protected $CreditCardTxnResultInfo_CreditCardTransID;
	protected $CreditCardTxnResultInfo_MerchantAccountNumber;
	protected $CreditCardTxnResultInfo_AuthorizationCode;
	protected $CreditCardTxnResultInfo_AVSStreet;
	protected $CreditCardTxnResultInfo_AVSZip;
	protected $CreditCardTxnResultInfo_CardSecurityCodeMatch;
	protected $CreditCardTxnResultInfo_ReconBatchID;
	protected $CreditCardTxnResultInfo_PaymentGroupingCode;
	protected $CreditCardTxnResultInfo_PaymentStatus;
	protected $CreditCardTxnResultInfo_TxnAuthorizationTime;
	protected $CreditCardTxnResultInfo_TxnAuthorizationStamp;
	protected $CreditCardTxnResultInfo_ClientTransID;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_creditmemo_creditmemoline';
		$this->_short_name = 'qb_creditmemo_creditmemoline';
		$this->_fields = array("qbxml_id","CreditMemo_TxnID","SortOrder","TxnLineID","Item_ListID","Item_FullName","Descrip","Quantity","UnitOfMeasure","OverrideUOMSet_ListID","OverrideUOMSet_FullName","Rate","RatePercent","Class_ListID","Class_FullName","Amount","ServiceDate","SalesTaxCode_ListID","SalesTaxCode_FullName","Other1","Other2","CreditCardTxnInputInfo_CreditCardNumber","CreditCardTxnInputInfo_ExpirationMonth","CreditCardTxnInputInfo_ExpirationYear","CreditCardTxnInputInfo_NameOnCard","CreditCardTxnInputInfo_CreditCardAddress","CreditCardTxnInputInfo_CreditCardPostalCode","CreditCardTxnInputInfo_CommercialCardCode","CreditCardTxnInputInfo_TransactionMode","CreditCardTxnInputInfo_CreditCardTxnType","CreditCardTxnResultInfo_ResultCode","CreditCardTxnResultInfo_ResultMessage","CreditCardTxnResultInfo_CreditCardTransID","CreditCardTxnResultInfo_MerchantAccountNumber","CreditCardTxnResultInfo_AuthorizationCode","CreditCardTxnResultInfo_AVSStreet","CreditCardTxnResultInfo_AVSZip","CreditCardTxnResultInfo_CardSecurityCodeMatch","CreditCardTxnResultInfo_ReconBatchID","CreditCardTxnResultInfo_PaymentGroupingCode","CreditCardTxnResultInfo_PaymentStatus","CreditCardTxnResultInfo_TxnAuthorizationTime","CreditCardTxnResultInfo_TxnAuthorizationStamp","CreditCardTxnResultInfo_ClientTransID");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: CreditMemo_TxnID -------------------------------------- 

	/** 
	* Sets a value to `CreditMemo_TxnID` variable
	* @access public
	*/

	public function setCreditmemoTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditMemo_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditMemo_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditMemo_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditMemo_TxnID` variable
	* @access public
	*/

	public function getCreditmemoTxnid() {
		return $this->CreditMemo_TxnID;
	}

	public function get_CreditMemo_TxnID() {
		return $this->CreditMemo_TxnID;
	}

	
// ------------------------------ End Field: CreditMemo_TxnID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `TxnLineID` variable
	* @access public
	*/

	public function setTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnLineID` variable
	* @access public
	*/

	public function getTxnlineid() {
		return $this->TxnLineID;
	}

	public function get_TxnLineID() {
		return $this->TxnLineID;
	}

	
// ------------------------------ End Field: TxnLineID --------------------------------------


// ---------------------------- Start Field: Item_ListID -------------------------------------- 

	/** 
	* Sets a value to `Item_ListID` variable
	* @access public
	*/

	public function setItemListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_ListID` variable
	* @access public
	*/

	public function getItemListid() {
		return $this->Item_ListID;
	}

	public function get_Item_ListID() {
		return $this->Item_ListID;
	}

	
// ------------------------------ End Field: Item_ListID --------------------------------------


// ---------------------------- Start Field: Item_FullName -------------------------------------- 

	/** 
	* Sets a value to `Item_FullName` variable
	* @access public
	*/

	public function setItemFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_FullName` variable
	* @access public
	*/

	public function getItemFullname() {
		return $this->Item_FullName;
	}

	public function get_Item_FullName() {
		return $this->Item_FullName;
	}

	
// ------------------------------ End Field: Item_FullName --------------------------------------


// ---------------------------- Start Field: Descrip -------------------------------------- 

	/** 
	* Sets a value to `Descrip` variable
	* @access public
	*/

	public function setDescrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Descrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Descrip` variable
	* @access public
	*/

	public function getDescrip() {
		return $this->Descrip;
	}

	public function get_Descrip() {
		return $this->Descrip;
	}

	
// ------------------------------ End Field: Descrip --------------------------------------


// ---------------------------- Start Field: Quantity -------------------------------------- 

	/** 
	* Sets a value to `Quantity` variable
	* @access public
	*/

	public function setQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Quantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Quantity` variable
	* @access public
	*/

	public function getQuantity() {
		return $this->Quantity;
	}

	public function get_Quantity() {
		return $this->Quantity;
	}

	
// ------------------------------ End Field: Quantity --------------------------------------


// ---------------------------- Start Field: UnitOfMeasure -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasure` variable
	* @access public
	*/

	public function setUnitofmeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasure` variable
	* @access public
	*/

	public function getUnitofmeasure() {
		return $this->UnitOfMeasure;
	}

	public function get_UnitOfMeasure() {
		return $this->UnitOfMeasure;
	}

	
// ------------------------------ End Field: UnitOfMeasure --------------------------------------


// ---------------------------- Start Field: OverrideUOMSet_ListID -------------------------------------- 

	/** 
	* Sets a value to `OverrideUOMSet_ListID` variable
	* @access public
	*/

	public function setOverrideuomsetListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OverrideUOMSet_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OverrideUOMSet_ListID` variable
	* @access public
	*/

	public function getOverrideuomsetListid() {
		return $this->OverrideUOMSet_ListID;
	}

	public function get_OverrideUOMSet_ListID() {
		return $this->OverrideUOMSet_ListID;
	}

	
// ------------------------------ End Field: OverrideUOMSet_ListID --------------------------------------


// ---------------------------- Start Field: OverrideUOMSet_FullName -------------------------------------- 

	/** 
	* Sets a value to `OverrideUOMSet_FullName` variable
	* @access public
	*/

	public function setOverrideuomsetFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OverrideUOMSet_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OverrideUOMSet_FullName` variable
	* @access public
	*/

	public function getOverrideuomsetFullname() {
		return $this->OverrideUOMSet_FullName;
	}

	public function get_OverrideUOMSet_FullName() {
		return $this->OverrideUOMSet_FullName;
	}

	
// ------------------------------ End Field: OverrideUOMSet_FullName --------------------------------------


// ---------------------------- Start Field: Rate -------------------------------------- 

	/** 
	* Sets a value to `Rate` variable
	* @access public
	*/

	public function setRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Rate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Rate` variable
	* @access public
	*/

	public function getRate() {
		return $this->Rate;
	}

	public function get_Rate() {
		return $this->Rate;
	}

	
// ------------------------------ End Field: Rate --------------------------------------


// ---------------------------- Start Field: RatePercent -------------------------------------- 

	/** 
	* Sets a value to `RatePercent` variable
	* @access public
	*/

	public function setRatepercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RatePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RatePercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RatePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RatePercent` variable
	* @access public
	*/

	public function getRatepercent() {
		return $this->RatePercent;
	}

	public function get_RatePercent() {
		return $this->RatePercent;
	}

	
// ------------------------------ End Field: RatePercent --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------


// ---------------------------- Start Field: ServiceDate -------------------------------------- 

	/** 
	* Sets a value to `ServiceDate` variable
	* @access public
	*/

	public function setServicedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ServiceDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ServiceDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ServiceDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ServiceDate` variable
	* @access public
	*/

	public function getServicedate() {
		return $this->ServiceDate;
	}

	public function get_ServiceDate() {
		return $this->ServiceDate;
	}

	
// ------------------------------ End Field: ServiceDate --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function setSalestaxcodeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function getSalestaxcodeListid() {
		return $this->SalesTaxCode_ListID;
	}

	public function get_SalesTaxCode_ListID() {
		return $this->SalesTaxCode_ListID;
	}

	
// ------------------------------ End Field: SalesTaxCode_ListID --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function setSalestaxcodeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function getSalestaxcodeFullname() {
		return $this->SalesTaxCode_FullName;
	}

	public function get_SalesTaxCode_FullName() {
		return $this->SalesTaxCode_FullName;
	}

	
// ------------------------------ End Field: SalesTaxCode_FullName --------------------------------------


// ---------------------------- Start Field: Other1 -------------------------------------- 

	/** 
	* Sets a value to `Other1` variable
	* @access public
	*/

	public function setOther1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other1` variable
	* @access public
	*/

	public function getOther1() {
		return $this->Other1;
	}

	public function get_Other1() {
		return $this->Other1;
	}

	
// ------------------------------ End Field: Other1 --------------------------------------


// ---------------------------- Start Field: Other2 -------------------------------------- 

	/** 
	* Sets a value to `Other2` variable
	* @access public
	*/

	public function setOther2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other2` variable
	* @access public
	*/

	public function getOther2() {
		return $this->Other2;
	}

	public function get_Other2() {
		return $this->Other2;
	}

	
// ------------------------------ End Field: Other2 --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInputInfo_CreditCardNumber -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInputInfo_CreditCardNumber` variable
	* @access public
	*/

	public function setCreditcardtxninputinfoCreditcardnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_CreditCardNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInputInfo_CreditCardNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_CreditCardNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInputInfo_CreditCardNumber` variable
	* @access public
	*/

	public function getCreditcardtxninputinfoCreditcardnumber() {
		return $this->CreditCardTxnInputInfo_CreditCardNumber;
	}

	public function get_CreditCardTxnInputInfo_CreditCardNumber() {
		return $this->CreditCardTxnInputInfo_CreditCardNumber;
	}

	
// ------------------------------ End Field: CreditCardTxnInputInfo_CreditCardNumber --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInputInfo_ExpirationMonth -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInputInfo_ExpirationMonth` variable
	* @access public
	*/

	public function setCreditcardtxninputinfoExpirationmonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_ExpirationMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInputInfo_ExpirationMonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_ExpirationMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInputInfo_ExpirationMonth` variable
	* @access public
	*/

	public function getCreditcardtxninputinfoExpirationmonth() {
		return $this->CreditCardTxnInputInfo_ExpirationMonth;
	}

	public function get_CreditCardTxnInputInfo_ExpirationMonth() {
		return $this->CreditCardTxnInputInfo_ExpirationMonth;
	}

	
// ------------------------------ End Field: CreditCardTxnInputInfo_ExpirationMonth --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInputInfo_ExpirationYear -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInputInfo_ExpirationYear` variable
	* @access public
	*/

	public function setCreditcardtxninputinfoExpirationyear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_ExpirationYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInputInfo_ExpirationYear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_ExpirationYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInputInfo_ExpirationYear` variable
	* @access public
	*/

	public function getCreditcardtxninputinfoExpirationyear() {
		return $this->CreditCardTxnInputInfo_ExpirationYear;
	}

	public function get_CreditCardTxnInputInfo_ExpirationYear() {
		return $this->CreditCardTxnInputInfo_ExpirationYear;
	}

	
// ------------------------------ End Field: CreditCardTxnInputInfo_ExpirationYear --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInputInfo_NameOnCard -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInputInfo_NameOnCard` variable
	* @access public
	*/

	public function setCreditcardtxninputinfoNameoncard($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_NameOnCard', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInputInfo_NameOnCard($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_NameOnCard', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInputInfo_NameOnCard` variable
	* @access public
	*/

	public function getCreditcardtxninputinfoNameoncard() {
		return $this->CreditCardTxnInputInfo_NameOnCard;
	}

	public function get_CreditCardTxnInputInfo_NameOnCard() {
		return $this->CreditCardTxnInputInfo_NameOnCard;
	}

	
// ------------------------------ End Field: CreditCardTxnInputInfo_NameOnCard --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInputInfo_CreditCardAddress -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInputInfo_CreditCardAddress` variable
	* @access public
	*/

	public function setCreditcardtxninputinfoCreditcardaddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_CreditCardAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInputInfo_CreditCardAddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_CreditCardAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInputInfo_CreditCardAddress` variable
	* @access public
	*/

	public function getCreditcardtxninputinfoCreditcardaddress() {
		return $this->CreditCardTxnInputInfo_CreditCardAddress;
	}

	public function get_CreditCardTxnInputInfo_CreditCardAddress() {
		return $this->CreditCardTxnInputInfo_CreditCardAddress;
	}

	
// ------------------------------ End Field: CreditCardTxnInputInfo_CreditCardAddress --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInputInfo_CreditCardPostalCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInputInfo_CreditCardPostalCode` variable
	* @access public
	*/

	public function setCreditcardtxninputinfoCreditcardpostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_CreditCardPostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInputInfo_CreditCardPostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_CreditCardPostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInputInfo_CreditCardPostalCode` variable
	* @access public
	*/

	public function getCreditcardtxninputinfoCreditcardpostalcode() {
		return $this->CreditCardTxnInputInfo_CreditCardPostalCode;
	}

	public function get_CreditCardTxnInputInfo_CreditCardPostalCode() {
		return $this->CreditCardTxnInputInfo_CreditCardPostalCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInputInfo_CreditCardPostalCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInputInfo_CommercialCardCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInputInfo_CommercialCardCode` variable
	* @access public
	*/

	public function setCreditcardtxninputinfoCommercialcardcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_CommercialCardCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInputInfo_CommercialCardCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_CommercialCardCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInputInfo_CommercialCardCode` variable
	* @access public
	*/

	public function getCreditcardtxninputinfoCommercialcardcode() {
		return $this->CreditCardTxnInputInfo_CommercialCardCode;
	}

	public function get_CreditCardTxnInputInfo_CommercialCardCode() {
		return $this->CreditCardTxnInputInfo_CommercialCardCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInputInfo_CommercialCardCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInputInfo_TransactionMode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInputInfo_TransactionMode` variable
	* @access public
	*/

	public function setCreditcardtxninputinfoTransactionmode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_TransactionMode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInputInfo_TransactionMode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_TransactionMode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInputInfo_TransactionMode` variable
	* @access public
	*/

	public function getCreditcardtxninputinfoTransactionmode() {
		return $this->CreditCardTxnInputInfo_TransactionMode;
	}

	public function get_CreditCardTxnInputInfo_TransactionMode() {
		return $this->CreditCardTxnInputInfo_TransactionMode;
	}

	
// ------------------------------ End Field: CreditCardTxnInputInfo_TransactionMode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInputInfo_CreditCardTxnType -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInputInfo_CreditCardTxnType` variable
	* @access public
	*/

	public function setCreditcardtxninputinfoCreditcardtxntype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_CreditCardTxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInputInfo_CreditCardTxnType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInputInfo_CreditCardTxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInputInfo_CreditCardTxnType` variable
	* @access public
	*/

	public function getCreditcardtxninputinfoCreditcardtxntype() {
		return $this->CreditCardTxnInputInfo_CreditCardTxnType;
	}

	public function get_CreditCardTxnInputInfo_CreditCardTxnType() {
		return $this->CreditCardTxnInputInfo_CreditCardTxnType;
	}

	
// ------------------------------ End Field: CreditCardTxnInputInfo_CreditCardTxnType --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_ResultCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_ResultCode` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoResultcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_ResultCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_ResultCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_ResultCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_ResultCode` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoResultcode() {
		return $this->CreditCardTxnResultInfo_ResultCode;
	}

	public function get_CreditCardTxnResultInfo_ResultCode() {
		return $this->CreditCardTxnResultInfo_ResultCode;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_ResultCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_ResultMessage -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_ResultMessage` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoResultmessage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_ResultMessage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_ResultMessage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_ResultMessage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_ResultMessage` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoResultmessage() {
		return $this->CreditCardTxnResultInfo_ResultMessage;
	}

	public function get_CreditCardTxnResultInfo_ResultMessage() {
		return $this->CreditCardTxnResultInfo_ResultMessage;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_ResultMessage --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_CreditCardTransID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_CreditCardTransID` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoCreditcardtransid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_CreditCardTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_CreditCardTransID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_CreditCardTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_CreditCardTransID` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoCreditcardtransid() {
		return $this->CreditCardTxnResultInfo_CreditCardTransID;
	}

	public function get_CreditCardTxnResultInfo_CreditCardTransID() {
		return $this->CreditCardTxnResultInfo_CreditCardTransID;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_CreditCardTransID --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_MerchantAccountNumber -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_MerchantAccountNumber` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoMerchantaccountnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_MerchantAccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_MerchantAccountNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_MerchantAccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_MerchantAccountNumber` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoMerchantaccountnumber() {
		return $this->CreditCardTxnResultInfo_MerchantAccountNumber;
	}

	public function get_CreditCardTxnResultInfo_MerchantAccountNumber() {
		return $this->CreditCardTxnResultInfo_MerchantAccountNumber;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_MerchantAccountNumber --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_AuthorizationCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_AuthorizationCode` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoAuthorizationcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_AuthorizationCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_AuthorizationCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_AuthorizationCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_AuthorizationCode` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoAuthorizationcode() {
		return $this->CreditCardTxnResultInfo_AuthorizationCode;
	}

	public function get_CreditCardTxnResultInfo_AuthorizationCode() {
		return $this->CreditCardTxnResultInfo_AuthorizationCode;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_AuthorizationCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_AVSStreet -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_AVSStreet` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoAvsstreet($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_AVSStreet', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_AVSStreet($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_AVSStreet', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_AVSStreet` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoAvsstreet() {
		return $this->CreditCardTxnResultInfo_AVSStreet;
	}

	public function get_CreditCardTxnResultInfo_AVSStreet() {
		return $this->CreditCardTxnResultInfo_AVSStreet;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_AVSStreet --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_AVSZip -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_AVSZip` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoAvszip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_AVSZip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_AVSZip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_AVSZip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_AVSZip` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoAvszip() {
		return $this->CreditCardTxnResultInfo_AVSZip;
	}

	public function get_CreditCardTxnResultInfo_AVSZip() {
		return $this->CreditCardTxnResultInfo_AVSZip;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_AVSZip --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_CardSecurityCodeMatch -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_CardSecurityCodeMatch` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoCardsecuritycodematch($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_CardSecurityCodeMatch', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_CardSecurityCodeMatch($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_CardSecurityCodeMatch', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_CardSecurityCodeMatch` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoCardsecuritycodematch() {
		return $this->CreditCardTxnResultInfo_CardSecurityCodeMatch;
	}

	public function get_CreditCardTxnResultInfo_CardSecurityCodeMatch() {
		return $this->CreditCardTxnResultInfo_CardSecurityCodeMatch;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_CardSecurityCodeMatch --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_ReconBatchID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_ReconBatchID` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoReconbatchid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_ReconBatchID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_ReconBatchID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_ReconBatchID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_ReconBatchID` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoReconbatchid() {
		return $this->CreditCardTxnResultInfo_ReconBatchID;
	}

	public function get_CreditCardTxnResultInfo_ReconBatchID() {
		return $this->CreditCardTxnResultInfo_ReconBatchID;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_ReconBatchID --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_PaymentGroupingCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_PaymentGroupingCode` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoPaymentgroupingcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_PaymentGroupingCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_PaymentGroupingCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_PaymentGroupingCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_PaymentGroupingCode` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoPaymentgroupingcode() {
		return $this->CreditCardTxnResultInfo_PaymentGroupingCode;
	}

	public function get_CreditCardTxnResultInfo_PaymentGroupingCode() {
		return $this->CreditCardTxnResultInfo_PaymentGroupingCode;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_PaymentGroupingCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_PaymentStatus -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_PaymentStatus` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoPaymentstatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_PaymentStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_PaymentStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_PaymentStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_PaymentStatus` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoPaymentstatus() {
		return $this->CreditCardTxnResultInfo_PaymentStatus;
	}

	public function get_CreditCardTxnResultInfo_PaymentStatus() {
		return $this->CreditCardTxnResultInfo_PaymentStatus;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_PaymentStatus --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_TxnAuthorizationTime -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_TxnAuthorizationTime` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoTxnauthorizationtime($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_TxnAuthorizationTime', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_TxnAuthorizationTime($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_TxnAuthorizationTime', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_TxnAuthorizationTime` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoTxnauthorizationtime() {
		return $this->CreditCardTxnResultInfo_TxnAuthorizationTime;
	}

	public function get_CreditCardTxnResultInfo_TxnAuthorizationTime() {
		return $this->CreditCardTxnResultInfo_TxnAuthorizationTime;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_TxnAuthorizationTime --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_TxnAuthorizationStamp -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_TxnAuthorizationStamp` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoTxnauthorizationstamp($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_TxnAuthorizationStamp', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_TxnAuthorizationStamp($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_TxnAuthorizationStamp', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_TxnAuthorizationStamp` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoTxnauthorizationstamp() {
		return $this->CreditCardTxnResultInfo_TxnAuthorizationStamp;
	}

	public function get_CreditCardTxnResultInfo_TxnAuthorizationStamp() {
		return $this->CreditCardTxnResultInfo_TxnAuthorizationStamp;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_TxnAuthorizationStamp --------------------------------------


// ---------------------------- Start Field: CreditCardTxnResultInfo_ClientTransID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnResultInfo_ClientTransID` variable
	* @access public
	*/

	public function setCreditcardtxnresultinfoClienttransid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_ClientTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnResultInfo_ClientTransID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnResultInfo_ClientTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnResultInfo_ClientTransID` variable
	* @access public
	*/

	public function getCreditcardtxnresultinfoClienttransid() {
		return $this->CreditCardTxnResultInfo_ClientTransID;
	}

	public function get_CreditCardTxnResultInfo_ClientTransID() {
		return $this->CreditCardTxnResultInfo_ClientTransID;
	}

	
// ------------------------------ End Field: CreditCardTxnResultInfo_ClientTransID --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'CreditMemo_TxnID' => (object) array(
										'Field'=>'CreditMemo_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnLineID' => (object) array(
										'Field'=>'TxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_ListID' => (object) array(
										'Field'=>'Item_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_FullName' => (object) array(
										'Field'=>'Item_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Descrip' => (object) array(
										'Field'=>'Descrip',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Quantity' => (object) array(
										'Field'=>'Quantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'UnitOfMeasure' => (object) array(
										'Field'=>'UnitOfMeasure',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'OverrideUOMSet_ListID' => (object) array(
										'Field'=>'OverrideUOMSet_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'OverrideUOMSet_FullName' => (object) array(
										'Field'=>'OverrideUOMSet_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Rate' => (object) array(
										'Field'=>'Rate',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'RatePercent' => (object) array(
										'Field'=>'RatePercent',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ServiceDate' => (object) array(
										'Field'=>'ServiceDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_ListID' => (object) array(
										'Field'=>'SalesTaxCode_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_FullName' => (object) array(
										'Field'=>'SalesTaxCode_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Other1' => (object) array(
										'Field'=>'Other1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Other2' => (object) array(
										'Field'=>'Other2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInputInfo_CreditCardNumber' => (object) array(
										'Field'=>'CreditCardTxnInputInfo_CreditCardNumber',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInputInfo_ExpirationMonth' => (object) array(
										'Field'=>'CreditCardTxnInputInfo_ExpirationMonth',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInputInfo_ExpirationYear' => (object) array(
										'Field'=>'CreditCardTxnInputInfo_ExpirationYear',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInputInfo_NameOnCard' => (object) array(
										'Field'=>'CreditCardTxnInputInfo_NameOnCard',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInputInfo_CreditCardAddress' => (object) array(
										'Field'=>'CreditCardTxnInputInfo_CreditCardAddress',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInputInfo_CreditCardPostalCode' => (object) array(
										'Field'=>'CreditCardTxnInputInfo_CreditCardPostalCode',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInputInfo_CommercialCardCode' => (object) array(
										'Field'=>'CreditCardTxnInputInfo_CommercialCardCode',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInputInfo_TransactionMode' => (object) array(
										'Field'=>'CreditCardTxnInputInfo_TransactionMode',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInputInfo_CreditCardTxnType' => (object) array(
										'Field'=>'CreditCardTxnInputInfo_CreditCardTxnType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_ResultCode' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_ResultCode',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_ResultMessage' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_ResultMessage',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_CreditCardTransID' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_CreditCardTransID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_MerchantAccountNumber' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_MerchantAccountNumber',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_AuthorizationCode' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_AuthorizationCode',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_AVSStreet' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_AVSStreet',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_AVSZip' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_AVSZip',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_CardSecurityCodeMatch' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_CardSecurityCodeMatch',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_ReconBatchID' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_ReconBatchID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_PaymentGroupingCode' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_PaymentGroupingCode',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_PaymentStatus' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_PaymentStatus',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_TxnAuthorizationTime' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_TxnAuthorizationTime',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_TxnAuthorizationStamp' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_TxnAuthorizationStamp',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnResultInfo_ClientTransID' => (object) array(
										'Field'=>'CreditCardTxnResultInfo_ClientTransID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'CreditMemo_TxnID' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditMemo_TxnID` varchar(40) NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'TxnLineID' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `TxnLineID` varchar(40) NULL   ;",
			'Item_ListID' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Item_ListID` varchar(40) NULL   ;",
			'Item_FullName' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Item_FullName` varchar(255) NULL   ;",
			'Descrip' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Descrip` text NULL   ;",
			'Quantity' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';",
			'UnitOfMeasure' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `UnitOfMeasure` text NULL   ;",
			'OverrideUOMSet_ListID' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `OverrideUOMSet_ListID` varchar(40) NULL   ;",
			'OverrideUOMSet_FullName' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `OverrideUOMSet_FullName` varchar(255) NULL   ;",
			'Rate' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Rate` decimal(13,5) NULL   ;",
			'RatePercent' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `RatePercent` decimal(12,5) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Class_FullName` varchar(255) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Amount` decimal(10,2) NULL   ;",
			'ServiceDate' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `ServiceDate` date NULL   ;",
			'SalesTaxCode_ListID' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;",
			'SalesTaxCode_FullName' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;",
			'Other1' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Other1` text NULL   ;",
			'Other2' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `Other2` text NULL   ;",
			'CreditCardTxnInputInfo_CreditCardNumber' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_CreditCardNumber` text NULL   ;",
			'CreditCardTxnInputInfo_ExpirationMonth' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned NULL   ;",
			'CreditCardTxnInputInfo_ExpirationYear' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned NULL   ;",
			'CreditCardTxnInputInfo_NameOnCard' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_NameOnCard` text NULL   ;",
			'CreditCardTxnInputInfo_CreditCardAddress' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_CreditCardAddress` text NULL   ;",
			'CreditCardTxnInputInfo_CreditCardPostalCode' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_CreditCardPostalCode` text NULL   ;",
			'CreditCardTxnInputInfo_CommercialCardCode' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_CommercialCardCode` text NULL   ;",
			'CreditCardTxnInputInfo_TransactionMode' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_TransactionMode` varchar(40) NULL   ;",
			'CreditCardTxnInputInfo_CreditCardTxnType' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) NULL   ;",
			'CreditCardTxnResultInfo_ResultCode' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_ResultCode` int(10) unsigned NULL   ;",
			'CreditCardTxnResultInfo_ResultMessage' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_ResultMessage` text NULL   ;",
			'CreditCardTxnResultInfo_CreditCardTransID' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_CreditCardTransID` text NULL   ;",
			'CreditCardTxnResultInfo_MerchantAccountNumber' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_MerchantAccountNumber` text NULL   ;",
			'CreditCardTxnResultInfo_AuthorizationCode' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_AuthorizationCode` text NULL   ;",
			'CreditCardTxnResultInfo_AVSStreet' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_AVSStreet` varchar(40) NULL   ;",
			'CreditCardTxnResultInfo_AVSZip' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_AVSZip` varchar(40) NULL   ;",
			'CreditCardTxnResultInfo_CardSecurityCodeMatch' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) NULL   ;",
			'CreditCardTxnResultInfo_ReconBatchID' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_ReconBatchID` text NULL   ;",
			'CreditCardTxnResultInfo_PaymentGroupingCode' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned NULL   ;",
			'CreditCardTxnResultInfo_PaymentStatus' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_PaymentStatus` varchar(40) NULL   ;",
			'CreditCardTxnResultInfo_TxnAuthorizationTime' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_TxnAuthorizationTime` datetime NULL   ;",
			'CreditCardTxnResultInfo_TxnAuthorizationStamp' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned NULL   ;",
			'CreditCardTxnResultInfo_ClientTransID' => "ALTER TABLE  `qb_creditmemo_creditmemoline` ADD  `CreditCardTxnResultInfo_ClientTransID` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setCreditmemoTxnid() - CreditMemo_TxnID
//setSortorder() - SortOrder
//setTxnlineid() - TxnLineID
//setItemListid() - Item_ListID
//setItemFullname() - Item_FullName
//setDescrip() - Descrip
//setQuantity() - Quantity
//setUnitofmeasure() - UnitOfMeasure
//setOverrideuomsetListid() - OverrideUOMSet_ListID
//setOverrideuomsetFullname() - OverrideUOMSet_FullName
//setRate() - Rate
//setRatepercent() - RatePercent
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setAmount() - Amount
//setServicedate() - ServiceDate
//setSalestaxcodeListid() - SalesTaxCode_ListID
//setSalestaxcodeFullname() - SalesTaxCode_FullName
//setOther1() - Other1
//setOther2() - Other2
//setCreditcardtxninputinfoCreditcardnumber() - CreditCardTxnInputInfo_CreditCardNumber
//setCreditcardtxninputinfoExpirationmonth() - CreditCardTxnInputInfo_ExpirationMonth
//setCreditcardtxninputinfoExpirationyear() - CreditCardTxnInputInfo_ExpirationYear
//setCreditcardtxninputinfoNameoncard() - CreditCardTxnInputInfo_NameOnCard
//setCreditcardtxninputinfoCreditcardaddress() - CreditCardTxnInputInfo_CreditCardAddress
//setCreditcardtxninputinfoCreditcardpostalcode() - CreditCardTxnInputInfo_CreditCardPostalCode
//setCreditcardtxninputinfoCommercialcardcode() - CreditCardTxnInputInfo_CommercialCardCode
//setCreditcardtxninputinfoTransactionmode() - CreditCardTxnInputInfo_TransactionMode
//setCreditcardtxninputinfoCreditcardtxntype() - CreditCardTxnInputInfo_CreditCardTxnType
//setCreditcardtxnresultinfoResultcode() - CreditCardTxnResultInfo_ResultCode
//setCreditcardtxnresultinfoResultmessage() - CreditCardTxnResultInfo_ResultMessage
//setCreditcardtxnresultinfoCreditcardtransid() - CreditCardTxnResultInfo_CreditCardTransID
//setCreditcardtxnresultinfoMerchantaccountnumber() - CreditCardTxnResultInfo_MerchantAccountNumber
//setCreditcardtxnresultinfoAuthorizationcode() - CreditCardTxnResultInfo_AuthorizationCode
//setCreditcardtxnresultinfoAvsstreet() - CreditCardTxnResultInfo_AVSStreet
//setCreditcardtxnresultinfoAvszip() - CreditCardTxnResultInfo_AVSZip
//setCreditcardtxnresultinfoCardsecuritycodematch() - CreditCardTxnResultInfo_CardSecurityCodeMatch
//setCreditcardtxnresultinfoReconbatchid() - CreditCardTxnResultInfo_ReconBatchID
//setCreditcardtxnresultinfoPaymentgroupingcode() - CreditCardTxnResultInfo_PaymentGroupingCode
//setCreditcardtxnresultinfoPaymentstatus() - CreditCardTxnResultInfo_PaymentStatus
//setCreditcardtxnresultinfoTxnauthorizationtime() - CreditCardTxnResultInfo_TxnAuthorizationTime
//setCreditcardtxnresultinfoTxnauthorizationstamp() - CreditCardTxnResultInfo_TxnAuthorizationStamp
//setCreditcardtxnresultinfoClienttransid() - CreditCardTxnResultInfo_ClientTransID

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_CreditMemo_TxnID() - CreditMemo_TxnID
//set_SortOrder() - SortOrder
//set_TxnLineID() - TxnLineID
//set_Item_ListID() - Item_ListID
//set_Item_FullName() - Item_FullName
//set_Descrip() - Descrip
//set_Quantity() - Quantity
//set_UnitOfMeasure() - UnitOfMeasure
//set_OverrideUOMSet_ListID() - OverrideUOMSet_ListID
//set_OverrideUOMSet_FullName() - OverrideUOMSet_FullName
//set_Rate() - Rate
//set_RatePercent() - RatePercent
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_Amount() - Amount
//set_ServiceDate() - ServiceDate
//set_SalesTaxCode_ListID() - SalesTaxCode_ListID
//set_SalesTaxCode_FullName() - SalesTaxCode_FullName
//set_Other1() - Other1
//set_Other2() - Other2
//set_CreditCardTxnInputInfo_CreditCardNumber() - CreditCardTxnInputInfo_CreditCardNumber
//set_CreditCardTxnInputInfo_ExpirationMonth() - CreditCardTxnInputInfo_ExpirationMonth
//set_CreditCardTxnInputInfo_ExpirationYear() - CreditCardTxnInputInfo_ExpirationYear
//set_CreditCardTxnInputInfo_NameOnCard() - CreditCardTxnInputInfo_NameOnCard
//set_CreditCardTxnInputInfo_CreditCardAddress() - CreditCardTxnInputInfo_CreditCardAddress
//set_CreditCardTxnInputInfo_CreditCardPostalCode() - CreditCardTxnInputInfo_CreditCardPostalCode
//set_CreditCardTxnInputInfo_CommercialCardCode() - CreditCardTxnInputInfo_CommercialCardCode
//set_CreditCardTxnInputInfo_TransactionMode() - CreditCardTxnInputInfo_TransactionMode
//set_CreditCardTxnInputInfo_CreditCardTxnType() - CreditCardTxnInputInfo_CreditCardTxnType
//set_CreditCardTxnResultInfo_ResultCode() - CreditCardTxnResultInfo_ResultCode
//set_CreditCardTxnResultInfo_ResultMessage() - CreditCardTxnResultInfo_ResultMessage
//set_CreditCardTxnResultInfo_CreditCardTransID() - CreditCardTxnResultInfo_CreditCardTransID
//set_CreditCardTxnResultInfo_MerchantAccountNumber() - CreditCardTxnResultInfo_MerchantAccountNumber
//set_CreditCardTxnResultInfo_AuthorizationCode() - CreditCardTxnResultInfo_AuthorizationCode
//set_CreditCardTxnResultInfo_AVSStreet() - CreditCardTxnResultInfo_AVSStreet
//set_CreditCardTxnResultInfo_AVSZip() - CreditCardTxnResultInfo_AVSZip
//set_CreditCardTxnResultInfo_CardSecurityCodeMatch() - CreditCardTxnResultInfo_CardSecurityCodeMatch
//set_CreditCardTxnResultInfo_ReconBatchID() - CreditCardTxnResultInfo_ReconBatchID
//set_CreditCardTxnResultInfo_PaymentGroupingCode() - CreditCardTxnResultInfo_PaymentGroupingCode
//set_CreditCardTxnResultInfo_PaymentStatus() - CreditCardTxnResultInfo_PaymentStatus
//set_CreditCardTxnResultInfo_TxnAuthorizationTime() - CreditCardTxnResultInfo_TxnAuthorizationTime
//set_CreditCardTxnResultInfo_TxnAuthorizationStamp() - CreditCardTxnResultInfo_TxnAuthorizationStamp
//set_CreditCardTxnResultInfo_ClientTransID() - CreditCardTxnResultInfo_ClientTransID

*/
/* End of file Qb_creditmemo_creditmemoline_model.php */
/* Location: ./application/models/Qb_creditmemo_creditmemoline_model.php */

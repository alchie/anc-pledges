<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_billingrate_billingrateperitem_model Class
 *
 * Manipulates `qb_billingrate_billingrateperitem` table on database

CREATE TABLE `qb_billingrate_billingrateperitem` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `BillingRate_ListID` varchar(40) DEFAULT NULL,
  `BillingRate_FullName` varchar(255) DEFAULT NULL,
  `Item_ListID` varchar(40) DEFAULT NULL,
  `Item_FullName` varchar(255) DEFAULT NULL,
  `CustomRate` decimal(13,5) DEFAULT NULL,
  `CustomRatePercent` decimal(12,5) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `BillingRate_ListID` (`BillingRate_ListID`),
  KEY `Item_ListID` (`Item_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `BillingRate_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `BillingRate_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `Item_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `Item_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `CustomRate` decimal(13,5) NULL   ;
ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `CustomRatePercent` decimal(12,5) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_billingrate_billingrateperitem_model extends MY_Model {

	protected $qbxml_id;
	protected $BillingRate_ListID;
	protected $BillingRate_FullName;
	protected $Item_ListID;
	protected $Item_FullName;
	protected $CustomRate;
	protected $CustomRatePercent;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_billingrate_billingrateperitem';
		$this->_short_name = 'qb_billingrate_billingrateperitem';
		$this->_fields = array("qbxml_id","BillingRate_ListID","BillingRate_FullName","Item_ListID","Item_FullName","CustomRate","CustomRatePercent");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: BillingRate_ListID -------------------------------------- 

	/** 
	* Sets a value to `BillingRate_ListID` variable
	* @access public
	*/

	public function setBillingrateListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillingRate_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillingRate_ListID` variable
	* @access public
	*/

	public function getBillingrateListid() {
		return $this->BillingRate_ListID;
	}

	public function get_BillingRate_ListID() {
		return $this->BillingRate_ListID;
	}

	
// ------------------------------ End Field: BillingRate_ListID --------------------------------------


// ---------------------------- Start Field: BillingRate_FullName -------------------------------------- 

	/** 
	* Sets a value to `BillingRate_FullName` variable
	* @access public
	*/

	public function setBillingrateFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillingRate_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillingRate_FullName` variable
	* @access public
	*/

	public function getBillingrateFullname() {
		return $this->BillingRate_FullName;
	}

	public function get_BillingRate_FullName() {
		return $this->BillingRate_FullName;
	}

	
// ------------------------------ End Field: BillingRate_FullName --------------------------------------


// ---------------------------- Start Field: Item_ListID -------------------------------------- 

	/** 
	* Sets a value to `Item_ListID` variable
	* @access public
	*/

	public function setItemListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_ListID` variable
	* @access public
	*/

	public function getItemListid() {
		return $this->Item_ListID;
	}

	public function get_Item_ListID() {
		return $this->Item_ListID;
	}

	
// ------------------------------ End Field: Item_ListID --------------------------------------


// ---------------------------- Start Field: Item_FullName -------------------------------------- 

	/** 
	* Sets a value to `Item_FullName` variable
	* @access public
	*/

	public function setItemFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_FullName` variable
	* @access public
	*/

	public function getItemFullname() {
		return $this->Item_FullName;
	}

	public function get_Item_FullName() {
		return $this->Item_FullName;
	}

	
// ------------------------------ End Field: Item_FullName --------------------------------------


// ---------------------------- Start Field: CustomRate -------------------------------------- 

	/** 
	* Sets a value to `CustomRate` variable
	* @access public
	*/

	public function setCustomrate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomRate` variable
	* @access public
	*/

	public function getCustomrate() {
		return $this->CustomRate;
	}

	public function get_CustomRate() {
		return $this->CustomRate;
	}

	
// ------------------------------ End Field: CustomRate --------------------------------------


// ---------------------------- Start Field: CustomRatePercent -------------------------------------- 

	/** 
	* Sets a value to `CustomRatePercent` variable
	* @access public
	*/

	public function setCustomratepercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomRatePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomRatePercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomRatePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomRatePercent` variable
	* @access public
	*/

	public function getCustomratepercent() {
		return $this->CustomRatePercent;
	}

	public function get_CustomRatePercent() {
		return $this->CustomRatePercent;
	}

	
// ------------------------------ End Field: CustomRatePercent --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'BillingRate_ListID' => (object) array(
										'Field'=>'BillingRate_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BillingRate_FullName' => (object) array(
										'Field'=>'BillingRate_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_ListID' => (object) array(
										'Field'=>'Item_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_FullName' => (object) array(
										'Field'=>'Item_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomRate' => (object) array(
										'Field'=>'CustomRate',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomRatePercent' => (object) array(
										'Field'=>'CustomRatePercent',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'BillingRate_ListID' => "ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `BillingRate_ListID` varchar(40) NULL   ;",
			'BillingRate_FullName' => "ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `BillingRate_FullName` varchar(255) NULL   ;",
			'Item_ListID' => "ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `Item_ListID` varchar(40) NULL   ;",
			'Item_FullName' => "ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `Item_FullName` varchar(255) NULL   ;",
			'CustomRate' => "ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `CustomRate` decimal(13,5) NULL   ;",
			'CustomRatePercent' => "ALTER TABLE  `qb_billingrate_billingrateperitem` ADD  `CustomRatePercent` decimal(12,5) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setBillingrateListid() - BillingRate_ListID
//setBillingrateFullname() - BillingRate_FullName
//setItemListid() - Item_ListID
//setItemFullname() - Item_FullName
//setCustomrate() - CustomRate
//setCustomratepercent() - CustomRatePercent

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_BillingRate_ListID() - BillingRate_ListID
//set_BillingRate_FullName() - BillingRate_FullName
//set_Item_ListID() - Item_ListID
//set_Item_FullName() - Item_FullName
//set_CustomRate() - CustomRate
//set_CustomRatePercent() - CustomRatePercent

*/
/* End of file Qb_billingrate_billingrateperitem_model.php */
/* Location: ./application/models/Qb_billingrate_billingrateperitem_model.php */

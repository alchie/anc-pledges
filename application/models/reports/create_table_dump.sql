CREATE TABLE `cash_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `beg_date` date DEFAULT NULL,
  `beg_operations` decimal(20,3) DEFAULT NULL,
  `beg_investments` decimal(20,3) DEFAULT NULL,
  `col_start_date` date DEFAULT NULL,
  `col_start_num` int(5) DEFAULT NULL,
  `col_end_date` date DEFAULT NULL,
  `col_end_num` int(5) DEFAULT NULL,
  `exp_start_date` date DEFAULT NULL,
  `exp_start_num` int(5) DEFAULT NULL,
  `exp_end_date` date DEFAULT NULL,
  `exp_end_num` int(5) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin;

-- Table structure for table `cash_report` 

CREATE TABLE `cash_report_collections` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `report_id` int(10) NOT NULL,
  `receipt_id` text NOT NULL,
  `type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin;

-- Table structure for table `cash_report_collections` 

CREATE TABLE `cash_report_expenses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `report_id` int(10) NOT NULL,
  `check_id` text NOT NULL,
  `type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin;

-- Table structure for table `cash_report_expenses` 

CREATE TABLE `revolving_fund` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` date NOT NULL,
  `revolving_fund` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=latin;

-- Table structure for table `revolving_fund` 

CREATE TABLE `revolving_fund_liquidations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `report_id` int(10) NOT NULL,
  `journal_id` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin;

-- Table structure for table `revolving_fund_liquidations` 


<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Cash_report_model Class
 *
 * Manipulates `cash_report` table on database

CREATE TABLE `cash_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `beg_date` date DEFAULT NULL,
  `beg_operations` decimal(20,3) DEFAULT NULL,
  `beg_investments` decimal(20,3) DEFAULT NULL,
  `col_start_date` date DEFAULT NULL,
  `col_start_num` int(5) DEFAULT NULL,
  `col_end_date` date DEFAULT NULL,
  `col_end_num` int(5) DEFAULT NULL,
  `exp_start_date` date DEFAULT NULL,
  `exp_start_num` int(5) DEFAULT NULL,
  `exp_end_date` date DEFAULT NULL,
  `exp_end_num` int(5) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin;

ALTER TABLE  `cash_report` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `cash_report` ADD  `beg_date` date NULL   ;
ALTER TABLE  `cash_report` ADD  `beg_operations` decimal(20,3) NULL   ;
ALTER TABLE  `cash_report` ADD  `beg_investments` decimal(20,3) NULL   ;
ALTER TABLE  `cash_report` ADD  `col_start_date` date NULL   ;
ALTER TABLE  `cash_report` ADD  `col_start_num` int(5) NULL   ;
ALTER TABLE  `cash_report` ADD  `col_end_date` date NULL   ;
ALTER TABLE  `cash_report` ADD  `col_end_num` int(5) NULL   ;
ALTER TABLE  `cash_report` ADD  `exp_start_date` date NULL   ;
ALTER TABLE  `cash_report` ADD  `exp_start_num` int(5) NULL   ;
ALTER TABLE  `cash_report` ADD  `exp_end_date` date NULL   ;
ALTER TABLE  `cash_report` ADD  `exp_end_num` int(5) NULL   ;
ALTER TABLE  `cash_report` ADD  `end_date` date NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Cash_report_model extends MY_Model {

	protected $id;
	protected $beg_date;
	protected $beg_operations;
	protected $beg_investments;
	protected $col_start_date;
	protected $col_start_num;
	protected $col_end_date;
	protected $col_end_num;
	protected $exp_start_date;
	protected $exp_start_num;
	protected $exp_end_date;
	protected $exp_end_num;
	protected $end_date;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'cash_report';
		$this->_short_name = 'cash_report';
		$this->_fields = array("id","beg_date","beg_operations","beg_investments","col_start_date","col_start_num","col_end_date","col_end_num","exp_start_date","exp_start_num","exp_end_date","exp_end_num","end_date");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: beg_date -------------------------------------- 

	/** 
	* Sets a value to `beg_date` variable
	* @access public
	*/

	public function setBegDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('beg_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `beg_date` variable
	* @access public
	*/

	public function getBegDate() {
		return $this->beg_date;
	}
	
// ------------------------------ End Field: beg_date --------------------------------------


// ---------------------------- Start Field: beg_operations -------------------------------------- 

	/** 
	* Sets a value to `beg_operations` variable
	* @access public
	*/

	public function setBegOperations($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('beg_operations', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `beg_operations` variable
	* @access public
	*/

	public function getBegOperations() {
		return $this->beg_operations;
	}
	
// ------------------------------ End Field: beg_operations --------------------------------------


// ---------------------------- Start Field: beg_investments -------------------------------------- 

	/** 
	* Sets a value to `beg_investments` variable
	* @access public
	*/

	public function setBegInvestments($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('beg_investments', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `beg_investments` variable
	* @access public
	*/

	public function getBegInvestments() {
		return $this->beg_investments;
	}
	
// ------------------------------ End Field: beg_investments --------------------------------------


// ---------------------------- Start Field: col_start_date -------------------------------------- 

	/** 
	* Sets a value to `col_start_date` variable
	* @access public
	*/

	public function setColStartDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('col_start_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `col_start_date` variable
	* @access public
	*/

	public function getColStartDate() {
		return $this->col_start_date;
	}
	
// ------------------------------ End Field: col_start_date --------------------------------------


// ---------------------------- Start Field: col_start_num -------------------------------------- 

	/** 
	* Sets a value to `col_start_num` variable
	* @access public
	*/

	public function setColStartNum($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('col_start_num', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `col_start_num` variable
	* @access public
	*/

	public function getColStartNum() {
		return $this->col_start_num;
	}
	
// ------------------------------ End Field: col_start_num --------------------------------------


// ---------------------------- Start Field: col_end_date -------------------------------------- 

	/** 
	* Sets a value to `col_end_date` variable
	* @access public
	*/

	public function setColEndDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('col_end_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `col_end_date` variable
	* @access public
	*/

	public function getColEndDate() {
		return $this->col_end_date;
	}
	
// ------------------------------ End Field: col_end_date --------------------------------------


// ---------------------------- Start Field: col_end_num -------------------------------------- 

	/** 
	* Sets a value to `col_end_num` variable
	* @access public
	*/

	public function setColEndNum($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('col_end_num', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `col_end_num` variable
	* @access public
	*/

	public function getColEndNum() {
		return $this->col_end_num;
	}
	
// ------------------------------ End Field: col_end_num --------------------------------------


// ---------------------------- Start Field: exp_start_date -------------------------------------- 

	/** 
	* Sets a value to `exp_start_date` variable
	* @access public
	*/

	public function setExpStartDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('exp_start_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `exp_start_date` variable
	* @access public
	*/

	public function getExpStartDate() {
		return $this->exp_start_date;
	}
	
// ------------------------------ End Field: exp_start_date --------------------------------------


// ---------------------------- Start Field: exp_start_num -------------------------------------- 

	/** 
	* Sets a value to `exp_start_num` variable
	* @access public
	*/

	public function setExpStartNum($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('exp_start_num', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `exp_start_num` variable
	* @access public
	*/

	public function getExpStartNum() {
		return $this->exp_start_num;
	}
	
// ------------------------------ End Field: exp_start_num --------------------------------------


// ---------------------------- Start Field: exp_end_date -------------------------------------- 

	/** 
	* Sets a value to `exp_end_date` variable
	* @access public
	*/

	public function setExpEndDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('exp_end_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `exp_end_date` variable
	* @access public
	*/

	public function getExpEndDate() {
		return $this->exp_end_date;
	}
	
// ------------------------------ End Field: exp_end_date --------------------------------------


// ---------------------------- Start Field: exp_end_num -------------------------------------- 

	/** 
	* Sets a value to `exp_end_num` variable
	* @access public
	*/

	public function setExpEndNum($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('exp_end_num', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `exp_end_num` variable
	* @access public
	*/

	public function getExpEndNum() {
		return $this->exp_end_num;
	}
	
// ------------------------------ End Field: exp_end_num --------------------------------------


// ---------------------------- Start Field: end_date -------------------------------------- 

	/** 
	* Sets a value to `end_date` variable
	* @access public
	*/

	public function setEndDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('end_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `end_date` variable
	* @access public
	*/

	public function getEndDate() {
		return $this->end_date;
	}
	
// ------------------------------ End Field: end_date --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'beg_date' => (object) array(
										'Field'=>'beg_date',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'beg_operations' => (object) array(
										'Field'=>'beg_operations',
										'Type'=>'decimal(20,3)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'beg_investments' => (object) array(
										'Field'=>'beg_investments',
										'Type'=>'decimal(20,3)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'col_start_date' => (object) array(
										'Field'=>'col_start_date',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'col_start_num' => (object) array(
										'Field'=>'col_start_num',
										'Type'=>'int(5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'col_end_date' => (object) array(
										'Field'=>'col_end_date',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'col_end_num' => (object) array(
										'Field'=>'col_end_num',
										'Type'=>'int(5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'exp_start_date' => (object) array(
										'Field'=>'exp_start_date',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'exp_start_num' => (object) array(
										'Field'=>'exp_start_num',
										'Type'=>'int(5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'exp_end_date' => (object) array(
										'Field'=>'exp_end_date',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'exp_end_num' => (object) array(
										'Field'=>'exp_end_num',
										'Type'=>'int(5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'end_date' => (object) array(
										'Field'=>'end_date',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `cash_report` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'beg_date' => "ALTER TABLE  `cash_report` ADD  `beg_date` date NULL   ;",
			'beg_operations' => "ALTER TABLE  `cash_report` ADD  `beg_operations` decimal(20,3) NULL   ;",
			'beg_investments' => "ALTER TABLE  `cash_report` ADD  `beg_investments` decimal(20,3) NULL   ;",
			'col_start_date' => "ALTER TABLE  `cash_report` ADD  `col_start_date` date NULL   ;",
			'col_start_num' => "ALTER TABLE  `cash_report` ADD  `col_start_num` int(5) NULL   ;",
			'col_end_date' => "ALTER TABLE  `cash_report` ADD  `col_end_date` date NULL   ;",
			'col_end_num' => "ALTER TABLE  `cash_report` ADD  `col_end_num` int(5) NULL   ;",
			'exp_start_date' => "ALTER TABLE  `cash_report` ADD  `exp_start_date` date NULL   ;",
			'exp_start_num' => "ALTER TABLE  `cash_report` ADD  `exp_start_num` int(5) NULL   ;",
			'exp_end_date' => "ALTER TABLE  `cash_report` ADD  `exp_end_date` date NULL   ;",
			'exp_end_num' => "ALTER TABLE  `cash_report` ADD  `exp_end_num` int(5) NULL   ;",
			'end_date' => "ALTER TABLE  `cash_report` ADD  `end_date` date NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Cash_report_model.php */
/* Location: ./application/models/Cash_report_model.php */

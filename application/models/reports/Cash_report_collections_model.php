<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Cash_report_collections_model Class
 *
 * Manipulates `cash_report_collections` table on database

CREATE TABLE `cash_report_collections` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `report_id` int(10) NOT NULL,
  `receipt_id` text NOT NULL,
  `type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin;

ALTER TABLE  `cash_report_collections` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `cash_report_collections` ADD  `report_id` int(10) NOT NULL   ;
ALTER TABLE  `cash_report_collections` ADD  `receipt_id` text NOT NULL   ;
ALTER TABLE  `cash_report_collections` ADD  `type` text NOT NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Cash_report_collections_model extends MY_Model {

	protected $id;
	protected $report_id;
	protected $receipt_id;
	protected $type;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'cash_report_collections';
		$this->_short_name = 'cash_report_collections';
		$this->_fields = array("id","report_id","receipt_id","type");
		$this->_required = array("report_id","receipt_id","type");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: report_id -------------------------------------- 

	/** 
	* Sets a value to `report_id` variable
	* @access public
	*/

	public function setReportId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('report_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `report_id` variable
	* @access public
	*/

	public function getReportId() {
		return $this->report_id;
	}
	
// ------------------------------ End Field: report_id --------------------------------------


// ---------------------------- Start Field: receipt_id -------------------------------------- 

	/** 
	* Sets a value to `receipt_id` variable
	* @access public
	*/

	public function setReceiptId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('receipt_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `receipt_id` variable
	* @access public
	*/

	public function getReceiptId() {
		return $this->receipt_id;
	}
	
// ------------------------------ End Field: receipt_id --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

	public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

	public function getType() {
		return $this->type;
	}
	
// ------------------------------ End Field: type --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'report_id' => (object) array(
										'Field'=>'report_id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'receipt_id' => (object) array(
										'Field'=>'receipt_id',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'type' => (object) array(
										'Field'=>'type',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `cash_report_collections` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'report_id' => "ALTER TABLE  `cash_report_collections` ADD  `report_id` int(10) NOT NULL   ;",
			'receipt_id' => "ALTER TABLE  `cash_report_collections` ADD  `receipt_id` text NOT NULL   ;",
			'type' => "ALTER TABLE  `cash_report_collections` ADD  `type` text NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Cash_report_collections_model.php */
/* Location: ./application/models/Cash_report_collections_model.php */

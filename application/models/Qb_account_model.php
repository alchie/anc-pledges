<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_account_model Class
 *
 * Manipulates `qb_account` table on database

CREATE TABLE `qb_account` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ListID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Name` varchar(31) DEFAULT NULL,
  `FullName` varchar(255) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT '0',
  `Parent_ListID` varchar(40) DEFAULT NULL,
  `Parent_FullName` varchar(255) DEFAULT NULL,
  `Sublevel` int(10) unsigned DEFAULT '0',
  `AccountType` varchar(40) DEFAULT NULL,
  `SpecialAccountType` varchar(40) DEFAULT NULL,
  `AccountNumber` varchar(7) DEFAULT NULL,
  `BankNumber` varchar(25) DEFAULT NULL,
  `Descrip` text,
  `Balance` decimal(10,2) DEFAULT NULL,
  `TotalBalance` decimal(10,2) DEFAULT NULL,
  `CashFlowClassification` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `FullName` (`FullName`),
  KEY `IsActive` (`IsActive`),
  KEY `Parent_ListID` (`Parent_ListID`),
  KEY `ListID` (`ListID`),
  KEY `Name` (`Name`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_account` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_account` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_account` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_account` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_account` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_account` ADD  `Name` varchar(31) NULL   ;
ALTER TABLE  `qb_account` ADD  `FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_account` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_account` ADD  `Parent_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_account` ADD  `Parent_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_account` ADD  `Sublevel` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_account` ADD  `AccountType` varchar(40) NULL   ;
ALTER TABLE  `qb_account` ADD  `SpecialAccountType` varchar(40) NULL   ;
ALTER TABLE  `qb_account` ADD  `AccountNumber` varchar(7) NULL   ;
ALTER TABLE  `qb_account` ADD  `BankNumber` varchar(25) NULL   ;
ALTER TABLE  `qb_account` ADD  `Descrip` text NULL   ;
ALTER TABLE  `qb_account` ADD  `Balance` decimal(10,2) NULL   ;
ALTER TABLE  `qb_account` ADD  `TotalBalance` decimal(10,2) NULL   ;
ALTER TABLE  `qb_account` ADD  `CashFlowClassification` varchar(40) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_account_model extends MY_Model {

	protected $qbxml_id;
	protected $ListID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Name;
	protected $FullName;
	protected $IsActive;
	protected $Parent_ListID;
	protected $Parent_FullName;
	protected $Sublevel;
	protected $AccountType;
	protected $SpecialAccountType;
	protected $AccountNumber;
	protected $BankNumber;
	protected $Descrip;
	protected $Balance;
	protected $TotalBalance;
	protected $CashFlowClassification;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_account';
		$this->_short_name = 'qb_account';
		$this->_fields = array("qbxml_id","ListID","TimeCreated","TimeModified","EditSequence","Name","FullName","IsActive","Parent_ListID","Parent_FullName","Sublevel","AccountType","SpecialAccountType","AccountNumber","BankNumber","Descrip","Balance","TotalBalance","CashFlowClassification");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: FullName -------------------------------------- 

	/** 
	* Sets a value to `FullName` variable
	* @access public
	*/

	public function setFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FullName` variable
	* @access public
	*/

	public function getFullname() {
		return $this->FullName;
	}

	public function get_FullName() {
		return $this->FullName;
	}

	
// ------------------------------ End Field: FullName --------------------------------------


// ---------------------------- Start Field: IsActive -------------------------------------- 

	/** 
	* Sets a value to `IsActive` variable
	* @access public
	*/

	public function setIsactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsActive` variable
	* @access public
	*/

	public function getIsactive() {
		return $this->IsActive;
	}

	public function get_IsActive() {
		return $this->IsActive;
	}

	
// ------------------------------ End Field: IsActive --------------------------------------


// ---------------------------- Start Field: Parent_ListID -------------------------------------- 

	/** 
	* Sets a value to `Parent_ListID` variable
	* @access public
	*/

	public function setParentListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Parent_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Parent_ListID` variable
	* @access public
	*/

	public function getParentListid() {
		return $this->Parent_ListID;
	}

	public function get_Parent_ListID() {
		return $this->Parent_ListID;
	}

	
// ------------------------------ End Field: Parent_ListID --------------------------------------


// ---------------------------- Start Field: Parent_FullName -------------------------------------- 

	/** 
	* Sets a value to `Parent_FullName` variable
	* @access public
	*/

	public function setParentFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Parent_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Parent_FullName` variable
	* @access public
	*/

	public function getParentFullname() {
		return $this->Parent_FullName;
	}

	public function get_Parent_FullName() {
		return $this->Parent_FullName;
	}

	
// ------------------------------ End Field: Parent_FullName --------------------------------------


// ---------------------------- Start Field: Sublevel -------------------------------------- 

	/** 
	* Sets a value to `Sublevel` variable
	* @access public
	*/

	public function setSublevel($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Sublevel', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Sublevel($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Sublevel', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Sublevel` variable
	* @access public
	*/

	public function getSublevel() {
		return $this->Sublevel;
	}

	public function get_Sublevel() {
		return $this->Sublevel;
	}

	
// ------------------------------ End Field: Sublevel --------------------------------------


// ---------------------------- Start Field: AccountType -------------------------------------- 

	/** 
	* Sets a value to `AccountType` variable
	* @access public
	*/

	public function setAccounttype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AccountType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AccountType` variable
	* @access public
	*/

	public function getAccounttype() {
		return $this->AccountType;
	}

	public function get_AccountType() {
		return $this->AccountType;
	}

	
// ------------------------------ End Field: AccountType --------------------------------------


// ---------------------------- Start Field: SpecialAccountType -------------------------------------- 

	/** 
	* Sets a value to `SpecialAccountType` variable
	* @access public
	*/

	public function setSpecialaccounttype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SpecialAccountType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SpecialAccountType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SpecialAccountType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SpecialAccountType` variable
	* @access public
	*/

	public function getSpecialaccounttype() {
		return $this->SpecialAccountType;
	}

	public function get_SpecialAccountType() {
		return $this->SpecialAccountType;
	}

	
// ------------------------------ End Field: SpecialAccountType --------------------------------------


// ---------------------------- Start Field: AccountNumber -------------------------------------- 

	/** 
	* Sets a value to `AccountNumber` variable
	* @access public
	*/

	public function setAccountnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AccountNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AccountNumber` variable
	* @access public
	*/

	public function getAccountnumber() {
		return $this->AccountNumber;
	}

	public function get_AccountNumber() {
		return $this->AccountNumber;
	}

	
// ------------------------------ End Field: AccountNumber --------------------------------------


// ---------------------------- Start Field: BankNumber -------------------------------------- 

	/** 
	* Sets a value to `BankNumber` variable
	* @access public
	*/

	public function setBanknumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BankNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BankNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BankNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BankNumber` variable
	* @access public
	*/

	public function getBanknumber() {
		return $this->BankNumber;
	}

	public function get_BankNumber() {
		return $this->BankNumber;
	}

	
// ------------------------------ End Field: BankNumber --------------------------------------


// ---------------------------- Start Field: Descrip -------------------------------------- 

	/** 
	* Sets a value to `Descrip` variable
	* @access public
	*/

	public function setDescrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Descrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Descrip` variable
	* @access public
	*/

	public function getDescrip() {
		return $this->Descrip;
	}

	public function get_Descrip() {
		return $this->Descrip;
	}

	
// ------------------------------ End Field: Descrip --------------------------------------


// ---------------------------- Start Field: Balance -------------------------------------- 

	/** 
	* Sets a value to `Balance` variable
	* @access public
	*/

	public function setBalance($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Balance', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Balance($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Balance', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Balance` variable
	* @access public
	*/

	public function getBalance() {
		return $this->Balance;
	}

	public function get_Balance() {
		return $this->Balance;
	}

	
// ------------------------------ End Field: Balance --------------------------------------


// ---------------------------- Start Field: TotalBalance -------------------------------------- 

	/** 
	* Sets a value to `TotalBalance` variable
	* @access public
	*/

	public function setTotalbalance($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalBalance', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TotalBalance($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalBalance', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TotalBalance` variable
	* @access public
	*/

	public function getTotalbalance() {
		return $this->TotalBalance;
	}

	public function get_TotalBalance() {
		return $this->TotalBalance;
	}

	
// ------------------------------ End Field: TotalBalance --------------------------------------


// ---------------------------- Start Field: CashFlowClassification -------------------------------------- 

	/** 
	* Sets a value to `CashFlowClassification` variable
	* @access public
	*/

	public function setCashflowclassification($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashFlowClassification', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CashFlowClassification($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CashFlowClassification', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CashFlowClassification` variable
	* @access public
	*/

	public function getCashflowclassification() {
		return $this->CashFlowClassification;
	}

	public function get_CashFlowClassification() {
		return $this->CashFlowClassification;
	}

	
// ------------------------------ End Field: CashFlowClassification --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'FullName' => (object) array(
										'Field'=>'FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsActive' => (object) array(
										'Field'=>'IsActive',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'Parent_ListID' => (object) array(
										'Field'=>'Parent_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Parent_FullName' => (object) array(
										'Field'=>'Parent_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Sublevel' => (object) array(
										'Field'=>'Sublevel',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'AccountType' => (object) array(
										'Field'=>'AccountType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SpecialAccountType' => (object) array(
										'Field'=>'SpecialAccountType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AccountNumber' => (object) array(
										'Field'=>'AccountNumber',
										'Type'=>'varchar(7)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BankNumber' => (object) array(
										'Field'=>'BankNumber',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Descrip' => (object) array(
										'Field'=>'Descrip',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Balance' => (object) array(
										'Field'=>'Balance',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TotalBalance' => (object) array(
										'Field'=>'TotalBalance',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CashFlowClassification' => (object) array(
										'Field'=>'CashFlowClassification',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_account` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ListID' => "ALTER TABLE  `qb_account` ADD  `ListID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_account` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_account` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_account` ADD  `EditSequence` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_account` ADD  `Name` varchar(31) NULL   ;",
			'FullName' => "ALTER TABLE  `qb_account` ADD  `FullName` varchar(255) NULL   ;",
			'IsActive' => "ALTER TABLE  `qb_account` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';",
			'Parent_ListID' => "ALTER TABLE  `qb_account` ADD  `Parent_ListID` varchar(40) NULL   ;",
			'Parent_FullName' => "ALTER TABLE  `qb_account` ADD  `Parent_FullName` varchar(255) NULL   ;",
			'Sublevel' => "ALTER TABLE  `qb_account` ADD  `Sublevel` int(10) unsigned NULL   DEFAULT '0';",
			'AccountType' => "ALTER TABLE  `qb_account` ADD  `AccountType` varchar(40) NULL   ;",
			'SpecialAccountType' => "ALTER TABLE  `qb_account` ADD  `SpecialAccountType` varchar(40) NULL   ;",
			'AccountNumber' => "ALTER TABLE  `qb_account` ADD  `AccountNumber` varchar(7) NULL   ;",
			'BankNumber' => "ALTER TABLE  `qb_account` ADD  `BankNumber` varchar(25) NULL   ;",
			'Descrip' => "ALTER TABLE  `qb_account` ADD  `Descrip` text NULL   ;",
			'Balance' => "ALTER TABLE  `qb_account` ADD  `Balance` decimal(10,2) NULL   ;",
			'TotalBalance' => "ALTER TABLE  `qb_account` ADD  `TotalBalance` decimal(10,2) NULL   ;",
			'CashFlowClassification' => "ALTER TABLE  `qb_account` ADD  `CashFlowClassification` varchar(40) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setListid() - ListID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setName() - Name
//setFullname() - FullName
//setIsactive() - IsActive
//setParentListid() - Parent_ListID
//setParentFullname() - Parent_FullName
//setSublevel() - Sublevel
//setAccounttype() - AccountType
//setSpecialaccounttype() - SpecialAccountType
//setAccountnumber() - AccountNumber
//setBanknumber() - BankNumber
//setDescrip() - Descrip
//setBalance() - Balance
//setTotalbalance() - TotalBalance
//setCashflowclassification() - CashFlowClassification

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ListID() - ListID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Name() - Name
//set_FullName() - FullName
//set_IsActive() - IsActive
//set_Parent_ListID() - Parent_ListID
//set_Parent_FullName() - Parent_FullName
//set_Sublevel() - Sublevel
//set_AccountType() - AccountType
//set_SpecialAccountType() - SpecialAccountType
//set_AccountNumber() - AccountNumber
//set_BankNumber() - BankNumber
//set_Descrip() - Descrip
//set_Balance() - Balance
//set_TotalBalance() - TotalBalance
//set_CashFlowClassification() - CashFlowClassification

*/
/* End of file Qb_account_model.php */
/* Location: ./application/models/Qb_account_model.php */

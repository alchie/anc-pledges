<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_receivepayment_model Class
 *
 * Manipulates `qb_receivepayment` table on database

CREATE TABLE `qb_receivepayment` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `Customer_ListID` varchar(40) DEFAULT NULL,
  `Customer_FullName` varchar(255) DEFAULT NULL,
  `ARAccount_ListID` varchar(40) DEFAULT NULL,
  `ARAccount_FullName` varchar(255) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `RefNumber` varchar(20) DEFAULT NULL,
  `TotalAmount` decimal(10,2) DEFAULT NULL,
  `PaymentMethod_ListID` varchar(40) DEFAULT NULL,
  `PaymentMethod_FullName` varchar(255) DEFAULT NULL,
  `Memo` text,
  `DepositToAccount_ListID` varchar(40) DEFAULT NULL,
  `DepositToAccount_FullName` varchar(255) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` varchar(25) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` varchar(41) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` varchar(41) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` varchar(18) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` varchar(24) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` varchar(60) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` varchar(24) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` varchar(32) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` varchar(12) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` varchar(84) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` datetime DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` varchar(16) DEFAULT NULL,
  `UnusedPayment` decimal(10,2) DEFAULT NULL,
  `UnusedCredits` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `Customer_ListID` (`Customer_ListID`),
  KEY `ARAccount_ListID` (`ARAccount_ListID`),
  KEY `TxnDate` (`TxnDate`),
  KEY `RefNumber` (`RefNumber`),
  KEY `PaymentMethod_ListID` (`PaymentMethod_ListID`),
  KEY `DepositToAccount_ListID` (`DepositToAccount_ListID`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_receivepayment` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_receivepayment` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_receivepayment` ADD  `Customer_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `Customer_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `ARAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `ARAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `RefNumber` varchar(20) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `TotalAmount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `PaymentMethod_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `PaymentMethod_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `Memo` text NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `DepositToAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `DepositToAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` varchar(25) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` varchar(41) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` varchar(41) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` varchar(18) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` varchar(24) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` varchar(40) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` int(10) unsigned NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` varchar(60) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` varchar(24) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` varchar(32) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` varchar(12) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` varchar(40) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` varchar(40) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` varchar(84) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` varchar(40) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` datetime NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` varchar(16) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `UnusedPayment` decimal(10,2) NULL   ;
ALTER TABLE  `qb_receivepayment` ADD  `UnusedCredits` decimal(10,2) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_receivepayment_model extends MY_Model {

	protected $qbxml_id;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $Customer_ListID;
	protected $Customer_FullName;
	protected $ARAccount_ListID;
	protected $ARAccount_FullName;
	protected $TxnDate;
	protected $RefNumber;
	protected $TotalAmount;
	protected $PaymentMethod_ListID;
	protected $PaymentMethod_FullName;
	protected $Memo;
	protected $DepositToAccount_ListID;
	protected $DepositToAccount_FullName;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID;
	protected $UnusedPayment;
	protected $UnusedCredits;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_receivepayment';
		$this->_short_name = 'qb_receivepayment';
		$this->_fields = array("qbxml_id","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","Customer_ListID","Customer_FullName","ARAccount_ListID","ARAccount_FullName","TxnDate","RefNumber","TotalAmount","PaymentMethod_ListID","PaymentMethod_FullName","Memo","DepositToAccount_ListID","DepositToAccount_FullName","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber","CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth","CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear","CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode","CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode","CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType","CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode","CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage","CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID","CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber","CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode","CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet","CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip","CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch","CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID","CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode","CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus","CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime","CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp","CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID","UnusedPayment","UnusedCredits");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: Customer_ListID -------------------------------------- 

	/** 
	* Sets a value to `Customer_ListID` variable
	* @access public
	*/

	public function setCustomerListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_ListID` variable
	* @access public
	*/

	public function getCustomerListid() {
		return $this->Customer_ListID;
	}

	public function get_Customer_ListID() {
		return $this->Customer_ListID;
	}

	
// ------------------------------ End Field: Customer_ListID --------------------------------------


// ---------------------------- Start Field: Customer_FullName -------------------------------------- 

	/** 
	* Sets a value to `Customer_FullName` variable
	* @access public
	*/

	public function setCustomerFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_FullName` variable
	* @access public
	*/

	public function getCustomerFullname() {
		return $this->Customer_FullName;
	}

	public function get_Customer_FullName() {
		return $this->Customer_FullName;
	}

	
// ------------------------------ End Field: Customer_FullName --------------------------------------


// ---------------------------- Start Field: ARAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `ARAccount_ListID` variable
	* @access public
	*/

	public function setAraccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ARAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ARAccount_ListID` variable
	* @access public
	*/

	public function getAraccountListid() {
		return $this->ARAccount_ListID;
	}

	public function get_ARAccount_ListID() {
		return $this->ARAccount_ListID;
	}

	
// ------------------------------ End Field: ARAccount_ListID --------------------------------------


// ---------------------------- Start Field: ARAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `ARAccount_FullName` variable
	* @access public
	*/

	public function setAraccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ARAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ARAccount_FullName` variable
	* @access public
	*/

	public function getAraccountFullname() {
		return $this->ARAccount_FullName;
	}

	public function get_ARAccount_FullName() {
		return $this->ARAccount_FullName;
	}

	
// ------------------------------ End Field: ARAccount_FullName --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: TotalAmount -------------------------------------- 

	/** 
	* Sets a value to `TotalAmount` variable
	* @access public
	*/

	public function setTotalamount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TotalAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TotalAmount` variable
	* @access public
	*/

	public function getTotalamount() {
		return $this->TotalAmount;
	}

	public function get_TotalAmount() {
		return $this->TotalAmount;
	}

	
// ------------------------------ End Field: TotalAmount --------------------------------------


// ---------------------------- Start Field: PaymentMethod_ListID -------------------------------------- 

	/** 
	* Sets a value to `PaymentMethod_ListID` variable
	* @access public
	*/

	public function setPaymentmethodListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PaymentMethod_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PaymentMethod_ListID` variable
	* @access public
	*/

	public function getPaymentmethodListid() {
		return $this->PaymentMethod_ListID;
	}

	public function get_PaymentMethod_ListID() {
		return $this->PaymentMethod_ListID;
	}

	
// ------------------------------ End Field: PaymentMethod_ListID --------------------------------------


// ---------------------------- Start Field: PaymentMethod_FullName -------------------------------------- 

	/** 
	* Sets a value to `PaymentMethod_FullName` variable
	* @access public
	*/

	public function setPaymentmethodFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PaymentMethod_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PaymentMethod_FullName` variable
	* @access public
	*/

	public function getPaymentmethodFullname() {
		return $this->PaymentMethod_FullName;
	}

	public function get_PaymentMethod_FullName() {
		return $this->PaymentMethod_FullName;
	}

	
// ------------------------------ End Field: PaymentMethod_FullName --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------


// ---------------------------- Start Field: DepositToAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `DepositToAccount_ListID` variable
	* @access public
	*/

	public function setDeposittoaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DepositToAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DepositToAccount_ListID` variable
	* @access public
	*/

	public function getDeposittoaccountListid() {
		return $this->DepositToAccount_ListID;
	}

	public function get_DepositToAccount_ListID() {
		return $this->DepositToAccount_ListID;
	}

	
// ------------------------------ End Field: DepositToAccount_ListID --------------------------------------


// ---------------------------- Start Field: DepositToAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `DepositToAccount_FullName` variable
	* @access public
	*/

	public function setDeposittoaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DepositToAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DepositToAccount_FullName` variable
	* @access public
	*/

	public function getDeposittoaccountFullname() {
		return $this->DepositToAccount_FullName;
	}

	public function get_DepositToAccount_FullName() {
		return $this->DepositToAccount_FullName;
	}

	
// ------------------------------ End Field: DepositToAccount_FullName --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardnumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoExpirationmonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoExpirationmonth() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoExpirationyear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoExpirationyear() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoNameoncard($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoNameoncard() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardaddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardaddress() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardpostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardpostalcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCommercialcardcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCommercialcardcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoTransactionmode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoTransactionmode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardtxntype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardtxntype() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoResultcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoResultcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoResultmessage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoResultmessage() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoCreditcardtransid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoCreditcardtransid() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoMerchantaccountnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoMerchantaccountnumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoAuthorizationcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoAuthorizationcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoAvsstreet($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoAvsstreet() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoAvszip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoAvszip() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoCardsecuritycodematch($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoCardsecuritycodematch() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoReconbatchid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoReconbatchid() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoPaymentgroupingcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoPaymentgroupingcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoPaymentstatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoPaymentstatus() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationtime($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationtime() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationstamp($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationstamp() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoClienttransid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoClienttransid() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID --------------------------------------


// ---------------------------- Start Field: UnusedPayment -------------------------------------- 

	/** 
	* Sets a value to `UnusedPayment` variable
	* @access public
	*/

	public function setUnusedpayment($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnusedPayment', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnusedPayment($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnusedPayment', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnusedPayment` variable
	* @access public
	*/

	public function getUnusedpayment() {
		return $this->UnusedPayment;
	}

	public function get_UnusedPayment() {
		return $this->UnusedPayment;
	}

	
// ------------------------------ End Field: UnusedPayment --------------------------------------


// ---------------------------- Start Field: UnusedCredits -------------------------------------- 

	/** 
	* Sets a value to `UnusedCredits` variable
	* @access public
	*/

	public function setUnusedcredits($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnusedCredits', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnusedCredits($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnusedCredits', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnusedCredits` variable
	* @access public
	*/

	public function getUnusedcredits() {
		return $this->UnusedCredits;
	}

	public function get_UnusedCredits() {
		return $this->UnusedCredits;
	}

	
// ------------------------------ End Field: UnusedCredits --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'Customer_ListID' => (object) array(
										'Field'=>'Customer_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_FullName' => (object) array(
										'Field'=>'Customer_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ARAccount_ListID' => (object) array(
										'Field'=>'ARAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ARAccount_FullName' => (object) array(
										'Field'=>'ARAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'varchar(20)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TotalAmount' => (object) array(
										'Field'=>'TotalAmount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PaymentMethod_ListID' => (object) array(
										'Field'=>'PaymentMethod_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PaymentMethod_FullName' => (object) array(
										'Field'=>'PaymentMethod_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DepositToAccount_ListID' => (object) array(
										'Field'=>'DepositToAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'DepositToAccount_FullName' => (object) array(
										'Field'=>'DepositToAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode',
										'Type'=>'varchar(18)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode',
										'Type'=>'varchar(24)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage',
										'Type'=>'varchar(60)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID',
										'Type'=>'varchar(24)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber',
										'Type'=>'varchar(32)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode',
										'Type'=>'varchar(12)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID',
										'Type'=>'varchar(84)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID',
										'Type'=>'varchar(16)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'UnusedPayment' => (object) array(
										'Field'=>'UnusedPayment',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'UnusedCredits' => (object) array(
										'Field'=>'UnusedCredits',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_receivepayment` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'TxnID' => "ALTER TABLE  `qb_receivepayment` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_receivepayment` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_receivepayment` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_receivepayment` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_receivepayment` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'Customer_ListID' => "ALTER TABLE  `qb_receivepayment` ADD  `Customer_ListID` varchar(40) NULL   ;",
			'Customer_FullName' => "ALTER TABLE  `qb_receivepayment` ADD  `Customer_FullName` varchar(255) NULL   ;",
			'ARAccount_ListID' => "ALTER TABLE  `qb_receivepayment` ADD  `ARAccount_ListID` varchar(40) NULL   ;",
			'ARAccount_FullName' => "ALTER TABLE  `qb_receivepayment` ADD  `ARAccount_FullName` varchar(255) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_receivepayment` ADD  `TxnDate` date NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_receivepayment` ADD  `RefNumber` varchar(20) NULL   ;",
			'TotalAmount' => "ALTER TABLE  `qb_receivepayment` ADD  `TotalAmount` decimal(10,2) NULL   ;",
			'PaymentMethod_ListID' => "ALTER TABLE  `qb_receivepayment` ADD  `PaymentMethod_ListID` varchar(40) NULL   ;",
			'PaymentMethod_FullName' => "ALTER TABLE  `qb_receivepayment` ADD  `PaymentMethod_FullName` varchar(255) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_receivepayment` ADD  `Memo` text NULL   ;",
			'DepositToAccount_ListID' => "ALTER TABLE  `qb_receivepayment` ADD  `DepositToAccount_ListID` varchar(40) NULL   ;",
			'DepositToAccount_FullName' => "ALTER TABLE  `qb_receivepayment` ADD  `DepositToAccount_FullName` varchar(255) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` varchar(25) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` varchar(41) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` varchar(41) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` varchar(18) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` varchar(24) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` int(10) unsigned NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` varchar(60) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` varchar(24) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` varchar(32) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` varchar(12) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` varchar(84) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` datetime NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID' => "ALTER TABLE  `qb_receivepayment` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` varchar(16) NULL   ;",
			'UnusedPayment' => "ALTER TABLE  `qb_receivepayment` ADD  `UnusedPayment` decimal(10,2) NULL   ;",
			'UnusedCredits' => "ALTER TABLE  `qb_receivepayment` ADD  `UnusedCredits` decimal(10,2) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setCustomerListid() - Customer_ListID
//setCustomerFullname() - Customer_FullName
//setAraccountListid() - ARAccount_ListID
//setAraccountFullname() - ARAccount_FullName
//setTxndate() - TxnDate
//setRefnumber() - RefNumber
//setTotalamount() - TotalAmount
//setPaymentmethodListid() - PaymentMethod_ListID
//setPaymentmethodFullname() - PaymentMethod_FullName
//setMemo() - Memo
//setDeposittoaccountListid() - DepositToAccount_ListID
//setDeposittoaccountFullname() - DepositToAccount_FullName
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardnumber() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber
//setCreditcardtxninfoCreditcardtxninputinfoExpirationmonth() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth
//setCreditcardtxninfoCreditcardtxninputinfoExpirationyear() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear
//setCreditcardtxninfoCreditcardtxninputinfoNameoncard() - CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardaddress() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardpostalcode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode
//setCreditcardtxninfoCreditcardtxninputinfoCommercialcardcode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode
//setCreditcardtxninfoCreditcardtxninputinfoTransactionmode() - CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardtxntype() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType
//setCreditcardtxninfoCreditcardtxnresultinfoResultcode() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode
//setCreditcardtxninfoCreditcardtxnresultinfoResultmessage() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage
//setCreditcardtxninfoCreditcardtxnresultinfoCreditcardtransid() - CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID
//setCreditcardtxninfoCreditcardtxnresultinfoMerchantaccountnumber() - CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber
//setCreditcardtxninfoCreditcardtxnresultinfoAuthorizationcode() - CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode
//setCreditcardtxninfoCreditcardtxnresultinfoAvsstreet() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet
//setCreditcardtxninfoCreditcardtxnresultinfoAvszip() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip
//setCreditcardtxninfoCreditcardtxnresultinfoCardsecuritycodematch() - CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch
//setCreditcardtxninfoCreditcardtxnresultinfoReconbatchid() - CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID
//setCreditcardtxninfoCreditcardtxnresultinfoPaymentgroupingcode() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode
//setCreditcardtxninfoCreditcardtxnresultinfoPaymentstatus() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus
//setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationtime() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime
//setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationstamp() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp
//setCreditcardtxninfoCreditcardtxnresultinfoClienttransid() - CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID
//setUnusedpayment() - UnusedPayment
//setUnusedcredits() - UnusedCredits

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_Customer_ListID() - Customer_ListID
//set_Customer_FullName() - Customer_FullName
//set_ARAccount_ListID() - ARAccount_ListID
//set_ARAccount_FullName() - ARAccount_FullName
//set_TxnDate() - TxnDate
//set_RefNumber() - RefNumber
//set_TotalAmount() - TotalAmount
//set_PaymentMethod_ListID() - PaymentMethod_ListID
//set_PaymentMethod_FullName() - PaymentMethod_FullName
//set_Memo() - Memo
//set_DepositToAccount_ListID() - DepositToAccount_ListID
//set_DepositToAccount_FullName() - DepositToAccount_FullName
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard() - CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode() - CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID() - CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber() - CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode() - CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch() - CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID() - CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID() - CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID
//set_UnusedPayment() - UnusedPayment
//set_UnusedCredits() - UnusedCredits

*/
/* End of file Qb_receivepayment_model.php */
/* Location: ./application/models/Qb_receivepayment_model.php */

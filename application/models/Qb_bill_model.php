<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_bill_model Class
 *
 * Manipulates `qb_bill` table on database

CREATE TABLE `qb_bill` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Tax1Total` text,
  `Tax2Total` text,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `Vendor_ListID` varchar(40) DEFAULT NULL,
  `Vendor_FullName` varchar(255) DEFAULT NULL,
  `APAccount_ListID` varchar(40) DEFAULT NULL,
  `APAccount_FullName` varchar(255) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `DueDate` date DEFAULT NULL,
  `AmountDue` decimal(10,2) DEFAULT NULL,
  `Currency_ListID` varchar(40) DEFAULT NULL,
  `Currency_FullName` varchar(255) DEFAULT NULL,
  `ExchangeRate` text,
  `AmountDueInHomeCurrency` decimal(10,2) DEFAULT NULL,
  `RefNumber` varchar(20) DEFAULT NULL,
  `Terms_ListID` varchar(40) DEFAULT NULL,
  `Terms_FullName` varchar(255) DEFAULT NULL,
  `Memo` text,
  `IsPaid` tinyint(1) DEFAULT '0',
  `OpenAmount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `Vendor_ListID` (`Vendor_ListID`),
  KEY `APAccount_ListID` (`APAccount_ListID`),
  KEY `TxnDate` (`TxnDate`),
  KEY `Currency_ListID` (`Currency_ListID`),
  KEY `RefNumber` (`RefNumber`),
  KEY `Terms_ListID` (`Terms_ListID`),
  KEY `IsPaid` (`IsPaid`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_bill` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_bill` ADD  `Tax1Total` text NULL   ;
ALTER TABLE  `qb_bill` ADD  `Tax2Total` text NULL   ;
ALTER TABLE  `qb_bill` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_bill` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_bill` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_bill` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_bill` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_bill` ADD  `Vendor_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_bill` ADD  `Vendor_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_bill` ADD  `APAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_bill` ADD  `APAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_bill` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_bill` ADD  `DueDate` date NULL   ;
ALTER TABLE  `qb_bill` ADD  `AmountDue` decimal(10,2) NULL   ;
ALTER TABLE  `qb_bill` ADD  `Currency_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_bill` ADD  `Currency_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_bill` ADD  `ExchangeRate` text NULL   ;
ALTER TABLE  `qb_bill` ADD  `AmountDueInHomeCurrency` decimal(10,2) NULL   ;
ALTER TABLE  `qb_bill` ADD  `RefNumber` varchar(20) NULL   ;
ALTER TABLE  `qb_bill` ADD  `Terms_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_bill` ADD  `Terms_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_bill` ADD  `Memo` text NULL   ;
ALTER TABLE  `qb_bill` ADD  `IsPaid` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_bill` ADD  `OpenAmount` decimal(10,2) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_bill_model extends MY_Model {

	protected $qbxml_id;
	protected $Tax1Total;
	protected $Tax2Total;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $Vendor_ListID;
	protected $Vendor_FullName;
	protected $APAccount_ListID;
	protected $APAccount_FullName;
	protected $TxnDate;
	protected $DueDate;
	protected $AmountDue;
	protected $Currency_ListID;
	protected $Currency_FullName;
	protected $ExchangeRate;
	protected $AmountDueInHomeCurrency;
	protected $RefNumber;
	protected $Terms_ListID;
	protected $Terms_FullName;
	protected $Memo;
	protected $IsPaid;
	protected $OpenAmount;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_bill';
		$this->_short_name = 'qb_bill';
		$this->_fields = array("qbxml_id","Tax1Total","Tax2Total","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","Vendor_ListID","Vendor_FullName","APAccount_ListID","APAccount_FullName","TxnDate","DueDate","AmountDue","Currency_ListID","Currency_FullName","ExchangeRate","AmountDueInHomeCurrency","RefNumber","Terms_ListID","Terms_FullName","Memo","IsPaid","OpenAmount");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: Tax1Total -------------------------------------- 

	/** 
	* Sets a value to `Tax1Total` variable
	* @access public
	*/

	public function setTax1Total($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Tax1Total', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Tax1Total($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Tax1Total', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Tax1Total` variable
	* @access public
	*/

	public function getTax1Total() {
		return $this->Tax1Total;
	}

	public function get_Tax1Total() {
		return $this->Tax1Total;
	}

	
// ------------------------------ End Field: Tax1Total --------------------------------------


// ---------------------------- Start Field: Tax2Total -------------------------------------- 

	/** 
	* Sets a value to `Tax2Total` variable
	* @access public
	*/

	public function setTax2Total($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Tax2Total', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Tax2Total($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Tax2Total', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Tax2Total` variable
	* @access public
	*/

	public function getTax2Total() {
		return $this->Tax2Total;
	}

	public function get_Tax2Total() {
		return $this->Tax2Total;
	}

	
// ------------------------------ End Field: Tax2Total --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: Vendor_ListID -------------------------------------- 

	/** 
	* Sets a value to `Vendor_ListID` variable
	* @access public
	*/

	public function setVendorListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vendor_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Vendor_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vendor_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Vendor_ListID` variable
	* @access public
	*/

	public function getVendorListid() {
		return $this->Vendor_ListID;
	}

	public function get_Vendor_ListID() {
		return $this->Vendor_ListID;
	}

	
// ------------------------------ End Field: Vendor_ListID --------------------------------------


// ---------------------------- Start Field: Vendor_FullName -------------------------------------- 

	/** 
	* Sets a value to `Vendor_FullName` variable
	* @access public
	*/

	public function setVendorFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vendor_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Vendor_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vendor_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Vendor_FullName` variable
	* @access public
	*/

	public function getVendorFullname() {
		return $this->Vendor_FullName;
	}

	public function get_Vendor_FullName() {
		return $this->Vendor_FullName;
	}

	
// ------------------------------ End Field: Vendor_FullName --------------------------------------


// ---------------------------- Start Field: APAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `APAccount_ListID` variable
	* @access public
	*/

	public function setApaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_APAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `APAccount_ListID` variable
	* @access public
	*/

	public function getApaccountListid() {
		return $this->APAccount_ListID;
	}

	public function get_APAccount_ListID() {
		return $this->APAccount_ListID;
	}

	
// ------------------------------ End Field: APAccount_ListID --------------------------------------


// ---------------------------- Start Field: APAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `APAccount_FullName` variable
	* @access public
	*/

	public function setApaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_APAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `APAccount_FullName` variable
	* @access public
	*/

	public function getApaccountFullname() {
		return $this->APAccount_FullName;
	}

	public function get_APAccount_FullName() {
		return $this->APAccount_FullName;
	}

	
// ------------------------------ End Field: APAccount_FullName --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: DueDate -------------------------------------- 

	/** 
	* Sets a value to `DueDate` variable
	* @access public
	*/

	public function setDuedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DueDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DueDate` variable
	* @access public
	*/

	public function getDuedate() {
		return $this->DueDate;
	}

	public function get_DueDate() {
		return $this->DueDate;
	}

	
// ------------------------------ End Field: DueDate --------------------------------------


// ---------------------------- Start Field: AmountDue -------------------------------------- 

	/** 
	* Sets a value to `AmountDue` variable
	* @access public
	*/

	public function setAmountdue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AmountDue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AmountDue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AmountDue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AmountDue` variable
	* @access public
	*/

	public function getAmountdue() {
		return $this->AmountDue;
	}

	public function get_AmountDue() {
		return $this->AmountDue;
	}

	
// ------------------------------ End Field: AmountDue --------------------------------------


// ---------------------------- Start Field: Currency_ListID -------------------------------------- 

	/** 
	* Sets a value to `Currency_ListID` variable
	* @access public
	*/

	public function setCurrencyListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_ListID` variable
	* @access public
	*/

	public function getCurrencyListid() {
		return $this->Currency_ListID;
	}

	public function get_Currency_ListID() {
		return $this->Currency_ListID;
	}

	
// ------------------------------ End Field: Currency_ListID --------------------------------------


// ---------------------------- Start Field: Currency_FullName -------------------------------------- 

	/** 
	* Sets a value to `Currency_FullName` variable
	* @access public
	*/

	public function setCurrencyFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_FullName` variable
	* @access public
	*/

	public function getCurrencyFullname() {
		return $this->Currency_FullName;
	}

	public function get_Currency_FullName() {
		return $this->Currency_FullName;
	}

	
// ------------------------------ End Field: Currency_FullName --------------------------------------


// ---------------------------- Start Field: ExchangeRate -------------------------------------- 

	/** 
	* Sets a value to `ExchangeRate` variable
	* @access public
	*/

	public function setExchangerate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ExchangeRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ExchangeRate` variable
	* @access public
	*/

	public function getExchangerate() {
		return $this->ExchangeRate;
	}

	public function get_ExchangeRate() {
		return $this->ExchangeRate;
	}

	
// ------------------------------ End Field: ExchangeRate --------------------------------------


// ---------------------------- Start Field: AmountDueInHomeCurrency -------------------------------------- 

	/** 
	* Sets a value to `AmountDueInHomeCurrency` variable
	* @access public
	*/

	public function setAmountdueinhomecurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AmountDueInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AmountDueInHomeCurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AmountDueInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AmountDueInHomeCurrency` variable
	* @access public
	*/

	public function getAmountdueinhomecurrency() {
		return $this->AmountDueInHomeCurrency;
	}

	public function get_AmountDueInHomeCurrency() {
		return $this->AmountDueInHomeCurrency;
	}

	
// ------------------------------ End Field: AmountDueInHomeCurrency --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: Terms_ListID -------------------------------------- 

	/** 
	* Sets a value to `Terms_ListID` variable
	* @access public
	*/

	public function setTermsListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Terms_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Terms_ListID` variable
	* @access public
	*/

	public function getTermsListid() {
		return $this->Terms_ListID;
	}

	public function get_Terms_ListID() {
		return $this->Terms_ListID;
	}

	
// ------------------------------ End Field: Terms_ListID --------------------------------------


// ---------------------------- Start Field: Terms_FullName -------------------------------------- 

	/** 
	* Sets a value to `Terms_FullName` variable
	* @access public
	*/

	public function setTermsFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Terms_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Terms_FullName` variable
	* @access public
	*/

	public function getTermsFullname() {
		return $this->Terms_FullName;
	}

	public function get_Terms_FullName() {
		return $this->Terms_FullName;
	}

	
// ------------------------------ End Field: Terms_FullName --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------


// ---------------------------- Start Field: IsPaid -------------------------------------- 

	/** 
	* Sets a value to `IsPaid` variable
	* @access public
	*/

	public function setIspaid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPaid', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsPaid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPaid', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsPaid` variable
	* @access public
	*/

	public function getIspaid() {
		return $this->IsPaid;
	}

	public function get_IsPaid() {
		return $this->IsPaid;
	}

	
// ------------------------------ End Field: IsPaid --------------------------------------


// ---------------------------- Start Field: OpenAmount -------------------------------------- 

	/** 
	* Sets a value to `OpenAmount` variable
	* @access public
	*/

	public function setOpenamount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OpenAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OpenAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OpenAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OpenAmount` variable
	* @access public
	*/

	public function getOpenamount() {
		return $this->OpenAmount;
	}

	public function get_OpenAmount() {
		return $this->OpenAmount;
	}

	
// ------------------------------ End Field: OpenAmount --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'Tax1Total' => (object) array(
										'Field'=>'Tax1Total',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Tax2Total' => (object) array(
										'Field'=>'Tax2Total',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'Vendor_ListID' => (object) array(
										'Field'=>'Vendor_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Vendor_FullName' => (object) array(
										'Field'=>'Vendor_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'APAccount_ListID' => (object) array(
										'Field'=>'APAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'APAccount_FullName' => (object) array(
										'Field'=>'APAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'DueDate' => (object) array(
										'Field'=>'DueDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AmountDue' => (object) array(
										'Field'=>'AmountDue',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_ListID' => (object) array(
										'Field'=>'Currency_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_FullName' => (object) array(
										'Field'=>'Currency_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ExchangeRate' => (object) array(
										'Field'=>'ExchangeRate',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AmountDueInHomeCurrency' => (object) array(
										'Field'=>'AmountDueInHomeCurrency',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'varchar(20)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Terms_ListID' => (object) array(
										'Field'=>'Terms_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Terms_FullName' => (object) array(
										'Field'=>'Terms_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsPaid' => (object) array(
										'Field'=>'IsPaid',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'OpenAmount' => (object) array(
										'Field'=>'OpenAmount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_bill` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'Tax1Total' => "ALTER TABLE  `qb_bill` ADD  `Tax1Total` text NULL   ;",
			'Tax2Total' => "ALTER TABLE  `qb_bill` ADD  `Tax2Total` text NULL   ;",
			'TxnID' => "ALTER TABLE  `qb_bill` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_bill` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_bill` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_bill` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_bill` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'Vendor_ListID' => "ALTER TABLE  `qb_bill` ADD  `Vendor_ListID` varchar(40) NULL   ;",
			'Vendor_FullName' => "ALTER TABLE  `qb_bill` ADD  `Vendor_FullName` varchar(255) NULL   ;",
			'APAccount_ListID' => "ALTER TABLE  `qb_bill` ADD  `APAccount_ListID` varchar(40) NULL   ;",
			'APAccount_FullName' => "ALTER TABLE  `qb_bill` ADD  `APAccount_FullName` varchar(255) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_bill` ADD  `TxnDate` date NULL   ;",
			'DueDate' => "ALTER TABLE  `qb_bill` ADD  `DueDate` date NULL   ;",
			'AmountDue' => "ALTER TABLE  `qb_bill` ADD  `AmountDue` decimal(10,2) NULL   ;",
			'Currency_ListID' => "ALTER TABLE  `qb_bill` ADD  `Currency_ListID` varchar(40) NULL   ;",
			'Currency_FullName' => "ALTER TABLE  `qb_bill` ADD  `Currency_FullName` varchar(255) NULL   ;",
			'ExchangeRate' => "ALTER TABLE  `qb_bill` ADD  `ExchangeRate` text NULL   ;",
			'AmountDueInHomeCurrency' => "ALTER TABLE  `qb_bill` ADD  `AmountDueInHomeCurrency` decimal(10,2) NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_bill` ADD  `RefNumber` varchar(20) NULL   ;",
			'Terms_ListID' => "ALTER TABLE  `qb_bill` ADD  `Terms_ListID` varchar(40) NULL   ;",
			'Terms_FullName' => "ALTER TABLE  `qb_bill` ADD  `Terms_FullName` varchar(255) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_bill` ADD  `Memo` text NULL   ;",
			'IsPaid' => "ALTER TABLE  `qb_bill` ADD  `IsPaid` tinyint(1) NULL   DEFAULT '0';",
			'OpenAmount' => "ALTER TABLE  `qb_bill` ADD  `OpenAmount` decimal(10,2) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTax1Total() - Tax1Total
//setTax2Total() - Tax2Total
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setVendorListid() - Vendor_ListID
//setVendorFullname() - Vendor_FullName
//setApaccountListid() - APAccount_ListID
//setApaccountFullname() - APAccount_FullName
//setTxndate() - TxnDate
//setDuedate() - DueDate
//setAmountdue() - AmountDue
//setCurrencyListid() - Currency_ListID
//setCurrencyFullname() - Currency_FullName
//setExchangerate() - ExchangeRate
//setAmountdueinhomecurrency() - AmountDueInHomeCurrency
//setRefnumber() - RefNumber
//setTermsListid() - Terms_ListID
//setTermsFullname() - Terms_FullName
//setMemo() - Memo
//setIspaid() - IsPaid
//setOpenamount() - OpenAmount

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_Tax1Total() - Tax1Total
//set_Tax2Total() - Tax2Total
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_Vendor_ListID() - Vendor_ListID
//set_Vendor_FullName() - Vendor_FullName
//set_APAccount_ListID() - APAccount_ListID
//set_APAccount_FullName() - APAccount_FullName
//set_TxnDate() - TxnDate
//set_DueDate() - DueDate
//set_AmountDue() - AmountDue
//set_Currency_ListID() - Currency_ListID
//set_Currency_FullName() - Currency_FullName
//set_ExchangeRate() - ExchangeRate
//set_AmountDueInHomeCurrency() - AmountDueInHomeCurrency
//set_RefNumber() - RefNumber
//set_Terms_ListID() - Terms_ListID
//set_Terms_FullName() - Terms_FullName
//set_Memo() - Memo
//set_IsPaid() - IsPaid
//set_OpenAmount() - OpenAmount

*/
/* End of file Qb_bill_model.php */
/* Location: ./application/models/Qb_bill_model.php */

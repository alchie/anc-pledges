<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_deposit_depositline_model Class
 *
 * Manipulates `qb_deposit_depositline` table on database

CREATE TABLE `qb_deposit_depositline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Deposit_TxnID` varchar(40) DEFAULT NULL,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `TxnType` varchar(40) DEFAULT NULL,
  `TxnID` varchar(40) DEFAULT NULL,
  `TxnLineID` varchar(40) DEFAULT NULL,
  `PaymentTxnLineID` varchar(40) DEFAULT NULL,
  `Entity_ListID` varchar(40) DEFAULT NULL,
  `Entity_FullName` varchar(255) DEFAULT NULL,
  `Account_ListID` varchar(40) DEFAULT NULL,
  `Account_FullName` varchar(255) DEFAULT NULL,
  `Memo` text,
  `CheckNumber` text,
  `PaymentMethod_ListID` varchar(40) DEFAULT NULL,
  `PaymentMethod_FullName` varchar(255) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `Deposit_TxnID` (`Deposit_TxnID`),
  KEY `TxnType` (`TxnType`),
  KEY `TxnLineID` (`TxnLineID`),
  KEY `PaymentTxnLineID` (`PaymentTxnLineID`),
  KEY `Entity_ListID` (`Entity_ListID`),
  KEY `Account_ListID` (`Account_ListID`),
  KEY `PaymentMethod_ListID` (`PaymentMethod_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM AUTO_INCREMENT=884 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_deposit_depositline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_deposit_depositline` ADD  `Deposit_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_deposit_depositline` ADD  `TxnType` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `TxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `PaymentTxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `Entity_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `Entity_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `Account_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `Account_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `Memo` text NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `CheckNumber` text NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `PaymentMethod_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `PaymentMethod_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_deposit_depositline` ADD  `Amount` decimal(10,2) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_deposit_depositline_model extends MY_Model {

	protected $qbxml_id;
	protected $Deposit_TxnID;
	protected $SortOrder;
	protected $TxnType;
	protected $TxnID;
	protected $TxnLineID;
	protected $PaymentTxnLineID;
	protected $Entity_ListID;
	protected $Entity_FullName;
	protected $Account_ListID;
	protected $Account_FullName;
	protected $Memo;
	protected $CheckNumber;
	protected $PaymentMethod_ListID;
	protected $PaymentMethod_FullName;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $Amount;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_deposit_depositline';
		$this->_short_name = 'qb_deposit_depositline';
		$this->_fields = array("qbxml_id","Deposit_TxnID","SortOrder","TxnType","TxnID","TxnLineID","PaymentTxnLineID","Entity_ListID","Entity_FullName","Account_ListID","Account_FullName","Memo","CheckNumber","PaymentMethod_ListID","PaymentMethod_FullName","Class_ListID","Class_FullName","Amount");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: Deposit_TxnID -------------------------------------- 

	/** 
	* Sets a value to `Deposit_TxnID` variable
	* @access public
	*/

	public function setDepositTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Deposit_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Deposit_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Deposit_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Deposit_TxnID` variable
	* @access public
	*/

	public function getDepositTxnid() {
		return $this->Deposit_TxnID;
	}

	public function get_Deposit_TxnID() {
		return $this->Deposit_TxnID;
	}

	
// ------------------------------ End Field: Deposit_TxnID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: TxnType -------------------------------------- 

	/** 
	* Sets a value to `TxnType` variable
	* @access public
	*/

	public function setTxntype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnType` variable
	* @access public
	*/

	public function getTxntype() {
		return $this->TxnType;
	}

	public function get_TxnType() {
		return $this->TxnType;
	}

	
// ------------------------------ End Field: TxnType --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `TxnLineID` variable
	* @access public
	*/

	public function setTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnLineID` variable
	* @access public
	*/

	public function getTxnlineid() {
		return $this->TxnLineID;
	}

	public function get_TxnLineID() {
		return $this->TxnLineID;
	}

	
// ------------------------------ End Field: TxnLineID --------------------------------------


// ---------------------------- Start Field: PaymentTxnLineID -------------------------------------- 

	/** 
	* Sets a value to `PaymentTxnLineID` variable
	* @access public
	*/

	public function setPaymenttxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentTxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PaymentTxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentTxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PaymentTxnLineID` variable
	* @access public
	*/

	public function getPaymenttxnlineid() {
		return $this->PaymentTxnLineID;
	}

	public function get_PaymentTxnLineID() {
		return $this->PaymentTxnLineID;
	}

	
// ------------------------------ End Field: PaymentTxnLineID --------------------------------------


// ---------------------------- Start Field: Entity_ListID -------------------------------------- 

	/** 
	* Sets a value to `Entity_ListID` variable
	* @access public
	*/

	public function setEntityListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Entity_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Entity_ListID` variable
	* @access public
	*/

	public function getEntityListid() {
		return $this->Entity_ListID;
	}

	public function get_Entity_ListID() {
		return $this->Entity_ListID;
	}

	
// ------------------------------ End Field: Entity_ListID --------------------------------------


// ---------------------------- Start Field: Entity_FullName -------------------------------------- 

	/** 
	* Sets a value to `Entity_FullName` variable
	* @access public
	*/

	public function setEntityFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Entity_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Entity_FullName` variable
	* @access public
	*/

	public function getEntityFullname() {
		return $this->Entity_FullName;
	}

	public function get_Entity_FullName() {
		return $this->Entity_FullName;
	}

	
// ------------------------------ End Field: Entity_FullName --------------------------------------


// ---------------------------- Start Field: Account_ListID -------------------------------------- 

	/** 
	* Sets a value to `Account_ListID` variable
	* @access public
	*/

	public function setAccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Account_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Account_ListID` variable
	* @access public
	*/

	public function getAccountListid() {
		return $this->Account_ListID;
	}

	public function get_Account_ListID() {
		return $this->Account_ListID;
	}

	
// ------------------------------ End Field: Account_ListID --------------------------------------


// ---------------------------- Start Field: Account_FullName -------------------------------------- 

	/** 
	* Sets a value to `Account_FullName` variable
	* @access public
	*/

	public function setAccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Account_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Account_FullName` variable
	* @access public
	*/

	public function getAccountFullname() {
		return $this->Account_FullName;
	}

	public function get_Account_FullName() {
		return $this->Account_FullName;
	}

	
// ------------------------------ End Field: Account_FullName --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------


// ---------------------------- Start Field: CheckNumber -------------------------------------- 

	/** 
	* Sets a value to `CheckNumber` variable
	* @access public
	*/

	public function setChecknumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CheckNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CheckNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CheckNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CheckNumber` variable
	* @access public
	*/

	public function getChecknumber() {
		return $this->CheckNumber;
	}

	public function get_CheckNumber() {
		return $this->CheckNumber;
	}

	
// ------------------------------ End Field: CheckNumber --------------------------------------


// ---------------------------- Start Field: PaymentMethod_ListID -------------------------------------- 

	/** 
	* Sets a value to `PaymentMethod_ListID` variable
	* @access public
	*/

	public function setPaymentmethodListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PaymentMethod_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PaymentMethod_ListID` variable
	* @access public
	*/

	public function getPaymentmethodListid() {
		return $this->PaymentMethod_ListID;
	}

	public function get_PaymentMethod_ListID() {
		return $this->PaymentMethod_ListID;
	}

	
// ------------------------------ End Field: PaymentMethod_ListID --------------------------------------


// ---------------------------- Start Field: PaymentMethod_FullName -------------------------------------- 

	/** 
	* Sets a value to `PaymentMethod_FullName` variable
	* @access public
	*/

	public function setPaymentmethodFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PaymentMethod_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PaymentMethod_FullName` variable
	* @access public
	*/

	public function getPaymentmethodFullname() {
		return $this->PaymentMethod_FullName;
	}

	public function get_PaymentMethod_FullName() {
		return $this->PaymentMethod_FullName;
	}

	
// ------------------------------ End Field: PaymentMethod_FullName --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'Deposit_TxnID' => (object) array(
										'Field'=>'Deposit_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnType' => (object) array(
										'Field'=>'TxnType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnLineID' => (object) array(
										'Field'=>'TxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PaymentTxnLineID' => (object) array(
										'Field'=>'PaymentTxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Entity_ListID' => (object) array(
										'Field'=>'Entity_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Entity_FullName' => (object) array(
										'Field'=>'Entity_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Account_ListID' => (object) array(
										'Field'=>'Account_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Account_FullName' => (object) array(
										'Field'=>'Account_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CheckNumber' => (object) array(
										'Field'=>'CheckNumber',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PaymentMethod_ListID' => (object) array(
										'Field'=>'PaymentMethod_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PaymentMethod_FullName' => (object) array(
										'Field'=>'PaymentMethod_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_deposit_depositline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'Deposit_TxnID' => "ALTER TABLE  `qb_deposit_depositline` ADD  `Deposit_TxnID` varchar(40) NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_deposit_depositline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'TxnType' => "ALTER TABLE  `qb_deposit_depositline` ADD  `TxnType` varchar(40) NULL   ;",
			'TxnID' => "ALTER TABLE  `qb_deposit_depositline` ADD  `TxnID` varchar(40) NULL   ;",
			'TxnLineID' => "ALTER TABLE  `qb_deposit_depositline` ADD  `TxnLineID` varchar(40) NULL   ;",
			'PaymentTxnLineID' => "ALTER TABLE  `qb_deposit_depositline` ADD  `PaymentTxnLineID` varchar(40) NULL   ;",
			'Entity_ListID' => "ALTER TABLE  `qb_deposit_depositline` ADD  `Entity_ListID` varchar(40) NULL   ;",
			'Entity_FullName' => "ALTER TABLE  `qb_deposit_depositline` ADD  `Entity_FullName` varchar(255) NULL   ;",
			'Account_ListID' => "ALTER TABLE  `qb_deposit_depositline` ADD  `Account_ListID` varchar(40) NULL   ;",
			'Account_FullName' => "ALTER TABLE  `qb_deposit_depositline` ADD  `Account_FullName` varchar(255) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_deposit_depositline` ADD  `Memo` text NULL   ;",
			'CheckNumber' => "ALTER TABLE  `qb_deposit_depositline` ADD  `CheckNumber` text NULL   ;",
			'PaymentMethod_ListID' => "ALTER TABLE  `qb_deposit_depositline` ADD  `PaymentMethod_ListID` varchar(40) NULL   ;",
			'PaymentMethod_FullName' => "ALTER TABLE  `qb_deposit_depositline` ADD  `PaymentMethod_FullName` varchar(255) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_deposit_depositline` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_deposit_depositline` ADD  `Class_FullName` varchar(255) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_deposit_depositline` ADD  `Amount` decimal(10,2) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setDepositTxnid() - Deposit_TxnID
//setSortorder() - SortOrder
//setTxntype() - TxnType
//setTxnid() - TxnID
//setTxnlineid() - TxnLineID
//setPaymenttxnlineid() - PaymentTxnLineID
//setEntityListid() - Entity_ListID
//setEntityFullname() - Entity_FullName
//setAccountListid() - Account_ListID
//setAccountFullname() - Account_FullName
//setMemo() - Memo
//setChecknumber() - CheckNumber
//setPaymentmethodListid() - PaymentMethod_ListID
//setPaymentmethodFullname() - PaymentMethod_FullName
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setAmount() - Amount

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_Deposit_TxnID() - Deposit_TxnID
//set_SortOrder() - SortOrder
//set_TxnType() - TxnType
//set_TxnID() - TxnID
//set_TxnLineID() - TxnLineID
//set_PaymentTxnLineID() - PaymentTxnLineID
//set_Entity_ListID() - Entity_ListID
//set_Entity_FullName() - Entity_FullName
//set_Account_ListID() - Account_ListID
//set_Account_FullName() - Account_FullName
//set_Memo() - Memo
//set_CheckNumber() - CheckNumber
//set_PaymentMethod_ListID() - PaymentMethod_ListID
//set_PaymentMethod_FullName() - PaymentMethod_FullName
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_Amount() - Amount

*/
/* End of file Qb_deposit_depositline_model.php */
/* Location: ./application/models/Qb_deposit_depositline_model.php */

<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_billtopay_model Class
 *
 * Manipulates `qb_billtopay` table on database

CREATE TABLE `qb_billtopay` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TxnID` varchar(40) DEFAULT NULL,
  `TxnType` varchar(40) DEFAULT NULL,
  `APAccount_ListID` varchar(40) DEFAULT NULL,
  `APAccount_FullName` varchar(255) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `RefNumber` text,
  `DueDate` date DEFAULT NULL,
  `AmountDue` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `TxnType` (`TxnType`),
  KEY `APAccount_ListID` (`APAccount_ListID`),
  KEY `TxnDate` (`TxnDate`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_billtopay` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_billtopay` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_billtopay` ADD  `TxnType` varchar(40) NULL   ;
ALTER TABLE  `qb_billtopay` ADD  `APAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_billtopay` ADD  `APAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_billtopay` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_billtopay` ADD  `RefNumber` text NULL   ;
ALTER TABLE  `qb_billtopay` ADD  `DueDate` date NULL   ;
ALTER TABLE  `qb_billtopay` ADD  `AmountDue` decimal(10,2) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_billtopay_model extends MY_Model {

	protected $qbxml_id;
	protected $TxnID;
	protected $TxnType;
	protected $APAccount_ListID;
	protected $APAccount_FullName;
	protected $TxnDate;
	protected $RefNumber;
	protected $DueDate;
	protected $AmountDue;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_billtopay';
		$this->_short_name = 'qb_billtopay';
		$this->_fields = array("qbxml_id","TxnID","TxnType","APAccount_ListID","APAccount_FullName","TxnDate","RefNumber","DueDate","AmountDue");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TxnType -------------------------------------- 

	/** 
	* Sets a value to `TxnType` variable
	* @access public
	*/

	public function setTxntype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnType` variable
	* @access public
	*/

	public function getTxntype() {
		return $this->TxnType;
	}

	public function get_TxnType() {
		return $this->TxnType;
	}

	
// ------------------------------ End Field: TxnType --------------------------------------


// ---------------------------- Start Field: APAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `APAccount_ListID` variable
	* @access public
	*/

	public function setApaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_APAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `APAccount_ListID` variable
	* @access public
	*/

	public function getApaccountListid() {
		return $this->APAccount_ListID;
	}

	public function get_APAccount_ListID() {
		return $this->APAccount_ListID;
	}

	
// ------------------------------ End Field: APAccount_ListID --------------------------------------


// ---------------------------- Start Field: APAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `APAccount_FullName` variable
	* @access public
	*/

	public function setApaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_APAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `APAccount_FullName` variable
	* @access public
	*/

	public function getApaccountFullname() {
		return $this->APAccount_FullName;
	}

	public function get_APAccount_FullName() {
		return $this->APAccount_FullName;
	}

	
// ------------------------------ End Field: APAccount_FullName --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: DueDate -------------------------------------- 

	/** 
	* Sets a value to `DueDate` variable
	* @access public
	*/

	public function setDuedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DueDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DueDate` variable
	* @access public
	*/

	public function getDuedate() {
		return $this->DueDate;
	}

	public function get_DueDate() {
		return $this->DueDate;
	}

	
// ------------------------------ End Field: DueDate --------------------------------------


// ---------------------------- Start Field: AmountDue -------------------------------------- 

	/** 
	* Sets a value to `AmountDue` variable
	* @access public
	*/

	public function setAmountdue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AmountDue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AmountDue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AmountDue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AmountDue` variable
	* @access public
	*/

	public function getAmountdue() {
		return $this->AmountDue;
	}

	public function get_AmountDue() {
		return $this->AmountDue;
	}

	
// ------------------------------ End Field: AmountDue --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnType' => (object) array(
										'Field'=>'TxnType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'APAccount_ListID' => (object) array(
										'Field'=>'APAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'APAccount_FullName' => (object) array(
										'Field'=>'APAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DueDate' => (object) array(
										'Field'=>'DueDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AmountDue' => (object) array(
										'Field'=>'AmountDue',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_billtopay` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'TxnID' => "ALTER TABLE  `qb_billtopay` ADD  `TxnID` varchar(40) NULL   ;",
			'TxnType' => "ALTER TABLE  `qb_billtopay` ADD  `TxnType` varchar(40) NULL   ;",
			'APAccount_ListID' => "ALTER TABLE  `qb_billtopay` ADD  `APAccount_ListID` varchar(40) NULL   ;",
			'APAccount_FullName' => "ALTER TABLE  `qb_billtopay` ADD  `APAccount_FullName` varchar(255) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_billtopay` ADD  `TxnDate` date NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_billtopay` ADD  `RefNumber` text NULL   ;",
			'DueDate' => "ALTER TABLE  `qb_billtopay` ADD  `DueDate` date NULL   ;",
			'AmountDue' => "ALTER TABLE  `qb_billtopay` ADD  `AmountDue` decimal(10,2) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTxnid() - TxnID
//setTxntype() - TxnType
//setApaccountListid() - APAccount_ListID
//setApaccountFullname() - APAccount_FullName
//setTxndate() - TxnDate
//setRefnumber() - RefNumber
//setDuedate() - DueDate
//setAmountdue() - AmountDue

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_TxnID() - TxnID
//set_TxnType() - TxnType
//set_APAccount_ListID() - APAccount_ListID
//set_APAccount_FullName() - APAccount_FullName
//set_TxnDate() - TxnDate
//set_RefNumber() - RefNumber
//set_DueDate() - DueDate
//set_AmountDue() - AmountDue

*/
/* End of file Qb_billtopay_model.php */
/* Location: ./application/models/Qb_billtopay_model.php */

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar'); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Tools</h1>

<div class="section-header-breadcrumb">

</div>

          </div>
          <div class="row">

            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("tools/sms_bat_generator"); ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                  <i class="fas fa-sms"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Send SMS using Gammu Application</h4>
                  </div>
                  <div class="card-body">
                    SMS Bat File Generator
                  </div>
                </div>
              </div>
              </a>
            </div>

          </div>



        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
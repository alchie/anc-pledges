<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar'); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>SMS Bat File Generator</h1>

<div class="section-header-breadcrumb">

</div>

          </div>
          <div class="row">

<div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                 
                    <div class="card-body">
 <form method="get">
                      <div class="form-group">

                        <label>Message (<span class="pull-right count_chars_output"><?php echo strlen($this->input->get("message")); ?></span>)</label>
                        <textarea name="message" class="form-control count_chars" required><?php echo ($this->input->get("message")) ? $this->input->get("message") : $message; ?></textarea>
                      </div>
                      <div class="form-group">

                        <label>Log Directory</label>
                        <input name="log_dir" class="form-control" required="" type="text" value="<?php echo ($this->input->get("log_dir")) ? $this->input->get("log_dir") : $logdir; ?>" />
                      </div>
<div class="row">
  <div class="col-12 col-md-2 col-lg-2">
                      <div class="form-group">
                        <label>Pledgers</label>
                        <select class="form-control" name="pledgers">
                      <?php foreach(
                        array(
                          'no_remittance'=>'No Remittance',
                          'with_remittance'=>'With Remittance',
                          'birthday'=>'Birthday',
                          'anniversary'=>'Anniversary',
                          'all'=>'All Pledgers',
                        ) as $key=>$value) {
                        echo '<option value="'.$key.'" '.(($this->input->get('pledgers')==$key) ? 'selected="selected"' : '').'>'.$value.'</option>';
                      } ?>
                      </select>
                      </div>
</div>

  <div class="col-12 col-md-2 col-lg-2">
                      <div class="form-group">
                        <label>Frequency</label>
                        <select class="form-control" name="frequency">
                      <?php foreach(
                        array(
                          'monthly'=>'Monthly',
                          'weekly'=>'Weekly',
                          'quarterly'=>'Quarterly',
                          'yearly'=>'Yearly',
                          'lumpsum'=>'One time / Lumpsum',
                          'all'=>'All',
                        ) as $key=>$value) {
                        echo '<option value="'.$key.'" '.(($this->input->get('frequency')==$key) ? 'selected="selected"' : '').'>'.$value.'</option>';
                      } ?>
                      </select>
                      </div>
</div>

  <div class="col-12 col-md-2 col-lg-2">
                      <div class="form-group">
                        <label>Month</label>
                        <select class="form-control" name="month">
                          <?php for( $i=1; $i<13; $i++) { 
                            $selected = false;
                            if($this->input->get('month')) {
                              $selected = ($this->input->get('month')==$i);
                            } else {
                              $selected = (date('m')==$i);
                            }
                            ?>
                            <option value="<?php echo $i; ?>" <?php echo ($selected) ? 'selected="selected"' : ''; ?>><?php echo date('F', strtotime("{$i}/1/2000")); ?></option>
                          <?php } ?>
                        </select>
                      </div>
</div>

  <div class="col-12 col-md-2 col-lg-2">
                      <div class="form-group">
                        <label>Year</label>
                        <select class="form-control" name="year">
                        <?php foreach(pledgers_years() as $year) { 
                            $selected = false;
                            if($this->input->get('year')) {
                              $selected = ($this->input->get('year')==$year->year);
                            } else {
                              $selected = (date('Y')==$year->year);
                            }
                          ?>
                          <option <?php echo ($selected) ? 'selected="selected"' : ''; ?>><?php echo $year->year; ?></option>
                        <?php } ?>
                      </select>
                      </div>
</div>

  <div class="col-12 col-md-2 col-lg-2">
                      <div class="form-group">
                        <label>Stewardship Leader</label>
                        <select class="form-control" name="leader">
                          <option value="">All Leaders</option>
                        <?php foreach($leaders as $leader) { ?>
                          <option value="<?php echo $leader->ListID; ?>" <?php echo ($this->input->get('leader')==$leader->ListID) ? 'selected="selected"' : ''; ?>><?php echo $leader->SalesRepEntity_FullName; ?></option>
                        <?php } ?>
                      </select>
                      </div>
</div>
 <div class="col-12 col-md-2 col-lg-2">
  <div class="form-group">
    <label>&nbsp;</label>
    
      <button class="form-control btn btn-primary">Preview</button>
    
  </div>
</div>
</div>
</form>
<?php if( isset($pledgers) && ($pledgers) ) { ?>

<form method="post">

  <div class="row justify-content-md-center">
  <div class="col-12 col-md-4 col-lg-4">
    <button class="form-control btn btn-warning" type="submit">Download BAT File</button>
  </div>
  </div>

<div class="row">
  <div class="col-12 col-md-12 col-lg-12">

                    <div class="table-responsive">
                    <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col"></th>
                          <th scope="col" width="20%">Name (<?php echo $pledgers_total; ?>)</th>
                          <th scope="col" width="20%">Phone</th>
                          <th scope="col">Address</th>
                          <th class="text-right" scope="col" width="15%">Pledge <a href="<?php echo site_url(uri_string()); ?>?sort=<?php echo ($this->input->get('sort')=='PLEDGE_ASC') ? 'PLEDGE_DESC' : 'PLEDGE_ASC'; ?>"><i class="fas fa-sort"></i></a></th>
                        </tr>
                      </thead>
                      <tbody>
<?php 
$n = 1;
foreach($pledgers as $pledger) { ?>
                        <tr>
                          <td><input type="checkbox" name="send_to[]" value="<?php echo $pledger->ListID; ?>" <?php echo ($pledger->Phone) ? "CHECKED" : ""; ?> /></td>
                          <td>
                            <?php echo $n++; ?>.
                            <a target="tool_sms_pledger" href="<?php echo site_url("pledgers/view/{$pledger->ListID}"); ?>"><?php echo $pledger->Name; ?></a>
                          </td>
                          <td><?php echo $pledger->Phone; ?></td>
                          <td><?php echo $pledger->ShipAddress_Addr1; ?> <?php echo $pledger->ShipAddress_Addr2; ?> <?php echo $pledger->ShipAddress_Addr3; ?> <?php echo $pledger->ShipAddress_Addr4; ?> <?php echo $pledger->ShipAddress_Addr5; ?></td>
                          <td class="text-right"><?php echo number_format($pledger->pledge_amount,2); ?><br><?php echo $pledger->pledge_frequency; ?></td>
                        </tr>
<?php } ?>
                      </tbody>
                    </table>

                  </div>

  </div>
</div>

</form>

<?php } ?>
                    </div>
                    <!--
                    <div class="card-footer text-right">
                      <button class="btn btn-primary">Submit</button>
                    </div>
                  -->
                  
                </div>

              </div>


          </div>



        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="<?php echo site_url("dashboard"); ?>">ANC Pledgers</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?php echo site_url("dashboard"); ?>">ANC</a>
          </div>
          <ul class="sidebar-menu">
              <li><a class="nav-link" href="<?php echo site_url("dashboard"); ?>"><i class="fas fa-tachometer-alt"></i> <span>Dashboard</span></a></li>
<li class="menu-header">Pledgers</li>
              <li><a class="nav-link" href="<?php echo site_url("pledgers"); ?>"><i class="fas fa-hands-helping"></i> <span>Pledgers</span></a></li>
              <li><a class="nav-link" href="<?php echo site_url("leaders"); ?>"><i class="fas fa-user-tie"></i> <span>Leaders</span></a></li>
              <li><a class="nav-link" href="<?php echo site_url("prospects"); ?>"><i class="fas fa-user-check"></i> <span>Prospects</span></a></li>
              <li><a class="nav-link" href="<?php echo site_url("groups"); ?>"><i class="fas fa-users"></i> <span>Groups</span></a></li>
<li class="menu-header">Collections</li>
 <li><a class="nav-link" href="<?php echo site_url("pledges"); ?>"><i class="fas fa-hand-holding-heart"></i> <span>Pledges</span></a></li>

 <li class="menu-header">Others</li>

  <li><a class="nav-link" href="<?php echo site_url("reports"); ?>"><i class="fas fa-file-powerpoint"></i> <span>Reports</span></a></li>
  <li><a class="nav-link" href="<?php echo site_url("tools"); ?>"><i class="fas fa-tools"></i> <span>Tools</span></a></li>
            </ul>

        </aside>
      </div>
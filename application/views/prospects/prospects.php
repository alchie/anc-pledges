<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar');  ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Prospects</h1> &nbsp;&nbsp;(<?php echo $prospects_total; ?>)
          </div>

<div class="row">
<div class="col-12 col-md-12 col-lg-12">

                <div class="card">
                  <div class="card-body p-0">
                    <div class="table-responsive">
                     <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col" width="20%">Name</th>
                          <th scope="col" width="20%">Phone</th>
                          <th scope="col">Address</th>
                          <th scope="col" width="15%">Pledge</th>
                        </tr>
                      </thead>
                      <tbody>
<?php foreach($prospects as $prospect) { ?>
                        <tr>
                          <td>
                            <a href="<?php echo site_url("pledgers/view/{$prospect->ListID}"); ?>"><?php echo $prospect->Name; ?></a>
                          </td>
                          <td><?php echo $prospect->Phone; ?></td>
                          <td><?php echo $prospect->ShipAddress_Addr1; ?> <?php echo $prospect->ShipAddress_Addr2; ?> <?php echo $prospect->ShipAddress_Addr3; ?> <?php echo $prospect->ShipAddress_Addr4; ?> <?php echo $prospect->ShipAddress_Addr5; ?></td>
                          <td><?php echo $prospect->JobDesc; ?></td>
                        </tr>
<?php } ?>
                      </tbody>
                    </table>
                  </div>
                  </div>
<nav class="text-center"><?php echo $pagination; ?></nav>
                </div>

</div>
</div>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar');  ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1><?php echo ($leader->SalesRepEntity_ListID==$current_account->ListID) ? "My Prospects" : $leader->SalesRepEntity_FullName; ?></h1> <?php echo ($leader->SalesRepEntity_ListID==$current_account->ListID) ? "" : " &nbsp; &nbsp; Prospects"; ?>

            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item"><a href="<?php echo site_url("leaders/pledgers/{$leader->SalesRepEntity_ListID}"); ?>"><?php echo ($leader->SalesRepEntity_ListID==$current_account->ListID) ? "My " : ""; ?>Pledgers</a></div>
              <div class="breadcrumb-item active"><?php echo ($leader->SalesRepEntity_ListID==$current_account->ListID) ? "My " : ""; ?>Prospects</div>
            </div>
          </div>

<div class="row">
<div class="col-12 col-md-12 col-lg-12">

                <div class="card">

<?php if( $prospects ) { ?>
                  <div class="card-body p-0">
                    <div class="table-responsive">
                    <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col" width="20%">Name</th>
                          <th scope="col" width="20%">Phone</th>
                          <th scope="col">Address</th>
                          <th scope="col" width="15%">Pledge</th>
                        </tr>
                      </thead>
                      <tbody>
<?php foreach($prospects as $prospect) { ?>
                        <tr>
                          <td>
                            <a href="<?php echo site_url("pledgers/view/{$prospect->ListID}"); ?>"><?php echo $prospect->Name; ?></a>
                          </td>
                          <td><?php echo $prospect->Phone; ?></td>
                          <td><?php echo $prospect->ShipAddress_Addr1; ?> <?php echo $prospect->ShipAddress_Addr2; ?> <?php echo $prospect->ShipAddress_Addr3; ?> <?php echo $prospect->ShipAddress_Addr4; ?> <?php echo $prospect->ShipAddress_Addr5; ?></td>
                          <td><?php echo $prospect->JobDesc; ?></td>
                        </tr>
<?php } ?>
                      </tbody>
                    </table>

                  </div>
                  </div>
<nav class="text-center"><?php echo $pagination; ?></nav>

<?php } else { ?>
  <div class="card-body">
    No Prospect Found!
    </div>
<?php } ?>
                </div>


</div>
</div>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
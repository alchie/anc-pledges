<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar');  ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1><?php echo ($leader->SalesRepEntity_ListID==$current_account->ListID) ? "My Pledgers" : $leader->SalesRepEntity_FullName; ?></h1><?php echo ($leader->SalesRepEntity_ListID==$current_account->ListID) ? "" : " &nbsp; &nbsp; Pledgers"; ?>
            <div class="section-header-breadcrumb">

<?php if( $pledgers ) { ?>
<div class="breadcrumb-item">
<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Monthly Remittance Checklist <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
<?php foreach(pledgers_years() as $year) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("leaders/monthly_remittance/{$leader->SalesRepEntity_ListID}/{$year->year}"); ?>"><?php echo $year->year; ?></a></li>
<?php } ?>
  </ul>
</div>
</div>
<?php } ?>
              <div class="breadcrumb-item active"><?php echo ($leader->SalesRepEntity_ListID==$current_account->ListID) ? "My " : ""; ?>Pledgers</div>
              <div class="breadcrumb-item"><a href="<?php echo site_url("leaders/prospects/{$leader->SalesRepEntity_ListID}"); ?>"><?php echo ($leader->SalesRepEntity_ListID==$current_account->ListID) ? "My " : ""; ?>Prospects</a></div>
            </div>

          </div>

<div class="row">
<div class="col-12 col-md-12 col-lg-12">

                <div class="card">

<?php if( $pledgers ) { ?>
                  <div class="card-body p-0">
                    <div class="table-responsive">
                    <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col" width="20%">Name</th>
                          <th scope="col" width="20%">Phone</th>
                          <th scope="col">Address</th>
                          <th  class="text-right" scope="col" width="15%">Pledge</th>
                        </tr>
                      </thead>
                      <tbody>
<?php foreach($pledgers as $pledger) { ?>
                        <tr>
                          <td>
                            <a href="<?php echo site_url("pledgers/view/{$pledger->ListID}"); ?>"><?php echo $pledger->Name; ?></a>
                          </td>
                          <td><?php echo $pledger->Phone; ?></td>
                          <td><?php echo $pledger->ShipAddress_Addr1; ?> <?php echo $pledger->ShipAddress_Addr2; ?> <?php echo $pledger->ShipAddress_Addr3; ?> <?php echo $pledger->ShipAddress_Addr4; ?> <?php echo $pledger->ShipAddress_Addr5; ?></td>
                          <td class="text-right"><?php echo number_format($pledger->pledge_amount,2); ?> <?php echo $pledger->pledge_frequency; ?></td>
                        </tr>
<?php } ?>
                      </tbody>
                    </table>

                  </div>
                  </div>
<nav class="text-center"><?php echo $pagination; ?></nav>

<?php } else { ?>
  <div class="card-body">
    No Pledger Found!
    </div>
<?php } ?>
                </div>


</div>
</div>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
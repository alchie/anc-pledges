<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar'); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Leaders</h1> &nbsp;&nbsp;(<?php echo $leaders_total; ?>)
          </div>

<div class="row">
<div class="col-12 col-md-12 col-lg-12">

                <div class="card">
<?php if( $leaders ) { ?>
                  <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col">Name</th>
                          <th scope="col" class="text-right">Pledgers</th>
                          <th scope="col" class="text-right">Prospects</th>
                        </tr>
                      </thead>
                      <tbody>
<?php foreach($leaders as $leader) { ?>
                        <tr>
                          <td><a href="<?php echo site_url("leaders/pledgers/{$leader->SalesRepEntity_ListID}"); ?>"><?php echo $leader->SalesRepEntity_FullName; ?></a></td>
                          <td  class="text-right"><a href="<?php echo site_url("leaders/monthly_remittance/{$leader->SalesRepEntity_ListID}"); ?>"><?php echo $leader->pledgers; ?></a></td>
                          <td  class="text-right"><a href="<?php echo site_url("leaders/prospects/{$leader->SalesRepEntity_ListID}"); ?>"><?php echo $leader->prospects; ?></a></td>
                        </tr>
<?php } ?>
                      </tbody>
                    </table>
                  </div>
                  </div>

<nav class="text-center"><?php echo $pagination; ?></nav>

<?php } else { ?>
  <div class="card-body">
    No Pledger Found!
    </div>
<?php } ?>
                </div>

</div>
</div>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
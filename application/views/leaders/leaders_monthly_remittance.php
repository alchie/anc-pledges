<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar');  ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1><?php echo ($leader->SalesRepEntity_ListID==$current_account->ListID) ? "My Pledgers" : $leader->SalesRepEntity_FullName; ?></h1> &nbsp; &nbsp; Monthly Remittance Checklist for <?php $current_month_f = date('F', strtotime($current_month."/1/".$current_year)); echo $current_month_f; ?> <?php echo $current_year; ?>
            <div class="section-header-breadcrumb">


<div class="breadcrumb-item">

<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $leader->SalesRepEntity_FullName; ?><span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
    <li class="dropdown-item"><a href="<?php echo site_url("pledgers/monthly_remittance/{$current_year}/{$current_month}"); ?>">All Leaders</a></li>
<?php foreach($leaders as $leadr) {  ?>
    <li class="dropdown-item"><a href="<?php echo site_url("leaders/monthly_remittance/{$leadr->SalesRepEntity_ListID}/{$current_month}/{$current_year}"); ?>"><?php echo $leadr->SalesRepEntity_FullName; ?></a></li>
<?php } ?>
  </ul>
</div>

<?php if( $pledgers ) { ?>
<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Month : <?php $current_month_f = date('F', strtotime($current_month."/1/".$current_year)); echo $current_month_f; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
    <?php if($this->input->get('year')) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledges"); ?>">All Time</a></li>
  <?php } ?>
<?php for($month=1;$month<=12;$month++) {
  $month_m = date('m', strtotime($month."/1/".$current_year)); 
  $month_f = date('F', strtotime($month."/1/".$current_year)); 
  ?>
    <li class="dropdown-item"><a href="<?php echo site_url("leaders/monthly_remittance/{$leader->SalesRepEntity_ListID}/{$month_m}/{$current_year}"); ?>"><?php echo $month_f; ?></a></li>
<?php } ?>
  </ul>
</div>

<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Year : <?php echo $current_year; ?><span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
    <?php if($this->input->get('year')) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledges"); ?>">All Time</a></li>
  <?php } ?>
<?php foreach(pledgers_years() as $year) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("leaders/monthly_remittance/{$leader->SalesRepEntity_ListID}/{$current_month}/{$year->year}"); ?>"><?php echo $year->year; ?></a></li>
<?php } ?>
  </ul>
</div>

<?php } ?>
</div>

<!--
              <div class="breadcrumb-item active"><?php echo ($leader->SalesRepEntity_ListID==$current_account->ListID) ? "My " : ""; ?>Pledgers</div>
              <div class="breadcrumb-item"><a href="<?php echo site_url("leaders/prospects/{$leader->SalesRepEntity_ListID}"); ?>"><?php echo ($leader->SalesRepEntity_ListID==$current_account->ListID) ? "My " : ""; ?>Prospects</a></div>
-->
            </div>

          </div>

<div class="row">
<div class="col-12 col-md-12 col-lg-12">

                <div class="card">

<?php if( $pledgers ) { ?>
                  <div class="card-body">
                    
<small class="text-center" style="display: block;">
  <?php if($this->input->get('show')=='all') { ?>
    <a href="<?php echo site_url(uri_string()); ?>?show=monthly&remitted=<?php echo $this->input->get('remitted'); ?>">Show Monthly</a> 
  <?php } else { ?>
    <a href="<?php echo site_url(uri_string()); ?>?show=all&remitted=<?php echo $this->input->get('remitted'); ?>">Show All</a>
  <?php } ?> 

  &middot;
  [<a href="<?php echo site_url(uri_string()); ?>?show=<?php echo $this->input->get('show'); ?>&remitted=">All</a> &middot;
  <a href="<?php echo site_url(uri_string()); ?>?show=<?php echo $this->input->get('show'); ?>&remitted=0">No Remittance</a> &middot;
  <a href="<?php echo site_url(uri_string()); ?>?show=<?php echo $this->input->get('show'); ?>&remitted=1">With Remittance</a>]
&middot;

<?php echo $pledgers_total; ?> Pledgers
</small>
                    <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col"> Name</th>
                          
                          <?php /*for($i=1;$i<=12;$i++) { ?>
                          <th class="text-center" scope="col" width="5%"><?php echo date("M", strtotime($i."/1/1990")); ?></th>
                          <?php }*/ ?>
                           <th class="text-center" scope="col" width="10%"><?php echo date("F", strtotime($current_month."/1/1990")); ?></th>
                        </tr>
                      </thead>
                      <tbody>
<?php foreach($pledgers as $pledger) { //print_r($pledger); ?>
                        <tr>
                          <td>
                            <a href="<?php echo site_url("pledgers/view/{$pledger->ListID}"); ?>"><?php echo $pledger->Name; ?></a>
                            <br /><?php echo $pledger->Phone; ?>
                            - <?php echo $pledger->pledge_frequency; ?>
                          </td>
                          
 <?php /* for($i=1;$i<=12;$i++) { 
$month_var = date('F', strtotime($i."/1/".$current_year));
  ?>
    <td class="text-center"><?php echo ($pledger->$month_var) ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-times" style="color:red"></i>'; ?></td>
  <?php } */ ?>
  <td class="text-center"><?php echo ($pledger->current_month) ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-times" style="color:red"></i>'; ?></td>
                        </tr>
<?php }  ?>
                      </tbody>
                    </table>
                  
                  </div>
<nav class="text-center"><?php echo $pagination; ?></nav>

<?php } else { ?>
  <div class="card-body">
    No Pledger Found!
    </div>
<?php } ?>
                </div>


</div>
</div>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
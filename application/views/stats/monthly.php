<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar'); //print_r($stats); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1><?php echo $current_year; ?> Monthly Collections</h1>

<div class="section-header-breadcrumb">
<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Change Year <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
<?php foreach(pledgers_years() as $year) { 
if( $year->year == $current_year) { continue; }
  ?>
    <li class="dropdown-item"><a href="<?php echo site_url("stats/monthly/{$year->year}"); ?>"><?php echo $year->year; ?></a></li>
<?php } ?>
  </ul>
</div>
</div>

          </div>


  <div class="card">
  <div class="card-body">
    <canvas id="canvas"></canvas>
  </div>
  </div>

  <script>
<?php 
$months = array();
$total_pledge = array();
$pledged = array();
$collections = array();
for($month=1;$month<=12;$month++) {
  $month_var = date('F', strtotime($month."/1/".$current_year));
  $months[] = $month_var;
  $total_pledge[] = $stats->total_pledge;
  $pledged_var = 'pledged_' . $month_var;
  $pledged[] = $stats->$pledged_var;
  $collection_var = 'collected_' . $month_var;
  $collections[] = $stats->$collection_var;

}
?>
    var MONTHS = ['<?php echo implode("', '", $months); ?>'];
    var config = {
      type: 'line',
      data: {
        labels: ['<?php echo implode("', '", $months); ?>'],
        datasets: [
        {
          label: 'Total Monthly Pledges',
          backgroundColor: 'red',
          borderColor: 'red',
          data: ['<?php echo implode("', '", $total_pledge); ?>'],
          fill: false,
        },
        {
          label: 'Total Collections during the Month',
          fill: false,
          backgroundColor: 'green',
          borderColor: 'green',
          data: ['<?php echo implode("', '", $collections); ?>'],
        },
        {
          label: 'Total Monthly Pledges collected during the month',
          fill: false,
          backgroundColor: 'blue',
          borderColor: 'blue',
          data: ['<?php echo implode("', '", $pledged); ?>'],
        }
        ]
      },
      options: {
        responsive: true,
        title: {
          display: false,
          text: 'Monthly Collections'
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Month'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Amount'
            },
            ticks : {
               callback: function(value, index, values) {
                 return value.toLocaleString("en-US",{style:"currency", currency:"PHP"});
              }
            }
          }]
        }
      }
    };

    window.onload = function() {
      var ctx = document.getElementById('canvas').getContext('2d');
      window.myLine = new Chart(ctx, config);
    };

  </script>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
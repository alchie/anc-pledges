<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar'); //print_r($stats); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Annual Collections</h1>
          </div>


  <div class="card">
  <div class="card-body">
    <canvas id="canvas"></canvas>
  </div>
  </div>

  <script>
    <?php 
    $years = array();
    $min_pledge = array();
    $collected_pledges = array();
    $collections = array();
    foreach(pledgers_years(FALSE, 'ASC') as $year) {
      $var = 'Y'. $year->year;
      $var2 = 'C'. $year->year;
      $years[] = $year->year;
      $min_pledge[] = ($stats->yearly_pledge + ($stats->monthly_pledge * 12) + ($stats->quarterly_pledge * 4));
      $collected_pledges[] = $stats->$var;
      $collections[] = $stats->$var2;
    }
    ?>
    var MONTHS = ['<?php echo implode("', '", $years); ?>'];
    var config = {
      type: 'line',
      data: {
        labels: ['<?php echo implode("', '", $years); ?>'],
        datasets: [{
          label: 'Total Annual Pledges',
          backgroundColor: 'red',
          borderColor: 'red',
          data: ['<?php echo implode("', '", $min_pledge); ?>'],
          fill: false,
        }, {
          label: 'Total Annual Collected Pledges',
          fill: false,
          backgroundColor: 'green',
          borderColor: 'green',
          data: ['<?php echo implode("', '", $collected_pledges); ?>'],
        }, {
          label: 'Total Annual Collections',
          fill: false,
          backgroundColor: 'blue',
          borderColor: 'blue',
          data: ['<?php echo implode("', '", $collections); ?>'],
        }]
      },
      options: {
        responsive: true,
        title: {
          display: false,
          text: 'Monthly Collections'
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Year'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Amount'
            },
            ticks : {
               callback: function(value, index, values) {
                 return value.toLocaleString("en-US",{style:"currency", currency:"PHP"});
              }
            }
          }]
        }
      }
    };

    window.onload = function() {
      var ctx = document.getElementById('canvas').getContext('2d');
      window.myLine = new Chart(ctx, config);
    };

  </script>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
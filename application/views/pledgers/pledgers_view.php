<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar');  ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1><?php echo $pledger->Name; ?> <?php echo ($this->input->get('year')) ? "<small>".$this->input->get('year')."</small>": ''; ?></h1>

<?php if( $pledges ) { ?>
<div class="section-header-breadcrumb">
<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Filter by Year <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
    <?php if( $this->input->get('year') ) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledgers/view/{$pledger->ListID}"); ?>">Show All</a></li>
  <?php } ?>
<?php foreach(pledgers_years($pledger->ListID) as $year) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledgers/view/{$pledger->ListID}"); ?>?year=<?php echo $year->year; ?>"><?php echo $year->year; ?></a></li>
<?php } ?>
  </ul>
</div>
</div>
<?php } ?>
          </div>
<p>
  <?php if( $pledger->Phone ) { ?>
    <strong>Phone Number:</strong> <?php echo $pledger->Phone; ?> 
  <?php } ?> 
  <?php if( $pledger->ShipAddress_Addr1 ) { ?>
  &middot; <strong>Address:</strong> <?php echo $pledger->ShipAddress_Addr1; ?> <?php echo $pledger->ShipAddress_Addr2; ?> <?php echo $pledger->ShipAddress_Addr3; ?> <?php echo $pledger->ShipAddress_Addr4; ?> <?php echo $pledger->ShipAddress_Addr5; ?> 
  <?php } ?>
  &middot; <strong>Group:</strong> <?php echo $pledger->CustomerType_FullName; ?> &middot; 
  <strong>Pledge Card #:</strong> <?php echo $pledger->pledge_card_number; ?> &middot; 
  <strong>Pledge Amount:</strong> <?php echo number_format($pledger->pledge_amount,2); ?> <?php echo $pledger->pledge_frequency; ?> &middot; 
  <strong>Stewardship Leader:</strong> <?php echo $pledger->leader; ?> 
  <?php if( $pledger->pledge_referrer_name ) { ?>
    &middot; <strong>Referrer:</strong> <?php echo $pledger->pledge_referrer_name; ?>
  <?php } ?>
</p>

<div class="row">
<div class="col-12 col-md-12 col-lg-12">

                <div class="card">
                  <div class="card-body p-0">
<?php if( $pledges ) { ?>
  <div class="card-body p-0">
    <div class="table-responsive">
                    <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col">Date</th>
                          <th scope="col">Name</th>
                          <th scope="col">Amount</th>
                          <th class="text-right" scope="col" width="200px">Month / Year</th>
                        </tr>
                      </thead>
                      <tbody>
<?php foreach($pledges as $pledge) { ?>
                        <tr>
                          <td><?php echo date("F d, Y", strtotime($pledge->TxnDate)); ?></td>
                          <td><?php echo $pledge->Customer_FullName; ?></td>
                          <td><?php echo number_format($pledge->Amount,2); ?></td>
                          <td class="text-right">
  <a href="<?php echo site_url("pledges/pledged/{$pledge->month}/{$pledge->year}"); ?>"><?php echo $pledge->month . " " . $pledge->year; ?></a>
                            
                            
                          </td>
                        </tr>
<?php } ?>

                      </tbody>
                    </table>
           </div>
           </div>
<?php } else { ?>
<div class="card-body">
  <span class="text-center">No Pledge Found!</span>
</div>
<?php } ?>
                  
<nav class="text-center"><?php echo $pagination; ?></nav>
                </div>

</div>
</div>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
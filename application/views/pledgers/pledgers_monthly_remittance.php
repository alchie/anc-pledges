<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar');  ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Monthly Remittance Checklist for <?php echo $current_year; ?> <small>(<?php echo $pledgers_total; ?> Pledgers)</small></h1>

<div class="section-header-breadcrumb">

<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Change Month <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
<?php for($month=1;$month<=12;$month++) { 
$month_m = date('m', strtotime($month."/1/".$current_year)); 
  $month_f = date('F', strtotime($month."/1/".$current_year)); 
  ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledgers/monthly_remittance/{$current_year}/{$month_m}") . "?show=" . $this->input->get('show') . "&remitted=" . $this->input->get('remitted'); ?>"><?php echo $month_f; ?></a></li>
<?php } ?>
  </ul>
</div>

<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Change Year <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
<?php foreach(pledgers_years() as $year) { 
  if( $year->year == $current_year ) {
    continue;
  }
  ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledgers/monthly_remittance/{$year->year}"); ?>"><?php echo $year->year; ?></a></li>
<?php } ?>
  </ul>
</div>
</div>

          </div>

<div class="row">
<div class="col-12 col-md-12 col-lg-12">

                <div class="card">

<?php if( $pledgers ) { 
$total_pledges = 0;
  ?>
                  <div class="card-body p-0">


<small class="text-center" style="display: block;">
  <a target="_print" href="<?php echo site_url(uri_string()); ?>?show=<?php echo $this->input->get('show'); ?>&remitted=<?php echo $this->input->get('remitted'); ?>&print=1">Print</a>
  &middot;
  <?php if($this->input->get('show')=='all') { ?>
    <a href="<?php echo site_url(uri_string()); ?>?show=less&remitted=<?php echo $this->input->get('remitted'); ?>">Show Less</a> 
  <?php } else { ?>
    <a href="<?php echo site_url(uri_string()); ?>?show=all&remitted=<?php echo $this->input->get('remitted'); ?>">Show All</a>
  <?php } ?> 

  &middot;
  [<a href="<?php echo site_url(uri_string()); ?>?show=<?php echo $this->input->get('show'); ?>&remitted=">All</a> &middot;
  <a href="<?php echo site_url(uri_string()); ?>?show=<?php echo $this->input->get('show'); ?>&remitted=0">No Remittance</a> &middot;
  <a href="<?php echo site_url(uri_string()); ?>?show=<?php echo $this->input->get('show'); ?>&remitted=1">With Remittance</a>]
&middot;

<?php echo $pledgers_total; ?> Pledgers
</small>

                    <div class="table-responsive">
                    <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col" width="20%">Name</th>
<?php if( $current_month ) { ?>
                          <th scope="col"> Leader <a href="<?php echo site_url(uri_string()); ?>?show=<?php echo $this->input->get('show'); ?>&remitted=<?php echo $this->input->get('remitted'); ?>&sort_leader=<?php echo ($this->input->get('sort_leader')=='ASC') ? 'DESC' : 'ASC'; ?>"><i class="fa fa-sort"></i></a></th>
<?php } ?>

  <?php for($i=1;$i<=12;$i++) { 
      if( ($current_month) && ($current_month!=$i) ) {
        continue;
      }

                            ?>
                          <th class="text-center" scope="col" width="<?php echo ( $current_month ) ? '15' : '1'; ?>%">

                            <a href="<?php echo site_url("pledgers/monthly_remittance/{$current_year}/{$i}"); ?>"><?php echo ( $current_month ) ? date("F", strtotime($i."/1/1990")) : date("M", strtotime($i."/1/1990")); ?></a>
<?php if( $current_month ) { ?>
<a class="badge" href="<?php echo site_url("pledgers/monthly_remittance/{$current_year}"); ?>"><i class="fa fa-times" style="color:red"></i></a>
<?php } ?>
                          </th>
        <?php } ?>
<?php if( $current_month ) { 
$total_pledges = 0;
  ?>
                          <th scope="col"  class="text-right"> Pledge Amount</th>
<?php } ?>
                        </tr>
                      </thead>
                      <tbody>
<?php foreach($pledgers as $pledger) { //print_r($pledger); ?>
                        <tr>
                          <td>
                            <a href="<?php echo site_url("pledgers/view/{$pledger->ListID}"); ?>"><?php echo $pledger->Name; ?></a>
                            <br /><?php echo $pledger->Phone; ?>
                          </td>
<?php if( $current_month ) { ?>
                          <td><?php echo $pledger->SalesRep_FullName; ?></td>
<?php } ?>
 <?php for($i=1;$i<=12;$i++) { 

      if( ($current_month) && ($current_month!=$i) ) {
        continue;
      }

$month_var = date('F', strtotime($i."/1/".$current_year));
  ?>
    <td class="text-center"><?php echo ($pledger->$month_var) ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-times" style="color:red"></i>'; ?></td>
  <?php } ?>
<?php if( $current_month ) { ?>
                          <td class="text-right"><?php echo number_format($pledger->pledge_amount,2); $total_pledges+=$pledger->pledge_amount; ?></td>
<?php } ?>
                        </tr>

<?php } ?>
<?php if( ( $this->input->get('show') == 'all' ) && ($total_pledges) ) { ?>
   <tr>
    <td colspan="3">TOTAL</td>
    <td class="text-right"><?php echo number_format($total_pledges,2); ?></td>
   </tr>
<?php } ?>
                      </tbody>
                    </table>

                  </div>
                  </div>
<nav class="text-center"><?php echo $pagination; ?></nav>

<?php } else { ?>
  <div class="card-body">
    No Pledger Found!
    </div>
<?php } ?>
                </div>


</div>
</div>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
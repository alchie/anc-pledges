<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar'); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Pledgers</h1> &nbsp;(<?php echo $pledgers_total; ?> Pledgers) &nbsp;(Total Pledges: <?php echo number_format($total_pledges->total_pledges,2); ?>)

<div class="section-header-breadcrumb">

<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Filter: Frequency <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
<?php foreach(array('all', 'lumpsum', 'monthly', 'weekly', 'quarterly', 'yearly') as $freq) { 
if( $freq == $frequency) {
  continue;
}
  ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledgers/filter/" . $freq); ?>"><?php echo ucwords($freq); ?></a></li>
<?php } ?>
  </ul>
</div>

<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Monthly Remittance Checklist <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
<?php foreach(pledgers_years() as $year) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledgers/monthly_remittance/{$year->year}"); ?>"><?php echo $year->year; ?></a></li>
<?php } ?>
  </ul>
</div>

</div>

          </div>


<div class="row">
<div class="col-12 col-md-12 col-lg-12">

                <div class="card">

<?php if( $pledgers ) { ?>
                  <div class="card-body p-0">
                    <div class="table-responsive">
                    <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col" width="20%">Name <a href="<?php echo site_url(uri_string()); ?>?sort=<?php echo ($this->input->get('sort')=='NAME_ASC') ? 'NAME_DESC' : 'NAME_ASC'; ?>"><i class="fas fa-sort"></i></a></th>
                          <th scope="col" width="20%">Phone</th>
                          <th scope="col">Address</th>
                          <th class="text-right" scope="col" width="15%">Pledge <a href="<?php echo site_url(uri_string()); ?>?sort=<?php echo ($this->input->get('sort')=='PLEDGE_ASC') ? 'PLEDGE_DESC' : 'PLEDGE_ASC'; ?>"><i class="fas fa-sort"></i></a></th>
                        </tr>
                      </thead>
                      <tbody>
<?php foreach($pledgers as $pledger) { ?>
                        <tr>
                          <td>
                            <a href="<?php echo site_url("pledgers/view/{$pledger->ListID}"); ?>"><?php echo $pledger->Name; ?></a>
                          </td>
                          <td><?php echo $pledger->Phone; ?></td>
                          <td><?php echo $pledger->ShipAddress_Addr1; ?> <?php echo $pledger->ShipAddress_Addr2; ?> <?php echo $pledger->ShipAddress_Addr3; ?> <?php echo $pledger->ShipAddress_Addr4; ?> <?php echo $pledger->ShipAddress_Addr5; ?></td>
                          <td class="text-right"><?php echo number_format($pledger->pledge_amount,2); ?><br><?php echo $pledger->pledge_frequency; ?></td>
                        </tr>
<?php } ?>
                      </tbody>
                    </table>

                  </div>
                  </div>
                  
<nav class="text-center"><?php echo $pagination; ?></nav>

<?php } else { ?>
  <div class="card-body">
    No Pledger Found!
    </div>
<?php } ?>
                </div>


</div>
</div>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
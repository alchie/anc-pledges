<?php $this->load->view('print_header'); ?>

<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <thead>

    <tr>
      <th colspan="4">
<table width="100%" cellpadding="3" cellspacing="0">



    <tr>
      <th><strong style="font-size: medium; text-decoration: underline;">Archdiocesan Nourishment Center, Inc.</strong></th>
    </tr>
    <tr>
      <th style="font-weight: normal;"><small>Pag-asa St., &middot; 082-285-1524<br>
https://www.ancdavao.com &middot; anc.archdavao@gmail.com</small>
      </th>
    </tr>
    <tr>
      <th>

<strong style="font-size: small;font-family: sans; letter-spacing: 10px; text-transform: uppercase;">Monthly Pledgers</strong>

<br><?php echo ($this->input->get('remitted')==1) ? 'With' : 'Without'; ?> Remittance (<?php echo date('F Y', strtotime($current_month."/1/".$current_year)); ?>) <small>(<?php echo $pledgers_total; ?> Pledgers)</small>

      </th>
    </tr>

</table>

      </th>
    </tr>

  </thead>
  <tbody style="vertical-align: top; height: 100%;" >
    <tr>
      <td colspan="4">

<table width="100%" cellpadding="3" cellspacing="0" class="bordered font85p" style="margin-top: 5px;">
 <tr>
    <td width="40%"><strong>NAME</strong></td>
    <td width="20%"><strong>PHONE NUMBER</strong></td>
    <td width="20%"><strong>LEADER</strong></td>
    <td width="20%" style="text-align: right;"><strong>PLEDGE AMOUNT</strong></td>
  </tr>
<?php 

$total = 0;
$n = 1;
foreach($pledgers as $pledger) { 
  $total += $pledger->pledge_amount;
  ?>
<tr>
  <td><?php echo $n; ?>. <?php echo $pledger->Name; ?></td>
  <td><?php echo $pledger->Phone; ?></td>
  <td><?php echo $pledger->SalesRep_FullName; ?></td>
  <td style="text-align: right;"><?php echo number_format($pledger->pledge_amount,2); ?></td>
</tr>
<?php $n++; } ?>
<tr>
  <td colspan="3" style="text-align: right;font-weight: bold">TOTAL</td>
  <td style="text-align: right;font-weight: bold"><?php echo number_format($total,2); ?></td>
</tr>
</table>


      </td>
    </tr>
  </tbody>
</table>

<?php $this->load->view('print_footer'); ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar'); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Pledgers</h1> &nbsp;(<?php echo $pledgers_total; ?> Pledgers)

<div class="section-header-breadcrumb">

<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Month <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
    <li class="dropdown-item"><a href="<?php echo site_url(uri_string()); ?>">All</a></li>
<?php for($i=1;$i<=12;$i++) {?>
    <li class="dropdown-item"><a href="<?php echo site_url(uri_string()) . "?month=" . date("m", strtotime($i."/1/2000")); ?>"><?php echo date("F", strtotime($i."/1/2000")); ?></a></li>
<?php } ?>
  </ul>
</div>

</div>

          </div>


<div class="row">
<div class="col-12 col-md-12 col-lg-12">


                <div class="card">

<?php if( $pledgers ) { ?>
                  <div class="card-body p-0">

<small class="text-center" style="display: block;">

  <?php if($this->input->get('show')=='all') { ?>
    <a href="<?php echo site_url(uri_string()); ?>?show=less&month=<?php echo $this->input->get('month'); ?>">Show Less</a> 
  <?php } else { ?>
    <a href="<?php echo site_url(uri_string()); ?>?show=all&month=<?php echo $this->input->get('month'); ?>">Show All</a>
  <?php } ?> 

  &middot;


<?php echo $pledgers_total; ?> Pledgers
</small>

                    <div class="table-responsive">
                    <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col" width="20%">Name</th>
                          <th scope="col" width="20%">Phone</th>
                          <th scope="col" class="text-right">Birthday</th>
                          <th scope="col" class="text-right">Anniversary</th>
                        </tr>
                      </thead>
                      <tbody>
<?php foreach($pledgers as $pledger) { //print_r($pledger); ?>
                        <tr>
                          <td>
                            <a href="<?php echo site_url("pledgers/view/{$pledger->ListID}"); ?>"><?php echo $pledger->Name; ?></a>
                          </td>
                          <td><?php echo $pledger->Phone; ?></td>
                          <td class="text-right"><?php echo ($pledger->JobStartDate!='0000-00-00') ? date("F d, Y", strtotime($pledger->JobStartDate)) : ''; ?></td>
                          <td class="text-right"><?php echo  ($pledger->JobProjectedEndDate!='0000-00-00') ? date("F d, Y", strtotime($pledger->JobProjectedEndDate)) : ''; ?></td>
                        </tr>
<?php } ?>
                      </tbody>
                    </table>

                  </div>
                  </div>
                  
<nav class="text-center"><?php echo $pagination; ?></nav>

<?php } else { ?>
  <div class="card-body">
    No Pledger Found!
    </div>
<?php } ?>
                </div>


</div>
</div>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar'); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Groups</h1>
          </div>

<div class="row">
<div class="col-12 col-md-12 col-lg-12">

                <div class="card">
<?php if( $groups ) { ?>
                  <div class="card-body p-0">
                  <div class="table-responsive">
                    <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col">Name</th>
                          <th scope="col" class="text-right">Members</th>
                        </tr>
                      </thead>
                      <tbody>
<?php foreach($groups as $group) { ?>
                        <tr>
                          <td><?php echo ($group->Sublevel) ? "---- " : ""; ?><a href="<?php echo site_url("groups/members/{$group->ListID}"); ?>"><?php echo $group->Name; ?></a></td>
                          <td  class="text-right"><a href="<?php echo site_url("groups/members/{$group->ListID}"); ?>"><?php echo $group->members; ?></a></td>
                        </tr>
<?php } ?>
                      </tbody>
                    </table>
                  </div>
                  </div>

<nav class="text-center"><?php echo $pagination; ?></nav>

<?php } else { ?>
  <div class="card-body">
    No Group Found!
    </div>
<?php } ?>
                </div>

</div>
</div>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
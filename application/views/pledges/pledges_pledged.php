<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar');  ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1><?php echo ($this->input->get('year')) ? $this->input->get('year')." ": ''; ?>Pledges</h1> 
            &nbsp; (<?php echo $pledges_count; ?>)
            &nbsp; (&#8369; <?php echo number_format($pledges_total->total,2); ?>)

<div class="section-header-breadcrumb">

<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Month : <?php $current_month_f = date('F', strtotime($current_month." 1, ".$current_year)); echo $current_month_f; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
    <?php if( $current_year ) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledges"); ?>">All Time</a></li>
  <?php } ?>
<?php for($month=1;$month<=12;$month++) {
  $month_m = date('m', strtotime($month."/1/".$current_year)); 
  $month_f = date('F', strtotime($month."/1/".$current_year)); 
  ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledges/pledged/{$month_f}/{$current_year}"); ?>"><?php echo $month_f; ?></a></li>
<?php } ?>
  </ul>
</div>

<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Year : <?php echo $current_year; ?><span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
    <?php if( $current_year ) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledges"); ?>">All Time</a></li>
  <?php } ?>
<?php foreach(pledgers_years() as $year) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("pledges/pledged/{$current_month}/{$year->year}"); ?>"><?php echo $year->year; ?></a></li>
<?php } ?>
  </ul>
</div>
</div>

          </div>

<div class="row">
<div class="col-12 col-md-12 col-lg-12">

<?php if( $pledges ) { ?>
                <div class="card">
                  <div class="card-body p-0">
                    <div class="table-responsive">
                    <table class="table table-md table-hover">
                      <thead>
                        <tr>
                          <th scope="col">Date</th>
                          <th scope="col">Name</th>
                          <th scope="col" class="text-right">Amount</th>
                          <th class="text-right" scope="col" width="200px">Month / Year</th>
                        </tr>
                      </thead>
                      <tbody>
<?php foreach($pledges as $pledge) { ?>
                        <tr>
                          <td><a href="<?php echo site_url("pledges/index/".date("m", strtotime($pledge->TxnDate))."/".date("Y", strtotime($pledge->TxnDate))); ?>"><?php echo date("F d, Y", strtotime($pledge->TxnDate)); ?></a></td>
                          <td><a href="<?php echo site_url("pledgers/view/{$pledge->Customer_ListID}"); ?>"><?php echo $pledge->Customer_FullName; ?></a></td>
                          <td class="text-right"><?php echo number_format($pledge->Amount,2); ?></td>
                          <td class="text-right">

<?php echo $pledge->month . " " . $pledge->year; ?>

                            
                          </td>
                        </tr>
<?php } ?>

                      </tbody>
                    </table>
                  </div>
                  </div>
<nav class="text-center"><?php echo $pagination; ?></nav>

<?php } else { ?>
  <div class="card-body">
    No Pledger Found!
    </div>
<?php } ?>
                </div>

</div>
</div>

        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
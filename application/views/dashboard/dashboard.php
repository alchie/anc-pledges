<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar'); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Dashboard</h1>

<div class="section-header-breadcrumb">
<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Year <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
    <?php if($this->input->get('year')) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("dashboard"); ?>">Home</a></li>
  <?php } ?>
<?php foreach(pledgers_years() as $year) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("dashboard/year/{$year->year}"); ?>"><?php echo $year->year; ?></a></li>
<?php } ?>
  </ul>
</div>
</div>

          </div>
          <div class="row">

            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("pledgers/monthly"); ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                  <i class="fas fa-hand-holding-heart"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Total Monthly Pledge</h4>
                  </div>
                  <div class="card-body">
                    <?php echo number_format($total_pledge->total_pledge,2); ?>
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("pledgers"); ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                  <i class="fas fa-hands-helping"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>All Pledgers</h4>
                  </div>
                  <div class="card-body">
                    <?php echo $all_pledgers->total; ?>
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("leaders"); ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                  <i class="fas fa-user-tie"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Stewardship Leaders</h4>
                  </div>
                  <div class="card-body">
                    <?php echo $stewardship_leaders->total; ?>
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("pledges"); ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                  <i class="fas fa-hand-holding-heart"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>All Time Collections</h4>
                  </div>
                  <div class="card-body">
                    <?php echo number_format($total_pledges->total,2); ?>
                  </div>
                </div>
              </div>
              </a>
            </div>

<?php foreach($years as $year) { ?>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("dashboard/year/{$year->year}"); ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                  <i class="fas fa-heart"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4><?php echo $year->year; ?> Collections (Pledges)</h4>
                  </div>
                  <div class="card-body">
                    <?php echo number_format($year->collections->total,2); ?>
                    <small>(<?php echo number_format($year->pledges->total,2); ?>)</small>
                  </div>
                </div>
              </div>
              </a>
            </div>
<?php } ?>


            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("pledgers/monthly_remittance/".date('Y')."/" .date('m')) . "?show=all&remitted=0"; ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                  <i class="fas fa-hand-holding-heart"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4><?php echo date('F Y'); ?> No Remittance</h4>
                  </div>
                  <div class="card-body">
                    <?php echo number_format($did_not_remit,0); ?> Pledgers
                  </div>
                </div>
              </div>
              </a>
            </div>

          </div>



        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar'); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Dashboard </h1> <a class="nav-link" href="<?php echo site_url("stats/monthly/{$current_year}"); ?>"><i class="fas fa-chart-bar"></i></a>

<div class="section-header-breadcrumb">
<div class="btn-group dropleft">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $current_year; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropleft">
    <?php if($current_year) { ?>
    <li class="dropdown-item"><a href="<?php echo site_url("dashboard"); ?>">Home</a></li>
  <?php } ?>
<?php foreach(pledgers_years() as $year) { 
if($current_year==$year->year) { continue; }
  ?>
    <li class="dropdown-item"><a href="<?php echo site_url("dashboard/year/{$year->year}"); ?>"><?php echo $year->year; ?></a></li>
<?php } ?>
  </ul>
</div>
</div>

          </div>
          <div class="row">

            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("pledgers"); ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                  <i class="fas fa-hands-helping"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Monthly Pledges</h4>
                  </div>
                  <div class="card-body">
                    <?php echo number_format($stats->total_pledge,2); ?>
                  </div>
                </div>
              </div>
              </a>
            </div>

<?php for($month=1;$month<=12;$month++) {
      $month_label = date('F Y', strtotime($month."/1/".$current_year)); 
      $month_var = date('F', strtotime($month."/1/".$current_year)); 
      $month_m = date('m', strtotime($month."/1/".$current_year)); 
      $pledged_var = 'pledged_'.$month_var;
      $collected_var = 'collected_'.$month_var;
      ?>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("pledges/pledged/{$month_var}/{$current_year}"); ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                  <i class="fas fa-heart"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4><?php echo $month_label; ?> Collections (Pledges)</h4>
                  </div>
                  <div class="card-body">
                    <?php echo number_format($stats->$collected_var,2); ?>
                    <small>(<?php echo number_format($stats->$pledged_var,2); ?>)</small>
                  </div>
                </div>
              </div>
              </a>
            </div>
<?php } ?>
          </div>



        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
<?php $this->load->view('print_header'); ?>

<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <thead>
    <tr>
      <th>
      	<h3>ANC Pledgers List (<small><?php echo $pledgers_total; ?></small>)</h3>
<?php if( $this->input->get('month') ) { ?>
  <?php echo (date("F", strtotime( $this->input->get('month') . "/1/1990" ))); ?>
<?php } ?>

  </th>
    </tr>
  </thead>
  <tbody>
  	<tr>
  		<td valign="top">
      	<table width="100%" cellpadding="3" cellspacing="0" class="bordered" style="margin-top: 5px;">
      		<thead>
      		<tr>
      			<th>#</th>
      			<th class="text-right">Full Name</th>
      			<th class="text-right">Phone</th>
      			<th>Birthday</th>
      			<th>Anniversary</th>
      			<th>Amount Pledge</th>
      			<th>Frequency</th>
      		</tr>
      	</thead>
      	<tbody>
      		<?php 
$n=1;
      		foreach($pledgers as $pledger) { ?>
      		<tr>
      			<td><?php echo $n++; ?></td>
      			<td><?php echo $pledger->FullName; ?></td>
      			<td><?php echo $pledger->Phone; ?></td>
      			<td><?php echo ($pledger->JobStartDate!='0000-00-00') ? date("F d", strtotime($pledger->JobStartDate)) : ' - '; ?></td>
      			<td><?php echo ($pledger->JobProjectedEndDate!='0000-00-00') ? date("F d", strtotime($pledger->JobProjectedEndDate)) : ' - '; ?></td>
      			<td><?php echo number_format($pledger->pledge_amount,2); ?></td>
      			<td><?php echo $pledger->pledge_frequency; ?></td>
      		</tr>
      	<?php } ?>
      </tbody>
      	</table>
      </td>
  	</tr>
  </tbody>
</table>


<?php $this->load->view('print_footer'); ?>
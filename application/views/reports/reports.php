<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('main-header'); ?>

<?php $this->load->view('main-sidebar'); ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Reports</h1>

<div class="section-header-breadcrumb">

</div>

          </div>
          <div class="row">

            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("pledgers/report/all"); ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                  <i class="fas fa-list-alt"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Graphs / Line Chart</h4>
                  </div>
                  <div class="card-body">
                    Pledgers List - All
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("stats/monthly"); ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                  <i class="fas fa-chart-bar"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Graphs / Line Chart</h4>
                  </div>
                  <div class="card-body">
                    Monthly Collections
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <a href="<?php echo site_url("stats/yearly"); ?>">
              <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                  <i class="fas fa-chart-bar"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Graphs / Line Chart</h4>
                  </div>
                  <div class="card-body">
                    Yearly Collections
                  </div>
                </div>
              </div>
              </a>
            </div>

          </div>



        </section>
      </div>

<?php $this->load->view('main-footer'); ?>
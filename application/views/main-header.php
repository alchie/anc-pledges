<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title><?php echo $page_title; ?></title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url("assets/dev/bootstrap.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo base_url("assets/dev/font-awesome/all.css"); ?>">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="<?php echo base_url("assets/dev/jqvmap.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo base_url("assets/dev/weather-icons.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo base_url("assets/dev/weather-icons-wind.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo base_url("assets/dev/summernote-bs4.css"); ?>">
  <link rel="stylesheet" href="<?php echo base_url("assets/dev/Chart.min.css"); ?>">

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url("assets/css/style.css"); ?>">
  <link rel="stylesheet" href="<?php echo base_url("assets/css/components.css"); ?>">
</head>

<body class="sidebar-mini">
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto" method="get" action="<?php echo site_url("pledgers"); ?>">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search Pledger" aria-label="Search" data-width="250" name="q">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>


          </div>
        </form>
        <ul class="navbar-nav navbar-right">

<li><a href="<?php echo site_url("celebrations/index" ) . "?month=" . date("m"); ?>" class="nav-link nav-link-lg beep"><i class="fas fa-birthday-cake"></i>&nbsp;&nbsp;<?php echo date('F'); ?> Celebrations</a></li>

<li><a href="<?php echo site_url("leaders/monthly_remittance/{$current_account->ListID}"); ?>" class="nav-link nav-link-lg beep"><i class="fas fa-hands-helping"></i>&nbsp;&nbsp;My Pledgers</a></li>

          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="<?php echo base_url("assets/img/avatar/avatar-1.png"); ?>" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, <?php echo $current_account->Name; ?></div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title"><?php echo $current_account->Name; ?></div>
              <a href="<?php echo site_url("leaders/prospects/{$current_account->ListID}"); ?>" class="dropdown-item has-icon"><i class="fas fa-user-check"></i> My Prospects</a>
              <div class="dropdown-divider"></div>
              <a href="<?php echo site_url("account/logout"); ?>" class="dropdown-item has-icon text-danger"><i class="fas fa-sign-out-alt"></i> Logout</a>
            </div>
          </li>
        </ul>
      </nav>
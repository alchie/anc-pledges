<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends MY_Controller {

	public function index()
	{
		$this->load->view('tools/tools', $this->template_data->get_data());
	}

	public function sms_bat_generator() 
	{

$message = get_pledgers_option('tools','sms_bat_generator', 'message');
$logdir = get_pledgers_option('tools','sms_bat_generator', 'logdir');

$this->template_data->set('message', $message);
$this->template_data->set('logdir', $logdir);

if($this->input->get()) {

		$message = str_replace("\n", " ", $this->input->get('message'));
		$log_dir = str_replace("\n", " ", $this->input->get('log_dir'));

		update_pledgers_option('tools','sms_bat_generator', 'message', $message);
		update_pledgers_option('tools','sms_bat_generator', 'logdir', $log_dir);

		$this->load->model('Qb_customer_model');
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),TRUE);
		if( $this->input->get('leader') ) {
			$pledgers->setSalesrepListid($this->input->get('leader'),TRUE);
		}
		$pledgers->set_select('c.ListID');
		$pledgers->set_select('c.Name');
		
		$pledgers->set_limit(0);
		$pledgers->set_order("c.Name", 'ASC');

		$pledgers->set_select('c.*');

			$this->load->model('Qb_salesreceipt_salesreceiptline_model');

			$month_var = date('F', strtotime($this->input->get('year')."-".$this->input->get('month')."-1"));

			$month_pledge = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$month_pledge->setItemListid($this->config->item('pledgers_item_id'),true);
			$month_pledge->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$month_pledge->set_where('s.Customer_ListID=c.ListID');
			$month_pledge->set_limit(1);
			$month_pledge->set_select('j.Amount as jAmount');
			//$month_pledge->set_where('j.Descrip LIKE "%{{'.$month_var.'_'.$current_year.'}}%"');
			
			$this->load->model('Qb_dataext_model');
			$dextmonth = new $this->Qb_dataext_model('d');
			$dextmonth->setEntitytype('SalesReceiptLine',true);
			$dextmonth->setDataextname('month',true);
			$dextmonth->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextmonth->set_select('d.DataExtValue');
			//$pledgers->set_select('('.$dextmonth->get_compiled_select().') as month');

			$dextmonth->setDataextvalue($month_var,true);
			$dextmonth->set_select('COUNT(*)', NULL, TRUE);
			$month_pledge->set_where('(('.$dextmonth->get_compiled_select().') > 0)');

			$dextyear = new $this->Qb_dataext_model('d');
			$dextyear->setEntitytype('SalesReceiptLine',true);
			$dextyear->setDataextname('year',true);
			$dextyear->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextyear->set_select('d.DataExtValue');
			//$pledgers->set_select('('.$dextyear->get_compiled_select().') as year');

			$dextyear->setDataextvalue($this->input->get('year'),true);
			$dextyear->set_select('COUNT(*)', NULL, TRUE);
			$month_pledge->set_where('(('.$dextyear->get_compiled_select().') > 0)');

			$pledgers->set_select('('.$month_pledge->get_compiled_select().') as current_month');

		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount") as pledge_amount');

		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") as pledge_frequency');

		switch($this->input->get('frequency')) {
			case 'monthly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Monthly")');
			break;
			case 'weekly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Weekly")');
			break;
			case 'quarterly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Quarterly")');
			break;
			case 'yearly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Yearly")');
			break;
			case 'lumpsum':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "One-Time / Lumpsum")');
			break;
		}
		

		switch($this->input->get('pledgers')) {
			case 'no_remittance':
				$pledgers->set_where('(('.$month_pledge->get_compiled_select().') IS NULL)');
			break;
			case 'with_remittance':
				$pledgers->set_where('(('.$month_pledge->get_compiled_select().') IS NOT NULL)');
			break;
			case 'birthday':
				$pledgers->set_where('(MONTH(c.JobStartDate)='.$this->input->get('month').')');
			break;
			case 'anniversary':
				$pledgers->set_where('(MONTH(c.JobProjectedEndDate)='.$this->input->get('month').')');
			break;
			default:
			break;
		}

	$pledgers_data = $pledgers->populate();

	$this->template_data->set('pledgers', $pledgers_data);
	$this->template_data->set('pledgers_total', $pledgers->count_all_results());	



if( $this->input->post('send_to') ) {



	$output="@echo off\n";
	$output.="cd D:\\Alchie\\Projects\\gammu\\Gammu-1.33.0-Windows\\bin\n";
	$output.="echo Identifying your sms modem device, please wait...\n";
	$output.="echo ----------------------------------------------------------\n";
	$output.="gammu identify\n";
	$output.="echo To start sending messages,\n";
	$output.="pause\n";


	$message = trim(@$message);

	if( $pledgers_data ) {
		$count = 0;

		$msg_len = strlen($message);
		$output .= "IF NOT EXIST \"{$log_dir}09462559955.log\" gammu sendsms TEXT 09462559955 -len {$msg_len} -text \"{$message}\" > \"{$log_dir}09462559955.log\" | echo Sending to 09462559955 - {$count}/[TOTAL_COUNT]\n";
		$output .= "IF NOT EXIST \"{$log_dir}09457351890.log\" gammu sendsms TEXT 09457351890 -len {$msg_len} -text \"{$message}\" > \"{$log_dir}09457351890.log\" | echo Sending to 09457351890 - {$count}/[TOTAL_COUNT]\n";
		$output.="exit\n";
		
		foreach( $pledgers_data as $num ) {

			if( !in_array($num->ListID, $this->input->post('send_to')) ) {
				continue;
			}

			$num = trim( $num->Phone );
			
			if( $num ) {
				$count++;
				$output .= "IF NOT EXIST \"{$log_dir}{$num}.log\" gammu sendsms TEXT {$num} -len {$msg_len} -text \"{$message}\" > \"{$log_dir}{$num}.log\" | echo Sending to {$num} - {$count}/[TOTAL_COUNT]\n";
			}
		}

		$output.="echo Done...\n";
		$output.="pause\n";
		$output.="exit\n";

		$output = str_replace('[TOTAL_COUNT]', $count, $output);
	}

	$this->load->helper('download');
	force_download("send_sms.bat", $output, 'text/plain');

}	

}


		$this->load->model('Qb_salesrep_model');
		$leaders = new $this->Qb_salesrep_model('s');
		$leaders->set_select('*');
		$leaders->set_select('(SELECT COUNT(*) FROM qb_customer c WHERE c.SalesRep_ListID=s.ListID AND c.CustomerType_ListID="'.$this->config->item('pledgers_type_id').'") as pledgers');
		$leaders->set_limit(0);

		$this->template_data->set('leaders', $leaders->populate());

		$this->load->view('tools/sms_bat_generator', $this->template_data->get_data());
	}
}

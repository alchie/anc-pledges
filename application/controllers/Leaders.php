<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leaders extends MY_Controller {

	public function index($start=0)
	{

		$this->load->model('Qb_salesrep_model');
		$leaders = new $this->Qb_salesrep_model('s');
		$leaders->set_select('*');
		$leaders->set_select('(SELECT COUNT(*) FROM qb_customer c WHERE c.SalesRep_ListID=s.ListID AND c.CustomerType_ListID="'.$this->config->item('pledgers_type_id').'") as pledgers');
		$leaders->set_select('(SELECT COUNT(*) FROM qb_customer c WHERE c.SalesRep_ListID=s.ListID AND c.CustomerType_ListID="'.$this->config->item('prospects_type_id').'") as prospects');
		$leaders->set_start($start);
		$leaders->set_limit(10);

		$this->template_data->set('leaders', $leaders->populate());
		$this->template_data->set('leaders_total', $leaders->count_all_results());

		$this->template_data->set('pagination', stisla_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/leaders/index"),
			'total_rows' => $leaders->count_all_results(),
			'per_page' => $leaders->get_limit()
		)));

		$this->load->view('leaders/leaders', $this->template_data->get_data());
	}

	public function pledgers($ListID, $start=0) {

		$this->load->model('Qb_salesrep_model');
		$leader = new $this->Qb_salesrep_model('s');
		$leader->setSalesrepentityListid($ListID,true);
		//$leader->set_join('qb_employee e', 'e.ListID=s.SalesRepEntity_ListID');
		$leader_data = $leader->get();
		$this->template_data->set('leader', $leader_data);

		$this->load->model('Qb_customer_model');
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),TRUE);
		$pledgers->setSalesrepListid($leader_data->ListID,TRUE);
		$pledgers->set_select('*');
		$pledgers->set_start($start);
		$pledgers->set_limit(10);

		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount") as pledge_amount');
		
		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") as pledge_frequency');
		
		$this->template_data->set('pledgers', $pledgers->populate());

		$this->template_data->set('pagination', stisla_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/leaders/pledgers"),
			'total_rows' => $pledgers->count_all_results(),
			'per_page' => $pledgers->get_limit()
		)));

		$this->load->view('leaders/leaders_pledgers', $this->template_data->get_data());
	}

	public function prospects($ListID, $start=0) {

		$this->load->model('Qb_salesrep_model');
		$leader = new $this->Qb_salesrep_model('s');
		$leader->setSalesrepentityListid($ListID,true);
		//$leader->set_join('qb_employee e', 'e.ListID=s.SalesRepEntity_ListID');
		$leader_data = $leader->get();
		$this->template_data->set('leader', $leader_data);

		$this->load->model('Qb_customer_model');
		$prospects = new $this->Qb_customer_model('c');
		$prospects->setCustomertypeListid($this->config->item('prospects_type_id'),TRUE);
		$prospects->setSalesrepListid($leader_data->ListID,TRUE);
		$prospects->set_select('*');
		$prospects->set_start($start);
		$prospects->set_limit(10);
		$this->template_data->set('prospects', $prospects->populate());

		$this->template_data->set('pagination', stisla_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/leaders/prospects"),
			'total_rows' => $prospects->count_all_results(),
			'per_page' => $prospects->get_limit()
		)));

		$this->load->view('leaders/leaders_prospects', $this->template_data->get_data());
	}

	public function monthly_remittance($ListID, $current_month=0, $current_year=0, $start=0)
	{

		$current_month = ($current_month) ? $current_month : date('m');
		$this->template_data->set('current_month', $current_month);

		$current_year = ($current_year) ? $current_year : date('Y');
		$this->template_data->set('current_year', $current_year);

		$this->load->model('Qb_salesrep_model');
		$leader = new $this->Qb_salesrep_model('s');
		$leader->setSalesrepentityListid($ListID,true);
		//$leader->set_join('qb_employee e', 'e.ListID=s.SalesRepEntity_ListID');
		$leader_data = $leader->get();
		$this->template_data->set('leader', $leader_data);

		$this->load->model('Qb_customer_model');
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),TRUE);
		$pledgers->setSalesrepListid($leader_data->ListID,TRUE);
		$pledgers->set_select('c.ListID');
		$pledgers->set_select('c.Name');
		$pledgers->set_start($start);
		
		$pledgers->set_limit(50);

		$pledgers->set_select('c.*');

		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
/*
		for($month=1;$month<=12;$month++) {
			$month_var = date('F', strtotime($month."/1/".$current_year));
			$month_pledge = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$month_pledge->setItemListid($this->config->item('pledgers_item_id'),true);
			$month_pledge->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$month_pledge->set_where('s.Customer_ListID=c.ListID');
			$month_pledge->set_limit(1);
			$month_pledge->set_select('j.Amount as jAmount');
			$month_pledge->set_where('j.Descrip LIKE "%{{'.$month_var.'_'.$current_year.'}}%"');
			$pledgers->set_select('('.$month_pledge->get_compiled_select().') as '.$month_var);
		}
*/

			$month_var = date('F', strtotime($current_month."/1/".$current_year));
			$month_pledge = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$month_pledge->setItemListid($this->config->item('pledgers_item_id'),true);
			$month_pledge->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$month_pledge->set_where('s.Customer_ListID=c.ListID');
			$month_pledge->set_limit(1);
			$month_pledge->set_select('j.Amount as jAmount');
			//$month_pledge->set_where('j.Descrip LIKE "%{{'.$month_var.'_'.$current_year.'}}%"');
			

			$this->load->model('Qb_dataext_model');
			$dextmonth = new $this->Qb_dataext_model('d');
			$dextmonth->setEntitytype('SalesReceiptLine',true);
			$dextmonth->setDataextname('month',true);
			$dextmonth->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextmonth->set_select('d.DataExtValue');
			//$pledgers->set_select('('.$dextmonth->get_compiled_select().') as month');

			$dextmonth->setDataextvalue($month_var,true);
			$dextmonth->set_select('COUNT(*)', NULL, TRUE);
			$month_pledge->set_where('(('.$dextmonth->get_compiled_select().') > 0)');

			$dextyear = new $this->Qb_dataext_model('d');
			$dextyear->setEntitytype('SalesReceiptLine',true);
			$dextyear->setDataextname('year',true);
			$dextyear->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextyear->set_select('d.DataExtValue');
			//$pledgers->set_select('('.$dextyear->get_compiled_select().') as year');

			$dextyear->setDataextvalue($current_year,true);
			$dextyear->set_select('COUNT(*)', NULL, TRUE);
			$month_pledge->set_where('(('.$dextyear->get_compiled_select().') > 0)');

			$pledgers->set_select('('.$month_pledge->get_compiled_select().') as current_month');

		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") as pledge_frequency');

		switch($this->input->get('show')) {
			case 'all':
			break;
			case 'monthly':
			default:
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Monthly")');
			break;
		}

		switch($this->input->get('remitted')) {
			case '0':
				$pledgers->set_where('(('.$month_pledge->get_compiled_select().') IS NULL)');
			break;
			case '1':
				$pledgers->set_where('(('.$month_pledge->get_compiled_select().') IS NOT NULL)');
			break;
			default:
			break;
		}

		$this->template_data->set('pledgers', $pledgers->populate());
		$this->template_data->set('pledgers_total', $pledgers->count_all_results());

		$this->template_data->set('pagination', stisla_pagination(array(
			'uri_segment' => 6,
			'base_url' => base_url( $this->config->item('index_page') . "/leaders/monthly_remittance/{$ListID}/{$current_month}/{$current_year}"),
			'total_rows' => $pledgers->count_all_results(),
			'per_page' => $pledgers->get_limit()
		)));

		$this->load->model('Qb_salesrep_model');
		$leaders = new $this->Qb_salesrep_model('s');
		$leaders->set_select('*');
		$leaders->set_limit(0);
		$this->template_data->set('leaders', $leaders->populate());

		$this->load->view('leaders/leaders_monthly_remittance', $this->template_data->get_data());
	}

}

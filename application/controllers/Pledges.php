<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pledges extends MY_Controller {

	public function index($current_month=0, $current_year=0, $start=0)
	{

		//$current_month = ($current_month) ? $current_month : date('m');
		$this->template_data->set('current_month', $current_month);
		$current_year = ($current_year) ? $current_year : date('Y');
		$this->template_data->set('current_year', $current_year);

		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		$pledges = new $this->Qb_salesreceipt_salesreceiptline_model('j');
		$pledges->setItemListid($this->config->item('pledgers_item_id'),true);
		$pledges->set_start($start);
		$pledges->set_select("j.*");
		$pledges->set_select("s.*");
		$pledges->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
		$pledges->set_order('s.TxnDate', 'ASC');

		if( $current_month ) {
			$pledges->set_where('MONTH(s.TxnDate)="'.$current_month.'"');
		}
		$pledges->set_where('YEAR(s.TxnDate)="'.$current_year.'"');

			$this->load->model('Qb_dataext_model');
			$dextmonth = new $this->Qb_dataext_model('d');
			$dextmonth->setEntitytype('SalesReceiptLine',true);
			$dextmonth->setDataextname('month',true);
			$dextmonth->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextmonth->set_select('d.DataExtValue');
			$pledges->set_select('('.$dextmonth->get_compiled_select().') as month');

			$dextyear = new $this->Qb_dataext_model('d');
			$dextyear->setEntitytype('SalesReceiptLine',true);
			$dextyear->setDataextname('year',true);
			$dextyear->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextyear->set_select('d.DataExtValue');
			$pledges->set_select('('.$dextyear->get_compiled_select().') as year');

		$this->template_data->set('pledges', $pledges->populate());
		$this->template_data->set('pledges_count', $pledges->count_all_results());

		$this->template_data->set('pagination', stisla_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url( $this->config->item('index_page') . "/pledges/index/{$current_month}/{$current_year}"),
			'total_rows' => $pledges->count_all_results(),
			'per_page' => $pledges->get_limit()
		)));
		
		$pledges->set_select("SUM(j.Amount) as total", null, true);
		$pledges->set_start(0);
		$pledges->set_limit(0);
		$this->template_data->set('pledges_total', $pledges->get());

		$this->load->view('pledges/pledges', $this->template_data->get_data());
	}

	public function pledged($current_month=0, $current_year=0, $start=0)
	{

		$current_month = ($current_month) ? $current_month : date('F');
		$this->template_data->set('current_month', $current_month);
		$current_year = ($current_year) ? $current_year : date('Y');
		$this->template_data->set('current_year', $current_year);

		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		$pledges = new $this->Qb_salesreceipt_salesreceiptline_model('j');
		$pledges->setItemListid($this->config->item('pledgers_item_id'),true);
		$pledges->set_start($start);
		$pledges->set_select("j.*");
		$pledges->set_select("s.*");
		$pledges->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
		$pledges->set_order('s.TxnDate', 'ASC');

		//$pledges->set_where('MONTH(s.TxnDate)="'.$current_month.'"');
		//$pledges->set_where('YEAR(s.TxnDate)="'.$current_year.'"');

			$this->load->model('Qb_dataext_model');
			$dextmonth = new $this->Qb_dataext_model('d');
			$dextmonth->setEntitytype('SalesReceiptLine',true);
			$dextmonth->setDataextname('month',true);
			$dextmonth->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextmonth->set_select('d.DataExtValue');
			$pledges->set_select('('.$dextmonth->get_compiled_select().') as month');

			$dextmonth->setDataextvalue($current_month,true);
			$dextmonth->set_select('COUNT(*)', NULL, TRUE);
			$pledges->set_where('(('.$dextmonth->get_compiled_select().') > 0)');

			$dextyear = new $this->Qb_dataext_model('d');
			$dextyear->setEntitytype('SalesReceiptLine',true);
			$dextyear->setDataextname('year',true);
			$dextyear->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextyear->set_select('d.DataExtValue');
			$pledges->set_select('('.$dextyear->get_compiled_select().') as year');

			$dextyear->setDataextvalue($current_year,true);
			$dextyear->set_select('COUNT(*)', NULL, TRUE);
			$pledges->set_where('(('.$dextyear->get_compiled_select().') > 0)');

		//$month_var = date('F', strtotime($current_month."/1/".$current_year));
		//$pledges->set_where('j.Descrip LIKE "%{{'.$month_var.'_'.$current_year.'}}%"');

		$this->template_data->set('pledges', $pledges->populate());
		$this->template_data->set('pledges_count', $pledges->count_all_results());

		$this->template_data->set('pagination', stisla_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url( $this->config->item('index_page') . "/pledges/pledged/{$current_month}/{$current_year}"),
			'total_rows' => $pledges->count_all_results(),
			'per_page' => $pledges->get_limit()
		)));

		$pledges->set_select("SUM(j.Amount) as total", null, true);
		$pledges->set_start(0);
		$pledges->set_limit(0);
		$this->template_data->set('pledges_total', $pledges->get());
		
		$this->load->view('pledges/pledges_pledged', $this->template_data->get_data());
	}

}

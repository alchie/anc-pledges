<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prospects extends MY_Controller {

	public function index($start=0)
	{

		$this->load->model('Qb_customer_model');
		$prospects = new $this->Qb_customer_model('c');
		$prospects->setCustomertypeListid($this->config->item('prospects_type_id'),TRUE);
		$prospects->set_select('*');
		$prospects->set_start($start);
		$prospects->set_limit(10);
		$this->template_data->set('prospects', $prospects->populate());
		$this->template_data->set('prospects_total', $prospects->count_all_results());

		$this->template_data->set('pagination', stisla_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/prospects/index"),
			'total_rows' => $prospects->count_all_results(),
			'per_page' => $prospects->get_limit()
		)));
		
		$this->load->view('prospects/prospects', $this->template_data->get_data());
	}
}

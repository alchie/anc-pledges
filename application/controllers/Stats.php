<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stats extends MY_Controller {

	public function index()
	{
		$this->monthly();
	}


	public function monthly($current_year=0)
	{
		$current_year = ($current_year) ? $current_year : date('Y');
		$this->template_data->set('current_year', $current_year);
		
		$this->load->model('Qb_dataext_model');

		$pledge_amount = new $this->Qb_dataext_model('d');
		$pledge_amount->setEntitytype('Customer',true);
		$pledge_amount->setDataextname('pledge_amount',true);
		$pledge_amount->set_select('SUM(d.DataExtValue) as total_pledge');

		$pledge_frequency = new $this->Qb_dataext_model('f');
		$pledge_frequency->setEntitytype('Customer',true);
		$pledge_frequency->setDataextname('pledge_frequency',true);
		$pledge_frequency->set_select('f.DataExtValue');
		$pledge_frequency->set_limit(1);
		$pledge_frequency->set_where('f.Entity_ListID=d.Entity_ListID');

		$pledge_amount->set_where('(('.$pledge_frequency->get_compiled_select().')="Monthly")');

		$pledge_amount->set_join('qb_customer c', 'c.ListID=d.Entity_ListID');
		$pledge_amount->set_where('c.CustomerType_ListID="'.$this->config->item('pledgers_type_id').'"');

		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		for($month=1;$month<=12;$month++) {
			
			$month_var = date('F', strtotime($month."/1/".$current_year));
			
			$month_pledges = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$month_pledges->setItemListid($this->config->item('pledgers_item_id'),true);
			$month_pledges->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$month_pledges->set_limit(1);
			$month_pledges->set_select('SUM(j.Amount)');
			//$month_pledges->set_where('j.Descrip LIKE "%{{'.$month_var.'_'.$current_year.'}}%"');

			$dextmonth = new $this->Qb_dataext_model('d');
			$dextmonth->setEntitytype('SalesReceiptLine',true);
			$dextmonth->setDataextname('month',true);
			$dextmonth->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextmonth->set_select('d.DataExtValue');
			//$month_pledges->set_select('('.$dextmonth->get_compiled_select().') as ' . $month_var);

			$dextmonth->setDataextvalue($month_var,true);
			$dextmonth->set_select('COUNT(*)', NULL, TRUE);
			$month_pledges->set_where('(('.$dextmonth->get_compiled_select().') > 0)');

			$dextyear = new $this->Qb_dataext_model('d');
			$dextyear->setEntitytype('SalesReceiptLine',true);
			$dextyear->setDataextname('year',true);
			$dextyear->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextyear->set_select('d.DataExtValue');
			//$month_pledges->set_select('('.$dextyear->get_compiled_select().') as year');

			$dextyear->setDataextvalue($current_year,true);
			$dextyear->set_select('COUNT(*)', NULL, TRUE);
			$month_pledges->set_where('(('.$dextyear->get_compiled_select().') > 0)');
			$pledge_amount->set_select('('.$month_pledges->get_compiled_select().') as pledged_'.$month_var);

			$month_collections = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$month_collections->setItemListid($this->config->item('pledgers_item_id'),true);
			$month_collections->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$month_collections->set_limit(1);
			$month_collections->set_select('SUM(j.Amount)');
			$month_collections->set_where('MONTH(s.TxnDate) = "'.$month.'"');
			$month_collections->set_where('YEAR(s.TxnDate) = "'.$current_year.'"');
			
			$pledge_amount->set_select('('.$month_collections->get_compiled_select().') as collected_'.$month_var);
			
		}

		$this->template_data->set('stats', $pledge_amount->get());

		$this->load->view('stats/monthly', $this->template_data->get_data());
	}

	public function yearly()
	{

		$this->load->model('Qb_dataext_model');

		$yearly_pledge = new $this->Qb_dataext_model('d');
		$yearly_pledge->setEntitytype('Customer',true);
		$yearly_pledge->setDataextname('pledge_amount',true);
		$yearly_pledge->set_select('SUM(d.DataExtValue) as yearly_pledge');

		$pledge_frequency = new $this->Qb_dataext_model('f');
		$pledge_frequency->setEntitytype('Customer',true);
		$pledge_frequency->setDataextname('pledge_frequency',true);
		$pledge_frequency->set_select('f.DataExtValue');
		$pledge_frequency->set_limit(1);
		$pledge_frequency->set_where('f.Entity_ListID=d.Entity_ListID');

		$yearly_pledge->set_where('(('.$pledge_frequency->get_compiled_select().')="Yearly")');

		$yearly_pledge->set_join('qb_customer c', 'c.ListID=d.Entity_ListID');
		$yearly_pledge->set_where('c.CustomerType_ListID="'.$this->config->item('pledgers_type_id').'"');


		$monthly_pledge = new $this->Qb_dataext_model('d');
		$monthly_pledge->setEntitytype('Customer',true);
		$monthly_pledge->setDataextname('pledge_amount',true);
		$monthly_pledge->set_select('SUM(d.DataExtValue) as monthly_pledge');

		$pledge_frequency = new $this->Qb_dataext_model('f');
		$pledge_frequency->setEntitytype('Customer',true);
		$pledge_frequency->setDataextname('pledge_frequency',true);
		$pledge_frequency->set_select('f.DataExtValue');
		$pledge_frequency->set_limit(1);
		$pledge_frequency->set_where('f.Entity_ListID=d.Entity_ListID');

		$monthly_pledge->set_where('(('.$pledge_frequency->get_compiled_select().')="Monthly")');

		$monthly_pledge->set_join('qb_customer c', 'c.ListID=d.Entity_ListID');
		$monthly_pledge->set_where('c.CustomerType_ListID="'.$this->config->item('pledgers_type_id').'"');

		$yearly_pledge->set_select('('.$monthly_pledge->get_compiled_select().') as monthly_pledge');


		$quarterly_pledge = new $this->Qb_dataext_model('d');
		$quarterly_pledge->setEntitytype('Customer',true);
		$quarterly_pledge->setDataextname('pledge_amount',true);
		$quarterly_pledge->set_select('SUM(d.DataExtValue) as monthly_pledge');

		$pledge_frequency = new $this->Qb_dataext_model('f');
		$pledge_frequency->setEntitytype('Customer',true);
		$pledge_frequency->setDataextname('pledge_frequency',true);
		$pledge_frequency->set_select('f.DataExtValue');
		$pledge_frequency->set_limit(1);
		$pledge_frequency->set_where('f.Entity_ListID=d.Entity_ListID');

		$quarterly_pledge->set_where('(('.$pledge_frequency->get_compiled_select().')="Monthly")');

		$quarterly_pledge->set_join('qb_customer c', 'c.ListID=d.Entity_ListID');
		$quarterly_pledge->set_where('c.CustomerType_ListID="'.$this->config->item('pledgers_type_id').'"');

		$yearly_pledge->set_select('('.$quarterly_pledge->get_compiled_select().') as quarterly_pledge');

		$this->load->model('Qb_salesreceipt_salesreceiptline_model');

		foreach(pledgers_years() as $year) {

			$year_total = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$year_total->setItemListid($this->config->item('pledgers_item_id'),true);
			$year_total->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$year_total->set_limit(1);
			$year_total->set_select('SUM(j.Amount)');
			//$year_total->set_where('j.Descrip LIKE "%_'.$year->year.'}}%"');

			$dextyear = new $this->Qb_dataext_model('d');
			$dextyear->setEntitytype('SalesReceiptLine',true);
			$dextyear->setDataextname('year',true);
			$dextyear->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextyear->set_select('d.DataExtValue');
			$dextyear->setDataextvalue($year->year,true);
			$dextyear->set_select('COUNT(*)', NULL, TRUE);

			$year_total->set_where('(('.$dextyear->get_compiled_select().') > 0)');

			$yearly_pledge->set_select('('.$year_total->get_compiled_select().') as Y'.$year->year);

			$year_collections = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$year_collections->setItemListid($this->config->item('pledgers_item_id'),true);
			$year_collections->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$year_collections->set_limit(1);
			$year_collections->set_select('SUM(j.Amount)');
			$year_collections->set_where('YEAR(s.TxnDate) = "'.$year->year.'"');
			
			$yearly_pledge->set_select('('.$year_collections->get_compiled_select().') as C'.$year->year);

			
		}

		$this->template_data->set('stats', $yearly_pledge->get());

		$this->load->view('stats/yearly', $this->template_data->get_data());
	}
}

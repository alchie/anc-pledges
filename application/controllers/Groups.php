<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends MY_Controller {

	public function index($start=0)
	{

		$this->load->model('Qb_jobtype_model');
		$groups = new $this->Qb_jobtype_model('s');
		$groups->set_select('*');
		$groups->setIsactive(1,true);
		$groups->set_order('s.FullName', 'ASC');
		
		$groups->set_select('(SELECT COUNT(*) FROM qb_customer c WHERE c.JobType_ListID=s.ListID) as members');

		$groups->set_start($start);
		$groups->set_limit(10);

		$this->template_data->set('groups', $groups->populate());
		//$this->template_data->set('groups', $groups->recursive('Parent_ListID', '', 'ListID', 2));

		$this->template_data->set('pagination', stisla_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/groups/index"),
			'total_rows' => $groups->count_all_results(),
			'per_page' => $groups->get_limit()
		)));

		$this->load->view('groups/groups', $this->template_data->get_data());
	}

	public function members($ListID, $start=0)
	{
		$this->load->model('Qb_jobtype_model');
		$group = new $this->Qb_jobtype_model('s');
		$group->setListid($ListID,true);
		$this->template_data->set('group', $group->get());

		$this->load->model('Qb_customer_model');
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),TRUE);
		$pledgers->setJobtypeListid($ListID,TRUE);
		$pledgers->set_select('*');
		$pledgers->set_start($start);
		$pledgers->set_limit(10);

		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount") as pledge_amount');
		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") as pledge_frequency');

		$this->template_data->set('pledgers', $pledgers->populate());
		$this->template_data->set('count', $pledgers->count_all_results());

		$this->template_data->set('pagination', stisla_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/groups/members/{$ListID}"),
			'total_rows' => $pledgers->count_all_results(),
			'per_page' => $pledgers->get_limit()
		)));

		$this->load->view('groups/groups_members', $this->template_data->get_data());
	}

}

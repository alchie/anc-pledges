<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {

	public function index()
	{
		$this->load->view('reports/reports', $this->template_data->get_data());
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pledgers extends MY_Controller {

	private $frequency = 'all';

	public function index($start=0)
	{
		$this->template_data->set('frequency', $this->frequency);

		$this->load->model('Qb_customer_model');
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),TRUE);
		$pledgers->set_select('*');
		$pledgers->set_start($start);
		$pledgers->set_limit(10);
		$pledgers->set_order('c.Name', 'ASC');

		if( $this->input->get('q') ) {
			$pledgers->set_where('(c.Name LIKE "%'.$this->input->get('q').'%"');
			$pledgers->set_where_or('c.Phone LIKE "%'.$this->input->get('q').'%"');
			$pledgers->set_where_or('c.ShipAddress_Addr1 LIKE "%'.$this->input->get('q').'%"');
			$pledgers->set_where_or('c.ShipAddress_Addr2 LIKE "%'.$this->input->get('q').'%"');
			$pledgers->set_where_or('c.ShipAddress_Addr3 LIKE "%'.$this->input->get('q').'%"');
			$pledgers->set_where_or('c.ShipAddress_Addr4 LIKE "%'.$this->input->get('q').'%"');
			$pledgers->set_where_or('c.ShipAddress_Addr5 LIKE "%'.$this->input->get('q').'%")');
		}

		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount") as pledge_amount');

		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") as pledge_frequency');

		$pagination_uri = 'index';
		switch($this->frequency) {
			case 'lumpsum':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "One-Time / Lumpsum")');
				$pagination_uri = "filter/{$this->frequency}";
			break;
			case 'monthly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Monthly")');
				$pagination_uri = "filter/{$this->frequency}";
			break;
			case 'weekly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Weekly")');
				$pagination_uri = "filter/{$this->frequency}";
			break;
			case 'quarterly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Quarterly")');
				$pagination_uri = "filter/{$this->frequency}";
			break;
			case 'yearly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Yearly")');
				$pagination_uri = "filter/{$this->frequency}";
			break;
			default:
			case 'all':
			break;
		}
		$this->template_data->set('pledgers', $pledgers->populate());
		$this->template_data->set('pledgers_total', $pledgers->count_all_results());

		$this->template_data->set('pagination', stisla_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/pledgers/{$pagination_uri}"),
			'total_rows' => $pledgers->count_all_results(),
			'per_page' => $pledgers->get_limit()
		),
		'?q=' . $this->input->get('q')
		));

		$pledgers->set_start(0);
		$pledgers->set_select('SUM((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount")) as total_pledges', 0, true);
		$this->template_data->set('total_pledges', $pledgers->get()); 

		$this->load->view('pledgers/pledgers', $this->template_data->get_data());
	}

	public function filter($freq='all', $start=0)
	{
		$this->frequency = $freq;
		$this->index($start);
	}

	public function monthly($start=0)
	{

		$this->template_data->set('frequency', 'monthly');

		$this->load->model('Qb_customer_model');
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),TRUE);
		$pledgers->set_select('*');
		$pledgers->set_start($start);
		$pledgers->set_limit(10);
		$pledgers->set_order('c.Name', 'ASC');

		switch ($this->input->get('sort')) {
			case 'NAME_ASC':
				$pledgers->set_order('c.Name', 'ASC');
				break;
			case 'NAME_DESC':
				$pledgers->set_order('c.Name', 'DESC');
				break;
			case 'PLEDGE_ASC':
				$pledgers->set_order('pledge_amount2', 'ASC');
				$pledgers->set_order('c.Name', 'ASC');
				break;
			case 'PLEDGE_DESC':
				$pledgers->set_order('pledge_amount2', 'DESC');
				$pledgers->set_order('c.Name', 'ASC');
				break;
			default:
				$pledgers->set_order('c.Name', 'ASC');
				break;
		}


		$pledgers->set_select('CONVERT(SUBSTRING_INDEX((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount"),\'-\',-1),UNSIGNED INTEGER) AS pledge_amount2');

		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount") as pledge_amount');

		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") as pledge_frequency');

		$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") = "Monthly")');

		$this->template_data->set('pledgers', $pledgers->populate());
		$this->template_data->set('pledgers_total', $pledgers->count_all_results());

		$this->template_data->set('pagination', stisla_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/pledgers/monthly"),
			'total_rows' => $pledgers->count_all_results(),
			'per_page' => $pledgers->get_limit()
		),
		'?sort=' . $this->input->get('sort')
		));

		$pledgers->set_start(0);
		$pledgers->set_select('SUM((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount")) as total_pledges', 0, true);
		$this->template_data->set('total_pledges', $pledgers->get()); 

		$this->load->view('pledgers/pledgers', $this->template_data->get_data());
	}

	public function monthly_remittance($current_year, $current_month=0, $start=0)
	{

		$this->template_data->set('current_year', $current_year);
		$this->template_data->set('current_month', $current_month);

		$this->load->model('Qb_customer_model');
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),TRUE);
		$pledgers->set_select('c.*');
		$pledgers->set_select('c.Name');
		$pledgers->set_select('c.Phone');
		$pledgers->set_select('c.SalesRep_ListID');
		$pledgers->set_select('c.SalesRep_FullName');
		$pledgers->set_start($start);
		$pledgers->set_limit(10);

		if( $this->input->get('show') == 'all' ) {
			$pledgers->set_start(0);
			$pledgers->set_limit(0);
		}

		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		$this->load->model('Qb_dataext_model');

		for($month=1;$month<=12;$month++) {

			if( ($current_month) && ($current_month!=$month) ) {
				continue;
			}
			
			$month_var = date('F', strtotime($month."/1/".$current_year));
			$month_pledge = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$month_pledge->setItemListid($this->config->item('pledgers_item_id'),true);
			$month_pledge->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$month_pledge->set_where('s.Customer_ListID=c.ListID');
			$month_pledge->set_limit(1);
			$month_pledge->set_select('j.Amount as jAmount');
			//$month_pledge->set_where('j.Descrip LIKE "%{{'.$month_var.'_'.$current_year.'}}%"');

			$dextmonth = new $this->Qb_dataext_model('d');
			$dextmonth->setEntitytype('SalesReceiptLine',true);
			$dextmonth->setDataextname('month',true);
			$dextmonth->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextmonth->set_select('d.DataExtValue');
			//$month_pledge->set_select('('.$dextmonth->get_compiled_select().') as month');

			$dextmonth->setDataextvalue($month_var,true);
			$dextmonth->set_select('COUNT(*)', NULL, TRUE);
			$month_pledge->set_where('(('.$dextmonth->get_compiled_select().') > 0)');

			$dextyear = new $this->Qb_dataext_model('d');
			$dextyear->setEntitytype('SalesReceiptLine',true);
			$dextyear->setDataextname('year',true);
			$dextyear->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextyear->set_select('d.DataExtValue');
			//$month_pledge->set_select('('.$dextyear->get_compiled_select().') as year');

			$dextyear->setDataextvalue($current_year,true);
			$dextyear->set_select('COUNT(*)', NULL, TRUE);
			$month_pledge->set_where('(('.$dextyear->get_compiled_select().') > 0)');


			$pledgers->set_select('('.$month_pledge->get_compiled_select().') as '.$month_var);

			switch($this->input->get('remitted')) {
				case '0':
					$pledgers->set_where('(('.$month_pledge->get_compiled_select().') IS NULL)');
				break;
				case '1':
					$pledgers->set_where('(('.$month_pledge->get_compiled_select().') IS NOT NULL)');
				break;
			}
		}

		$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") = "Monthly")');
		
		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount") as pledge_amount');
		
		if($this->input->get('print')) {
			$view_file = 'pledgers_monthly_remittance_print';
			$pledgers->set_order('c.SalesRep_FullName', 'ASC');
		} else {
			$view_file = 'pledgers_monthly_remittance';
		}


		if($this->input->get('sort_leader')) {
			$pledgers->set_order('c.SalesRep_FullName', $this->input->get('sort_leader'));
		}
		$pledgers->set_order('c.Name', 'ASC');

		$this->template_data->set('pledgers', $pledgers->populate());
		$this->template_data->set('pledgers_total', $pledgers->count_all_results());

		$this->template_data->set('pagination', stisla_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url( $this->config->item('index_page') . "/pledgers/monthly_remittance/{$current_year}/{$current_month}"),
			'total_rows' => $pledgers->count_all_results(),
			'per_page' => $pledgers->get_limit()
		), '?show='.$this->input->get('show').'&remitted='.$this->input->get('remitted')));

		$this->load->view('pledgers/' . $view_file, $this->template_data->get_data());
	}

	public function view($ListID, $start=0)
	{
		$this->load->model('Qb_customer_model');
		$pledger = new $this->Qb_customer_model('c');
		$pledger->setListid($ListID,true);
		$pledger->set_select('c.*');
		$pledger->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount") as pledge_amount');

		$pledger->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") as pledge_frequency');

		$pledger->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_referrer_name") as pledge_referrer_name');

		$pledger->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_card_number") as pledge_card_number');

		$pledger->set_select('(SELECT d.SalesRepEntity_FullName FROM qb_salesrep d WHERE c.SalesRep_ListID=d.ListID) as leader');

		$this->template_data->set('pledger', $pledger->get());

		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		$pledges = new $this->Qb_salesreceipt_salesreceiptline_model('j');
		$pledges->setItemListid($this->config->item('pledgers_item_id'),true);
		$pledges->set_start($start);
		$pledges->set_select("*");
		$pledges->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
		$pledges->set_order('s.TxnDate', 'DESC');
		$pledges->set_limit(12);
		$pledges->set_where('s.Customer_ListID="'.$ListID.'"');

			$this->load->model('Qb_dataext_model');
			$dextmonth = new $this->Qb_dataext_model('d');
			$dextmonth->setEntitytype('SalesReceiptLine',true);
			$dextmonth->setDataextname('month',true);
			$dextmonth->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextmonth->set_select('d.DataExtValue');
			$pledges->set_select('('.$dextmonth->get_compiled_select().') as month');

			$dextyear = new $this->Qb_dataext_model('d');
			$dextyear->setEntitytype('SalesReceiptLine',true);
			$dextyear->setDataextname('year',true);
			$dextyear->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextyear->set_select('d.DataExtValue');
			$pledges->set_select('('.$dextyear->get_compiled_select().') as year');

		if( $this->input->get('year') ) {
			$pledges->set_where('YEAR(s.TxnDate)="'.$this->input->get('year').'"');
		}

		$this->template_data->set('pledges', $pledges->populate());	
		$this->template_data->set('pledges_total', $pledges->count_all_results());	

		$this->template_data->set('pagination', stisla_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/pledgers/view/{$ListID}"),
			'total_rows' => $pledges->count_all_results(),
			'per_page' => $pledges->get_limit()
		),
'?year=' . $this->input->get('year')
	));

		$this->load->view('pledgers/pledgers_view', $this->template_data->get_data());
	}


	public function report($frequency='all')
	{

		$this->template_data->set('frequency', $frequency);

		$this->load->model('Qb_customer_model');
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),TRUE);
		$pledgers->set_select('*');
		$pledgers->set_start(0);
		$pledgers->set_limit(0);

		switch ($this->input->get('sort')) {
			case 'MONTHLY':
				$pledgers->set_order('MONTH(c.JobStartDate)', 'ASC');
				$pledgers->set_order('DAY(c.JobStartDate)', 'ASC');
				//$pledgers->set_order('c.Name', 'ASC');
				break;
			case 'NAME_ASC':
				$pledgers->set_order('c.Name', 'ASC');
				break;
			case 'NAME_DESC':
				$pledgers->set_order('c.Name', 'DESC');
				break;
			case 'PLEDGE_ASC':
				$pledgers->set_order('pledge_amount2', 'ASC');
				$pledgers->set_order('c.Name', 'ASC');
				break;
			case 'PLEDGE_DESC':
				$pledgers->set_order('pledge_amount2', 'DESC');
				$pledgers->set_order('c.Name', 'ASC');
				break;
			default:
				$pledgers->set_order('c.Name', 'ASC');
				break;
		}

		switch($frequency) {
			case 'lumpsum':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "One-Time / Lumpsum")');
				
			break;
			case 'monthly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Monthly")');
				
			break;
			case 'weekly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Weekly")');
				
			break;
			case 'quarterly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Quarterly")');
				
			break;
			case 'yearly':
				$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") LIKE "Yearly")');
				
			break;
			default:
			case 'all':
			break;
		}

		if( $this->input->get('month') ) {
			$pledgers->set_where('(MONTH(c.JobStartDate) = '.$this->input->get('month').')');
		}

		$pledgers->set_select('CONVERT(SUBSTRING_INDEX((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount"),\'-\',-1),UNSIGNED INTEGER) AS pledge_amount2');

		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount") as pledge_amount');

		$pledgers->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") as pledge_frequency');

		//$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") = "Monthly")');

		$this->template_data->set('pledgers', $pledgers->populate());
		$this->template_data->set('pledgers_total', $pledgers->count_all_results());

		$this->template_data->set('pagination', stisla_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/pledgers/monthly"),
			'total_rows' => $pledgers->count_all_results(),
			'per_page' => $pledgers->get_limit()
		),
		'?sort=' . $this->input->get('sort')
		));

		$pledgers->set_start(0);
		$pledgers->set_select('SUM((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_amount")) as total_pledges', 0, true);
		$this->template_data->set('total_pledges', $pledgers->get()); 

		$this->load->view('reports/pledgers_list', $this->template_data->get_data());
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Celebrations extends MY_Controller {

	private $frequency = 'all';

	public function index($start=0)
	{
		$this->template_data->set('frequency', $this->frequency);

		$this->load->model('Qb_customer_model');
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),TRUE);
		$pledgers->set_select('*');
		$pledgers->set_select('MONTH(c.JobStartDate) as month');
		$pledgers->set_start($start);
		$pledgers->set_limit(10);
		$pledgers->set_order('c.Name', 'ASC');

		if( $this->input->get('month') ) {
			$pledgers->set_where( '(MONTH(c.JobStartDate)=' . $this->input->get('month') );
			$pledgers->set_where_or( 'MONTH(c.JobProjectedEndDate)=' . $this->input->get('month') . ")" );
		}

		if( $this->input->get('show') == 'all' ) {
			$pledgers->set_start(0);
			$pledgers->set_limit(0);
		}
		
		$this->template_data->set('pledgers', $pledgers->populate());
		$this->template_data->set('pledgers_total', $pledgers->count_all_results());

		$this->template_data->set('pagination', stisla_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/celebrations/index"),
			'total_rows' => $pledgers->count_all_results(),
			'per_page' => $pledgers->get_limit()
		), '?month=' . $this->input->get('month')));

		$this->load->view('celebrations/celebrations', $this->template_data->get_data());
	}

}

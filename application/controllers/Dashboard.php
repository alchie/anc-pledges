<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function index()
	{

		$this->load->model('Qb_customer_model');
		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		$this->load->model('Qb_dataext_model');

		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),TRUE);
		$pledgers->set_select('COUNT(*) as total');
		$this->template_data->set('all_pledgers', $pledgers->get());

		
		$total_pledges = new $this->Qb_salesreceipt_salesreceiptline_model('j');
		$total_pledges->setItemListid($this->config->item('pledgers_item_id'),true);
		$total_pledges->set_select('SUM(j.Amount) as total');
		$this->template_data->set('total_pledges', $total_pledges->get());

		$years = pledgers_years(FALSE, 'DESC', 3);
		foreach($years as $i=>$year) {
			
			$year_pledges = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$year_pledges->setItemListid($this->config->item('pledgers_item_id'),true);
			$year_pledges->set_select('SUM(j.Amount) as total');
			//$year_pledges->set_where('j.Descrip LIKE "%_'.$year->year.'}}%"');

			$dextyear = new $this->Qb_dataext_model('d');
			$dextyear->setEntitytype('SalesReceiptLine',true);
			$dextyear->setDataextname('year',true);
			$dextyear->setDataextvalue($year->year,true);
			$dextyear->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextyear->set_select('COUNT(*)');
			$year_pledges->set_where('(('.$dextyear->get_compiled_select().') > 0)');
			$years[$i]->pledges = $year_pledges->get();

			$year_collections = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$year_collections->setItemListid($this->config->item('pledgers_item_id'),true);
			$year_collections->set_select('SUM(j.Amount) as total');
			$year_collections->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$year_collections->set_where('YEAR(s.TxnDate) = "'.$year->year.'"');
			$years[$i]->collections = $year_collections->get();
		}
		
		$this->template_data->set('years', $years);
		
		$this->load->model('Qb_salesrep_model');
		$leaders = new $this->Qb_salesrep_model('c');
		$leaders->set_select('COUNT(*) as total');
		$this->template_data->set('stewardship_leaders', $leaders->get());

/*
		$prospects = new $this->Qb_customer_model('c');
		$prospects->setCustomertypeListid($this->config->item('prospects_type_id'),TRUE);
		$prospects->set_select('COUNT(*) as total');
		$this->template_data->set('all_prospects', $prospects->get());
*/

		$this->load->model('Qb_dataext_model');
		$pledge_amount = new $this->Qb_dataext_model('d');
		$pledge_amount->setEntitytype('Customer',true);
		$pledge_amount->setDataextname('pledge_amount',true);
		$pledge_amount->set_select('SUM(d.DataExtValue) as total_pledge');

			$pledge_frequency = new $this->Qb_dataext_model('f');
			$pledge_frequency->setEntitytype('Customer',true);
			$pledge_frequency->setDataextname('pledge_frequency',true);
			$pledge_frequency->set_select('f.DataExtValue');
			$pledge_frequency->set_limit(1);
			$pledge_frequency->set_where('f.Entity_ListID=d.Entity_ListID');

		$pledge_amount->set_where('(('.$pledge_frequency->get_compiled_select().')="Monthly")');
		$pledge_amount->set_join('qb_customer c', 'c.ListID=d.Entity_ListID');
		$pledge_amount->set_where('c.CustomerType_ListID="'.$this->config->item('pledgers_type_id').'"');

		$this->template_data->set('total_pledge', $pledge_amount->get());


		$this->load->model('Qb_customer_model');
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),TRUE);
		$pledgers->set_select('c.*');
		$pledgers->set_select('c.Name');
		$pledgers->set_select('c.Phone');
		$pledgers->set_select('c.SalesRep_ListID');
		$pledgers->set_select('c.SalesRep_FullName');
		$pledgers->set_start(0);
		$pledgers->set_limit(0);

			$current_year = date('Y');
			$month = date('m');

			$month_var = date('F', strtotime($month."/1/".$current_year));
			$month_pledge = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$month_pledge->setItemListid($this->config->item('pledgers_item_id'),true);
			$month_pledge->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$month_pledge->set_where('s.Customer_ListID=c.ListID');
			$month_pledge->set_limit(1);
			$month_pledge->set_select('j.Amount as jAmount');
			//$month_pledge->set_where('j.Descrip LIKE "%{{'.$month_var.'_'.$current_year.'}}%"');

			$dextmonth = new $this->Qb_dataext_model('d');
			$dextmonth->setEntitytype('SalesReceiptLine',true);
			$dextmonth->setDataextname('month',true);
			$dextmonth->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextmonth->set_select('d.DataExtValue');
			//$month_pledge->set_select('('.$dextmonth->get_compiled_select().') as month');

			$dextmonth->setDataextvalue($month_var,true);
			$dextmonth->set_select('COUNT(*)', NULL, TRUE);
			$month_pledge->set_where('(('.$dextmonth->get_compiled_select().') > 0)');

			$dextyear = new $this->Qb_dataext_model('d');
			$dextyear->setEntitytype('SalesReceiptLine',true);
			$dextyear->setDataextname('year',true);
			$dextyear->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextyear->set_select('d.DataExtValue');
			//$month_pledge->set_select('('.$dextyear->get_compiled_select().') as year');

			$dextyear->setDataextvalue($current_year,true);
			$dextyear->set_select('COUNT(*)', NULL, TRUE);
			$month_pledge->set_where('(('.$dextyear->get_compiled_select().') > 0)');

			$pledgers->set_select('('.$month_pledge->get_compiled_select().') as '.$month_var);
			$pledgers->set_where('(('.$month_pledge->get_compiled_select().') IS NULL)');			

			$pledgers->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="pledge_frequency") = "Monthly")');

			$this->template_data->set('did_not_remit', $pledgers->count_all_results());

		$this->load->view('dashboard/dashboard', $this->template_data->get_data());
	}

	public function year($current_year=0)
	{
		$current_year = ($current_year) ? $current_year : date('Y');
		$this->template_data->set('current_year', $current_year);

		$this->load->model('Qb_dataext_model');
		$pledge_amount = new $this->Qb_dataext_model('d');
		$pledge_amount->setEntitytype('Customer',true);
		$pledge_amount->setDataextname('pledge_amount',true);
		$pledge_amount->set_select('SUM(d.DataExtValue) as total_pledge');

			$pledge_frequency = new $this->Qb_dataext_model('f');
			$pledge_frequency->setEntitytype('Customer',true);
			$pledge_frequency->setDataextname('pledge_frequency',true);
			$pledge_frequency->set_select('f.DataExtValue');
			$pledge_frequency->set_limit(1);
			$pledge_frequency->set_where('f.Entity_ListID=d.Entity_ListID');

		$pledge_amount->set_where('(('.$pledge_frequency->get_compiled_select().')="Monthly")');
		$pledge_amount->set_join('qb_customer c', 'c.ListID=d.Entity_ListID');
		$pledge_amount->set_where('c.CustomerType_ListID="'.$this->config->item('pledgers_type_id').'"');

		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		for($month=1;$month<=12;$month++) {
			
			$month_var = date('F', strtotime($month."/1/".$current_year));
			$month_pledges = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$month_pledges->setItemListid($this->config->item('pledgers_item_id'),true);
			$month_pledges->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$month_pledges->set_limit(1);
			$month_pledges->set_select('SUM(j.Amount)');
			//$month_pledges->set_where('j.Descrip LIKE "%{{'.$month_var.'_'.$current_year.'}}%"');

			$dextmonth = new $this->Qb_dataext_model('d');
			$dextmonth->setEntitytype('SalesReceiptLine',true);
			$dextmonth->setDataextname('month',true);
			$dextmonth->setDataextvalue($month_var,true);
			$dextmonth->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextmonth->set_select('COUNT(*)');
			$month_pledges->set_where('(('.$dextmonth->get_compiled_select().') > 0)');

			$dextyear = new $this->Qb_dataext_model('d');
			$dextyear->setEntitytype('SalesReceiptLine',true);
			$dextyear->setDataextname('year',true);
			$dextyear->setDataextvalue($current_year,true);
			$dextyear->set_where('j.TxnLineID=d.Txn_TxnID');
			$dextyear->set_select('COUNT(*)');
			$month_pledges->set_where('(('.$dextyear->get_compiled_select().') > 0)');

			$pledge_amount->set_select('('.$month_pledges->get_compiled_select().') as pledged_'.$month_var);
			
			$month_collections = new $this->Qb_salesreceipt_salesreceiptline_model('j');
			$month_collections->setItemListid($this->config->item('pledgers_item_id'),true);
			$month_collections->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
			$month_collections->set_limit(1);
			$month_collections->set_select('SUM(j.Amount)');
			$month_collections->set_where('MONTH(s.TxnDate) = "'.$month.'"');
			$month_collections->set_where('YEAR(s.TxnDate) = "'.$current_year.'"');
			
			$pledge_amount->set_select('('.$month_collections->get_compiled_select().') as collected_'.$month_var);
			
		}

		$this->template_data->set('stats', $pledge_amount->get());

		$this->load->view('dashboard/dashboard_year', $this->template_data->get_data());

	}

}

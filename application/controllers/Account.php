<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function index()
	{
            if( ! $this->session->userdata('account') ) {
            	redirect("account/login");
            } else {
            	redirect("dashboard");
            }
	}

	public function login()
	{
		if( $this->input->post('username') && $this->input->post('password') ) {
			$this->load->model('Qb_dataext_model');
			$username = new $this->Qb_dataext_model('u');
			$username->setEntitytype('Employee',TRUE);
			$username->setDataextname('pledger_username',TRUE);
			$username->setDataextvalue($this->input->post('username'),TRUE);

			//$username->set_select("u.DataExtValue as username");
			//$username->set_select("(SELECT p.DataExtValue FROM qb_dataext p WHERE u.Entity_ListID=p.Entity_ListID AND p.DataExtName='pledger_password' AND p.DataExtValue='".$this->input->post('password')."') as password");

			$username->set_select("COUNT(*) as username");
			$username->set_select("(SELECT COUNT(*) FROM qb_dataext p WHERE u.Entity_ListID=p.Entity_ListID AND p.EntityType='Employee' AND p.DataExtName='pledger_password' AND p.DataExtValue='".$this->input->post('password')."') as password");
			$username->set_select("(SELECT COUNT(*) FROM qb_employee e WHERE u.Entity_ListID=e.ListID) as is_employee");
			$username->set_select("(SELECT COUNT(*) FROM qb_salesrep sr WHERE u.Entity_ListID=sr.SalesRepEntity_ListID) as is_salesrep");

			$account = $username->get();

			if( $account->username && $account->password && $account->is_employee && $account->is_salesrep ) {
				$username->set_join('qb_employee e', 'u.Entity_ListID=e.ListID');
				$username->set_select('e.*');
				$account = $username->get();
				$this->session->set_userdata('account', $account);
				redirect("dashboard");
				exit;
			}

		}
		$this->load->view('login/login', $this->template_data->get_data());
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect("account/login");
	}
}

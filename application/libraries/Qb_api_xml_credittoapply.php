<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_credittoapply extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_credittoapply_model');
        $model = new $this->CI->Qb_credittoapply_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_CREDITTOAPPLYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CREDITTOAPPLYQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<CredittoapplyQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<IncludeLineItems>true</IncludeLineItems>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</CredittoapplyQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'CredittoapplyRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_credittoapply_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_credittoapply_model();
                $model->setTxnid($item_obj->TxnID);
                $model->setTxntype($item_obj->TxnType);
                $model->setApaccountListid($item_obj->APAccount_ListID);
                $model->setApaccountFullname($item_obj->APAccount_FullName);
                $model->setTxndate($item_obj->TxnDate);
                $model->setRefnumber($item_obj->RefNumber);
                $model->setCreditremaining($item_obj->CreditRemaining);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'CreditToApplyRet') {

          $this->items[] = array(
            'TxnID' => $this->get_text_content($item, array('TxnID')),
            'TxnType' => $this->get_text_content($item, array('TxnType')),
            'APAccount_ListID' => $this->get_text_content($item, array('APAccountRef','ListID')),
            'APAccount_FullName' => $this->get_text_content($item, array('APAccountRef','FullName')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'RefNumber' => $this->get_text_content($item, array('RefNumber')),
            'CreditRemaining' => $this->get_text_content($item, array('CreditRemaining')),
            'DataExtItems' => $this->get_dataext_items($item, 'CreditToApply', 'TxnID'),
          );

        }
      }
    }
 
    public function delete($TxnID) {
        $this->CI->load->model('Qb_credittoapply_model');
        $query = new $this->CI->Qb_credittoapply_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }

}

/* End of file */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_billpaymentcheck extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_billpaymentcheck_model');
        $model = new $this->CI->Qb_billpaymentcheck_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_BILLPAYMENTCHECKQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_BILLPAYMENTCHECKQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<BillPaymentCheckQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<IncludeLineItems>true</IncludeLineItems>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</BillPaymentCheckQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>BillPaymentCheck</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'BillpaymentcheckRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_billpaymentcheck_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_billpaymentcheck_model();
                $model->setExchangerate($item_obj->ExchangeRate);
                $model->setAmountinhomecurrency($item_obj->AmountInHomeCurrency);
                $model->setTxnid($item_obj->TxnID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setTxnnumber($item_obj->TxnNumber);
                $model->setPayeeentityListid($item_obj->PayeeEntity_ListID);
                $model->setPayeeentityFullname($item_obj->PayeeEntity_FullName);
                $model->setApaccountListid($item_obj->APAccount_ListID);
                $model->setApaccountFullname($item_obj->APAccount_FullName);
                $model->setTxndate($item_obj->TxnDate);
                $model->setBankaccountListid($item_obj->BankAccount_ListID);
                $model->setBankaccountFullname($item_obj->BankAccount_FullName);
                $model->setAmount($item_obj->Amount);
                $model->setRefnumber($item_obj->RefNumber);
                $model->setMemo($item_obj->Memo);
                $model->setAddressAddr1($item_obj->Address_Addr1);
                $model->setAddressAddr2($item_obj->Address_Addr2);
                $model->setAddressAddr3($item_obj->Address_Addr3);
                $model->setAddressAddr4($item_obj->Address_Addr4);
                $model->setAddressNote($item_obj->Address_Note);
                $model->setAddressCity($item_obj->Address_City);
                $model->setAddressState($item_obj->Address_State);
                $model->setAddressPostalcode($item_obj->Address_PostalCode);
                $model->setAddressCountry($item_obj->Address_Country);
                $model->setAddressblockAddr1($item_obj->AddressBlock_Addr1);
                $model->setAddressblockAddr2($item_obj->AddressBlock_Addr2);
                $model->setAddressblockAddr3($item_obj->AddressBlock_Addr3);
                $model->setAddressblockAddr4($item_obj->AddressBlock_Addr4);
                $model->setAddressblockAddr5($item_obj->AddressBlock_Addr5);
                $model->setIstobeprinted($item_obj->IsToBePrinted);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'BillPaymentCheckRet') {

          $this->items[] = array(
            'ExchangeRate' => $this->get_text_content($item, array('ExchangeRate')),
            'AmountInHomeCurrency' => $this->get_text_content($item, array('AmountInHomeCurrency')),
            'TxnID' => $this->get_text_content($item, array('TxnID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'TxnNumber' => $this->get_text_content($item, array('TxnNumber')),
            'PayeeEntity_ListID' => $this->get_text_content($item, array('PayeeEntityRef','ListID')),
            'PayeeEntity_FullName' => $this->get_text_content($item, array('PayeeEntityRef','FullName')),
            'APAccount_ListID' => $this->get_text_content($item, array('APAccountRef','ListID')),
            'APAccount_FullName' => $this->get_text_content($item, array('APAccountRef','FullName')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'BankAccount_ListID' => $this->get_text_content($item, array('BankAccountRef','ListID')),
            'BankAccount_FullName' => $this->get_text_content($item, array('BankAccountRef','FullName')),
            'Amount' => $this->get_text_content($item, array('Amount')),
            'RefNumber' => $this->get_text_content($item, array('RefNumber')),
            'Memo' => $this->get_text_content($item, array('Memo')),
            'Address_Addr1' => $this->get_text_content($item, array('AddressRef','Addr1')),
            'Address_Addr2' => $this->get_text_content($item, array('AddressRef','Addr2')),
            'Address_Addr3' => $this->get_text_content($item, array('AddressRef','Addr3')),
            'Address_Addr4' => $this->get_text_content($item, array('AddressRef','Addr4')),
            'Address_Note' => $this->get_text_content($item, array('AddressRef','Note')),
            'Address_City' => $this->get_text_content($item, array('AddressRef','City')),
            'Address_State' => $this->get_text_content($item, array('AddressRef','State')),
            'Address_PostalCode' => $this->get_text_content($item, array('AddressRef','PostalCode')),
            'Address_Country' => $this->get_text_content($item, array('AddressRef','Country')),
            'AddressBlock_Addr1' => $this->get_text_content($item, array('AddressBlockRef','Addr1')),
            'AddressBlock_Addr2' => $this->get_text_content($item, array('AddressBlockRef','Addr2')),
            'AddressBlock_Addr3' => $this->get_text_content($item, array('AddressBlockRef','Addr3')),
            'AddressBlock_Addr4' => $this->get_text_content($item, array('AddressBlockRef','Addr4')),
            'AddressBlock_Addr5' => $this->get_text_content($item, array('AddressBlockRef','Addr5')),
            'IsToBePrinted' => $this->get_text_content($item, array('IsToBePrinted')),
            'DataExtItems' => $this->get_dataext_items($item, 'BillPaymentCheck', 'TxnID'),
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_billpaymentcheck_model');
        $query = new $this->CI->Qb_billpaymentcheck_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }
    
}

/* End of file */

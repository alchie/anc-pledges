<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_employee extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }

    public function getLastTimeModified() {
        $this->CI->load->model('Qb_employee_model');
        $customers = new $this->CI->Qb_employee_model();
        $customers->set_order('TimeModified', 'DESC');
        $customers_data = $customers->get();
        return (($customers_data) && isset($customers_data->TimeModified)) ? $customers_data->TimeModified : false;
    }

    public function request($queue) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_EMPLOYEEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_EMPLOYEEQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified . " +1 second" ) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<EmployeeQueryRq requestID="'.$requestID.'" >' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ActiveStatus>All</ActiveStatus>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</EmployeeQueryRq>' .  "\n" .
'<ListDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<ListDelType>Employee</ListDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</ListDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'saveItems');

        if( $this->items ) {
            
            $this->CI->load->model('Qb_employee_model');


            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $employees = new $this->CI->Qb_employee_model();
                $employees->setListid($item_obj->ListID,true);
                $employees->setTimecreated($item_obj->TimeCreated);
                $employees->setTimemodified($item_obj->TimeModified);
                $employees->setEditsequence($item_obj->EditSequence);
                $employees->setName($item_obj->Name);
                $employees->setIsactive(($item_obj->IsActive)?1:0);
                $employees->setSalutation($item_obj->Salutation);
                $employees->setFirstname($item_obj->FirstName);
                $employees->setMiddlename($item_obj->MiddleName);
                $employees->setLastname($item_obj->LastName);
                $employees->setEmployeeaddressAddr1($item_obj->EmployeeAddress_Addr1);
                $employees->setEmployeeaddressAddr2($item_obj->EmployeeAddress_Addr2);
                $employees->setEmployeeaddressCity($item_obj->EmployeeAddress_City);
                $employees->setEmployeeaddressState($item_obj->EmployeeAddress_State);
                $employees->setEmployeeaddressPostalcode($item_obj->EmployeeAddress_PostalCode);
                $employees->setPrintas($item_obj->PrintAs);
                $employees->setPhone($item_obj->Phone);
                $employees->setMobile($item_obj->Mobile);
                $employees->setPager($item_obj->Pager);
                $employees->setPagerpin($item_obj->PagerPIN);
                $employees->setAltphone($item_obj->AltPhone);
                $employees->setFax($item_obj->Fax);
                $employees->setSsn($item_obj->SSN);
                $employees->setEmail($item_obj->Email);
                $employees->setEmployeetype($item_obj->EmployeeType);
                $employees->setGender($item_obj->Gender);
                $employees->setHireddate($item_obj->HiredDate);
                $employees->setReleaseddate($item_obj->ReleasedDate);
                $employees->setBirthdate($item_obj->BirthDate);
                $employees->setAccountnumber($item_obj->AccountNumber);
                $employees->setNotes($item_obj->Notes);
                $employees->setBillingrateListid($item_obj->BillingRate_ListID);
                $employees->setBillingrateFullname($item_obj->BillingRate_FullName);
                if( $employees->nonEmpty() ) {
                  $employees->update();
                } else {
                  $employees->insert();
                }

                $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'EmployeeRet') {

          $this->items[] = array(
            'ListID' => $this->get_text_content($item, array('ListID')), 
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')), 
            'TimeModified' => $this->get_text_content($item, array('TimeModified')), 
            'EditSequence' => $this->get_text_content($item, array('EditSequence')), 
            'Name' => $this->get_text_content($item, array('Name')), 
            'IsActive' => $this->get_text_content($item, array('IsActive')), 
            'Salutation' => $this->get_text_content($item, array('Salutation')), 
            'FirstName' => $this->get_text_content($item, array('FirstName')), 
            'MiddleName' => $this->get_text_content($item, array('MiddleName')), 
            'LastName' => $this->get_text_content($item, array('LastName')), 
            'EmployeeAddress_Addr1' => $this->get_text_content($item, array('EmployeeAddress','Addr1')), 
            'EmployeeAddress_Addr2' => $this->get_text_content($item, array('EmployeeAddress','Addr2')), 
            'EmployeeAddress_City' => $this->get_text_content($item, array('EmployeeAddress','City')), 
            'EmployeeAddress_State' => $this->get_text_content($item, array('EmployeeAddress','State')), 
            'EmployeeAddress_PostalCode' => $this->get_text_content($item, array('EmployeeAddress','PostalCode')), 
            'PrintAs' => $this->get_text_content($item, array('PrintAs')), 
            'Phone' => $this->get_text_content($item, array('Phone')), 
            'Mobile' => $this->get_text_content($item, array('Mobile')), 
            'Pager' => $this->get_text_content($item, array('Pager')), 
            'PagerPIN' => $this->get_text_content($item, array('PagerPIN')), 
            'AltPhone' => $this->get_text_content($item, array('AltPhone')), 
            'Fax' => $this->get_text_content($item, array('Fax')), 
            'SSN' => $this->get_text_content($item, array('SSN')), 
            'Email' => $this->get_text_content($item, array('Email')), 
            'EmployeeType' => $this->get_text_content($item, array('EmployeeType')), 
            'Gender' => $this->get_text_content($item, array('Gender')), 
            'HiredDate' => $this->get_text_content($item, array('HiredDate')), 
            'ReleasedDate' => $this->get_text_content($item, array('ReleasedDate')), 
            'BirthDate' => $this->get_text_content($item, array('BirthDate')), 
            'AccountNumber' => $this->get_text_content($item, array('AccountNumber')), 
            'Notes' => $this->get_text_content($item, array('Notes')), 
            'BillingRate_ListID' => $this->get_text_content($item, array('BillingRateRef','ListID')), 
            'BillingRate_FullName' => $this->get_text_content($item, array('BillingRateRef','FullName')), 
            'DataExtItems' => $this->get_dataext_items($item, 'Employee', 'ListID'),
          );

        }
      }
    }

    public function delete($ListID) {
        $this->CI->load->model('Qb_employee_model');
        $query = new $this->CI->Qb_employee_model();
        $query->setListid($ListID,true);
        $query->delete();
    }
    
}

/* End of file Global_variables.php */
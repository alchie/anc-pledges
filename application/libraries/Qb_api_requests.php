<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_requests {

    public function __construct()
    {
       $this->CI =& get_instance();

       // load config
       $this->CI->load->config('quickbooks_api');
       
       // load queue model
       $this->CI->load->model('Qb_api_queue_model');


    }

    public function init( $reqNode, $queue )
    {

      switch( $reqNode ) 
      {

        case 'AccountQuery':
            $this->CI->load->library('Qb_api_xml_account');
            $query = new $this->CI->qb_api_xml_account();
            return $query->request( $queue );
        break;

        case 'BillingRateQuery':
            $this->CI->load->library('Qb_api_xml_billingrate');
            $query = new $this->CI->qb_api_xml_billingrate();
            return $query->request( $queue );
        break;

        case 'BillPaymentCheckQuery':
            $this->CI->load->library('Qb_api_xml_billpaymentcheck');
            $query = new $this->CI->qb_api_xml_billpaymentcheck();
            return $query->request( $queue );
        break;

        case 'BillPaymentCreditCardQuery':
            $this->CI->load->library('Qb_api_xml_billpaymentcreditcard');
            $query = new $this->CI->qb_api_xml_billpaymentcreditcard();
            return $query->request( $queue );
        break;

        case 'BillQuery':
            $this->CI->load->library('Qb_api_xml_bill');
            $query = new $this->CI->qb_api_xml_bill();
            return $query->request( $queue );
        break;

        case 'BillToPayQuery':
            $this->CI->load->library('Qb_api_xml_billtopay');
            $query = new $this->CI->qb_api_xml_billtopay();
            return $query->request( $queue );
        break;

        case 'BuildAssemblyQuery':
            $this->CI->load->library('Qb_api_xml_buildassembly');
            $query = new $this->CI->qb_api_xml_buildassembly();
            return $query->request( $queue );
        break;
        
        case 'ChargeQuery':
            $this->CI->load->library('Qb_api_xml_charge');
            $query = new $this->CI->qb_api_xml_charge();
            return $query->request( $queue );
        break;

        case 'CheckQuery':
            $this->CI->load->library('Qb_api_xml_check');
            $query = new $this->CI->qb_api_xml_check();
            return $query->request( $queue );
        break;

        case 'ClassQuery':
            $this->CI->load->library('Qb_api_xml_class');
            $query = new $this->CI->qb_api_xml_class();
            return $query->request( $queue );
        break;

        case 'CompanyQuery':
            $this->CI->load->library('Qb_api_xml_company');
            $query = new $this->CI->qb_api_xml_company();
            return $query->request( $queue );
        break;

        case 'CreditCardChargeQuery':
            $this->CI->load->library('Qb_api_xml_creditcardcharge');
            $query = new $this->CI->qb_api_xml_creditcardcharge();
            return $query->request( $queue );
        break;

        case 'CreditCardCreditQuery':
            $this->CI->load->library('Qb_api_xml_creditcardcredit');
            $query = new $this->CI->qb_api_xml_creditcardcredit();
            return $query->request( $queue );
        break;

        case 'CreditMemoQuery':
            $this->CI->load->library('Qb_api_xml_creditmemo');
            $query = new $this->CI->qb_api_xml_creditmemo();
            return $query->request( $queue );
        break;

        case 'CurrencyQuery':
            $this->CI->load->library('Qb_api_xml_currency');
            $query = new $this->CI->qb_api_xml_currency();
            return $query->request( $queue );
        break;

        case 'CustomerMsgQuery':
            $this->CI->load->library('Qb_api_xml_customermsg');
            $query = new $this->CI->qb_api_xml_customermsg();
            return $query->request( $queue );
        break;

        case 'CustomerQuery':
            $this->CI->load->library('Qb_api_xml_customer');
            $query = new $this->CI->qb_api_xml_customer();
            return $query->request( $queue );
        break;

        case 'CustomerTypeQuery':
            $this->CI->load->library('Qb_api_xml_customertype');
            $query = new $this->CI->qb_api_xml_customertype();
            return $query->request( $queue );
        break;

        case 'DataExtDefQuery':
            $this->CI->load->library('Qb_api_xml_dataextdef');
            $query = new $this->CI->qb_api_xml_dataextdef();
            return $query->request( $queue );
        break;

        case 'DateDrivenTermsQuery':
            $this->CI->load->library('Qb_api_xml_datedriventerms');
            $query = new $this->CI->qb_api_xml_datedriventerms();
            return $query->request( $queue );
        break;

        case 'DepositQuery':
            $this->CI->load->library('Qb_api_xml_deposit');
            $query = new $this->CI->qb_api_xml_deposit();
            return $query->request( $queue );
        break;

        case 'EmployeeQuery':
            $this->CI->load->library('Qb_api_xml_employee');
            $query = new $this->CI->qb_api_xml_employee();
            return $query->request( $queue );
        break;

        case 'EstimateQuery':
            $this->CI->load->library('Qb_api_xml_estimate');
            $query = new $this->CI->qb_api_xml_estimate();
            return $query->request( $queue );
        break;

        case 'HostQuery':
            $this->CI->load->library('Qb_api_xml_host');
            $query = new $this->CI->qb_api_xml_host();
            return $query->request( $queue );
        break;

        case 'InventoryAdjustmentQuery':
            $this->CI->load->library('Qb_api_xml_inventoryadjustment');
            $query = new $this->CI->qb_api_xml_inventoryadjustment();
            return $query->request( $queue );
        break;

        case 'InvoiceQuery':
            $this->CI->load->library('Qb_api_xml_invoice');
            $query = new $this->CI->qb_api_xml_invoice();
            return $query->request( $queue );
        break;

        case 'ItemDiscountQuery':
            $this->CI->load->library('Qb_api_xml_itemdiscount');
            $query = new $this->CI->qb_api_xml_itemdiscount();
            return $query->request( $queue );
        break;

        case 'ItemFixedAssetQuery':
            $this->CI->load->library('Qb_api_xml_itemfixedasset');
            $query = new $this->CI->qb_api_xml_itemfixedasset();
            return $query->request( $queue );
        break;

        case 'ItemGroupQuery':
            $this->CI->load->library('Qb_api_xml_itemgroup');
            $query = new $this->CI->qb_api_xml_itemgroup();
            return $query->request( $queue );
        break;

        case 'ItemInventoryAssemblyQuery':
            $this->CI->load->library('Qb_api_xml_iteminventoryassembly');
            $query = new $this->CI->qb_api_xml_iteminventoryassembly();
            return $query->request( $queue );
        break;

        case 'ItemInventoryQuery':
            $this->CI->load->library('Qb_api_xml_iteminventory');
            $query = new $this->CI->qb_api_xml_iteminventory();
            return $query->request( $queue );
        break;

        case 'ItemNonInventoryQuery':
            $this->CI->load->library('Qb_api_xml_itemnoninventory');
            $query = new $this->CI->qb_api_xml_itemnoninventory();
            return $query->request( $queue );
        break;

        case 'ItemOtherChargeQuery':
            $this->CI->load->library('Qb_api_xml_itemothercharge');
            $query = new $this->CI->qb_api_xml_itemothercharge();
            return $query->request( $queue );
        break;

        case 'ItemPaymentQuery':
            $this->CI->load->library('Qb_api_xml_itempayment');
            $query = new $this->CI->qb_api_xml_itempayment();
            return $query->request( $queue );
        break;

        case 'ItemReceiptQuery':
            $this->CI->load->library('Qb_api_xml_itemreceipt');
            $query = new $this->CI->qb_api_xml_itemreceipt();
            return $query->request( $queue );
        break;

        case 'ItemSalesTaxQuery':
            $this->CI->load->library('Qb_api_xml_itemsalestax');
            $query = new $this->CI->qb_api_xml_itemsalestax();
            return $query->request( $queue );
        break;

        case 'ItemServiceQuery':
            $this->CI->load->library('Qb_api_xml_itemservice');
            $query = new $this->CI->qb_api_xml_itemservice();
            return $query->request( $queue );
        break;

        case 'ItemSubtotalQuery':
            $this->CI->load->library('Qb_api_xml_itemsubtotal');
            $query = new $this->CI->qb_api_xml_itemsubtotal();
            return $query->request( $queue );
        break;

        case 'JobTypeQuery':
            $this->CI->load->library('Qb_api_xml_jobtype');
            $query = new $this->CI->qb_api_xml_jobtype();
            return $query->request( $queue );
        break;

        case 'JournalEntryQuery':
            $this->CI->load->library('Qb_api_xml_journalentry');
            $query = new $this->CI->qb_api_xml_journalentry();
            return $query->request( $queue );
        break;

        case 'PaymentMethodQuery':
            $this->CI->load->library('Qb_api_xml_paymentmethod');
            $query = new $this->CI->qb_api_xml_paymentmethod();
            return $query->request( $queue );
        break;

        case 'PayrollItemWageQuery':
            $this->CI->load->library('Qb_api_xml_payrollitemwage');
            $query = new $this->CI->qb_api_xml_payrollitemwage();
            return $query->request( $queue );
        break;

        case 'PreferencesQuery':
            $this->CI->load->library('Qb_api_xml_preferences');
            $query = new $this->CI->qb_api_xml_preferences();
            return $query->request( $queue );
        break;

        case 'PriceLevelQuery':
            $this->CI->load->library('Qb_api_xml_pricelevel');
            $query = new $this->CI->qb_api_xml_pricelevel();
            return $query->request( $queue );
        break;

        case 'PurchaseOrderQuery':
            $this->CI->load->library('Qb_api_xml_purchaseorder');
            $query = new $this->CI->qb_api_xml_purchaseorder();
            return $query->request( $queue );
        break;

        case 'ReceivePaymentQuery':
            $this->CI->load->library('Qb_api_xml_receivepayment');
            $query = new $this->CI->qb_api_xml_receivepayment();
            return $query->request( $queue );
        break;

        case 'SalesOrderQuery':
            $this->CI->load->library('Qb_api_xml_salesorder');
            $query = new $this->CI->qb_api_xml_salesorder();
            return $query->request( $queue );
        break;

        case 'SalesReceiptQuery':
            $this->CI->load->library('Qb_api_xml_salesreceipt');
            $query = new $this->CI->qb_api_xml_salesreceipt();
            return $query->request( $queue );
        break;

        case 'SalesRepQuery':
            $this->CI->load->library('Qb_api_xml_salesrep');
            $query = new $this->CI->qb_api_xml_salesrep();
            return $query->request( $queue );
        break;

        case 'SalesTaxCodeQuery':
            $this->CI->load->library('Qb_api_xml_salestaxcode');
            $query = new $this->CI->qb_api_xml_salestaxcode();
            return $query->request( $queue );
        break;

        case 'ShipMethodQuery':
            $this->CI->load->library('Qb_api_xml_shipmethod');
            $query = new $this->CI->qb_api_xml_shipmethod();
            return $query->request( $queue );
        break;

        case 'StandardTermsQuery':
            $this->CI->load->library('Qb_api_xml_standardterms');
            $query = new $this->CI->qb_api_xml_standardterms();
            return $query->request( $queue );
        break;

        case 'TemplateQuery':
            $this->CI->load->library('Qb_api_xml_template');
            $query = new $this->CI->qb_api_xml_template();
            return $query->request( $queue );
        break;

        case 'TermQuery':
            $this->CI->load->library('Qb_api_xml_term');
            $query = new $this->CI->qb_api_xml_term();
            return $query->request( $queue );
        break;

        case 'TimeTrackingQuery':
            $this->CI->load->library('Qb_api_xml_timetracking');
            $query = new $this->CI->qb_api_xml_timetracking();
            return $query->request( $queue );
        break;

        case 'UnitOfMeasureSetQuery':
            $this->CI->load->library('Qb_api_xml_unitofmeasureset');
            $query = new $this->CI->qb_api_xml_unitofmeasureset();
            return $query->request( $queue );
        break;

        case 'VehicleMileageQuery':
            $this->CI->load->library('Qb_api_xml_vehiclemileage');
            $query = new $this->CI->qb_api_xml_vehiclemileage();
            return $query->request( $queue );
        break;

        case 'VehicleQuery':
            $this->CI->load->library('Qb_api_xml_vehicle');
            $query = new $this->CI->qb_api_xml_vehicle();
            return $query->request( $queue );
        break;

        case 'VendorCreditQuery':
            $this->CI->load->library('Qb_api_xml_vendorcredit');
            $query = new $this->CI->qb_api_xml_vendorcredit();
            return $query->request( $queue );
        break;

        case 'VendorQuery':
            $this->CI->load->library('Qb_api_xml_vendor');
            $query = new $this->CI->qb_api_xml_vendor();
            return $query->request( $queue );
        break;

        case 'VendorTypeQuery':
            $this->CI->load->library('Qb_api_xml_vendortype');
            $query = new $this->CI->qb_api_xml_vendortype();
            return $query->request( $queue );
        break;

        case 'WorkersCompCodeQuery':
            $this->CI->load->library('Qb_api_xml_workerscompcode');
            $query = new $this->CI->qb_api_xml_workerscompcode();
            return $query->request( $queue );
        break;

      }

    }


}
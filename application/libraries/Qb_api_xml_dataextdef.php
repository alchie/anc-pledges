<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_dataextdef extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<DataExtDefQueryRq requestID="'.$requestID.'">' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</DataExtDefQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'DataextdefRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_dataextdef_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_dataextdef_model();
                $model->setOwnerid($item_obj->OwnerID);
                $model->setDataextid($item_obj->DataExtID);
                $model->setDataextname($item_obj->DataExtName,TRUE);
                $model->setDataexttype($item_obj->DataExtType);
                $model->setDataextlistrequire($item_obj->DataExtListRequire);
                $model->setDataexttxnrequire($item_obj->DataExtTxnRequire);
                $model->setDataextformatstring($item_obj->DataExtFormatString);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                if( $item_obj->AssignToObjects ) {
                  $this->CI->load->model('Qb_dataextdef_assigntoobject_model');
                  foreach($item_obj->AssignToObjects as $ato) {
                    $model2 = new $this->CI->Qb_dataextdef_assigntoobject_model();
                    $model2->setAssigntoobject($ato,true);
                    $model2->setDataextdefOwnerid($item_obj->OwnerID);
                    $model2->setDataextdefDataextname($item_obj->DataExtName,true);
                    if( $model2->nonEmpty() ) {
                      $model2->update();
                    } else {
                      $model2->insert();
                    }
                  }
                }

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'DataExtDefRet') {

          $AssignToObject = $item->getElementsByTagName('AssignToObject');
          $AssignToObjects = array();

          if( $AssignToObject ) {
            foreach($AssignToObject as $ato) {
              $AssignToObjects[] = $ato->textContent;
            }
          }

          $this->items[] = array(
            'OwnerID' => $this->get_text_content($item, array('OwnerID')),
            'DataExtID' => $this->get_text_content($item, array('DataExtID')),
            'DataExtName' => $this->get_text_content($item, array('DataExtName')),
            'DataExtType' => $this->get_text_content($item, array('DataExtType')),
            'DataExtListRequire' => $this->get_text_content($item, array('DataExtListRequire')),
            'DataExtTxnRequire' => $this->get_text_content($item, array('DataExtTxnRequire')),
            'DataExtFormatString' => $this->get_text_content($item, array('DataExtFormatString')),
            'AssignToObjects' => $AssignToObjects,

          );

        }
      }
    }
 
}

/* End of file */

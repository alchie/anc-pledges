<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_bill extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_bill_model');
        $model = new $this->CI->Qb_bill_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_BILLQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_BILLQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified  . " +1 second") ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<BillQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<IncludeLineItems>true</IncludeLineItems>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</BillQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>Bill</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'BillRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_bill_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_bill_model();
                $model->setTax1Total($item_obj->Tax1Total);
                $model->setTax2Total($item_obj->Tax2Total);
                $model->setTxnid($item_obj->TxnID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setTxnnumber($item_obj->TxnNumber);
                $model->setVendorListid($item_obj->Vendor_ListID);
                $model->setVendorFullname($item_obj->Vendor_FullName);
                $model->setApaccountListid($item_obj->APAccount_ListID);
                $model->setApaccountFullname($item_obj->APAccount_FullName);
                $model->setTxndate($item_obj->TxnDate);
                $model->setDuedate($item_obj->DueDate);
                $model->setAmountdue($item_obj->AmountDue);
                $model->setCurrencyListid($item_obj->Currency_ListID);
                $model->setCurrencyFullname($item_obj->Currency_FullName);
                $model->setExchangerate($item_obj->ExchangeRate);
                $model->setAmountdueinhomecurrency($item_obj->AmountDueInHomeCurrency);
                $model->setRefnumber($item_obj->RefNumber);
                $model->setTermsListid($item_obj->Terms_ListID);
                $model->setTermsFullname($item_obj->Terms_FullName);
                $model->setMemo($item_obj->Memo);
                $model->setIspaid($item_obj->IsPaid);
                $model->setOpenamount($item_obj->OpenAmount);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items( $item_obj );

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'BillRet') {

          $this->items[] = array(
            'Tax1Total' => $this->get_text_content($item, array('Tax1Total')),
            'Tax2Total' => $this->get_text_content($item, array('Tax2Total')),
            'TxnID' => $this->get_text_content($item, array('TxnID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'TxnNumber' => $this->get_text_content($item, array('TxnNumber')),
            'Vendor_ListID' => $this->get_text_content($item, array('VendorRef','ListID')),
            'Vendor_FullName' => $this->get_text_content($item, array('VendorRef','FullName')),
            'APAccount_ListID' => $this->get_text_content($item, array('APAccountRef','ListID')),
            'APAccount_FullName' => $this->get_text_content($item, array('APAccountRef','FullName')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'DueDate' => $this->get_text_content($item, array('DueDate')),
            'AmountDue' => $this->get_text_content($item, array('AmountDue')),
            'Currency_ListID' => $this->get_text_content($item, array('CurrencyRef','ListID')),
            'Currency_FullName' => $this->get_text_content($item, array('CurrencyRef','FullName')),
            'ExchangeRate' => $this->get_text_content($item, array('ExchangeRate')),
            'AmountDueInHomeCurrency' => $this->get_text_content($item, array('AmountDueInHomeCurrency')),
            'RefNumber' => $this->get_text_content($item, array('RefNumber')),
            'Terms_ListID' => $this->get_text_content($item, array('TermsRef','ListID')),
            'Terms_FullName' => $this->get_text_content($item, array('TermsRef','FullName')),
            'Memo' => $this->get_text_content($item, array('Memo')),
            'IsPaid' => $this->get_text_content($item, array('IsPaid')),
            'OpenAmount' => $this->get_text_content($item, array('OpenAmount')),
            'DataExtItems' => $this->get_dataext_items($item, 'Bill', 'TxnID'),
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_bill_model');
        $query = new $this->CI->Qb_bill_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }

}

/* End of file */

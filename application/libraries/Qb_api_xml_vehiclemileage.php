<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_vehiclemileage extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_vehiclemileage_model');
        $model = new $this->CI->Qb_vehiclemileage_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_VEHICLEMILEAGEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_VEHICLEMILEAGEQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<VehicleMileageQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'</VehicleMileageQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>VehicleMileage</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'VehicleMileageRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_vehiclemileage_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_vehiclemileage_model();
                $model->setTxnid($item_obj->TxnID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setVehicleListid($item_obj->Vehicle_ListID);
                $model->setVehicleFullname($item_obj->Vehicle_FullName);
                $model->setCustomerListid($item_obj->Customer_ListID);
                $model->setCustomerFullname($item_obj->Customer_FullName);
                $model->setItemListid($item_obj->Item_ListID);
                $model->setItemFullname($item_obj->Item_FullName);
                $model->setClassListid($item_obj->Class_ListID);
                $model->setClassFullname($item_obj->Class_FullName);
                $model->setTripstartdate($item_obj->TripStartDate);
                $model->setTripenddate($item_obj->TripEndDate);
                $model->setOdometerstart($item_obj->OdometerStart);
                $model->setOdometerend($item_obj->OdometerEnd);
                $model->setTotalmiles($item_obj->TotalMiles);
                $model->setNotes($item_obj->Notes);
                $model->setBillablestatus($item_obj->BillableStatus);
                $model->setStandardmileagerate($item_obj->StandardMileageRate);
                $model->setStandardmileagetotalamount($item_obj->StandardMileageTotalAmount);
                $model->setBillablerate($item_obj->BillableRate);
                $model->setBillableamount($item_obj->BillableAmount);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'VehicleMileageRet') {

          $this->items[] = array(
            'TxnID' => $this->get_text_content($item, array('TxnID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'Vehicle_ListID' => $this->get_text_content($item, array('VehicleRef','ListID')),
            'Vehicle_FullName' => $this->get_text_content($item, array('VehicleRef','FullName')),
            'Customer_ListID' => $this->get_text_content($item, array('CustomerRef','ListID')),
            'Customer_FullName' => $this->get_text_content($item, array('CustomerRef','FullName')),
            'Item_ListID' => $this->get_text_content($item, array('ItemRef','ListID')),
            'Item_FullName' => $this->get_text_content($item, array('ItemRef','FullName')),
            'Class_ListID' => $this->get_text_content($item, array('ClassRef','ListID')),
            'Class_FullName' => $this->get_text_content($item, array('ClassRef','FullName')),
            'TripStartDate' => $this->get_text_content($item, array('TripStartDate')),
            'TripEndDate' => $this->get_text_content($item, array('TripEndDate')),
            'OdometerStart' => $this->get_text_content($item, array('OdometerStart')),
            'OdometerEnd' => $this->get_text_content($item, array('OdometerEnd')),
            'TotalMiles' => $this->get_text_content($item, array('TotalMiles')),
            'Notes' => $this->get_text_content($item, array('Notes')),
            'BillableStatus' => $this->get_text_content($item, array('BillableStatus')),
            'StandardMileageRate' => $this->get_text_content($item, array('StandardMileageRate')),
            'StandardMileageTotalAmount' => $this->get_text_content($item, array('StandardMileageTotalAmount')),
            'BillableRate' => $this->get_text_content($item, array('BillableRate')),
            'BillableAmount' => $this->get_text_content($item, array('BillableAmount')),
            'DataExtItems' => $this->get_dataext_items($item, 'VehicleMileage', 'TxnID'),
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_vehiclemileage_model');
        $query = new $this->CI->Qb_vehiclemileage_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }
     
}

/* End of file */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_deposit extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_deposit_model');
        $deposit = new $this->CI->Qb_deposit_model();
        $deposit->set_order('TimeModified', 'DESC');
        $deposit_data = $deposit->get();
        return (($deposit_data) && isset($deposit_data->TimeModified)) ? $deposit_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_DEPOSITQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_DEPOSITQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified . " +1 second" ) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<DepositQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<IncludeLineItems>true</IncludeLineItems>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</DepositQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>Deposit</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'Deposit saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_deposit_model');
            $this->CI->load->model('Qb_deposit_depositline_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $deposit = new $this->CI->Qb_deposit_model();
                $deposit->setTxnid($item_obj->TxnID,true);
                $deposit->setTimecreated($item_obj->TimeCreated);
                $deposit->setTimemodified($item_obj->TimeModified);
                $deposit->setEditsequence($item_obj->EditSequence);
                $deposit->setTxnnumber($item_obj->TxnNumber);
                $deposit->setTxndate($item_obj->TxnDate);
                $deposit->setDeposittoaccountListid($item_obj->DepositToAccount_ListID);
                $deposit->setDeposittoaccountFullname($item_obj->DepositToAccount_FullName);
                $deposit->setMemo($item_obj->Memo);
                $deposit->setDeposittotal($item_obj->DepositTotal);
                $deposit->setCashbackinfoTxnlineid($item_obj->CashBackInfo_TxnLineID);
                $deposit->setCashbackinfoMemo($item_obj->CashBackInfo_Memo);
                $deposit->setCashbackinfoAmount($item_obj->CashBackInfo_Amount);
                $deposit->setCashbackinfoAccountListid($item_obj->CashBackInfo_Account_ListID);
                $deposit->setCashbackinfoAccountFullname($item_obj->CashBackInfo_Account_FullName);

                if( $deposit->nonEmpty() ) {
                  $deposit->update();
                } else {
                  $deposit->insert();
                }

                if( count($item_obj->LineItems) > 0 ) {
                    foreach( $item_obj->LineItems as $line_item ) {

                        $line_item_obj = (object) $line_item;
                        
                        $depositline = new $this->CI->Qb_deposit_depositline_model();
                        $depositline->setDepositTxnid($item_obj->TxnID);
                        $depositline->setTxntype($line_item_obj->TxnType);
                        $depositline->setTxnid($line_item_obj->TxnID);
                        $depositline->setTxnlineid($line_item_obj->TxnLineID,true);
                        $depositline->setMemo($line_item_obj->Memo);
                        $depositline->setPaymenttxnlineid($line_item_obj->PaymentTxnLineID);
                        $depositline->setAmount($line_item_obj->Amount);
                        $depositline->setEntityListid($line_item_obj->Entity_ListID);
                        $depositline->setEntityFullname($line_item_obj->Entity_FullName);
                        $depositline->setAccountListid($line_item_obj->Account_ListID);
                        $depositline->setAccountFullname($line_item_obj->Account_FullName);
                        $depositline->setPaymentmethodListid($line_item_obj->PaymentMethod_ListID);
                        $depositline->setPaymentmethodFullname($line_item_obj->PaymentMethod_FullName);
                        $depositline->setChecknumber($line_item_obj->CheckNumber);
                        $depositline->setClassListid($line_item_obj->Class_ListID);
                        $depositline->setClassFullname($line_item_obj->Class_FullName);

                        if( $depositline->nonEmpty() ) {
                          $depositline->update();
                        } else {
                          $depositline->insert();
                        }

                    }
                }

                $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'DepositRet') {

            $DepositLineRet = $item->getElementsByTagName('DepositLineRet');
            $line_items = array();

            if( $DepositLineRet->length > 0 ) {
                foreach($DepositLineRet as $line_item) {

                    $line_items[] = array(
                        'TxnType' => $this->get_text_content($line_item, 'TxnType'), 
                        'TxnID' => $this->get_text_content($line_item, 'TxnID'), 
                        'TxnLineID' => $this->get_text_content($line_item, 'TxnLineID'), 
                        'PaymentTxnLineID' => $this->get_text_content($line_item, 'PaymentTxnLineID'), 
                        'Memo' => $this->get_text_content($line_item, 'Memo'), 
                        'Amount' => $this->get_text_content($line_item, 'Amount'), 
                        'CheckNumber' => $this->get_text_content($line_item, 'CheckNumber'), 
                        'Entity_ListID' => $this->get_text_content($line_item, array('EntityRef','ListID')), 
                        'Entity_FullName' => $this->get_text_content($line_item, array('EntityRef','FullName')), 
                        'Account_ListID' => $this->get_text_content($line_item, array('AccountRef','ListID')), 
                        'Account_FullName' => $this->get_text_content($line_item, array('AccountRef','FullName')), 
                        'PaymentMethod_ListID' => $this->get_text_content($line_item, array('PaymentMethodRef','ListID')), 
                        'PaymentMethod_FullName' => $this->get_text_content($line_item, array('PaymentMethodRef','FullName')), 
                        'Class_ListID' => $this->get_text_content($line_item, array('ClassRef','ListID')), 
                        'Class_FullName' => $this->get_text_content($line_item, array('ClassRef','FullName')), 
                    );
                    
                }
            }
            

          $this->items[] = array(
            'TxnID' => $this->get_text_content($item, 'TxnID'), 
            'TimeCreated' => $this->get_text_content($item, 'TimeCreated'), 
            'TimeModified' => $this->get_text_content($item, 'TimeModified'), 
            'EditSequence' => $this->get_text_content($item, 'EditSequence'), 
            'TxnNumber' => $this->get_text_content($item, 'TxnNumber'), 
            'TxnDate' => $this->get_text_content($item, 'TxnDate'), 
            'Memo' => $this->get_text_content($item, 'Memo'), 
            'DepositTotal' => $this->get_text_content($item, 'DepositTotal'), 
            'DepositToAccount_ListID' => $this->get_text_content($item, array('DepositToAccountRef','ListID')), 
            'DepositToAccount_FullName' => $this->get_text_content($item, array('DepositToAccountRef','FullName')), 
            'CashBackInfo_TxnLineID' => $this->get_text_content($item, array('CashBackInfoRet','TxnLineID')), 
            'CashBackInfo_Memo' => $this->get_text_content($item, array('CashBackInfoRet','Memo')), 
            'CashBackInfo_Amount' => $this->get_text_content($item, array('CashBackInfoRet','Amount')), 
            'CashBackInfo_Account_ListID' => $this->get_text_content($item, array('CashBackInfoRet','AccountRef','ListID')), 
            'CashBackInfo_Account_FullName' => $this->get_text_content($item, array('CashBackInfoRet','AccountRef','FullName')), 
            'LineItems' => $line_items,
            'DataExtItems' => $this->get_dataext_items($item, 'Deposit', 'TxnID'),
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_deposit_model');
        $query = new $this->CI->Qb_deposit_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }
    
}

/* End of file Global_variables.php */
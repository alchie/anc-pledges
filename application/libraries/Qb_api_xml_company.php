<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_company extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<CompanyQueryRq requestID="'.$requestID.'">' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</CompanyQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'CompanyRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_company_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_company_model();
                $model->setIssamplecompany($item_obj->IsSampleCompany);
                $model->setCompanyname($item_obj->CompanyName,TRUE);
                $model->setLegalcompanyname($item_obj->LegalCompanyName);
                $model->setAddressAddr1($item_obj->Address_Addr1);
                $model->setAddressAddr2($item_obj->Address_Addr2);
                $model->setAddressAddr3($item_obj->Address_Addr3);
                $model->setAddressAddr4($item_obj->Address_Addr4);
                $model->setAddressAddr5($item_obj->Address_Addr5);
                $model->setAddressCity($item_obj->Address_City);
                $model->setAddressState($item_obj->Address_State);
                $model->setAddressPostalcode($item_obj->Address_PostalCode);
                $model->setAddressCountry($item_obj->Address_Country);
                $model->setAddressNote($item_obj->Address_Note);
                $model->setAddressblockAddr1($item_obj->AddressBlock_Addr1);
                $model->setAddressblockAddr2($item_obj->AddressBlock_Addr2);
                $model->setAddressblockAddr3($item_obj->AddressBlock_Addr3);
                $model->setAddressblockAddr4($item_obj->AddressBlock_Addr4);
                $model->setAddressblockAddr5($item_obj->AddressBlock_Addr5);
                $model->setLegaladdressAddr1($item_obj->LegalAddress_Addr1);
                $model->setLegaladdressAddr2($item_obj->LegalAddress_Addr2);
                $model->setLegaladdressAddr3($item_obj->LegalAddress_Addr3);
                $model->setLegaladdressAddr4($item_obj->LegalAddress_Addr4);
                $model->setLegaladdressAddr5($item_obj->LegalAddress_Addr5);
                $model->setLegaladdressCity($item_obj->LegalAddress_City);
                $model->setLegaladdressState($item_obj->LegalAddress_State);
                $model->setLegaladdressPostalcode($item_obj->LegalAddress_PostalCode);
                $model->setLegaladdressCountry($item_obj->LegalAddress_Country);
                $model->setLegaladdressNote($item_obj->LegalAddress_Note);
                $model->setCompanyCompanyaddressforcustomerAddr1($item_obj->Company_CompanyAddressForCustomer_Addr1);
                $model->setCompanyCompanyaddressforcustomerAddr2($item_obj->Company_CompanyAddressForCustomer_Addr2);
                $model->setCompanyCompanyaddressforcustomerAddr3($item_obj->Company_CompanyAddressForCustomer_Addr3);
                $model->setCompanyCompanyaddressforcustomerAddr4($item_obj->Company_CompanyAddressForCustomer_Addr4);
                $model->setCompanyCompanyaddressforcustomerAddr5($item_obj->Company_CompanyAddressForCustomer_Addr5);
                $model->setCompanyCompanyaddressforcustomerCity($item_obj->Company_CompanyAddressForCustomer_City);
                $model->setCompanyCompanyaddressforcustomerState($item_obj->Company_CompanyAddressForCustomer_State);
                $model->setCompanyCompanyaddressforcustomerPostalcode($item_obj->Company_CompanyAddressForCustomer_PostalCode);
                $model->setCompanyCompanyaddressforcustomerCountry($item_obj->Company_CompanyAddressForCustomer_Country);
                $model->setCompanyCompanyaddressforcustomerNote($item_obj->Company_CompanyAddressForCustomer_Note);
                $model->setCompanyCompanyaddressblockforcustomerAddr1($item_obj->Company_CompanyAddressBlockForCustomer_Addr1);
                $model->setCompanyCompanyaddressblockforcustomerAddr2($item_obj->Company_CompanyAddressBlockForCustomer_Addr2);
                $model->setCompanyCompanyaddressblockforcustomerAddr3($item_obj->Company_CompanyAddressBlockForCustomer_Addr3);
                $model->setCompanyCompanyaddressblockforcustomerAddr4($item_obj->Company_CompanyAddressBlockForCustomer_Addr4);
                $model->setCompanyCompanyaddressblockforcustomerAddr5($item_obj->Company_CompanyAddressBlockForCustomer_Addr5);
                $model->setPhone($item_obj->Phone);
                $model->setFax($item_obj->Fax);
                $model->setEmail($item_obj->Email);
                $model->setCompanywebsite($item_obj->CompanyWebSite);
                $model->setFirstmonthfiscalyear($item_obj->FirstMonthFiscalYear);
                $model->setFirstmonthincometaxyear($item_obj->FirstMonthIncomeTaxYear);
                $model->setCompanytype($item_obj->CompanyType);
                $model->setEin($item_obj->EIN);
                $model->setSsn($item_obj->SSN);
                $model->setTaxform($item_obj->TaxForm);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

            $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
        
      foreach($this->data as $item) {
        if( $item->nodeName == 'CompanyRet') {

          $this->items[] = array(
            'IsSampleCompany' => $this->get_text_content($item, array('IsSampleCompany')),
            'CompanyName' => $this->get_text_content($item, array('CompanyName')),
            'LegalCompanyName' => $this->get_text_content($item, array('LegalCompanyName')),
            'Address_Addr1' => $this->get_text_content($item, array('Address','Addr1')),
            'Address_Addr2' => $this->get_text_content($item, array('Address','Addr2')),
            'Address_Addr3' => $this->get_text_content($item, array('Address','Addr3')),
            'Address_Addr4' => $this->get_text_content($item, array('Address','Addr4')),
            'Address_Addr5' => $this->get_text_content($item, array('Address','Addr5')),
            'Address_City' => $this->get_text_content($item, array('Address','City')),
            'Address_State' => $this->get_text_content($item, array('Address','State')),
            'Address_PostalCode' => $this->get_text_content($item, array('Address','PostalCode')),
            'Address_Country' => $this->get_text_content($item, array('Address','Country')),
            'Address_Note' => $this->get_text_content($item, array('Address','Note')),
            'AddressBlock_Addr1' => $this->get_text_content($item, array('AddressBlock','Addr1')),
            'AddressBlock_Addr2' => $this->get_text_content($item, array('AddressBlock','Addr2')),
            'AddressBlock_Addr3' => $this->get_text_content($item, array('AddressBlock','Addr3')),
            'AddressBlock_Addr4' => $this->get_text_content($item, array('AddressBlock','Addr4')),
            'AddressBlock_Addr5' => $this->get_text_content($item, array('AddressBlock','Addr5')),
            'LegalAddress_Addr1' => $this->get_text_content($item, array('LegalAddress','Addr1')),
            'LegalAddress_Addr2' => $this->get_text_content($item, array('LegalAddress','Addr2')),
            'LegalAddress_Addr3' => $this->get_text_content($item, array('LegalAddress','Addr3')),
            'LegalAddress_Addr4' => $this->get_text_content($item, array('LegalAddress','Addr4')),
            'LegalAddress_Addr5' => $this->get_text_content($item, array('LegalAddress','Addr5')),
            'LegalAddress_City' => $this->get_text_content($item, array('LegalAddress','City')),
            'LegalAddress_State' => $this->get_text_content($item, array('LegalAddress','State')),
            'LegalAddress_PostalCode' => $this->get_text_content($item, array('LegalAddress','PostalCode')),
            'LegalAddress_Country' => $this->get_text_content($item, array('LegalAddress','Country')),
            'LegalAddress_Note' => $this->get_text_content($item, array('LegalAddress','Note')),
            'Company_CompanyAddressForCustomer_Addr1' => $this->get_text_content($item, array('CompanyAddressForCustomer','Addr1')),
            'Company_CompanyAddressForCustomer_Addr2' => $this->get_text_content($item, array('CompanyAddressForCustomer','Addr2')),
            'Company_CompanyAddressForCustomer_Addr3' => $this->get_text_content($item, array('CompanyAddressForCustomer','Addr3')),
            'Company_CompanyAddressForCustomer_Addr4' => $this->get_text_content($item, array('CompanyAddressForCustomer','Addr4')),
            'Company_CompanyAddressForCustomer_Addr5' => $this->get_text_content($item, array('CompanyAddressForCustomer','Addr5')),
            'Company_CompanyAddressForCustomer_City' => $this->get_text_content($item, array('CompanyAddressForCustomer','City')),
            'Company_CompanyAddressForCustomer_State' => $this->get_text_content($item, array('CompanyAddressForCustomer','State')),
            'Company_CompanyAddressForCustomer_PostalCode' => $this->get_text_content($item, array('CompanyAddressForCustomer','PostalCode')),
            'Company_CompanyAddressForCustomer_Country' => $this->get_text_content($item, array('CompanyAddressForCustomer','Country')),
            'Company_CompanyAddressForCustomer_Note' => $this->get_text_content($item, array('CompanyAddressForCustomer','Note')),
            'Company_CompanyAddressBlockForCustomer_Addr1' => $this->get_text_content($item, array('CompanyAddressBlockForCustomer','Addr1')),
            'Company_CompanyAddressBlockForCustomer_Addr2' => $this->get_text_content($item, array('CompanyAddressBlockForCustomer','Addr2')),
            'Company_CompanyAddressBlockForCustomer_Addr3' => $this->get_text_content($item, array('CompanyAddressBlockForCustomer','Addr3')),
            'Company_CompanyAddressBlockForCustomer_Addr4' => $this->get_text_content($item, array('CompanyAddressBlockForCustomer','Addr4')),
            'Company_CompanyAddressBlockForCustomer_Addr5' => $this->get_text_content($item, array('CompanyAddressBlockForCustomer','Addr5')),
            'Phone' => $this->get_text_content($item, array('Phone')),
            'Fax' => $this->get_text_content($item, array('Fax')),
            'Email' => $this->get_text_content($item, array('Email')),
            'CompanyWebSite' => $this->get_text_content($item, array('CompanyWebSite')),
            'FirstMonthFiscalYear' => $this->get_text_content($item, array('FirstMonthFiscalYear')),
            'FirstMonthIncomeTaxYear' => $this->get_text_content($item, array('FirstMonthIncomeTaxYear')),
            'CompanyType' => $this->get_text_content($item, array('CompanyType')),
            'EIN' => $this->get_text_content($item, array('EIN')),
            'SSN' => $this->get_text_content($item, array('SSN')),
            'TaxForm' => $this->get_text_content($item, array('TaxForm')),
            'DataExtItems' => $this->get_dataext_items($item, 'Company', 'CompanyName'),
          );

        }
      }
    }
 
}

/* End of file */

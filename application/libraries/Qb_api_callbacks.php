<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_callbacks {
    
    protected $CI;
    protected $output;
    protected $method;
    protected $data;

    public function __construct($method='')
    {
       $this->CI =& get_instance();
       $this->method = $method;
    }


    public function setData($data) {
      $this->data = $data;
    }

    public function output() {

      $output = '<?xml version="1.0" encoding="UTF-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" 
       xmlns:ns1="http://developer.intuit.com/">
        <SOAP-ENV:Body><ns1:' . $this->method . 'Response>';
            
      $output .= $this->_serialize($this->data);
      
      $output .= '</ns1:' . $this->method . 'Response>
      </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>';

      //php_error_log($output, 'Qb_api_xml_callbacks->output');

      return $output;

    }

    protected function _serialize($data)
    {
      $output = '';
      
      if (is_array($data))
      {
        foreach ($data as $key => $value)
        {
          
          $output .= '<ns1:' . $key . '>';
          
          if (is_array($value))
          {
            foreach ($value as $subkey => $subvalue)
            {
              $output .= '<ns1:string>' . htmlspecialchars($subvalue) . '</ns1:string>' . "\n";
            }
          }
          else
          {
            $output .= htmlspecialchars($value);
          }
          
          $output .= '</ns1:' . $key . '>';
        }
      }
      
      return $output;
    }

}

/* End of file Global_variables.php */
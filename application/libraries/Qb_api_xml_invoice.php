<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_invoice extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_invoice_model');
        $model = new $this->CI->Qb_invoice_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_INVOICEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_INVOICEQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<InvoiceQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<IncludeLineItems>true</IncludeLineItems>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</InvoiceQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>Invoice</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'InvoiceRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_invoice_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_invoice_model();
                $model->setTxnid($item_obj->TxnID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setTxnnumber($item_obj->TxnNumber);
                $model->setCustomerListid($item_obj->Customer_ListID);
                $model->setCustomerFullname($item_obj->Customer_FullName);
                $model->setClassListid($item_obj->Class_ListID);
                $model->setClassFullname($item_obj->Class_FullName);
                $model->setAraccountListid($item_obj->ARAccount_ListID);
                $model->setAraccountFullname($item_obj->ARAccount_FullName);
                $model->setTemplateListid($item_obj->Template_ListID);
                $model->setTemplateFullname($item_obj->Template_FullName);
                $model->setTxndate($item_obj->TxnDate);
                $model->setRefnumber($item_obj->RefNumber);
                $model->setBilladdressAddr1($item_obj->BillAddress_Addr1);
                $model->setBilladdressAddr2($item_obj->BillAddress_Addr2);
                $model->setBilladdressAddr3($item_obj->BillAddress_Addr3);
                $model->setBilladdressAddr4($item_obj->BillAddress_Addr4);
                $model->setBilladdressAddr5($item_obj->BillAddress_Addr5);
                $model->setBilladdressCity($item_obj->BillAddress_City);
                $model->setBilladdressState($item_obj->BillAddress_State);
                $model->setBilladdressPostalcode($item_obj->BillAddress_PostalCode);
                $model->setBilladdressCountry($item_obj->BillAddress_Country);
                $model->setBilladdressNote($item_obj->BillAddress_Note);
                $model->setBilladdressblockAddr1($item_obj->BillAddressBlock_Addr1);
                $model->setBilladdressblockAddr2($item_obj->BillAddressBlock_Addr2);
                $model->setBilladdressblockAddr3($item_obj->BillAddressBlock_Addr3);
                $model->setBilladdressblockAddr4($item_obj->BillAddressBlock_Addr4);
                $model->setBilladdressblockAddr5($item_obj->BillAddressBlock_Addr5);
                $model->setShipaddressAddr1($item_obj->ShipAddress_Addr1);
                $model->setShipaddressAddr2($item_obj->ShipAddress_Addr2);
                $model->setShipaddressAddr3($item_obj->ShipAddress_Addr3);
                $model->setShipaddressAddr4($item_obj->ShipAddress_Addr4);
                $model->setShipaddressAddr5($item_obj->ShipAddress_Addr5);
                $model->setShipaddressCity($item_obj->ShipAddress_City);
                $model->setShipaddressState($item_obj->ShipAddress_State);
                $model->setShipaddressPostalcode($item_obj->ShipAddress_PostalCode);
                $model->setShipaddressCountry($item_obj->ShipAddress_Country);
                $model->setShipaddressNote($item_obj->ShipAddress_Note);
                $model->setShipaddressblockAddr1($item_obj->ShipAddressBlock_Addr1);
                $model->setShipaddressblockAddr2($item_obj->ShipAddressBlock_Addr2);
                $model->setShipaddressblockAddr3($item_obj->ShipAddressBlock_Addr3);
                $model->setShipaddressblockAddr4($item_obj->ShipAddressBlock_Addr4);
                $model->setShipaddressblockAddr5($item_obj->ShipAddressBlock_Addr5);
                $model->setIspending($item_obj->IsPending);
                $model->setIsfinancecharge($item_obj->IsFinanceCharge);
                $model->setPonumber($item_obj->PONumber);
                $model->setTermsListid($item_obj->Terms_ListID);
                $model->setTermsFullname($item_obj->Terms_FullName);
                $model->setDuedate($item_obj->DueDate);
                $model->setSalesrepListid($item_obj->SalesRep_ListID);
                $model->setSalesrepFullname($item_obj->SalesRep_FullName);
                $model->setFob($item_obj->FOB);
                $model->setShipdate($item_obj->ShipDate);
                $model->setShipmethodListid($item_obj->ShipMethod_ListID);
                $model->setShipmethodFullname($item_obj->ShipMethod_FullName);
                $model->setSubtotal($item_obj->Subtotal);
                $model->setItemsalestaxListid($item_obj->ItemSalesTax_ListID);
                $model->setItemsalestaxFullname($item_obj->ItemSalesTax_FullName);
                $model->setSalestaxpercentage($item_obj->SalesTaxPercentage);
                $model->setSalestaxtotal($item_obj->SalesTaxTotal);
                $model->setAppliedamount($item_obj->AppliedAmount);
                $model->setBalanceremaining($item_obj->BalanceRemaining);
                $model->setMemo($item_obj->Memo);
                $model->setIspaid($item_obj->IsPaid);
                $model->setCurrencyListid($item_obj->Currency_ListID);
                $model->setCurrencyFullname($item_obj->Currency_FullName);
                $model->setExchangerate($item_obj->ExchangeRate);
                $model->setBalanceremaininginhomecurrency($item_obj->BalanceRemainingInHomeCurrency);
                $model->setCustomermsgListid($item_obj->CustomerMsg_ListID);
                $model->setCustomermsgFullname($item_obj->CustomerMsg_FullName);
                $model->setIstobeprinted($item_obj->IsToBePrinted);
                $model->setIstobeemailed($item_obj->IsToBeEmailed);
                $model->setCustomersalestaxcodeListid($item_obj->CustomerSalesTaxCode_ListID);
                $model->setCustomersalestaxcodeFullname($item_obj->CustomerSalesTaxCode_FullName);
                $model->setSuggesteddiscountamount($item_obj->SuggestedDiscountAmount);
                $model->setSuggesteddiscountdate($item_obj->SuggestedDiscountDate);
                $model->setOther($item_obj->Other);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                if( count($item_obj->LineItems) > 0 ) {

                    $this->CI->load->model('Qb_invoice_invoiceline_model');
                    foreach( $item_obj->LineItems as $line_item ) {

                        $line_item_obj = (object) $line_item;
                        
                        $invoiceline = new $this->CI->Qb_invoice_invoiceline_model();
                        $invoiceline->setInvoiceTxnid($line_item_obj->Invoice_TxnID,TRUE);
                        $invoiceline->setSortorder($line_item_obj->SortOrder);
                        $invoiceline->setTxnlineid($line_item_obj->TxnLineID,TRUE);
                        $invoiceline->setItemListid($line_item_obj->Item_ListID);
                        $invoiceline->setItemFullname($line_item_obj->Item_FullName);
                        $invoiceline->setDescrip($line_item_obj->Descrip);
                        $invoiceline->setQuantity($line_item_obj->Quantity);
                        $invoiceline->setUnitofmeasure($line_item_obj->UnitOfMeasure);
                        $invoiceline->setOverrideuomsetListid($line_item_obj->OverrideUOMSet_ListID);
                        $invoiceline->setOverrideuomsetFullname($line_item_obj->OverrideUOMSet_FullName);
                        $invoiceline->setRate($line_item_obj->Rate);
                        $invoiceline->setRatepercent($line_item_obj->RatePercent);
                        $invoiceline->setClassListid($line_item_obj->Class_ListID);
                        $invoiceline->setClassFullname($line_item_obj->Class_FullName);
                        $invoiceline->setAmount($line_item_obj->Amount);
                        $invoiceline->setInventorysiteListid($line_item_obj->InventorySite_ListID);
                        $invoiceline->setInventorysiteFullname($line_item_obj->InventorySite_FullName);
                        $invoiceline->setSerialnumber($line_item_obj->SerialNumber);
                        $invoiceline->setLotnumber($line_item_obj->LotNumber);
                        $invoiceline->setServicedate($line_item_obj->ServiceDate);
                        $invoiceline->setSalestaxcodeListid($line_item_obj->SalesTaxCode_ListID);
                        $invoiceline->setSalestaxcodeFullname($line_item_obj->SalesTaxCode_FullName);
                        $invoiceline->setOther1($line_item_obj->Other1);
                        $invoiceline->setOther2($line_item_obj->Other2);

                        if( $invoiceline->nonEmpty() ) {
                          $invoiceline->update();
                        } else {
                          $invoiceline->insert();
                        }

                    }
                }

                $this->insert_dataext_items($item_obj);
                
            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'InvoiceRet') {

              $InvoiceLineRet = $item->getElementsByTagName('InvoiceLineRet');
              $LineItems = array();

              if( $InvoiceLineRet ) {
                foreach($InvoiceLineRet as $line_item) {
                  $LineItems[] = array(
                    'Invoice_TxnID' => $this->get_text_content($item, array('TxnID')),
                    'SortOrder' => $this->get_text_content($line_item, array('SortOrder')),
                    'TxnLineID' => $this->get_text_content($line_item, array('TxnLineID')),
                    'Item_ListID' => $this->get_text_content($line_item, array('ItemRef','ListID')),
                    'Item_FullName' => $this->get_text_content($line_item, array('ItemRef','FullName')),
                    'Descrip' => $this->get_text_content($line_item, array('Descrip')),
                    'Quantity' => $this->get_text_content($line_item, array('Quantity')),
                    'UnitOfMeasure' => $this->get_text_content($line_item, array('UnitOfMeasure')),
                    'OverrideUOMSet_ListID' => $this->get_text_content($line_item, array('OverrideUOMSetRef','ListID')),
                    'OverrideUOMSet_FullName' => $this->get_text_content($line_item, array('OverrideUOMSetRef','FullName')),
                    'Rate' => $this->get_text_content($line_item, array('Rate')),
                    'RatePercent' => $this->get_text_content($line_item, array('RatePercent')),
                    'Class_ListID' => $this->get_text_content($line_item, array('ClassRef','ListID')),
                    'Class_FullName' => $this->get_text_content($line_item, array('ClassRef','FullName')),
                    'Amount' => $this->get_text_content($line_item, array('Amount')),
                    'InventorySite_ListID' => $this->get_text_content($line_item, array('InventorySiteRef','ListID')),
                    'InventorySite_FullName' => $this->get_text_content($line_item, array('InventorySiteRef','FullName')),
                    'SerialNumber' => $this->get_text_content($line_item, array('SerialNumber')),
                    'LotNumber' => $this->get_text_content($line_item, array('LotNumber')),
                    'ServiceDate' => $this->get_text_content($line_item, array('ServiceDate')),
                    'SalesTaxCode_ListID' => $this->get_text_content($line_item, array('SalesTaxCodeRef','ListID')),
                    'SalesTaxCode_FullName' => $this->get_text_content($line_item, array('SalesTaxCodeRef','FullName')),
                    'Other1' => $this->get_text_content($line_item, array('Other1')),
                    'Other2' => $this->get_text_content($line_item, array('Other2')),   
                  );
                }
              }

          $this->items[] = array(
            'TxnID' => $this->get_text_content($item, array('TxnID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'TxnNumber' => $this->get_text_content($item, array('TxnNumber')),
            'Customer_ListID' => $this->get_text_content($item, array('CustomerRef','ListID')),
            'Customer_FullName' => $this->get_text_content($item, array('CustomerRef','FullName')),
            'Class_ListID' => $this->get_text_content($item, array('ClassRef','ListID')),
            'Class_FullName' => $this->get_text_content($item, array('ClassRef','FullName')),
            'ARAccount_ListID' => $this->get_text_content($item, array('ARAccountRef','ListID')),
            'ARAccount_FullName' => $this->get_text_content($item, array('ARAccountRef','FullName')),
            'Template_ListID' => $this->get_text_content($item, array('TemplateRef','ListID')),
            'Template_FullName' => $this->get_text_content($item, array('TemplateRef','FullName')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'RefNumber' => $this->get_text_content($item, array('RefNumber')),
            'BillAddress_Addr1' => $this->get_text_content($item, array('BillAddress','Addr1')),
            'BillAddress_Addr2' => $this->get_text_content($item, array('BillAddress','Addr2')),
            'BillAddress_Addr3' => $this->get_text_content($item, array('BillAddress','Addr3')),
            'BillAddress_Addr4' => $this->get_text_content($item, array('BillAddress','Addr4')),
            'BillAddress_Addr5' => $this->get_text_content($item, array('BillAddress','Addr5')),
            'BillAddress_City' => $this->get_text_content($item, array('BillAddress','City')),
            'BillAddress_State' => $this->get_text_content($item, array('BillAddress','State')),
            'BillAddress_PostalCode' => $this->get_text_content($item, array('BillAddress','PostalCode')),
            'BillAddress_Country' => $this->get_text_content($item, array('BillAddress','Country')),
            'BillAddress_Note' => $this->get_text_content($item, array('BillAddress','Note')),
            'BillAddressBlock_Addr1' => $this->get_text_content($item, array('BillAddressBlock','Addr1')),
            'BillAddressBlock_Addr2' => $this->get_text_content($item, array('BillAddressBlock','Addr2')),
            'BillAddressBlock_Addr3' => $this->get_text_content($item, array('BillAddressBlock','Addr3')),
            'BillAddressBlock_Addr4' => $this->get_text_content($item, array('BillAddressBlock','Addr4')),
            'BillAddressBlock_Addr5' => $this->get_text_content($item, array('BillAddressBlock','Addr5')),
            'ShipAddress_Addr1' => $this->get_text_content($item, array('ShipAddress','Addr1')),
            'ShipAddress_Addr2' => $this->get_text_content($item, array('ShipAddress','Addr2')),
            'ShipAddress_Addr3' => $this->get_text_content($item, array('ShipAddress','Addr3')),
            'ShipAddress_Addr4' => $this->get_text_content($item, array('ShipAddress','Addr4')),
            'ShipAddress_Addr5' => $this->get_text_content($item, array('ShipAddress','Addr5')),
            'ShipAddress_City' => $this->get_text_content($item, array('ShipAddress','City')),
            'ShipAddress_State' => $this->get_text_content($item, array('ShipAddress','State')),
            'ShipAddress_PostalCode' => $this->get_text_content($item, array('ShipAddress','PostalCode')),
            'ShipAddress_Country' => $this->get_text_content($item, array('ShipAddress','Country')),
            'ShipAddress_Note' => $this->get_text_content($item, array('ShipAddress','Note')),
            'ShipAddressBlock_Addr1' => $this->get_text_content($item, array('ShipAddressBlock','Addr1')),
            'ShipAddressBlock_Addr2' => $this->get_text_content($item, array('ShipAddressBlock','Addr2')),
            'ShipAddressBlock_Addr3' => $this->get_text_content($item, array('ShipAddressBlock','Addr3')),
            'ShipAddressBlock_Addr4' => $this->get_text_content($item, array('ShipAddressBlock','Addr4')),
            'ShipAddressBlock_Addr5' => $this->get_text_content($item, array('ShipAddressBlock','Addr5')),
            'IsPending' => $this->get_text_content($item, array('IsPending')),
            'IsFinanceCharge' => $this->get_text_content($item, array('IsFinanceCharge')),
            'PONumber' => $this->get_text_content($item, array('PONumber')),
            'Terms_ListID' => $this->get_text_content($item, array('TermsRef','ListID')),
            'Terms_FullName' => $this->get_text_content($item, array('TermsRef','FullName')),
            'DueDate' => $this->get_text_content($item, array('DueDate')),
            'SalesRep_ListID' => $this->get_text_content($item, array('SalesRepRef','ListID')),
            'SalesRep_FullName' => $this->get_text_content($item, array('SalesRepRef','FullName')),
            'FOB' => $this->get_text_content($item, array('FOB')),
            'ShipDate' => $this->get_text_content($item, array('ShipDate')),
            'ShipMethod_ListID' => $this->get_text_content($item, array('ShipMethodRef','ListID')),
            'ShipMethod_FullName' => $this->get_text_content($item, array('ShipMethodRef','FullName')),
            'Subtotal' => $this->get_text_content($item, array('Subtotal')),
            'ItemSalesTax_ListID' => $this->get_text_content($item, array('ItemSalesTaxRef','ListID')),
            'ItemSalesTax_FullName' => $this->get_text_content($item, array('ItemSalesTaxRef','FullName')),
            'SalesTaxPercentage' => $this->get_text_content($item, array('SalesTaxPercentage')),
            'SalesTaxTotal' => $this->get_text_content($item, array('SalesTaxTotal')),
            'AppliedAmount' => $this->get_text_content($item, array('AppliedAmount')),
            'BalanceRemaining' => $this->get_text_content($item, array('BalanceRemaining')),
            'Memo' => $this->get_text_content($item, array('Memo')),
            'IsPaid' => $this->get_text_content($item, array('IsPaid')),
            'Currency_ListID' => $this->get_text_content($item, array('CurrencyRef','ListID')),
            'Currency_FullName' => $this->get_text_content($item, array('CurrencyRef','FullName')),
            'ExchangeRate' => $this->get_text_content($item, array('ExchangeRate')),
            'BalanceRemainingInHomeCurrency' => $this->get_text_content($item, array('BalanceRemainingInHomeCurrency')),
            'CustomerMsg_ListID' => $this->get_text_content($item, array('CustomerMsgRef','ListID')),
            'CustomerMsg_FullName' => $this->get_text_content($item, array('CustomerMsgRef','FullName')),
            'IsToBePrinted' => $this->get_text_content($item, array('IsToBePrinted')),
            'IsToBeEmailed' => $this->get_text_content($item, array('IsToBeEmailed')),
            'CustomerSalesTaxCode_ListID' => $this->get_text_content($item, array('CustomerSalesTaxCodeRef','ListID')),
            'CustomerSalesTaxCode_FullName' => $this->get_text_content($item, array('CustomerSalesTaxCodeRef','FullName')),
            'SuggestedDiscountAmount' => $this->get_text_content($item, array('SuggestedDiscountAmount')),
            'SuggestedDiscountDate' => $this->get_text_content($item, array('SuggestedDiscountDate')),
            'Other' => $this->get_text_content($item, array('Other')),
            'LineItems' => $LineItems,
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_invoice_model');
        $query = new $this->CI->Qb_invoice_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }
     
}

/* End of file */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_itemfixedasset extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_itemfixedasset_model');
        $model = new $this->CI->Qb_itemfixedasset_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_ITEMFIXEDASSETQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMFIXEDASSETQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<ItemFixedAssetQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ActiveStatus>All</ActiveStatus>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</ItemFixedAssetQueryRq>' .  "\n" .
'<ListDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<ListDelType>ItemFixedAsset</ListDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</ListDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'ItemfixedassetRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_itemfixedasset_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_itemfixedasset_model();
                $model->setListid($item_obj->ListID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setName($item_obj->Name);
                $model->setIsactive(($item_obj->IsActive)?1:0);
                $model->setAcquiredas($item_obj->AcquiredAs);
                $model->setPurchasedesc($item_obj->PurchaseDesc);
                $model->setPurchasedate($item_obj->PurchaseDate);
                $model->setPurchasecost($item_obj->PurchaseCost);
                $model->setVendororpayeename($item_obj->VendorOrPayeeName);
                $model->setAssetaccountListid($item_obj->AssetAccount_ListID);
                $model->setAssetaccountFullname($item_obj->AssetAccount_FullName);
                $model->setFixedassetsalesinfoSalesdesc($item_obj->FixedAssetSalesInfo_SalesDesc);
                $model->setFixedassetsalesinfoSalesdate($item_obj->FixedAssetSalesInfo_SalesDate);
                $model->setFixedassetsalesinfoSalesprice($item_obj->FixedAssetSalesInfo_SalesPrice);
                $model->setFixedassetsalesinfoSalesexpense($item_obj->FixedAssetSalesInfo_SalesExpense);
                $model->setAssetdesc($item_obj->AssetDesc);
                $model->setLocation($item_obj->Location);
                $model->setPonumber($item_obj->PONumber);
                $model->setSerialnumber($item_obj->SerialNumber);
                $model->setWarrantyexpdate($item_obj->WarrantyExpDate);
                $model->setNotes($item_obj->Notes);
                $model->setAssetnumber($item_obj->AssetNumber);
                $model->setCostbasis($item_obj->CostBasis);
                $model->setYearendaccumulateddepreciation($item_obj->YearEndAccumulatedDepreciation);
                $model->setYearendbookvalue($item_obj->YearEndBookValue);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);
            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'ItemFixedAssetRet') {

          $this->items[] = array(
            'ListID' => $this->get_text_content($item, array('ListID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'Name' => $this->get_text_content($item, array('Name')),
            'IsActive' => $this->get_text_content($item, array('IsActive')),
            'AcquiredAs' => $this->get_text_content($item, array('AcquiredAs')),
            'PurchaseDesc' => $this->get_text_content($item, array('PurchaseDesc')),
            'PurchaseDate' => $this->get_text_content($item, array('PurchaseDate')),
            'PurchaseCost' => $this->get_text_content($item, array('PurchaseCost')),
            'VendorOrPayeeName' => $this->get_text_content($item, array('VendorOrPayeeName')),
            'AssetAccount_ListID' => $this->get_text_content($item, array('AssetAccountRef','ListID')),
            'AssetAccount_FullName' => $this->get_text_content($item, array('AssetAccountRef','FullName')),
            'FixedAssetSalesInfo_SalesDesc' => $this->get_text_content($item, array('FixedAssetSalesInfoRef','SalesDesc')),
            'FixedAssetSalesInfo_SalesDate' => $this->get_text_content($item, array('FixedAssetSalesInfoRef','SalesDate')),
            'FixedAssetSalesInfo_SalesPrice' => $this->get_text_content($item, array('FixedAssetSalesInfoRef','SalesPrice')),
            'FixedAssetSalesInfo_SalesExpense' => $this->get_text_content($item, array('FixedAssetSalesInfoRef','SalesExpense')),
            'AssetDesc' => $this->get_text_content($item, array('AssetDesc')),
            'Location' => $this->get_text_content($item, array('Location')),
            'PONumber' => $this->get_text_content($item, array('PONumber')),
            'SerialNumber' => $this->get_text_content($item, array('SerialNumber')),
            'WarrantyExpDate' => $this->get_text_content($item, array('WarrantyExpDate')),
            'Notes' => $this->get_text_content($item, array('Notes')),
            'AssetNumber' => $this->get_text_content($item, array('AssetNumber')),
            'CostBasis' => $this->get_text_content($item, array('CostBasis')),
            'YearEndAccumulatedDepreciation' => $this->get_text_content($item, array('YearEndAccumulatedDepreciation')),
            'YearEndBookValue' => $this->get_text_content($item, array('YearEndBookValue')),
            'DataExtItems' => $this->get_dataext_items($item, 'ItemFixedAsset', 'ListID'),
          );

        }
      }
    }

    public function delete($ListID) {
        $this->CI->load->model('Qb_itemfixedasset_model');
        $query = new $this->CI->Qb_itemfixedasset_model();
        $query->setListid($ListID,true);
        $query->delete();
    }
    
}

/* End of file */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_billtopay extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;     

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<BillToPayQueryRq requestID="'.$requestID. '">' .  "\n" .
'<PayeeEntityRef></PayeeEntityRef>' .  "\n" .
'</BillToPayQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'BilltopayRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_billtopay_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_billtopay_model();
                $model->setTxnid($item_obj->TxnID,TRUE);
                $model->setTxntype($item_obj->TxnType);
                $model->setApaccountListid($item_obj->APAccount_ListID);
                $model->setApaccountFullname($item_obj->APAccount_FullName);
                $model->setTxndate($item_obj->TxnDate);
                $model->setRefnumber($item_obj->RefNumber);
                $model->setDuedate($item_obj->DueDate);
                $model->setAmountdue($item_obj->AmountDue);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'BillToPayRet') {

          $this->items[] = array(
            'TxnID' => $this->get_text_content($item, array('TxnID')),
            'TxnType' => $this->get_text_content($item, array('TxnType')),
            'APAccount_ListID' => $this->get_text_content($item, array('APAccountRef','ListID')),
            'APAccount_FullName' => $this->get_text_content($item, array('APAccountRef','FullName')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'RefNumber' => $this->get_text_content($item, array('RefNumber')),
            'DueDate' => $this->get_text_content($item, array('DueDate')),
            'AmountDue' => $this->get_text_content($item, array('AmountDue')),

          );

        }
      }
    }
 
}

/* End of file */

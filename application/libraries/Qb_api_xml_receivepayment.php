<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_receivepayment extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_receivepayment_model');
        $model = new $this->CI->Qb_receivepayment_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_RECEIVEPAYMENTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_RECEIVEPAYMENTQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<ReceivePaymentQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<IncludeLineItems>true</IncludeLineItems>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</ReceivePaymentQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>ReceivePayment</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'ReceivepaymentRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_receivepayment_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_receivepayment_model();
                $model->setTxnid($item_obj->TxnID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setTxnnumber($item_obj->TxnNumber);
                $model->setCustomerListid($item_obj->Customer_ListID);
                $model->setCustomerFullname($item_obj->Customer_FullName);
                $model->setAraccountListid($item_obj->ARAccount_ListID);
                $model->setAraccountFullname($item_obj->ARAccount_FullName);
                $model->setTxndate($item_obj->TxnDate);
                $model->setRefnumber($item_obj->RefNumber);
                $model->setTotalamount($item_obj->TotalAmount);
                $model->setPaymentmethodListid($item_obj->PaymentMethod_ListID);
                $model->setPaymentmethodFullname($item_obj->PaymentMethod_FullName);
                $model->setMemo($item_obj->Memo);
                $model->setDeposittoaccountListid($item_obj->DepositToAccount_ListID);
                $model->setDeposittoaccountFullname($item_obj->DepositToAccount_FullName);
                $model->setCreditcardtxninfoCreditcardtxninputinfoCreditcardnumber($item_obj->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber);
                $model->setCreditcardtxninfoCreditcardtxninputinfoExpirationmonth($item_obj->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth);
                $model->setCreditcardtxninfoCreditcardtxninputinfoExpirationyear($item_obj->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear);
                $model->setCreditcardtxninfoCreditcardtxninputinfoNameoncard($item_obj->CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard);
                $model->setCreditcardtxninfoCreditcardtxninputinfoCreditcardaddress($item_obj->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress);
                $model->setCreditcardtxninfoCreditcardtxninputinfoCreditcardpostalcode($item_obj->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode);
                $model->setCreditcardtxninfoCreditcardtxninputinfoCommercialcardcode($item_obj->CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode);
                $model->setCreditcardtxninfoCreditcardtxninputinfoTransactionmode($item_obj->CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode);
                $model->setCreditcardtxninfoCreditcardtxninputinfoCreditcardtxntype($item_obj->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoResultcode($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoResultmessage($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoCreditcardtransid($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoMerchantaccountnumber($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoAuthorizationcode($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoAvsstreet($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoAvszip($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoCardsecuritycodematch($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoReconbatchid($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoPaymentgroupingcode($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoPaymentstatus($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationtime($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationstamp($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp);
                $model->setCreditcardtxninfoCreditcardtxnresultinfoClienttransid($item_obj->CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID);
                $model->setUnusedpayment($item_obj->UnusedPayment);
                $model->setUnusedcredits($item_obj->UnusedCredits);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);
            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'ReceivePaymentRet') {

          $this->items[] = array(
            'TxnID' => $this->get_text_content($item, array('TxnID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'TxnNumber' => $this->get_text_content($item, array('TxnNumber')),
            'Customer_ListID' => $this->get_text_content($item, array('CustomerRef','ListID')),
            'Customer_FullName' => $this->get_text_content($item, array('CustomerRef','FullName')),
            'ARAccount_ListID' => $this->get_text_content($item, array('ARAccountRef','ListID')),
            'ARAccount_FullName' => $this->get_text_content($item, array('ARAccountRef','FullName')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'RefNumber' => $this->get_text_content($item, array('RefNumber')),
            'TotalAmount' => $this->get_text_content($item, array('TotalAmount')),
            'PaymentMethod_ListID' => $this->get_text_content($item, array('PaymentMethodRef','ListID')),
            'PaymentMethod_FullName' => $this->get_text_content($item, array('PaymentMethodRef','FullName')),
            'Memo' => $this->get_text_content($item, array('Memo')),
            'DepositToAccount_ListID' => $this->get_text_content($item, array('DepositToAccountRef','ListID')),
            'DepositToAccount_FullName' => $this->get_text_content($item, array('DepositToAccountRef','FullName')),
            'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnInputInfoRef','CreditCardNumber')),
            'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnInputInfoRef','ExpirationMonth')),
            'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnInputInfoRef','ExpirationYear')),
            'CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnInputInfoRef','NameOnCard')),
            'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnInputInfoRef','CreditCardAddress')),
            'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnInputInfoRef','CreditCardPostalCode')),
            'CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnInputInfoRef','CommercialCardCode')),
            'CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnInputInfoRef','TransactionMode')),
            'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnInputInfoRef','CreditCardTxnType')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','ResultCode')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','ResultMessage')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','CreditCardTransID')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','MerchantAccountNumber')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','AuthorizationCode')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','AVSStreet')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','AVSZip')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','CardSecurityCodeMatch')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','ReconBatchID')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','PaymentGroupingCode')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','PaymentStatus')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','TxnAuthorizationTime')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','TxnAuthorizationStamp')),
            'CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID' => $this->get_text_content($item, array('CreditCardTxnInfoRef','CreditCardTxnResultInfoRef','ClientTransID')),
            'UnusedPayment' => $this->get_text_content($item, array('UnusedPayment')),
            'UnusedCredits' => $this->get_text_content($item, array('UnusedCredits')),
            'DataExtItems' => $this->get_dataext_items($item, 'ReceivePayment', 'TxnID'),
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_receivepayment_model');
        $query = new $this->CI->Qb_receivepayment_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }
    
}

/* End of file */

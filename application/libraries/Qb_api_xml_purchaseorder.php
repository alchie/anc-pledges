<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_purchaseorder extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_purchaseorder_model');
        $model = new $this->CI->Qb_purchaseorder_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_PURCHASEORDERQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_PURCHASEORDERQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<PurchaseOrderQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<IncludeLineItems>true</IncludeLineItems>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</PurchaseOrderQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>PurchaseOrder</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'PurchaseorderRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_purchaseorder_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_purchaseorder_model();
                $model->setTxnid($item_obj->TxnID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setTxnnumber($item_obj->TxnNumber);
                $model->setVendorListid($item_obj->Vendor_ListID);
                $model->setVendorFullname($item_obj->Vendor_FullName);
                $model->setClassListid($item_obj->Class_ListID);
                $model->setClassFullname($item_obj->Class_FullName);
                $model->setShiptoentityListid($item_obj->ShipToEntity_ListID);
                $model->setShiptoentityFullname($item_obj->ShipToEntity_FullName);
                $model->setTemplateListid($item_obj->Template_ListID);
                $model->setTemplateFullname($item_obj->Template_FullName);
                $model->setTxndate($item_obj->TxnDate);
                $model->setRefnumber($item_obj->RefNumber);
                $model->setVendoraddressAddr1($item_obj->VendorAddress_Addr1);
                $model->setVendoraddressAddr2($item_obj->VendorAddress_Addr2);
                $model->setVendoraddressAddr3($item_obj->VendorAddress_Addr3);
                $model->setVendoraddressAddr4($item_obj->VendorAddress_Addr4);
                $model->setVendoraddressAddr5($item_obj->VendorAddress_Addr5);
                $model->setVendoraddressCity($item_obj->VendorAddress_City);
                $model->setVendoraddressState($item_obj->VendorAddress_State);
                $model->setVendoraddressPostalcode($item_obj->VendorAddress_PostalCode);
                $model->setVendoraddressCountry($item_obj->VendorAddress_Country);
                $model->setVendoraddressNote($item_obj->VendorAddress_Note);
                $model->setVendoraddressblockAddr1($item_obj->VendorAddressBlock_Addr1);
                $model->setVendoraddressblockAddr2($item_obj->VendorAddressBlock_Addr2);
                $model->setVendoraddressblockAddr3($item_obj->VendorAddressBlock_Addr3);
                $model->setVendoraddressblockAddr4($item_obj->VendorAddressBlock_Addr4);
                $model->setVendoraddressblockAddr5($item_obj->VendorAddressBlock_Addr5);
                $model->setShipaddressAddr1($item_obj->ShipAddress_Addr1);
                $model->setShipaddressAddr2($item_obj->ShipAddress_Addr2);
                $model->setShipaddressAddr3($item_obj->ShipAddress_Addr3);
                $model->setShipaddressAddr4($item_obj->ShipAddress_Addr4);
                $model->setShipaddressAddr5($item_obj->ShipAddress_Addr5);
                $model->setShipaddressCity($item_obj->ShipAddress_City);
                $model->setShipaddressState($item_obj->ShipAddress_State);
                $model->setShipaddressPostalcode($item_obj->ShipAddress_PostalCode);
                $model->setShipaddressCountry($item_obj->ShipAddress_Country);
                $model->setShipaddressNote($item_obj->ShipAddress_Note);
                $model->setShipaddressblockAddr1($item_obj->ShipAddressBlock_Addr1);
                $model->setShipaddressblockAddr2($item_obj->ShipAddressBlock_Addr2);
                $model->setShipaddressblockAddr3($item_obj->ShipAddressBlock_Addr3);
                $model->setShipaddressblockAddr4($item_obj->ShipAddressBlock_Addr4);
                $model->setShipaddressblockAddr5($item_obj->ShipAddressBlock_Addr5);
                $model->setTermsListid($item_obj->Terms_ListID);
                $model->setTermsFullname($item_obj->Terms_FullName);
                $model->setDuedate($item_obj->DueDate);
                $model->setExpecteddate($item_obj->ExpectedDate);
                $model->setShipmethodListid($item_obj->ShipMethod_ListID);
                $model->setShipmethodFullname($item_obj->ShipMethod_FullName);
                $model->setFob($item_obj->FOB);
                $model->setTotalamount($item_obj->TotalAmount);
                $model->setCurrencyListid($item_obj->Currency_ListID);
                $model->setCurrencyFullname($item_obj->Currency_FullName);
                $model->setExchangerate($item_obj->ExchangeRate);
                $model->setTotalamountinhomecurrency($item_obj->TotalAmountInHomeCurrency);
                $model->setIsmanuallyclosed($item_obj->IsManuallyClosed);
                $model->setIsfullyreceived($item_obj->IsFullyReceived);
                $model->setMemo($item_obj->Memo);
                $model->setVendormsg($item_obj->VendorMsg);
                $model->setIstobeprinted($item_obj->IsToBePrinted);
                $model->setIstobeemailed($item_obj->IsToBeEmailed);
                $model->setOther1($item_obj->Other1);
                $model->setOther2($item_obj->Other2);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'PurchaseOrderRet') {

          $this->items[] = array(
            'TxnID' => $this->get_text_content($item, array('TxnID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'TxnNumber' => $this->get_text_content($item, array('TxnNumber')),
            'Vendor_ListID' => $this->get_text_content($item, array('VendorRef','ListID')),
            'Vendor_FullName' => $this->get_text_content($item, array('VendorRef','FullName')),
            'Class_ListID' => $this->get_text_content($item, array('ClassRef','ListID')),
            'Class_FullName' => $this->get_text_content($item, array('ClassRef','FullName')),
            'ShipToEntity_ListID' => $this->get_text_content($item, array('ShipToEntityRef','ListID')),
            'ShipToEntity_FullName' => $this->get_text_content($item, array('ShipToEntityRef','FullName')),
            'Template_ListID' => $this->get_text_content($item, array('TemplateRef','ListID')),
            'Template_FullName' => $this->get_text_content($item, array('TemplateRef','FullName')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'RefNumber' => $this->get_text_content($item, array('RefNumber')),
            'VendorAddress_Addr1' => $this->get_text_content($item, array('VendorAddress','Addr1')),
            'VendorAddress_Addr2' => $this->get_text_content($item, array('VendorAddress','Addr2')),
            'VendorAddress_Addr3' => $this->get_text_content($item, array('VendorAddress','Addr3')),
            'VendorAddress_Addr4' => $this->get_text_content($item, array('VendorAddress','Addr4')),
            'VendorAddress_Addr5' => $this->get_text_content($item, array('VendorAddress','Addr5')),
            'VendorAddress_City' => $this->get_text_content($item, array('VendorAddress','City')),
            'VendorAddress_State' => $this->get_text_content($item, array('VendorAddress','State')),
            'VendorAddress_PostalCode' => $this->get_text_content($item, array('VendorAddress','PostalCode')),
            'VendorAddress_Country' => $this->get_text_content($item, array('VendorAddress','Country')),
            'VendorAddress_Note' => $this->get_text_content($item, array('VendorAddress','Note')),
            'VendorAddressBlock_Addr1' => $this->get_text_content($item, array('VendorAddressBlock','Addr1')),
            'VendorAddressBlock_Addr2' => $this->get_text_content($item, array('VendorAddressBlock','Addr2')),
            'VendorAddressBlock_Addr3' => $this->get_text_content($item, array('VendorAddressBlock','Addr3')),
            'VendorAddressBlock_Addr4' => $this->get_text_content($item, array('VendorAddressBlock','Addr4')),
            'VendorAddressBlock_Addr5' => $this->get_text_content($item, array('VendorAddressBlock','Addr5')),
            'ShipAddress_Addr1' => $this->get_text_content($item, array('ShipAddress','Addr1')),
            'ShipAddress_Addr2' => $this->get_text_content($item, array('ShipAddress','Addr2')),
            'ShipAddress_Addr3' => $this->get_text_content($item, array('ShipAddress','Addr3')),
            'ShipAddress_Addr4' => $this->get_text_content($item, array('ShipAddress','Addr4')),
            'ShipAddress_Addr5' => $this->get_text_content($item, array('ShipAddress','Addr5')),
            'ShipAddress_City' => $this->get_text_content($item, array('ShipAddress','City')),
            'ShipAddress_State' => $this->get_text_content($item, array('ShipAddress','State')),
            'ShipAddress_PostalCode' => $this->get_text_content($item, array('ShipAddress','PostalCode')),
            'ShipAddress_Country' => $this->get_text_content($item, array('ShipAddress','Country')),
            'ShipAddress_Note' => $this->get_text_content($item, array('ShipAddress','Note')),
            'ShipAddressBlock_Addr1' => $this->get_text_content($item, array('ShipAddressBlock','Addr1')),
            'ShipAddressBlock_Addr2' => $this->get_text_content($item, array('ShipAddressBlock','Addr2')),
            'ShipAddressBlock_Addr3' => $this->get_text_content($item, array('ShipAddressBlock','Addr3')),
            'ShipAddressBlock_Addr4' => $this->get_text_content($item, array('ShipAddressBlock','Addr4')),
            'ShipAddressBlock_Addr5' => $this->get_text_content($item, array('ShipAddressBlock','Addr5')),
            'Terms_ListID' => $this->get_text_content($item, array('TermsRef','ListID')),
            'Terms_FullName' => $this->get_text_content($item, array('TermsRef','FullName')),
            'DueDate' => $this->get_text_content($item, array('DueDate')),
            'ExpectedDate' => $this->get_text_content($item, array('ExpectedDate')),
            'ShipMethod_ListID' => $this->get_text_content($item, array('ShipMethodRef','ListID')),
            'ShipMethod_FullName' => $this->get_text_content($item, array('ShipMethodRef','FullName')),
            'FOB' => $this->get_text_content($item, array('FOB')),
            'TotalAmount' => $this->get_text_content($item, array('TotalAmount')),
            'Currency_ListID' => $this->get_text_content($item, array('CurrencyRef','ListID')),
            'Currency_FullName' => $this->get_text_content($item, array('CurrencyRef','FullName')),
            'ExchangeRate' => $this->get_text_content($item, array('ExchangeRate')),
            'TotalAmountInHomeCurrency' => $this->get_text_content($item, array('TotalAmountInHomeCurrency')),
            'IsManuallyClosed' => $this->get_text_content($item, array('IsManuallyClosed')),
            'IsFullyReceived' => $this->get_text_content($item, array('IsFullyReceived')),
            'Memo' => $this->get_text_content($item, array('Memo')),
            'VendorMsg' => $this->get_text_content($item, array('VendorMsg')),
            'IsToBePrinted' => $this->get_text_content($item, array('IsToBePrinted')),
            'IsToBeEmailed' => $this->get_text_content($item, array('IsToBeEmailed')),
            'Other1' => $this->get_text_content($item, array('Other1')),
            'Other2' => $this->get_text_content($item, array('Other2')),
            'DataExtItems' => $this->get_dataext_items($item, 'PurchaseOrder', 'TxnID'),
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_purchaseorder_model');
        $query = new $this->CI->Qb_purchaseorder_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }
     
}

/* End of file */

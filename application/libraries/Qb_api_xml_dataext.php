<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_dataext extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_dataext_model');
        $model = new $this->CI->Qb_dataext_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_DATAEXTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_DATAEXTQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<DataextQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<IncludeLineItems>true</IncludeLineItems>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</DataextQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'DataextRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_dataext_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_dataext_model();
                $model->setEntitytype($item_obj->EntityType);
                $model->setTxntype($item_obj->TxnType);
                $model->setEntityListid($item_obj->Entity_ListID);
                $model->setTxnTxnid($item_obj->Txn_TxnID);
                $model->setOwnerid($item_obj->OwnerID);
                $model->setDataextname($item_obj->DataExtName);
                $model->setDataexttype($item_obj->DataExtType);
                $model->setDataextvalue($item_obj->DataExtValue);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'DataextRet') {

          $this->items[] = array(
            'EntityType' => $this->get_text_content($item, array('EntityType')),
            'TxnType' => $this->get_text_content($item, array('TxnType')),
            'Entity_ListID' => $this->get_text_content($item, array('EntityRef','ListID')),
            'Txn_TxnID' => $this->get_text_content($item, array('TxnRef','TxnID')),
            'OwnerID' => $this->get_text_content($item, array('OwnerID')),
            'DataExtName' => $this->get_text_content($item, array('DataExtName')),
            'DataExtType' => $this->get_text_content($item, array('DataExtType')),
            'DataExtValue' => $this->get_text_content($item, array('DataExtValue')),

          );

        }
      }
    }
 
}

/* End of file */
